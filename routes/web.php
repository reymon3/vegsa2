<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::middleware(['auth'])->group(function () {


    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


    Route::resource('areas', 'AreasController');

    Route::resource('empleados', 'EmpleadosController');
    Route::get('getEmpleado/{id}', 'EmpleadosController@getEmpleado');

    Route::resource('puestos', 'PuestosController');

    Route::resource('rubros', 'RubrosController');

    Route::resource('tipoAvaluos', 'TipoAvaluosController');

    Route::resource('tipoCambios', 'TipoCambioController');

    Route::resource('clientes', 'ClienteController');
    Route::get('getClientes', 'ClienteController@getClientes');
    Route::get('getCliente/{id}', 'ClienteController@getCliente');
    Route::resource('clienteRazonSocials', 'ClienteRazonSocialController');

    Route::resource('servicios', 'ServicioController');

    Route::resource('normas', 'NormasController');

    Route::resource('propositoAvaluos', 'PropositoAvaluoController');


    Route::resource('tipoPlaqueos', 'TipoPlaqueoController');

    Route::resource('cotizacions', 'CotizacionController');


    Route::resource('users', 'UserController');


    Route::resource('cotizacionServicios', 'CotizacionServicioController');

    // Route::post('genera-orden/{id}', [
    //     'as' => 'generaOrden',
    //     'uses' =>'CotizacionController@generaOrden'
    // ]);

    Route::resource('ordenTrabajos', 'OrdenTrabajoController');
    Route::get('asignacion-proyecto', 'OrdenTrabajoController@asignacionProyecto');
    Route::get('asignacion-proyecto-detalle/{ordenTrabajoId}', 'OrdenTrabajoController@asignacionProyectoDetalle');

    Route::get('genera-orden/{cotizacionId}', 'OrdenTrabajoController@generaOrden');

    Route::get('actualiza-asignacion-empleado/{id}', [
        'as' => 'actualizaAsignacionEmpleado',
        'uses' =>'OrdenTrabajoController@actualizaAsignacionEmpleado'
    ]);

    Route::resource('ordenTrabajoRazonSocials', 'OrdenTrabajoRazonSocialController');

    Route::resource('ordenTrabajoBaseContables', 'OrdenTrabajoBaseContableController');

    Route::post('subir-documento', 'OrdenTrabajoBaseContableController@subirDocumento')->name('subirDocumento');


    Route::get('descargar-plantilla/{id}', 'OrdenTrabajoBaseContableController@descargaPlantilla')->name('descargaPlantilla');
    Route::get('descargar-conteo/{id}', 'OrdenTrabajoConteoController@descargaConteo')->name('descargaConteo');
    Route::get('descargar-cruce/{id}', 'OrdenTrabajoBaseContableController@descargaCruce')->name('descargaCruce');

    Route::get('descargar-repetidos-placa_actual/{id}', 'OrdenTrabajoBaseContableController@descargaRepetidosPlacaActual')->name('descargaRepetidosPlacaActual');
    Route::get('descargar-repetidos-serie/{id}', 'OrdenTrabajoBaseContableController@descargaRepetidosSerie')->name('descargaRepetidosSerie');
    Route::get('descargar-repetidos-placa_anterior/{id}', 'OrdenTrabajoBaseContableController@descargaRepetidosPlacaAnterior')->name('descargaRepetidosPlacaAnterior');

    Route::get('descargar-repetidos-placa_auxiliar_1/{id}', 'OrdenTrabajoBaseContableController@descargaRepetidosPlacaAuxiliar1')->name('descargaRepetidosPlacaAuxiliar1');
    Route::get('descargar-repetidos-placa_auxiliar_2/{id}', 'OrdenTrabajoBaseContableController@descargaRepetidosPlacaAuxiliar2')->name('descargaRepetidosPlacaAuxiliar2');

    Route::resource('ordenTrabajoServicios', 'OrdenTrabajoServicioController');
    Route::resource('ordenTrabajoNormas', 'OrdenTrabajoNormaController');
    Route::resource('ordenTrabajoEmpleados', 'OrdenTrabajoEmpleadoController');


    Route::get('avaluos-antecedentes', [
        'as' => 'avaluos-antecedentes',
        'uses' => 'AvaluoController@antecedentes'
    ]);
    Route::post('avaluos-antecedentes-insert', [
        'as' => 'avaluos-antecedentes-insert',
        'uses' => 'AvaluoDetalleController@insert'
    ]);
    Route::get('avaluos-caracteristicas', [
        'as' => 'avaluos-caracteristicas',
        'uses' => 'AvaluoController@caracteristicas'
    ]);
    Route::get('avaluos-terreno', [
        'as' => 'avaluos-terreno',
        'uses' => 'AvaluoController@terreno'
    ]);
    Route::get('avaluos-descripcion', [
        'as' => 'avaluos-descripcion',
        'uses' => 'AvaluoController@descripcion'
    ]);
    Route::get('avaluos-elementos', [
        'as' => 'avaluos-elementos',
        'uses' => 'AvaluoController@elementos'
    ]);
    Route::get('avaluos-conclusiones', [
        'as' => 'avaluos-conclusiones',
        'uses' => 'AvaluoController@conclusiones'
    ]);

    Route::get('descargar-avaluo/{id}', [
        'as' => 'descargar-avaluo',
        'uses' => 'AvaluoController@descargarAvaluo'
    ]);

    Route::resource('avaluoDetalles', 'AvaluoDetalleController');

    Route::resource('ordenTrabajoPropositoAvaluos', 'OrdenTrabajoPropositoAvaluoController');

    Route::resource('ordenTrabajoRubroAlcances', 'OrdenTrabajoRubroAlcanceController');

    Route::resource('ordenTrabajoTipoPlaqueos', 'OrdenTrabajoTipoPlaqueoController');

    Route::resource('ordenTrabajoTipoAvaluos', 'OrdenTrabajoTipoAvaluoController');

    Route::resource('ordenTrabajoDocumentos', 'OrdenTrabajoDocumentoController');
    Route::post('subir-archivo', 'OrdenTrabajoDocumentoController@subirArchivo')->name('subirArchivo');
    Route::post('subir-conteo', 'OrdenTrabajoConteoController@subirConteo')->name('subirConteo');
    Route::resource('ordenTrabajoConteos', 'OrdenTrabajoConteoController');

    Route::get('homologar-columnas/{id}', 'OrdenTrabajoBaseContableController@homologarColumnas')->name('homologarColumnas');

    Route::get('obtener-datos-agrupados/{id}/columna/{columna}', [
        'as' => 'obtenerDatosAgrupados',
        'uses' => 'OrdenTrabajoBaseContableController@obtenerDatosAgrupados'
    ]);

    Route::post('reemplaza-datos-por-columna', [
        'as' => 'reemplazaDatosPorColumna',
        'uses' => 'OrdenTrabajoBaseContableController@reemplazaDatosPorColumna'
    ]);

    Route::resource('civils', 'CivilController');
    Route::resource('civil', 'CivilOrdenesController');

    Route::resource('civilDetalles', 'CivilDetalleController');

    Route::resource('avaluos', 'AvaluoController');
    Route::post('subir-avaluo', 'AvaluoController@subirAvaluo')->name('subirAvaluo');

    Route::resource('avaluoDetalles', 'AvaluoDetalleController');

    Route::resource('ordenTrabajoBaseContableDetalles', 'OrdenTrabajoBaseContableDetalleController');

    Route::resource('archivoMaestros', 'ArchivoMaestroController');

    Route::get('descargar-archivo-maestro/{id}', [
        'as' => 'descargar-archivo-maestro',
        'uses' => 'ArchivoMaestroController@descargarArchivoMaestro'
    ]);

    Route::get('descargar-sobrante-fisico/{id}', [
        'as' => 'descargar-sobrante-fisico',
        'uses' => 'ArchivoMaestroController@descargarSobranteFisico'
    ]);

    Route::get('descargar-sobrante-contable/{id}', [
        'as' => 'descargar-sobrante-contable',
        'uses' => 'ArchivoMaestroController@descargarSobranteContable'
    ]);

    Route::post('subir-archivo-maestro', 'ArchivoMaestroController@subirArchivoMaestro')->name('subirArchivoMaestro');

    Route::resource('archivoMaestroDetalles', 'ArchivoMaestroDetalleController');

    Route::resource('activoFijos', 'ActivoFijoController');

    Route::resource('conciliacions', 'ConciliacionController');
    Route::post('subir-conciliacion', 'ConciliacionController@subirConciliacion')->name('subirConciliacion');
    Route::resource('conciliacionDetalles', 'ConciliacionDetalleController');

    Route::get('descarga-conciliacion', 'ConciliacionController@getConciliacion');

    Route::get('armado-conciliacion', 'AvaluoController@armadoConciliacion');
    Route::get('armado-avaluo', 'ConciliacionController@armadoAvaluo');
    Route::get('armado-conciliacion-avaluo', 'AvaluoController@armadoConciliacionAvaluo');

    Route::get('factura-armado-conciliacion', 'AvaluoController@facturaArmadoConciliacion');
    Route::get('factura-armado-avaluo', 'AvaluoController@facturaArmadoAvaluo');
    Route::get('factura-armado-conciliacion-avaluo', 'AvaluoController@FacturaArmadoConciliacionAvaluo');

    Route::post('subir-factura-armado-conciliacion', 'AvaluoController@subirFacturaArmadoConciliacion')->name('subirFacturaArmadoConciliacion');
    Route::post('subir-factura-armado-avaluo', 'AvaluoController@subirFacturaArmadoAvaluo')->name('subirFacturaArmadoAvaluo');
    Route::post('subir-factura-armado-conciliacion-avaluo', 'AvaluoController@subirFacturaArmadoConciliacionAvaluo')->name('subirFacturaArmadoConciliacionAvaluo');

    // Route::get('asignacion-proyecto-detalle/{ordenTrabajoId}', 'OrdenTrabajoController@asignacionProyectoDetalle');

    Route::resource('fichaTecnicas', 'FichaTecnicaController');
    Route::resource('fichaTecnicaDetalles', 'FichaTecnicaDetalleController');

    Route::resource('placas', 'PlacasController');
    Route::resource('cartas', 'CartasController');
    Route::get('actualizar-carta/{id}', 'CartasController@actualizarCarta')->name('actualizarCarta');
    Route::get('download/{nombre}', 'CartasController@download')->name('descarga.archivo');
    Route::resource('reporteador', 'ReporteadorController');
    Route::get('descargar-reporteador', 'ReporteadorController@download')->name('reporteador.download');
    Route::resource('cuadernillo', 'CuadernilloController');
    Route::get('portada', 'CuadernilloController@portada')->name('cuadernillo.portada');
    Route::get('crear-contenido', 'CuadernilloController@contenidoCrear')->name('cuadernillo.contenido.crear');
    Route::get('crear-contenido-indice', 'CuadernilloController@contenidoCrearIndice')->name('cuadernillo.indice.crear');
    Route::post('crear-indice', 'CuadernilloController@crearIndice')->name('indice.crear');
    Route::get('crear-separador', 'CuadernilloController@separador')->name('cuadernillo.separador');
    

    Route::get('catalogo/{id}', 'CatalogoController@create')->name('catalogo.crear');
});
