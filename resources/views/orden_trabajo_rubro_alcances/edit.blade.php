@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Rubro Alcance
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoRubroAlcance, ['route' => ['ordenTrabajoRubroAlcances.update', $ordenTrabajoRubroAlcance->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_rubro_alcances.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection