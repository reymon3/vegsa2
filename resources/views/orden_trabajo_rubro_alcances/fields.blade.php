<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}

<!-- Rubro Alcance Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('rubro_alcance_id', 'Rubro Alcance:') !!}
    {!! Form::select('rubro_alcance_id', $rubros, null, [
        'class' => 'form-control',
        'id'=>'rubro_alcance_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoRubroAlcances.index') !!}"  onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar</a>
</div>
