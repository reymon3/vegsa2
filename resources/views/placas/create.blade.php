@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Placas
        </h1>
    </section>
    <div class="content">
    <!-- TODO -->
        <!-- include('adminlte-templates::common.errors') -->
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'placas.store','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('placas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
