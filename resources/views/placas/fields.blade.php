<!-- Cliente Field -->
<div class="form-group col-sm-6">
    {{ Form::label('cliente', 'Cliente :')}}
    <!-- {!! Form::select('cliente_id', $clientes, null, ['class' => 'form-control']) !!} -->
    <select name="cliente" id="cliente" class="form-control" id="exampleFormControlSelect1">
    @foreach($clientes as $cliente)
      <option value="{{$cliente->nombre}}">{{$cliente->nombre}}</option>
    @endforeach
    </select>
</div>
<!-- File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Archivo', 'Archivo:') !!}
    {!! Form::file('Archivo', null, array('class' => 'form-control')) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#update_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Subir', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('placas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
