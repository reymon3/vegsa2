<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}


<!-- Empleado Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('empleado_id', 'Empleado:') !!}
    {!! Form::select('empleado_id', $empleados, null, [
        'class' => 'form-control',
        'id'=>'empleado_id',
        'placeholder' => 'Seleccione...'
    ])!!}

</div>

<!-- Rol Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rol_id', 'Rol:') !!}

    {!! Form::select('rol_id', $roles, null, [
        'class' => 'form-control',
        'id'=>'rol_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoEmpleados.index') !!}" onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar </a>
</div>
