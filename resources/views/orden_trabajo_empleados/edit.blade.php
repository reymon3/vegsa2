@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Empleado
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoEmpleado, ['route' => ['ordenTrabajoEmpleados.update', $ordenTrabajoEmpleado->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_empleados.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection