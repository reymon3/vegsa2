@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Razon Social
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoRazonSocial, ['route' => ['ordenTrabajoRazonSocials.update', $ordenTrabajoRazonSocial->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_razon_socials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection