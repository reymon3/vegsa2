<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}

<!-- Cliente Razon Social Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('cliente_razon_social_id', 'Razon Social:') !!}
    {!! Form::select('cliente_razon_social_id', $razonesSociales, null, [
        'class' => 'form-control',
        'id'=>'cliente_razon_social_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoRazonSocials.index') !!}" onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar</a>
</div>
