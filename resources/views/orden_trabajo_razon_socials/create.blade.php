@extends('layouts.app')

@section('content')
@if ( isset(session('flash_notification')[0]))
    @php
        $message = session('flash_notification')[0]
    @endphp

    <div class="alta col-md-12">

        <!--COLLAPSER-->
        <div class="col-xs-12 col-md-12 padding-alta">
            <p class="text-alta centers margin-alta">
                {!! $message['message'] !!}
            </p>
        </div>
        <br />
        <div class="form-group col-sm-12 col-md-12 col-lg-12 padding-tag margin-traspaso">
            <div class="col-xs-12 col-sm-12 col-md-12 margin-validar center-validar margin-10 centers">

                <button type="button" onclick="closeFancyboxIframe();"
                class="btn  btn-default" data-dismiss="modal" aria-label="Close">
                    Cerrar
                </button>

            </div>
        </div>
        <!--COLLAPSER-->
    </div>

@else
    <section class="content-header">
        <h1>
            Razon Social
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ordenTrabajoRazonSocials.store']) !!}

                        @include('orden_trabajo_razon_socials.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
