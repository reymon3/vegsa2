<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $servicio->nombre !!}</p>
</div>

<!-- Descripción Field -->
<div class="form-group">
    {!! Form::label('Descripción', 'Descripción:') !!}
    <p>{!! $servicio->descripcion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Usuario alta:') !!}
    <p>{!! $servicio->blame->name !!}</p>
</div>
