@extends('layouts.app')

@section('content')

    <section class="content-header">

            <h1>Carta de Entrega</h1>
            @include('flash::message')
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')

        @if (\Session::has('errores'))
            <div class="alert alert-error">
                <p>Ocurrieron los siguientes errores.</p>
                <ul>
                    @foreach (\Session::get('errores') as $fila => $errores)
                    <br /><li>Fila: {!!$fila !!}
                        @foreach ($errores as $error)
                            <br />{!! $error !!}, <br />
                        @endforeach
                    </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="padding-left: 20px">

                    <!-- <div class="form-group">
                        {!! Form::label('orden_trabajo_id', 'Orden Trabajo:') !!}
                        <p>0000006/2019	</p>
                    </div> -->

                </div>
                <div class="row">
                    <!-- Orden Trabajo Id Field -->
                    <form id="formSubir" name="formSubir" enctype="multipart/form-data" action="{!! route('cartas.update', ['id' => $id]) !!}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                        <div class="col-xs-12 col-lg-12 sinpadding2">
                            <div class="custom-upload-file">
                                <input
                                type="file"
                                name="nombre"
                                id="nombre"
                                class="inputfile inputfile-transparent"
                                data-multiple-caption="{count} files selected"
                                />
                                <p class="help-block">Seleccione el archivo a subir.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-12 padding-no">
                            <div class="col-xs-12 col-lg-6 padding-derecho">
                                <input type="submit" class="btn btn-primary">
                                <a href="{!! url('armado-conciliacion') !!}" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
