<div class='btn-group'>
    <!-- <a href="http://localhost/vegsa/public/uploads/facturas/factura-00001.pdf" -->
    <a href="{!! Route('descarga.archivo', ['file' => $nombre]) !!}"
        class="btn btn-default">
        <i class="fa fa-download"></i>
    </a>
    <a href="{!! Route('actualizarCarta', ['id' => $id]) !!}"
        class="btn btn-default ">
        <i class="fa fa-cloud-upload"></i>
    </a>
</div>