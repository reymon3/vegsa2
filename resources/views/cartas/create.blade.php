@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cartas
        </h1>
    </section>
    <div class="content">
    <!-- TODO -->
        <!-- include('adminlte-templates::common.errors') -->
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'cartas.store','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('cartas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
