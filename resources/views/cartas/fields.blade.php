<!-- Cliente Field -->
<div class="form-group col-sm-6">
    {{ Form::label('orden_trabajo_id', 'Orden de Trabajo :')}}
    {!! Form::select('orden_trabajo_id', $ordenes, null, ['class' => 'form-control']) !!}
</div>
<!-- File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Archivo:') !!}
    <input
                                type="file"
                                name="nombre"
                                id="nombre"
                                accept="application/pdf"
                                class="inputfile inputfile-transparent"
                                data-multiple-caption="{count} files selected"
                                />
                                <p class="help-block">Seleccione el archivo a subir.</p>
</div>

@section('scripts')
    <script type="text/javascript">
        $('#update_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Subir', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('placas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
