<!-- Cotizacion Id Field -->
<div class="form-group">
    {!! Form::label('cotizacion_id', 'Cotizacion Id:') !!}
    <p>{!! $cotizacionServicio->cotizacion_id !!}</p>
</div>

<!-- Servicio Id Field -->
<div class="form-group">
    {!! Form::label('servicio_id', 'Servicio Id:') !!}
    <p>{!! $cotizacionServicio->servicio_id !!}</p>
</div>

<!-- Importe Field -->
<div class="form-group">
    {!! Form::label('importe', 'Importe:') !!}
    <p>{!! $cotizacionServicio->importe !!}</p>
</div>

<!-- Iva Field -->
<div class="form-group">
    {!! Form::label('iva', 'Iva:') !!}
    <p>{!! $cotizacionServicio->iva !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $cotizacionServicio->blame_id !!}</p>
</div>

