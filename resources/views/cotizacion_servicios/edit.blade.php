@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cotizacion Servicio
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cotizacionServicio, ['route' => ['cotizacionServicios.update', $cotizacionServicio->id], 'method' => 'patch']) !!}

                        @include('cotizacion_servicios.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection