<!-- Cotizacion Id Field -->
{!! Form::hidden('cotizacion_id',  app('request')->input('cotizacion_id'))!!}

<!-- Servicio Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('servicio_id', 'Servicio:') !!}
    {!! Form::select('servicio_id', $servicios, null, [
      'class' => 'form-control',
      'id'=>'servicio_id',
      'placeholder' => 'Seleccione...'
      ])
    !!}
</div>

<!-- Importe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importe', 'Importe:') !!}
    {!! Form::number('importe', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cotizacionServicios.index') !!}" class="btn btn-default">Cancelar</a>
</div>
