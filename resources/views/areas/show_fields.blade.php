<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $areas->nombre !!}</p>
</div>

<!-- Descripción Field -->
<div class="form-group">
    {!! Form::label('Descripción', 'Descripción:') !!}
    <p>{!! $areas->descripcion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $areas->blame_id !!}</p>
</div>

