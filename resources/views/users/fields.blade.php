<div class="form-group col-sm-6">
    {!! Form::label('empleado_id', 'Empleado:') !!}

    @if (getAction() != 'edit')
    <?php $empleado = isset($user->empleado_id) ? $user->empleado_id : null; ?>
    {!!
        Form::select('empleado_id', $empleados, $empleado, [
            'class' => 'form-control',
            'id'=>'empleado',
            'placeholder' => 'Seleccione...',
            'onchange' => 'cargaEmpleado();'
        ])
    !!}
    @elseif(getAction() == 'edit' && $user->empleado_id == 0)
        {!!
            Form::select('empleado_id', $empleados, null, [
                'class' => 'form-control',
                'id'=>'empleado',
                'placeholder' => 'Seleccione...',
                'onchange' => 'cargaEmpleado();'
            ])
        !!}
    @else
        {!! Form::Hidden('empleado_id', null)!!}
        {!! Form::text('empleado[nombre]', null, [
        'class' => 'form-control',
        'disabled' => 'disabled'
        ])!!}

    @endif
</div>
<div class=" clearfix"></div>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, [
        'class' => 'form-control deshabilitado',
        'readonly' => 'readonly'
    ]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Correo:') !!}
    {!! Form::email('email', null, [
        'class' => 'form-control deshabilitado',
        'readonly' => 'readonly'
    ]) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Contraseña:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('groups', 'Rol:') !!}
    <?php $role = isset($rolUser->roles[0]->id)?$rolUser->roles[0]->id:null; ?>
    {!!
        Form::select('rol_id', $roles, $role, [
            'class' => 'form-control select-template2',
            'id'=>'groups',
            'placeholder' => 'Seleccione...'
        ])
    !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancelar</a>
</div>


@section('scripts')
<script>

    function cargaEmpleado(){

        var empleado_id = $( "#empleado option:selected" ).val();

        if (empleado_id === '') {
            alert('Seleccione un empleado válido.');
            $('#name').val('');
            $('#email').val('');
            return ;
        }

        $.ajax({
            type: "GET",
            url: '{{ url("getEmpleado") }}/' + empleado_id,
            dataType: 'json',
            timeout:45000,
            beforeSend: function(){
                console.log('Buscar Empleado ' + empleado_id);
            }
        })
        .done(function (data) {

            noRegistros = Object.keys(data).length;
            console.log(data);
            console.log(noRegistros);

            if(noRegistros == 0){
                alert('Error al cargar datos de empleado.');
                return;
            }

            $('#name').val('');
            $('#email').val('');

            $('#name').val(data.nombre);
            $('#email').val(data.correo);

        })
        .fail( function(jqXHR, textStatus, errorThrown) {
            //
        });

    }

</script>

@append
