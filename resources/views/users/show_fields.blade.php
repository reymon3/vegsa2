<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Correo:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- ROL -->
<div class="form-group">
    {!! Form::label('rol', 'Rol:') !!}    
    <p>{!! $user->roles[0]->name !!}</p>
</div>
