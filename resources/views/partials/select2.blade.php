@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
@append

@section('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/es.js'></script>

    <script>
        // $.fn.select2.defaults.set('amdBase', 'select2/');
        $.fn.select2.defaults.set('language', 'es');
        $.fn.select2.defaults.set('minimumInputLength', 3);
        $.fn.select2.defaults.set( "theme", "bootstrap" );

    </script>
@append
