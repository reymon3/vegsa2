@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Base Contable
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoBaseContable, ['route' => ['ordenTrabajoBaseContables.update', $ordenTrabajoBaseContable->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_base_contables.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection