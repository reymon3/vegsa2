@extends('layouts.app')
@include('partials.custom_datatable.datatable')
@section('content')
    <section class="content-header">
        <h1>
            Inventario
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <br />
                        <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                            <thead>
                                <tr style="background-color: #f9f9f9">
                                    <th>Archivo</th>
                                    <th>Servidor</th>
                                    <th>Base de datos</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 0 ?>
                                @foreach ($basesContables as $registro)
                                <?php $contador++ ?>
                                <tr  class="text-center">
                                    <td>{{ $registro->nombre}}</td>
                                    <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
                                    <td><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        @includeWhen(count($plantas) == 0, 'orden_trabajo_base_contables.detalles.conteo', [
            'conteo' => $conteo
        ])

        @includeWhen(count($plantas) > 0, 'orden_trabajo_base_contables.detalles.conteo-planta', [
            'conteo' => $conteo
        ])

        <div class="clearfix"></div>
        @include('orden_trabajo_base_contables.detalles.componetizacion')
        <div class="clearfix"></div>
        @include('orden_trabajo_base_contables.detalles.cruce')
        <div class="clearfix"></div>
    </div>
@endsection


@section('scripts')
    <script>
        $( "[id^='base_contable_']" ).hide();
        function muestraDiv(id){
            $( "[id^='base_contable_']" ).hide();
            $( "#base_contable_" + id ).toggle('slow');
        }

        $(document).ready(function() {
            $("#tbl_excel_1").dataTable( {
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                    // "url": "dataTables.german.lang"
                }
            });
        });
    </script>
@append
