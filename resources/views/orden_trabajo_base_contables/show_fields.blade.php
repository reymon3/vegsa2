<!-- cliente-->

<div class="form-group col-sm-6">
    {!! Form::label('cliente', 'Cliente:') !!}
    <p>{!! $ordenTrabajoBaseContable->ordenTrabajo->cotizacion->cliente->nombre !!}</p>
</div>

<div class="form-group col-sm-6">
    {!! Form::label('num_orden', 'N° de orden de trabajo:') !!}
    <p>{!! $ordenTrabajoBaseContable->ordenTrabajo->num_orden_trabajo !!}</p>

</div>
<div class="clearfix"></div>
<form id="formSubir" name="formSubir" enctype="multipart/form-data" action="{!! url('subir-documento') !!}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="orden_trabajo_id" value="{{ $ordenTrabajoBaseContable->orden_trabajo_id }}">
    <div class="col-xs-12 col-lg-12 sinpadding2">
        <div class="custom-upload-file">
            <input
                type="file"
                name="fileImport"
                id="fileImport"
                class="inputfile inputfile-transparent"
                data-multiple-caption="{count} files selected"
            />
            <p class="help-block">Seleccione el archivo a subir.</p>
        </div>
    </div>
    <div class="col-xs-12 col-lg-12 padding-no">
        <div class="col-xs-12 col-lg-6 padding-derecho">
            <input type="submit" class="btn btn-primary">
        </div>
        <div class="col-xs-12 col-lg-6 padding-dizquierdo">
            <!-- <button class="btn_avatar btn_avatar_gris">Cancelar</button> -->
        </div>
    </div>
</form>

<div class="clearfix"></div>


<div class="clearfix"></div>
