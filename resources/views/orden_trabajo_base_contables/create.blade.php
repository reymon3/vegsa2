@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Base Contable
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <!-- Orden Trabajo Id Field -->
                    <form id="formSubir" name="formSubir" enctype="multipart/form-data" action="{!! route('subirDocumento') !!}" method="post">
                    <div class="form-group col-sm-6">

                        {!! Form::label('orden_trabajo_id', 'Orden de Trabajo:') !!}
                        {!! Form::select('orden_trabajo_id', $ordenesTrabajo, null, [
                            'class' => 'form-control',
                            'id'=>'orden_trabajo_id',
                            'placeholder' => 'Seleccione...'
                        ])!!}
                    </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-xs-12 col-lg-12 sinpadding2">
                            <div class="custom-upload-file">
                                <input
                                type="file"
                                name="fileImport"
                                id="fileImport"
                                class="inputfile inputfile-transparent"
                                data-multiple-caption="{count} files selected"
                                />
                                <p class="help-block">Seleccione el archivo a subir.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-12 padding-no">
                            <div class="col-xs-12 col-lg-6 padding-derecho">
                                <input type="submit" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
@endsection
