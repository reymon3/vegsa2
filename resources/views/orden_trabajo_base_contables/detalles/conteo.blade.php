<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Levantamiento Fisico
        </h4>
        <h4 class="pull-right">

            @if(count($conteo))
            <a href="{{route('homologarColumnas', ['id'=>$ordenTrabajoBaseContable->id])}}" class="btn btn-default">
                Homologar columnas
            </a>
            <a href="javascript:;"
            onclick="testConfirm();"
            class="btn btn-primary">Agregar</a>

            <a href="{{route('descargaConteo', ['id' => $ordenTrabajo->id])}}"

            class="btn btn-info">Descargar</a>
            @else
            <a href="javascript:;"
            onclick="fancyboxOpenIframe('{!! route('ordenTrabajoConteos.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"
            class="btn btn-primary">Agregar</a>


            @endif
        </h4>

          <div class="row">
              <div class="col-md-12">
                  <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                      <thead>
                          <tr style="background-color: #f9f9f9;">
                              <th class="text-center">Rubros</th>
                              <th class="text-center">Registros</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                            $contador = 0;

                            $totalRegistros = 0;
                            $totalVRN = 0;
                            $totalDepAcum = 0;
                            $totalVNR = 0;
                            $totalVUR = 0;
                            $totalDA = 0;
                            // dd($plantas, $conteo);
                          ?>
                          @foreach ($conteo as $registro)

                          <?php
                          if(!isset($registro->rubro)){
                              break;
                          }

                          $contador++;
                          $totalRegistros += $registro->registros;

                          ?>
                              <tr>
                                  <td>{{$registro->rubro}}</td>
                                  <td class="text-center">{{ $registro->registros}}</td>
                              </tr>
                          @endforeach
                      </tbody>
                      @if($contador > 0)
                      <tfoot>
                          <tr>
                              <th>Total general</th>
                              <th class="text-center">{{$totalRegistros}}</th>
                          </tr>
                      </tfoot>
                      @endif
                  </table>
              </div>
          </div>
      </div>
  </div>

<script>

// var txt;
function testConfirm(){
    var r = confirm("Existe un conteo previo, ¿está seguro de querer reemplazar dicho conteo?");
    if (r == true) {
      fancyboxOpenIframe('{!! route('ordenTrabajoConteos.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)
    }
return
}
</script>
