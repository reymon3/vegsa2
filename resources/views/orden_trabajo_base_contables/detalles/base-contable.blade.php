<div class="row" id="base_contable_{{$contador}}">
    <div class="col-md-12" style="overflow:auto;">
        <br />
        <table id="tbl_excel_{{$contador}}" class="table table-responsive table-hover table-bordered"  >
            <thead>
                <tr style="background-color: #f9f9f9">
                <?php
                    $aEncabezado = array_filter(json_decode($ordenTrabajoBaseContable->json_cabeceras, true));
                    // $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
                    $nofilas = count($aEncabezado);
                    // dd($aEncabezado, $nofilas );

                    foreach ($aEncabezado as $clave => $valor) {
                        echo "<th>{$valor}</th>";
                    }
                ?>
                <th></th>
                </tr>
            </thead>
            <tbody>

                <?php
                    $aFilas = json_decode($ordenTrabajoBaseContable->json_excel, true);
                    // dd($aFilas);
                    unset($aFilas[1]);
                    // dd($aFilas );
                    $nofilas = count($aFilas);

                    foreach ($aFilas as $clave => $fila) {
                        echo '<tr  class="text-center">';
                        foreach ($fila as $celda => $contenido) {
                            echo "<td>{$contenido}</td>";
                        }
                        echo '</tr>';
                    }
                ?>
                </tr>
            </tbody>
        </table>
    </div>
</div>
