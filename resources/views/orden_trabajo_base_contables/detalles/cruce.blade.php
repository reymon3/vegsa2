<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Cruce de información <small>Base Contable vs Conteo</small>
        </h4>
        <div class="row">
            <!--COLLAPSER-->
            <div class="form-group col-sm-12 col-md-12 col-lg-12 padding-tag margin-traspaso">
                <div class="col-xs-12 col-sm-12 col-md-12 margin-validar center-validar margin-10 centers">
                    <br />
                    @if(count($conteo))
                        <a href="{{route('descargaCruce', ['id' => $ordenTrabajoBaseContable->id])}}"
                        class="btn btn-default"
                        >
                            Descargar
                        </a>
                    @else

                        <div class="well well-sm">
                            Aún no se ha subido ningun conteo.
                        </div>
                    @endif
                    <br />
                </div>
            </div>
            <!--COLLAPSER-->
        </div>
    </div>
</div>
