<div class="box box-primary">
    <div class="box-body">
        <div class="row" style="<?php
         if (!isset($registrosRepetidos['placa_actual']['no_repetidos'][0]->no_repetidos)){
             echo 'display:none;';
         }
         ?>">
            <div class="col-md-12">
                <h4 class="pull-left">
                    Registros Repetidos
                </h4>
                <h4 class="pull-right">
                    @if(count($conteo))
                    <a href="{{url('descargar-repetidos-placa_actual', ['id' => $ordenTrabajo->id])}}"
                    class="btn btn-info">Descargar</a>
                    @endif
                </h4>
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr style="background-color: #f9f9f9;">
                            <th class="text-center" style="width:500px">Placa Actual</th>
                            <th class="text-center" style="width:100px"># Registros</th>
                            <th class="text-center">Folio</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($registrosRepetidos['placa_actual']['no_repetidos'] as $registro)
                            <tr>
                                <td>
                                    {{ $registro->placa_actual }}
                                </td>
                                <td class="text-center">{{ $registro->no_repetidos }}</td>
                                <td class="text-right" >
                                    <?php $folios = [];?>
                                    @foreach ($registrosRepetidos['placa_actual']['detalle_repetidos'] as $detalle)
                                    <?php
                                    if( $detalle['placa_actual'] == $registro->placa_actual ){
                                        // dd($componetizacionDetalle, $componetizacion, $registro, $detalle,  $detalle['serie'], $registro->serie );
                                        $folios[] = $detalle['folio'];
                                    }
                                    ?>
                                    @endforeach
                                    <?php echo implode(", ", $folios);?>
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row"  style="<?php
             if (!isset($registrosRepetidos['serie']['no_repetidos'][0]->no_repetidos)){
                 echo 'display:none;';
             }
         ?>" >
            <div class="col-md-12">

                <h4 class="pull-right">
                    @if(count($conteo))
                    <a href="{{url('descargar-repetidos-serie', ['id' => $ordenTrabajo->id])}}"
                    class="btn btn-info">Descargar</a>
                    @endif
                </h4>
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr style="background-color: #f9f9f9;">
                            <th class="text-center" style="width:500px">Serie</th>
                            <th class="text-center" style="width:100px"># Registros</th>
                            <th class="text-center">Folio</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($registrosRepetidos['serie']['no_repetidos'] as $registro)
                        <tr>
                            <td>
                                {{ $registro->serie }}
                            </td>
                            <td class="text-center">{{ $registro->no_repetidos }}</td>
                            <td class="text-right" >
                                <?php $folios = [];?>
                                @foreach ($registrosRepetidos['serie']['detalle_repetidos'] as $detalle)
                                <?php
                                // dd( $registro, $detalle,  $detalle['serie'], $registro->serie );
                                if( $detalle['serie'] == $registro->serie ){
                                    $folios[] = $detalle['folio'];
                                }
                                ?>
                                @endforeach
                                <?php echo implode(", ", $folios);?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row"  style="<?php
             if (!isset($registrosRepetidos['placa_anterior']['no_repetidos'][0]->no_repetidos)){
                 echo 'display:none;';
             }
         ?>" >
            <div class="col-md-12">
                <h4 class="pull-right">
                    @if(count($conteo))
                    <a href="{{url('descargar-repetidos-placa_anterior', ['id' => $ordenTrabajo->id])}}"
                    class="btn btn-info">Descargar</a>
                    @endif
                </h4>
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr style="background-color: #f9f9f9;">
                            <th class="text-center" style="width:500px">Placa Anterior</th>
                            <th class="text-center" style="width:100px"># Registros</th>
                            <th class="text-center">Folio</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($registrosRepetidos['placa_anterior']['no_repetidos'] as $registro)
                        <tr>
                            <td>
                                {{ $registro->placa_anterior }}
                            </td>
                            <td class="text-center">{{ $registro->no_repetidos }}</td>
                            <td class="text-right" >
                                <?php $folios = [];?>
                                @foreach ($registrosRepetidos['placa_anterior']['detalle_repetidos'] as $detalle)
                                <?php
                                // dd( $registro, $detalle,  $detalle['placa_anterior'], $registro->placa_anterior );
                                if( $detalle['placa_anterior'] == $registro->placa_anterior ){
                                    $folios[] = $detalle['folio'];
                                }
                                ?>
                                @endforeach
                                <?php echo implode(", ", $folios);?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row"  style="<?php
             if (!isset($registrosRepetidos['placa_auxiliar_1']['no_repetidos'][0]->no_repetidos)){
                 echo 'display:none;';
             }
         ?>">
            <div class="col-md-12">
                <h4 class="pull-right">
                    @if(count($conteo))
                    <a href="{{url('descargar-repetidos-placa_auxiliar_1', ['id' => $ordenTrabajo->id])}}"
                    class="btn btn-info">Descargar</a>
                    @endif
                </h4>
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr style="background-color: #f9f9f9;">
                            <th class="text-center" style="width:500px">Placa Auxiliar 1</th>
                            <th class="text-center" style="width:100px"># Registros</th>
                            <th class="text-center">Folio</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($registrosRepetidos['placa_auxiliar_1']['no_repetidos'] as $registro)
                        <tr>
                            <td>
                                {{ $registro->placa_auxiliar_1 }}
                            </td>
                            <td class="text-center">{{ $registro->no_repetidos }}</td>
                            <td class="text-right" >
                                <?php $folios = [];?>
                                @foreach ($registrosRepetidos['placa_auxiliar_1']['detalle_repetidos'] as $detalle)
                                <?php
                                // dd( $registro, $detalle,  $detalle['placa_auxiliar_1'], $registro->placa_auxiliar_1 );
                                if( $detalle['placa_auxiliar_1'] == $registro->placa_auxiliar_1 ){
                                    $folios[] = $detalle['folio'];
                                }
                                ?>
                                @endforeach
                                <?php echo implode(", ", $folios);?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row"  style="<?php
             if (!isset($registrosRepetidos['placa_auxiliar_2']['no_repetidos'][0]->no_repetidos)){
                 echo 'display:none;';
             }
         ?>" >
            <div class="col-md-12">
                <h4 class="pull-right">
                    @if(count($conteo))
                    <a href="{{url('descargar-repetidos-placa_auxiliar_2', ['id' => $ordenTrabajo->id])}}"
                    class="btn btn-info">Descargar</a>
                    @endif
                </h4>
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr style="background-color: #f9f9f9;">
                            <th class="text-center" style="width:500px">Placa Auxiliar 2</th>
                            <th class="text-center" style="width:100px"># Registros</th>
                            <th class="text-center">Folio</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($registrosRepetidos['placa_auxiliar_2']['no_repetidos'] as $registro)
                        <tr>
                            <td>
                                {{ $registro->placa_auxiliar_2 }}
                            </td>
                            <td class="text-center">{{ $registro->no_repetidos }}</td>
                            <td class="text-right" >
                                <?php $folios = [];?>
                                @foreach ($registrosRepetidos['placa_auxiliar_2']['detalle_repetidos'] as $detalle)
                                <?php
                                // dd( $registro, $detalle,  $detalle['placa_auxiliar_2'], $registro->placa_auxiliar_2 );
                                if( $detalle['placa_auxiliar_2'] == $registro->placa_auxiliar_2 ){
                                    $folios[] = $detalle['folio'];
                                }
                                ?>
                                @endforeach
                                <?php echo implode(", ", $folios);?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
