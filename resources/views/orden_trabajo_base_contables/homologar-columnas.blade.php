@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Homologar Columnas - {{$ordenTrabajoBaseContable->nombre}}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" id="base_contable_1">
                    <div class="col-md-12" style="overflow:auto;">
                        <br />
                        <input type="hidden" id="orden_trabajo_base_contable_id" name="orden_trabajo_base_contable_id" value="{{$ordenTrabajoBaseContable->id}}">
                        <input type="hidden" id="public_url" name="public_url" value="{{url('/')}}">
                        <input type="hidden" id="csrf-token" name="csrf-token" value="{{ csrf_token() }}">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <?php
                                // dd(json_decode($ordenTrabajoBaseContable->json_cabeceras));
                                    $aEncabezado = array_filter(json_decode($ordenTrabajoBaseContable->json_cabeceras, true));
                                    $nofilas = count($aEncabezado);
                                    $contador = 0;

// dd(url()->current(), url()->full(), Illuminate\Support\Facades\URL::current(), url('/'));
                                    foreach ($aEncabezado as $clave => $valor) {
                                        $contador++;

                                        ?>
                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="heading_{{$contador}}">
                                            <h4 class="panel-title">
                                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$contador}}" aria-expanded="true" aria-controls="collapse_{{$contador}}">
                                                {{$valor}}
                                              </a>
                                            </h4>
                                          </div>
                                          <div
                                              id="collapse_{{$contador}}"
                                              data-contador="{{$contador}}"
                                              data-columna="{{$valor}}"
                                              class="panel-collapse collapse "
                                              role="tabpanel"
                                              aria-labelledby="heading_{{$contador}}"
                                          >
                                            <div class="panel-body" id="panel-body-{{$contador}}">
                                                <form method="POST" action="{{route('reemplazaDatosPorColumna')}}" id="form-{{$contador}}"><br /></form>
                                            </div>
                                          </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
@endsection


@section('scripts')

    <script type="text/javascript">

        $(function() {

            $(".collapse").on('shown.bs.collapse', function(e) {

                var contador = $(this).data('contador');
                var columna = $(this).data('columna');

                var divId = this.id;
                var orden_trabajo_base_contable_id = $('#orden_trabajo_base_contable_id').val();
                var url = $('#public_url').val() + '/obtener-datos-agrupados/' +
                + orden_trabajo_base_contable_id+'/'
                + 'columna/' + columna
                console.log(url);

                if ($(this).is(e.target)) {

                    blockDiv(this.id);

                    $.getJSON(url, function(data) {
                        console.log(data)
                        var items = [];
                        var selectReemplazar = 'Reemplazar registros seleccionados por: <br /> <select name="valor">';
                        selectReemplazar += "<option value='' >Seleccione... </option>";

                        $( "#" + 'form-'+contador ).empty();

                          $.each( data, function( key, val ) {
                            items.push( " <input type='checkbox' id ='chk-" + key + "' name='registros[]' value='"+val+"'>"
                            + " <label for='chk-" + key + "'>" + val + "</label>" );

                            selectReemplazar += "<option value='"+val+"' >"+val+" </option>";

                          });

                          selectReemplazar += "</select>";
                          selectReemplazar += " &nbsp; <input type='submit' value='Aceptar'></p>";

                          items.sort((a,b)=>a-b);
                          console.log(items.sort((a,b)=>a-b));

                          $( items.join( "<br />" )).appendTo( "#" + 'form-'+contador );
                          $( "#" + 'form-'+contador ).append('<br />' + selectReemplazar);

                        unblockDiv(divId)
                        console.log(divId)
                    });

                }

                return ;

            });

            $(".collapse").on('hide.bs.collapse', function(e) {

                var contador = $(this).data('contador');
                if ($(this).is(e.target)) {

                    $('#loader_' + contador).hide();

                    unblockDiv(this.id)
                    $( "#" + 'form-'+contador ).empty().append('<br />');
                }

            });

        });

        // https://programandoointentandolo.com/2017/12/comprobar-elemento-esta-visible-jquery.html

        // Si elemento esta visible lo oculta y si no lo muestra

        // if ($('#elemento').is(':visible')) {
        //     $('#elemento').hide();
        // } else {
        //     $('#elemento').show();
        // }
        //
        // // Si el elemento2 esta oculto lo indica por consola y si no ejecuta una funcion
        // if ($('#elemento2').is(':hidden')) {
        //     console.log("Esta oculto");
        // } else {
        //     hacerAlgo();
        // }

    $(document).ready(function() {


        // $('#blockButton').click(function() {
        //     $('div.test').block({ message: null });
        // });
        //
        // $('#blockButton2').click(function() {
        //     $('div.test').block({
        //         message: '<h1>Processing</h1>',
        //         css: { border: '3px solid #a00' }
        //     });
        // });
        //
        // $('#unblockButton').click(function() {
        //     $('div.test').unblock();
        // });
        //
        // $('a.test').click(function() {
        //     alert('link clicked');
        //     return false;
        // });
        //
        // $('#collapse_1').block({
        //     message: '<h5>Cargando <img src="http://localhost/vegsa/public/images/ajax-loader.gif"></h5>',
        //     css: { border: '1px solid #666' }
        // });
        //
        // $('#collapse_1').unblock();

    });


    function blockDiv(id){
        // $('#' + id).block();
        $('#' + id).block({
            message: '<h5>Cargando &nbsp; <img src="{{ asset('images/clasic-loader.gif') }}"></h5>',
            css: { border: '1px solid #666' }
        });

        return ;
    }

    function unblockDiv(id){
        $('#' + id).unblock();

        return ;
    }

    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('#csrf-token').val()
    //     }
    // });

    $(document).on("submit", "form", function(event)
    {
        event.preventDefault();

        console.log('envia formulario');

        var formData = new FormData(this); // use serializeArray
        var formId = this.id;
        var divCollapse = $(this).parent("div").parent("div").attr('id');

        console.log(formData);
        console.log(divCollapse);
        // return ;
        formData.append('nombre_columna', $(this).parent("div").parent("div").data('columna'));
        formData.append('orden_trabajo_base_contable_id', $('#orden_trabajo_base_contable_id').val());
        // formData.append('nombre_columna', $('#orden_trabajo_base_contable_id').val());

        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            headers: {
                'X-CSRF-TOKEN': $('#csrf-token').val()
            },
            // type: $(this).attr("method"),
            dataType: "JSON",
            data: formData,
            beforeSend: function(){
                blockDiv(divCollapse);
            },
            processData: false,
            contentType: false,
            success: function (data, status){
                unblockDiv(divCollapse);
                // $('#' + divCollapse).collapse('hide');
                $('#' + divCollapse).collapse('toggle');
                // $('#' + divCollapse).collapse('show');
            },
            error: function (xhr, desc, err){
                unblockDiv(divCollapse);

            }
        })
        .done(function (resul) {
            unblockDiv(divCollapse);
        });
    });
</script>

@append
