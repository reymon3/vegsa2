<!-- Orden Trabajo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden_trabajo_id', 'Orden Trabajo Id:') !!}
    {!! Form::number('orden_trabajo_id', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoBaseContables.index') !!}" class="btn btn-default">Cancelar</a>
</div>
