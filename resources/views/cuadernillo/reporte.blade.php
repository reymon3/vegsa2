<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }
            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }
            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                width:    21.59cm;
                height:   27.94cm;
                z-index:  -1000;
            }
            h1 {
                font-size: 8em;
            }
            th {
                font-size:40%;
            }
            table, th {
                border: 1px solid black;
                border-collapse: collapse;
            }
            thead {
                color: black;
                background-color: #DDEDBE;
            }
            .myCliente {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <main> 
            @if(!empty($maquirariaEquipo))
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Maquinaria y Equipo</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($maquirariaEquipo as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
<!-- FIN MAQUINARIA Y EQUIPO -->

            @if(!empty($equipoLaboratorio))
            <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Equipo de Laboratorio</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipoLaboratorio as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
<!-- FIN EQUIPO DE LABORATORIO -->

            @if(!empty($mobiliarioIndustrial))
            <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Mobiliario Industial</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($mobiliarioIndustrial as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
<!-- FIN MOBILIARIO INDUSTRIAL -->

            @if(!empty($equipoTransporte))
            <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Equipo de Transporte</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipoTransporte as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
<!-- FIN EQUIPO DE TRANSPORTE -->

            @if(!empty($equipoComputo))
            <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Equipo de Transporte</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($equipoComputo as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
<!-- FIN EQUIPO DE COMPUTO -->

            @if(!empty($mobiliarioEquipo))
            <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
            <img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="100px">
            <div class="myCliente">
                <h1>Equipo de Transporte</h1>
            </div>
                <p style="page-break-before: always;"></p>  <!-- Nueva Pagina -->
                <table>
                    <thead>
                        <tr>
                        <th scope="col">Compañia</th>
                        <th scope="col">Número de Activo</th>
                        <th scope="col">Folio</th>
                        <th scope="col">Desgloses</th>
                        <th scope="col">Descripción de Activo</th>
                        <th scope="col">Fecha de Adquisición</th>
                        <th scope="col">Monto Original de Inversión</th>
                        <th scope="col">Depreciación Acumulada</th>
                        <th scope="col">Valor Neto</th>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        <th scope="col">Area</th>
                        <th scope="col">Ubicación</th>
                        <th scope="col">V.R.N</th>
                        <th scope="col">%DEP</th>
                        <th scope="col">DEP. ACUM</th>
                        <th scope="col">V.N.R</th>
                        <th scope="col">V.U.R</th>
                        <th scope="col">D.A</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($mobiliarioEquipo as $value)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>{{$value->fecha_adquisicion}}</th>
                                <th></th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th></th>
                                <!--  -->
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                                <!--  -->
                                <th>{{$value->area}}</th>
                                <th></th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->porcentaje_dep}}</th>
                                <th>{{$value->dep_acum}}</th>
                                <th>{{$value->vrn}}</th>
                                <th>{{$value->vur}}</th>
                                <th>{{$value->da}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            @endif
        </main>
    </body>
    </div>
</html></div>