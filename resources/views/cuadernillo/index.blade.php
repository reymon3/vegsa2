@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Cuadernillo</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <form action="{{ Route('cuadernillo.store') }}" method="post" enctype="multipart/form-data">
            @csrf
                <!-- Hay que seleccionar al cliente con un input tipo select -->
                <!-- input de tipo texto para una descripcion de lo que se realizo  -->
                <!-- Cuando se envie tiene que ir el id para hacer un filtro de informacion del cliente  -->
                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select class="form-control" id="cliente" name="cliente">
                        @foreach($clientes as $cliente)
                            <option value="{{ $cliente->id }}">{{ $cliente->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="corto">Nombre Corto del Cliente</label>
                    <input type="text" class="form-control" id="corto" name="corto">
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="fecha">Fecha</label>
                        <input type="date" class="form-control" id="fecha" name="fecha">
                    </div>
                <div class="col-md-6">
                    <label for="logotipo">Logotipo del cliente</label>
                    <input type="file" class="form-control-file" id="logotipo" name="logotipo">
                    <label for="tamano">Resolucion</label>
                    <!--  -->
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tamano" id="tamano" value="1" checked>
                        <label class="form-check-label" for="tamano">
                            100px * 320px
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="tamano" id="tamano" value="2">
                        <label class="form-check-label" for="tamano">
                            130px * 130px
                        </label>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <label for="moneda">Cifras en: </label>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="moneda" id="moneda" value="PESOS MONEDA NACIONAL">
                        <label class="form-check-label" for="moneda">
                            PESOS MONEDA NACIONAL
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="moneda" id="moneda" value="DOLARES">
                        <label class="form-check-label" for="moneda">
                            DOLARES
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Indice</label>
                    <hr>
                        @foreach($indices as $indice)
                            <input class="form-check-input" type="checkbox" value="{{ $indice->nombre }}" id="indices[]" name="indices[]">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $indice->nombre }}
                            </label>
                            <br>
                        @endforeach
                </div>
                
                <button type="submit" name="operacion" value="portada" class="btn btn-primary">Portada</button>
                <button type="submit" name="operacion" value="contenido" class="btn btn-primary">Contenido</button>
                <button type="submit" name="operacion" value="indice" class="btn btn-primary">Indice</button>
                <button type="submit" name="operacion" value="separadores" class="btn btn-primary">Separadores</button>
                <!-- <button type="submit" name="operacion" value="certificado" class="btn btn-primary">Certificado</button>
                <button type="submit" name="operacion" value="limitantes" class="btn btn-primary">Condiciones Limitantes</button> -->
                <button type="submit" name="operacion" value="valores" class="btn btn-primary">Valores Expresados</button>
                <button type="submit" name="operacion" value="reporte" class="btn btn-primary">Reporte</button>
                <button type="submit" name="operacion" value="resumen" class="btn btn-primary">Cuadros de Resumen</button>
            </form>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

