<html>
    <head>
        <style>
            /** 
            * Set the margins of the PDF to 0
            * so the background image will cover the entire page.
            **/
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            /**
            * Define the real margins of the content of your PDF
            * Here you will fix the margins of the header and footer
            * Of your background image.
            **/
            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            /** 
            * Define the width, height, margins and position of the watermark.
            **/
            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                /** The width and height may change 
                    according to the dimensions of your letterhead
                **/
                width:    21.59cm;
                height:   27.94cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
            h1 {
                display: block;
                font-size: 5em;
                margin-top: 0.67em;
                margin-bottom: 0.67em;
                margin-left: 0.67em;
                margin-right: 0;
                font-weight: bold;
            }
            .myCliente {
                text-align: right;
            }
            .descripcion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1.5em;
            }
            .direccion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1em;
            }
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/contenido2.jpg" height="100%" width="100%" />
        </div>

        <main> 
            <br>
            <h1>CONTENIDO</h1>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="myCliente">
                <h2>{{$cliente->nombre}}</h2>
            </div>
            <br>
            <br>
            <div class="descripcion">
                <p>{{ $descripcion }}</p>
            </div>
            <div class="direccion">
                <p>{{ $cliente->domicilio }}</p>
            </div>
        </main>
    </body>
</html>