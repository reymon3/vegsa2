<html>
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <style>
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                width:    21.59cm;
                height:   27.94cm;
                z-index:  -1000;
            }
            h1 {
                font-size: 2em;
            }
            .myCliente {
                text-align: center;
            }
          
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/condicioneslimitantes.jpg" height="100%" width="100%" />
        </div>
        <main> 
            <br><br><br><br>
            <div class="myCliente">
                <h1>VALORES EXPRESADOS EN EL AVALÚO</h1>
            </div>

            <br>
            <br>
                <p><b>"VALOR DE REPOSICIÓN NUEVO" (V.R.N):</b>
                Se entenderá como el valor de cotización o estimado de mercado, de una construcción, equipo igual o equivalente: más los gastos en que se incurriría en la actualidad por concepto de derechos de importación, fletes, gastos de instalación eléctrica, mecánica, instrumentaciones civil, ingeniería, maniobras, etc., en su caso.
                </p>

                <p><b>“PORCENTAJE DE DEPRECIACIÓN” (% DEP.):</b>
                Corresponde a la tasa de depreciación física acumulada (en porciento) que tiene el activo valuado, este porcentaje es calculado considerando tres factores principales que son: edad, condición física y obsolescencia. Esta depreciación es base a la Vida Útil Total estimada en el avalúo.
                </p>
                
                <p><b>(DEP. ACUM.) DEPRECIACIÓN ACUMULADA:</b>Corresponde a la tasa de depreciación física hasta la fecha del avalúo (en valor monetario) y con los datos recabados con los departamentos Ingeniería y Mantenimientos en el desarrollo del proyecto.
                </p>

                <p><b>“VALOR COMERCIAL APROXIMADO " (V.C.Aprox):</b>
                Se entenderá como el valor de mercado aproximado de los bienes en la fecha en que se practicó el avalúo y se determina a partir del Valor de Reposición Nuevo, corregido por los factores de depreciación debida a la vida consumida respecto a la vida útil de producción y económica; estado de conservación y grado de obsolescencia relativa; para la empresa en cuestión.
                </p>

                <p><b>“VIDA ÚTIL REMANENTE " (V.U.R.):</b>
                Se entenderá como la vida que se estima tendrán los bienes en el futuro, dentro de los límites de eficiencia de producción y económica para la empresa en cuestión, y con estimaciones hechas con los departamentos de ingeniería y mantenimiento de dichas empresas.
                </p>

                <p><b>(DEP. POR PERIODO) DEPRECIACIÓN POR PERIODO:</b>
                Es el resultado de dividir el Valor Neto de Reposición entre el estimado de Vida Remanente, que corresponde a la tasa de depreciación física acumulada en un futuro, con respecto a la Vida Útil Remanente asignado al activo fijo.
                </p>
        </main>
    </body>
</html> 