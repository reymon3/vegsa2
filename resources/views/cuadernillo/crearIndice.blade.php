@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Cuadernillo- Indice</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <form action="{{ Route('indice.crear') }}" method="post">
            @csrf
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Cliente</label>
                    <select class="form-control" id="cliente" name="cliente">
                        @foreach($clientes as $cliente)
                            <option value="{{ $cliente->id }}">{{ $cliente->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Indice</label>
                    <hr>
                        @foreach($indices as $indice)
                            <input class="form-check-input" type="checkbox" value="{{ $indice->nombre }}" id="indices[]" name="indices[]">
                            <label class="form-check-label" for="defaultCheck1">
                                {{ $indice->nombre }}
                            </label>
                            <br>
                        @endforeach
                </div>
                
                <button type="submit" name="operacion" value="indice" class="btn btn-primary">Indice</button>
                <button type="submit" name="operacion" value="separadores" class="btn btn-primary">Separadores</button>
            </form>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

