<html>
    <head>
        <style>
            /** 
            * Set the margins of the PDF to 0
            * so the background image will cover the entire page.
            **/
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            /**
            * Define the real margins of the content of your PDF
            * Here you will fix the margins of the header and footer
            * Of your background image.
            **/
            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            /** 
            * Define the width, height, margins and position of the watermark.
            **/
            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                /** The width and height may change 
                    according to the dimensions of your letterhead
                **/
                width:    21.59cm;
                height:   27.94cm;

                /** Your watermark should be behind every content**/
                z-index:  -1000;
            }
            h1 {
                display: block;
                font-size: 2em;
                margin-top: 0.67em;
                margin-bottom: 0.67em;
                margin-left: 0.67em;
                margin-right: 0;
                font-weight: bold;
            }
            h1.txt1 {
                color: #2F5597; 
            }
            h1.txt2 {
                color: #FFC000; 
            }
            h1.txt3 {
                color: #7CE2B9; 
            }
            h1.txt4 {
                color: #2091CA; 
            }
            h1.txt5 {
                color: #BFBFBF; 
            }
            h1.txt6 {
                color: #2F5597; 
            }
            h1.txt7 {
                color: #FFC000; 
            }
            h1.txt8 {
                color: #7CE2B9; 
            }
            h1.txt9 {
                color: #2091CA; 
            }
            h1.txt10 {
                color: #BFBFBF; 
            }
            .myCliente {
                text-align: right;
            }
            .descripcion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1.5em;
            }
            .direccion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1em;
            }
            p.box1 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#2F5597;
            color:#000;
            }
            p.box2 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#FFC000;
            color:#000;
            }
            p.box3 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#7CE2B9;
            color:#000;
            }
            p.box4 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#2091CA;
            color:#000;
            }
            p.box5 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#BFBFBF;
            color:#000;
            }
            p.box6 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#2F5597;
            color:#000;
            }
            p.box7 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#FFC000;
            color:#000;
            }
            p.box8 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#7CE2B9;
            color:#000;
            }
            p.box9 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#2091CA;
            color:#000;
            }
            p.box10 {
            width:150px;
            margin:200px 50px 0px 450px;
            padding:20px;
            font-size:italic;
            background-color:#BFBFBF;
            color:#000;
            }
        </style>
    </head>
    <body>
        <!-- <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/contenido2.jpg" height="100%" width="100%" />
        </div> -->
        <main> 
            <div>
            
            <?php $i = 1; ?>
            @foreach($indice as $value)
                <?php $numero = count($indice);?>
                @if($i == 1)
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                    <h1 class="txt1">{{ $value }}</h1>
                    <p class="box1"> {{'"'.$nombreCorto.'"'}} {{ $descripcion }} </p>
                    <?php $i++; ?>
                @else 
                    <p style="page-break-before: always;"></p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <h1 class="txt{{$i}}">{{ $value }}</h1>
                    <p class="box{{$i}}"> {{'"'.$nombreCorto.'"'}} {{ $descripcion }} </p>
                    <?php $i++; ?>
                @endif 
            @endforeach
                
            </div>
        </main>
    </body>
</html>