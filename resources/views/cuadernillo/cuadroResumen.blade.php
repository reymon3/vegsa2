<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                width:    21.59cm;
                height:   27.94cm;
                z-index:  -1000;
            }
            h1 {
                font-size: 1.5em;
            }
            .myCliente {
                text-align: center;
            }
            #div-1a {
                float:left;
                width:250px;
            }
            #div-1b {
                float:left;
                width:150px;
            }
           
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/CuadroResumen.jpg" height="100%" width="100%" />
        </div>
        <main>
            <table style="width:100%">
                <tr>
                    <th><img src="{{public_path()}}./uploads/logotipo/{{$nombreArchivo}}" height="50px"></th>
                    <th><img src="{{public_path()}}./images/cuadernillo/vegsa.jpg" height="80px"></th>
                </tr>
            </table>
            <div class="myCliente">
                <h1>"Cuadro de Resumen"</h1>
                <!-- TABLA -->
                <div id="div-1a"></div>
                <div id="div-1b">
                <table>
                    <thead style="color: black; background-color: #DDEDBE;font-size:90%; text-align: left;">
                        <tr>
                            <th>Compañia</th>
                            <th>Rubro</th>
                            <th>No. de Activos</th>
                            <th>V.R.N</th>
                            <th>Dep. Acum</th>
                            <th>V.N.R</th>
                            <th>V.U.R</th>
                            <th>D.A</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid black; font-size:60%;">
                        @if($maquirariaEquipo[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Maquinaria y Equipo</th>
                                <th>{{$maquirariaEquipo[0]->valores}}</th>
                                <th>{{$maquirariaEquipoSUM[0]->valores}}</th>
                                <th>{{$maquirariaEquipoDEP[0]->valores}}</th>
                                <th>{{$maquirariaEquipoVNR[0]->valores}}</th>
                                <th>{{$maquirariaEquipoVUR[0]->valores}}</th>
                                <th>{{$maquirariaEquipoDA[0]->valores}}</th>
                            </tr>
                        @endif


                        @if($equipoLaboratorio[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Equipo de Laboratorio</th>
                                <th>{{$equipoLaboratorio[0]->valores}}</th>
                                <th>{{$equipoLaboratorioSUM[0]->valores}}</th>
                                <th>{{$equipoLaboratorioDEP[0]->valores}}</th>
                                <th>{{$equipoLaboratorioVNR[0]->valores}}</th>
                                <th>{{$equipoLaboratorioVUR[0]->valores}}</th>
                                <th>{{$equipoLaboratorioDA[0]->valores}}</th>
                            </tr>
                        @endif


                        @if($mobiliarioIndustrial[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Mobiliario Industrial</th>
                                <th>{{$mobiliarioIndustrial[0]->valores}}</th>
                                <th>{{$mobiliarioIndustrialSUM[0]->valores}}</th>
                                <th>{{$mobiliarioIndustrialDEP[0]->valores}}</th>
                                <th>{{$mobiliarioIndustrialVNR[0]->valores}}</th>
                                <th>{{$mobiliarioIndustrialVUR[0]->valores}}</th>
                                <th>{{$mobiliarioIndustrialDA[0]->valores}}</th>
                            </tr>
                        @endif


                        @if($equipoTransporte[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Equipo de Transporte</th>
                                <th>{{$equipoTransporte[0]->valores}}</th>
                                <th>{{$equipoTransporteSUM[0]->valores}}</th>
                                <th>{{$equipoTransporteDEP[0]->valores}}</th>
                                <th>{{$equipoTransporteVNR[0]->valores}}</th>
                                <th>{{$equipoTransporteVUR[0]->valores}}</th>
                                <th>{{$equipoTransporteDA[0]->valores}}</th>
                            </tr>
                        @endif


                        @if($equipoComputo[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Equipo de Computo</th>
                                <th>{{$equipoComputo[0]->valores}}</th>
                                <th>{{$equipoComputoSUM[0]->valores}}</th>
                                <th>{{$equipoComputoDEP[0]->valores}}</th>
                                <th>{{$equipoComputoVNR[0]->valores}}</th>
                                <th>{{$equipoComputoVUR[0]->valores}}</th>
                                <th>{{$equipoComputoDA[0]->valores}}</th>
                            </tr>
                        @endif


                        @if($mobiliarioEquipo[0]->valores > 0)
                            <tr>
                                <th>{{$clienteInfo->nombre}}</th>
                                <th>Mobiliario y Equipo</th>
                                <th>{{$mobiliarioEquipo[0]->valores}}</th>
                                <th>{{$mobiliarioEquipoSUM[0]->valores}}</th>
                                <th>{{$mobiliarioEquipoDEP[0]->valores}}</th>
                                <th>{{$mobiliarioEquipoVNR[0]->valores}}</th>
                                <th>{{$mobiliarioEquipoVUR[0]->valores}}</th>
                                <th>{{$mobiliarioEquipoDA[0]->valores}}</th>
                            </tr>
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </main>
    </body>
</html>