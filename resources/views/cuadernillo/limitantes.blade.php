<html>
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <style>
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                width:    21.59cm;
                height:   27.94cm;
                z-index:  -1000;
            }
            h1 {
                font-size: 2em;
            }
            .myCliente {
                text-align: center;
            }
            .descripcion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1.5em;
            }
            .direccion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1em;
            }
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/condicioneslimitantes.jpg" height="100%" width="100%" />
        </div>
        <main> 
            <br><br>
            <div class="myCliente">
                <h1>CONDICIONES LIMITANTES</h1>
                <p>Inventario, Etiquetado, Plaqueo, Conciliación y Avaluo de los activos fijos de la empresa</p>
                <p>{{$clienteInfo->nombre}}</p>
                <table class="table">
                    <thead class="thead-light">
                        <tr>
                        <th scope="col">Rubro</th>
                        <th scope="col">Catalogo</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Modelo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($consulta as $value)
                            <tr>
                                <th>{{$value->rubro}}</th>
                                <th>{{$value->catalogo}}</th>
                                <th>{{$value->descripcion}}</th>
                                <th>{{$value->marca}}</th>
                                <th>{{$value->modelo}}</th>
                            </tr>
                            @endforeach
                    </tbody>
                </table>

            </div>
        </main>
    </body>
</html>