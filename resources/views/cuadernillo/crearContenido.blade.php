@extends('layouts.app')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Cuadernillo- Contenido</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
            <form action="{{ Route('cuadernillo.store') }}" method="post">
            @csrf
                <!-- Hay que seleccionar al cliente con un input tipo select -->
                <!-- input de tipo texto para una descripcion de lo que se realizo  -->
                <!-- Cuando se envie tiene que ir el id para hacer un filtro de informacion del cliente  -->
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Cliente</label>
                    <select class="form-control" id="cliente" name="cliente">
                        @foreach($clientes as $cliente)
                            <option value="{{ $cliente->id }}">{{ $cliente->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Crear</button>
            </form>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

