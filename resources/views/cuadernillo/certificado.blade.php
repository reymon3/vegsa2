<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm 0cm 0cm;
            }

            body {
                margin-top:    3.5cm;
                margin-bottom: 1cm;
                margin-left:   1cm;
                margin-right:  1cm;
            }

            #watermark {
                position: fixed;
                bottom:   0px;
                right:    0px;
                top:      0px;
                left:     0px;
                width:    21.59cm;
                height:   27.94cm;
                z-index:  -1000;
            }
            h1 {
                font-size: 2em;
            }
            .myCliente {
                text-align: center;
            }
            .descripcion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1.5em;
            }
            .direccion {
                padding: -10px;
                margin: -10px;
                text-align: right;
                color: # F0FFFF;
                font-size: 1em;
            }
        </style>
    </head>
    <body>
        <div id="watermark">
            <img src="{{public_path()}}./images/cuadernillo/certificado.jpg" height="100%" width="100%" />
        </div>
        <main> 
            <div class="myCliente">
                <h1>CERTIFICADO</h1>
                <p>AVALÚO DE ACTIVOS FIJOS CON CIFRAS AL {{$fecha}}</p>
                <P>(CIFRAS EN {{$moneda}})</P>
            </div>
        </main>
    </body>
</html>