@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '80%']) !!}

@section('js')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endsection
