@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reportes <small>Archivo Maestro</small>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('archivo_maestros.show_fields')
                    <a href="{!! route('descargar-archivo-maestro', ['id' => $archivoMaestro->id]) !!}" class="btn btn-primary">Descargar</a>
                    <a href="{!! route('descargar-sobrante-fisico', ['id' => $archivoMaestro->id]) !!}" class="btn btn-info">Sobrante Fisico</a>
                    <a href="{!! route('descargar-sobrante-contable', ['id' => $archivoMaestro->id]) !!}" class="btn btn-success">Sobrante Contable</a>
                    <a href="{!! route('archivoMaestros.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('archivo_maestros.detalle')
            </div>
        </div>

    </div>
@endsection
