@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Archivo Maestro
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($archivoMaestro, ['route' => ['archivoMaestros.update', $archivoMaestro->id], 'method' => 'patch']) !!}

                        @include('archivo_maestros.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection