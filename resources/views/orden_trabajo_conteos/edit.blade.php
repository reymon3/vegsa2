@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Conteo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoConteo, ['route' => ['ordenTrabajoConteos.update', $ordenTrabajoConteo->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_conteos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection