<!-- Orden Trabajo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden_trabajo_id', 'Orden Trabajo Id:') !!}
    {!! Form::number('orden_trabajo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Folio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::text('folio', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro_id', 'Rubro Id:') !!}
    {!! Form::number('rubro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Activo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activo', 'Activo:') !!}
    {!! Form::text('activo', null, ['class' => 'form-control']) !!}
</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Ubicacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ubicacion', 'Ubicacion:') !!}
    {!! Form::text('ubicacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Piso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('piso', 'Piso:') !!}
    {!! Form::text('piso', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Vrn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vrn', 'Vrn:') !!}
    {!! Form::number('vrn', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    {!! Form::number('porcentaje_dep', null, ['class' => 'form-control']) !!}
</div>

<!-- Dep Acum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    {!! Form::number('dep_acum', null, ['class' => 'form-control']) !!}
</div>

<!-- Vnr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vnr', 'Vnr:') !!}
    {!! Form::number('vnr', null, ['class' => 'form-control']) !!}
</div>

<!-- Vur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vur', 'Vur:') !!}
    {!! Form::number('vur', null, ['class' => 'form-control']) !!}
</div>

<!-- Da Field -->
<div class="form-group col-sm-6">
    {!! Form::label('da', 'Da:') !!}
    {!! Form::number('da', null, ['class' => 'form-control']) !!}
</div>

<!-- Vut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vut', 'Vut:') !!}
    {!! Form::number('vut', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoConteos.index') !!}" class="btn btn-default">Cancel</a>
</div>
