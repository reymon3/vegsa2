<!-- Orden Trabajo Id Field -->
<div class="form-group">
    {!! Form::label('orden_trabajo_id', 'Orden Trabajo Id:') !!}
    <p>{!! $ordenTrabajoConteo->orden_trabajo_id !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $ordenTrabajoConteo->folio !!}</p>
</div>

<!-- Rubro Id Field -->
<div class="form-group">
    {!! Form::label('rubro_id', 'Rubro Id:') !!}
    <p>{!! $ordenTrabajoConteo->rubro_id !!}</p>
</div>

<!-- Activo Field -->
<div class="form-group">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{!! $ordenTrabajoConteo->activo !!}</p>
</div>

<!-- Marca Field -->
<div class="form-group">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $ordenTrabajoConteo->marca !!}</p>
</div>

<!-- Modelo Field -->
<div class="form-group">
    {!! Form::label('modelo', 'Modelo:') !!}
    <p>{!! $ordenTrabajoConteo->modelo !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $ordenTrabajoConteo->serie !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $ordenTrabajoConteo->area !!}</p>
</div>

<!-- Ubicacion Field -->
<div class="form-group">
    {!! Form::label('ubicacion', 'Ubicacion:') !!}
    <p>{!! $ordenTrabajoConteo->ubicacion !!}</p>
</div>

<!-- Piso Field -->
<div class="form-group">
    {!! Form::label('piso', 'Piso:') !!}
    <p>{!! $ordenTrabajoConteo->piso !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $ordenTrabajoConteo->observaciones !!}</p>
</div>

<!-- Vrn Field -->
<div class="form-group">
    {!! Form::label('vrn', 'Vrn:') !!}
    <p>{!! $ordenTrabajoConteo->vrn !!}</p>
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <p>{!! $ordenTrabajoConteo->porcentaje_dep !!}</p>
</div>

<!-- Dep Acum Field -->
<div class="form-group">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    <p>{!! $ordenTrabajoConteo->dep_acum !!}</p>
</div>

<!-- Vnr Field -->
<div class="form-group">
    {!! Form::label('vnr', 'Vnr:') !!}
    <p>{!! $ordenTrabajoConteo->vnr !!}</p>
</div>

<!-- Vur Field -->
<div class="form-group">
    {!! Form::label('vur', 'Vur:') !!}
    <p>{!! $ordenTrabajoConteo->vur !!}</p>
</div>

<!-- Da Field -->
<div class="form-group">
    {!! Form::label('da', 'Da:') !!}
    <p>{!! $ordenTrabajoConteo->da !!}</p>
</div>

<!-- Vut Field -->
<div class="form-group">
    {!! Form::label('vut', 'Vut:') !!}
    <p>{!! $ordenTrabajoConteo->vut !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $ordenTrabajoConteo->blame_id !!}</p>
</div>

