@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Proposito Avaluo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($propositoAvaluo, ['route' => ['propositoAvaluos.update', $propositoAvaluo->id], 'method' => 'patch']) !!}

                        @include('proposito_avaluos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection