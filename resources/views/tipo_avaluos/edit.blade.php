@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tipo Avaluos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoAvaluos, ['route' => ['tipoAvaluos.update', $tipoAvaluos->id], 'method' => 'patch']) !!}

                        @include('tipo_avaluos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection