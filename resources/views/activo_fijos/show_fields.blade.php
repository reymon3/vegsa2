<!-- Rubro Id Field -->

<div class="form-group">
    {!! Form::label('rubro_id', 'Rubro:') !!}
    <p>{!! $activoFijo->rubro->nombre !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $activoFijo->nombre !!}</p>
</div>

<!-- Vut Field -->
<div class="form-group">
    {!! Form::label('vut', 'Vut:') !!}
    <p>{!! $activoFijo->vut !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $activoFijo->blame_id !!}</p>
</div>
