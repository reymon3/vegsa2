<!-- Rubro Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('rubro_id', 'Rubro:') !!}
    <?php $rubro = isset($rubros->rubro_id) ? $rubros->rubro_id : null; ?>
    {!!
        Form::select('rubro_id', $rubros, $rubro, [
            'class' => 'form-control',
            'placeholder' => 'Seleccione...'
        ])
    !!}
</div>
<div class="clearfix"></div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Vut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vut', 'VUT (Vida Util Total):') !!}
    {!! Form::number('vut', null, ['class' => 'form-control']) !!}
</div>

<!-- porcentaje_gasto_instalacion -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_gasto_instalacion', '% Gastos e instalación') !!}
    {!! Form::number('porcentaje_gasto_instalacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('activoFijos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
