@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Activo Fijo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($activoFijo, ['route' => ['activoFijos.update', $activoFijo->id], 'method' => 'patch']) !!}

                        @include('activo_fijos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection