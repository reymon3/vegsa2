<!-- Cotizacion Id Field -->
{!! Form::hidden('cotizacion_id', null) !!}
<!-- Fecha Orden Trabajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente', 'Cliente:') !!}
    {!! Form::text('cliente', $ordenTrabajo->cotizacion->cliente->nombre, [
        'class' => 'form-control',
        'disabled' => 'disabled'
    ]) !!}
</div>

<!-- Fecha Orden Trabajo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_orden_trabajo', 'Fecha Orden Trabajo:') !!}
    {!! Form::text('fecha_orden_trabajo', null, [
        'class' => 'form-control',
        'id'=>'fecha_orden_trabajo',
        'readonly'=>'readonly'
    ]) !!}
</div>

<!-- Num Orden Trabajo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('num_orden_trabajo', 'Num Orden Trabajo:') !!}
    {!! Form::hidden('num_orden_trabajo') !!}
    {!! Form::text('', $ordenTrabajo->num_orden_trabajo, [
        'class' => 'form-control',
        'disabled' => 'disabled',
    ]) !!}
</div>

<!-- Fecha Orden Trabajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente', 'Vendedor:') !!}
    {!! Form::text('cliente', $ordenTrabajo->cotizacion->vendedor->name, [
        'class' => 'form-control',
        'disabled' => 'disabled'
    ]) !!}
</div>


<!-- Fecha Arranque Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_arranque', 'Fecha Arranque:') !!}
    {!! Form::text('fecha_arranque', null, [
        'class' => 'form-control',
        'id'=>'fecha_arranque',
        'readonly'=>'readonly'
    ]) !!}
</div>

<!-- Fecha Estimada Entrega Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_estimada_entrega', 'Fecha Estimada Entrega:') !!}
    {!! Form::text('fecha_estimada_entrega', null, [
        'class' => 'form-control',
        'id'=>'fecha_estimada_entrega',
        'readonly'=>'readonly'
    ]) !!}
</div>

<!-- Localidades Field -->
<div class="form-group col-sm-6">
    {!! Form::label('localidades', 'Localidades:') !!}
    {!! Form::text('localidades', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-6">
    {!! Form::label('num_factura', 'Número de factura:') !!}
    {!! Form::text('num_factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('reporte_fotografico', 'El reporte fotografico será:') !!}
    {!! Form::text('reporte_fotografico', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('factura_revisada', 'Las facturas serán revisadas:') !!}
    {!! Form::text('factura_revisada', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('valor_agregado', 'El valor agregado de este proyecto es:') !!}
    {!! Form::text('valor_agregado', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajos.index') !!}" class="btn btn-default">Cancelar</a>
</div>

@section('scripts')
<script>
    $(document).ready(function(){
        // https://bootstrap-datepicker.readthedocs.io/en/stable/options.html#format
        $('#fecha_orden_trabajo').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        $('#fecha_arranque').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true

        });
        $('#fecha_estimada_entrega').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        //     $('#fecha_orden_trabajo').datepicker("setDate", new Date('2019-11-12'));
    });
</script>
@append
