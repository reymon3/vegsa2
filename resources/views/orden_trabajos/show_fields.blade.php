<!-- Cotizacion Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente', 'Cliente:') !!}
    <p>{!! $ordenTrabajo->cotizacion->cliente->nombre !!}</p>
</div>

<!-- Fecha Orden Trabajo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_orden_trabajo', 'Fecha Orden Trabajo:') !!}
    <p>{!! $ordenTrabajo->fecha_orden_trabajo !!}</p>
</div>

<!-- Num Orden Trabajo Field -->
<div class="form-group col-sm-3">
    {!! Form::label('num_orden_trabajo', 'Num Orden Trabajo:') !!}
    <p>{!! $ordenTrabajo->num_orden_trabajo !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vendedor', 'Vendedor:') !!}
    <p>{!! $ordenTrabajo->cotizacion->vendedor->name !!}</p>
</div>

<!-- Fecha Arranque Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_arranque', 'Fecha Arranque:') !!}
    <p>{!! $ordenTrabajo->fecha_arranque !!}</p>
</div>

<!-- Fecha Estimada Entrega Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_estimada_entrega', 'Fecha Estimada Entrega:') !!}
    <p>{!! $ordenTrabajo->fecha_estimada_entrega !!}</p>
</div>
<div class="clearfix"></div>
<!-- Localidades Field -->
<div class="form-group  col-sm-6">
    {!! Form::label('localidades', 'Localidades:') !!}
    <p>{!! $ordenTrabajo->localidades !!}</p>
</div>

<!--  -->
<div class="form-group  col-sm-6">
    {!! Form::label('num_factura', 'Número de factura:') !!}
    <p>{!! $ordenTrabajo->num_factura !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $ordenTrabajo->observaciones !!}</p>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('reporte_fotografico', 'El reporte fotografico será:') !!}
    <p>{!! $ordenTrabajo->reporte_fotografico !!}</p>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('factura_revisada', 'Las facturas serán revisadas:') !!}
    <p>{!! $ordenTrabajo->factura_revisada !!}</p>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('valor_agregado', 'El valor agregado de este proyecto es:') !!}
    <p>{!! $ordenTrabajo->valor_agregado !!}</p>
</div>
<div class="clearfix"></div>
