@extends('layouts.app')
@include('partials.custom_datatable.datatable')
@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo
        </h1>
    </section>
    <div class="content">
        @if ( isset(session('flash_notification')[0]))
            @php
                $message = session('flash_notification')[0]
            @endphp
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="alert alert-success" role="alert">
                        {!! $message['message'] !!}
                    </div>
                </div>
            </div>
        @endif

        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('orden_trabajos.show_fields')
                    <a href="{!! route('ordenTrabajos.index') !!}" class="btn btn-default">Regresar</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.razon-social')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.servicios')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.normas')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.propositos')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.rubros')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.tipos-plaqueo')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.tipos-avaluo')
        <div class="clearfix"></div>
        @include('orden_trabajos.detalles.documentacion')
        <div class="clearfix"></div>
    </div>
@endsection
