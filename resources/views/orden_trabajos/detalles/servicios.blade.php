<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Servicio que requiere
        </h4>
        <h4 class="pull-right">
            <a
            onclick="fancyboxOpenIframe('{!! route('ordenTrabajoServicios.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"
            class="btn btn-primary"
            style="display:none"
            >Agregar</a>
        </h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $contador = 0 ?>
                        @foreach ($servicios as $registro)
                        <?php
                        if(is_null($registro->servicio)){
                            break;
                        }
                        $contador++
                        ?>
                            <tr>
                                <td>
                                    {{ $contador}}
                                </td>
                                <td>{{$registro->servicio->nombre}}</td>
                                <td>{{ $registro->servicio->descripcion}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
