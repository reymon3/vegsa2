@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Asignación de proyectos
        </h1>
    </section>
    <div class="content">

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('orden_trabajos.show_fields')

                    <a
                    <a href="javascript:;"
                    onclick="fancyboxOpenIframe('{!! route('ordenTrabajoEmpleados.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"

                    class="btn btn-primary">Agregar</a>
                    <a href="{!! url('asignacion-proyecto') !!}" class="btn btn-default">Regresar</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!-- Cotizacion Id Field -->
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table  class="table dataTable table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Rol</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $contador = 0 ?>

                        @foreach ($ordenTrabajoEmpleado as $registro)

                        <?php $contador++; ?>
                        <tr>

                            <td>
                                {{ $contador}}
                            </td>
                            <td>
                                {{ $registro->empleados->nombre }}
                            </td>
                            <td>
                                {{ $registro->empleados->correo }}
                            </td>
                            <td>
                                {{ $registro->roles->name }}
                            </td>
                            <td class="text-center">
                                {!! Form::open(['route' => ['ordenTrabajoEmpleados.destroy', $registro->id], 'method' => 'delete']) !!}
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'onclick' => "return confirm('¿Está seguro?')"
                                    ]) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
