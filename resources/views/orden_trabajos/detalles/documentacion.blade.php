<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Documentación
        </h4>
        <h4 class="pull-right">

            <a href="javascript:;"
            onclick="fancyboxOpenIframe('{!! route('ordenTrabajoDocumentos.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"

            class="btn btn-primary">Agregar</a>
        </h4>
          <div class="row">
              <div class="col-md-12">
                  <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Nombre</th>
                              <th></th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php $contador = 0 ?>
                          @foreach ($documentos as $registro)

                          <?php
                          // dd($documentos);
                          // if(isset($registro->nombre)){
                          //     break;
                          // }
                          $contador++
                          ?>
                              <tr>
                                  <td>
                                      {{ $contador}}
                                  </td>
                                  <td>{{$registro->nombre}}</td>
                                  <td class="text-center">{!! getIcon($registro->extension)  !!}</td>
                                  <td class="text-center" style="vertical-align: middle;">

                                      {!! Form::open(['route' => ['ordenTrabajoDocumentos.destroy', $registro->id], 'method' => 'delete']) !!}

                                      <a
                                          href='{!! asset("$registro->ruta")!!}'
                                          target="_blank"
                                          class='btn btn-primary btn-xs '
                                      >
                                          <i class="fa fa-download"></i>
                                      </a>

                                          {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                              'type' => 'submit',
                                              'class' => 'btn btn-danger btn-xs',
                                              'onclick' => "return confirm('¿Está seguro?')"
                                          ]) !!}
                                      {!! Form::close() !!}
                                  </td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
