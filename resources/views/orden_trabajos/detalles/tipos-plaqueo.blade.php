<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Tipo de plaqueo
        </h4>
        <h4 class="pull-right">


            <a href="javascript:;"
            onclick="fancyboxOpenIframe('{!! route('ordenTrabajoTipoPlaqueos.create') !!}?pop=1&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"

            class="btn btn-primary">Agregar</a>
        </h4>
        <div class="row">
            <div class="col-md-12">
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Observaciones</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $contador = 0 ?>
                        @foreach ($tiposPlaqueo as $registro)

                        <?php
                        // dd($registro->TipoPlaqueo);
                        if(is_null($registro->TipoPlaqueo)){
                            break;
                        }
                        $contador++
                        ?>
                            <tr>
                                <td>
                                    {{ $contador}}
                                </td>
                                <td>{{$registro->TipoPlaqueo->nombre}}</td>
                                <td>{{ $registro->observaciones}}</td>
                                <td class="text-center">
                                    {!! Form::open(['route' => ['ordenTrabajoTipoPlaqueos.destroy', $registro->id], 'method' => 'delete']) !!}
                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'onclick' => "return confirm('¿Está seguro?')"
                                        ]) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
