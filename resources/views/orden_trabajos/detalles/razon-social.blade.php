<div class="box box-primary">
    <div class="box-body">
        <h4 class="pull-left">
            Razones Sociales
        </h4>
        <h4 class="pull-right">
            <a href="javascript:;"
            onclick="fancyboxOpenIframe('{!! route('ordenTrabajoRazonSocials.create') !!}?pop=1&cliente_id={{ $ordenTrabajo->cotizacion->cliente_id}}&orden_trabajo_id={{ $ordenTrabajo->id}}', 300, 550)"
            class="btn btn-primary">Agregar</a>
        </h4>
        <div class="row">
            <div class="col-md-12">
                <table id="tbl_razon_social" class="table table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>RFC</th>
                            <th>Razon Social</th>
                            <th>Domicilio</th>
                            <th>Nombre Contacto</th>
                            <th>Puesto</th>
                            <th>Teléfono</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $contador = 0 ?>
                    @foreach ($ordenTrabajo->razonSocial as $registro)
                    <?php
                    // dd($registro);

                    if(is_null($registro->clienteRazonSocial)){
                        break;
                    }
                    $contador++
                    ?>
                        <tr>
                            <td>
                                {{ $contador}}
                            </td>
                            <td>{{$registro->clienteRazonSocial->nombre}}</td>
                            <td>{{ $registro->clienteRazonSocial->razon_social}}</td>
                            <td>{{ $registro->clienteRazonSocial->rfc }}</td>
                            <td>{{ $registro->clienteRazonSocial->domicilio}} L</td>
                            <td>{{ $registro->clienteRazonSocial->nombre_contacto}}</td>
                            <td>
                                 {{ $registro->clienteRazonSocial->puesto_contacto}}
                            </td>
                            <td>
                                {{ $registro->clienteRazonSocial->telefono_contacto }}
                            </td>
                            <td>
                                {{ $registro->clienteRazonSocial->email_contacto }}
                            </td>

                            <td>
                                {!! Form::open(['route' => ['ordenTrabajoRazonSocials.destroy', $registro->id], 'method' => 'delete']) !!}
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'onclick' => "return confirm('¿Está seguro?')"
                                    ]) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
