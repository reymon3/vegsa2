@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>Asignación de proyectos</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table  class="table dataTable table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>

                            <th>Orden Trabajo</th>
                            <th>Cliente</th>
                            <th>Fecha arranque</th>
                            <th>Fecha entrega</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $contador = 0 ?>
                        @foreach ($ordenesTrabajo as $registro)

                        <?php $contador++; ?>
                        <tr>

                            <td>
                                {{ $registro->num_orden_trabajo}}
                            </td>
                            <td>
                                {{ $registro->cotizacion->cliente->nombre }}
                            </td>
                            <td class="text-right">
                                {{ $registro->fecha_arranque }}
                            </td>
                            <td class="text-right">
                                {{ $registro->fecha_estimada_entrega }}
                            </td>

                            <td class="text-center">

                                <a href="{!! url('asignacion-proyecto-detalle', ['id' => $registro->id]) !!}"

                                    class="btn btn-default btn-xs">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection
