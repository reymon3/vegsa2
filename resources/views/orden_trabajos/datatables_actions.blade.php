
<div class='btn-group'>

    <a href="{!! url('actualiza-asignacion-empleado', ['id' => $id]) !!}"
        class="btn btn-default btn-xs {{ ($asigna_empleado) ? 'disabled' : '' }}"
        onclick="return confirm('¿Está seguro?')"
    >
        <i class="fa fa-exchange"></i>
    </a>
{!! Form::open(['route' => ['ordenTrabajos.destroy', $id], 'method' => 'delete']) !!}
    <a href="{{ route('ordenTrabajos.show', $id) }}" class='btn btn-default btn-xs '>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('ordenTrabajos.edit', $id) }}" class='btn btn-default btn-xs '>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('¿Está Seguro?')"
    ]) !!}
{!! Form::close() !!}
</div>
