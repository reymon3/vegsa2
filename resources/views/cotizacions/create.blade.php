@extends('layouts.app')
@include('partials.select2')
@section('content')
    <section class="content-header">
        <h1>
            Cotización
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'cotizacions.store']) !!}

                        @include('cotizacions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script>

    $('#cliente_id').select2({
        ajax: {
            dataType: 'json',
            url: '{!! url("getClientes") !!}',
            delay: 400,
            data: function(params) {
                return {
                    term: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    }).on('select2:select', function(e){
        console.log('select');
        // console.log($('#cliente_id').val());
        var selected_element = $(e.currentTarget);
        var select_val = selected_element.val();
        console.log(selected_element);
        console.log(select_val);
        loadInput(select_val);

    });

    function loadInput(id){

        $.ajax({
            type: "GET",
            url: '{{ url("getCliente") }}/' + id,
            dataType: 'json',
            timeout:45000,
            beforeSend: function(){
                console.log('Buscar cliente ' + id);
            }
        })
        .done(function (data) {

            noRegistros = Object.keys(data).length;
            console.log(data);
            console.log(noRegistros);

            if(noRegistros == 0){
                alert('Error al cargar datos de cliente.');
                return;
            }

            $('#nombre_contacto').val('');
            $('#puesto_contacto').val('');
            $('#telefono_contacto').val('');
            $('#celular_contacto').val('');
            $('#correo_contacto').val('');

            $('#nombre_contacto').val(data.nombre_contacto);
            $('#puesto_contacto').val(data.puesto_contacto);
            $('#telefono_contacto').val(data.telefono_contacto);
            $('#celular_contacto').val(data.telefono_celular);
            $('#correo_contacto').val(data.correo_contacto);

        })
        .fail( function(jqXHR, textStatus, errorThrown) {
            //
        });

    }

</script>

@append
