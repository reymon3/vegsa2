@php
    $disabled = getAction() == 'edit' ? 'true' : 'false'
@endphp

<!-- Cliente Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente_id', 'Cliente:') !!}
    @if (getAction() != 'edit')
        {!! Form::select('cliente_id', [], null, [
        'class' => 'form-control',
        'id'=>'cliente_id',
        'placeholder' => 'Seleccione..'
        ])!!}
    @else
        {!! Form::hidden('cliente_id', null)!!}
        {!! Form::text('cliente[nombre]', null, [
            'class' => 'form-control',
            'disabled' => $disabled
        ])!!}
    @endif

</div>
<!-- Fecha Cotizacion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_cotizacion_disabled', 'Fecha Cotización:') !!}
    {!! Form::hidden('fecha_cotizacion', date('Y-m-d')) !!}
    {!! Form::text('fecha_cotizacion_disabled', date('d/m/Y'), [
        'class' => 'form-control',
        'disabled' => $disabled
    ]) !!}
</div>
<!-- Num Cotizacion Field -->
<div class="form-group col-sm-3">
    <?php $folio = isset($cotizacion->num_cotizacion) ? $cotizacion->num_cotizacion : $folio; ?>
    {!! Form::hidden('num_cotizacion', $folio) !!}
    {!! Form::label('num_cotizacion_disabled', 'Num Cotización:') !!}
    {!! Form::text('num_cotizacion_disabled', $folio, [
        'class' => 'form-control',
        'disabled' => $disabled
    ])!!}
</div>

<!--  -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    {!! Form::text('nombre_contacto', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-6">
    {!! Form::label('puesto_contacto', 'Puesto:') !!}
    {!! Form::text('puesto_contacto', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('telefono_contacto', 'Teléfono Oficina:') !!}
    {!! Form::text('telefono_contacto', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('celular_contacto', 'Teléfono Celular:') !!}
    {!! Form::text('celular_contacto', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('correo_contacto', 'Correo Electrónico:') !!}
    {!! Form::text('correo_contacto', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('forma_pago', 'Forma de Pago:') !!}
    {!! Form::text('forma_pago', null, ['class' => 'form-control']) !!}
</div>

<!--  -->
<div class="form-group col-sm-12">
    {!! Form::label('condiciones_pago', 'Condiciones de pago:') !!}
    {!! Form::textarea('condiciones_pago', null, ['class' => 'form-control', 'rows' => 10, 'cols' => 1]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cotizacions.index') !!}" class="btn btn-default">Cancelar</a>
</div>
