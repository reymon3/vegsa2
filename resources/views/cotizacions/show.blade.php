@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Cotizacion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">

                <div class="row" style="padding-left: 20px">
                    @include('cotizacions.show_fields')
                </div>
                <div class="row" style="padding-left: 20px">
                    <div class="box-body table-responsive">
                        <a href="javascript:;"
                        onclick="fancyboxOpenIframe('{!! route('cotizacionServicios.create') !!}?pop=1&cotizacion_id={!! $cotizacion->id !!}', 350, 550)"
                        class="btn btn-primary {{ ($cotizacion->bloqueado) ? 'disabled' : '' }}">
                            Agregar
                        </a>
                        <?php if(count($servicios) > 0){ ?>
                        <a href="{!! url('genera-orden', ['id' => $cotizacion->id]) !!}"
                            class="btn btn-info {{ ($cotizacion->bloqueado) ? 'disabled' : '' }}"

                        >
                            Generar Orden
                        </a>
                    <?php } else {?>
                        <a href="#"
                            class="btn btn-info disabled"
                        >
                            Generar Orden
                        </a>

                    <?php } ?>
                        <a href="{!! route('cotizacions.index') !!}" class="btn btn-default">Regresar</a>
                        <br />
                        <br />
                        <table  class="table table-hover table-bordered"  style="width:100%">
                            <thead>
                                <tr>
                                    <th  class="text-center">#</th>
                                    <th  class="text-center">Concepto</th>
                                    <th  class="text-center">Costo</th>
                                    <th  class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 0 ?>
                            @foreach ($servicios as $servicio)
                                <?php $contador++ ?>
                                <tr>
                                    <td class="text-center">{{ $contador }}</td>
                                    <td>
                                        {{ $servicio->servicio->nombre }}
                                    </td>
                                    <td  class="text-right">
                                        ${{ number_format($servicio->importe, 2, '.', ',') }}
                                        {!! Form::hidden('importe_'.$contador,  $servicio->importe, ['id' => 'importe_'.$contador])!!}
                                    </td>
                                    <td  class="text-center">
                                        {!! Form::open(['route' => ['cotizacionServicios.destroy', $servicio->id], 'method' => 'delete']) !!}
                                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'onclick' => "return confirm('¿Está seguro?')"
                                            ]) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-right"></td>
                                    <th class="text-right" >Subtotal</th>
                                    <td class="text-right" id="subtotal">$0.00</td>
                                    <td class="text-right"></td>
                                </tr>
                                <tr>
                                    <td class="text-right"></td>
                                    <th class="text-right" >IVA</th>
                                    <td class="text-right" id="iva">$0.00</td>
                                    <td class="text-right"></td>
                                </tr>
                                <tr>
                                    <td class="text-right"></td>
                                    <th class="text-right" >Total</th>
                                    <td class="text-right" id="total">$0.00</td>
                                    <td class="text-right"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
<script>

    loadImporte();

    function loadImporte(){
        var iva = 0.16;
        var importe = 0;
        $.each($(" [id^='importe_']"), function(index) {
            importe += parseInt($(this).val())
            console.log($(this).val());
            // noAdultos  += parseInt($(this).val())
            // adultos.push((parseInt(index) + 1 )+ '_' + $(this).val())
        })
        $('#importe').text(currencyFormat(importe) );
        $('#subtotal').text(currencyFormat(importe) );
        $('#iva').text(currencyFormat(importe * iva) );
        $('#total').text(currencyFormat(importe + (importe * iva)) );
        $('#ingreso_factura').val(importe);

    }

</script>
@append
