
<div class='btn-group'>

<a href="{!! url('genera-orden', ['id' => $id]) !!}"
    class="btn btn-default btn-xs {{ ($bloqueado) ? 'disabled' : '' }}"
    onclick="return confirm('¿Está seguro?, esta acción deshabilita los opciones editar y eliminar.')"
>
    <i class="fa fa-exchange"></i>
</a>

{!! Form::open(['route' => ['cotizacions.destroy', $id], 'method' => 'delete']) !!}
    <a href="{{ route('cotizacions.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('cotizacions.edit', $id) }}" class='btn btn-default btn-xs {{$bloqueado ? 'disabled' : '' }}'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <?php
    if ($bloqueado) {
        echo Form::button('<i class="glyphicon glyphicon-trash"></i>', [
            'type' => 'button',
            'class' => 'btn btn-danger btn-xs ' . ($bloqueado ? 'disabled' : '')
        ]);
    } else {
        echo Form::button('<i class="glyphicon glyphicon-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs ' . ($bloqueado ? 'disabled' : ''),
            'onclick' => "return confirm('¿Está seguro?')",
        ]);
    }
    ?>
{!! Form::close() !!}
</div>
