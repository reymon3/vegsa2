<!-- Cliente Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cliente_id', 'Cliente:') !!}
    <p>{!! $cotizacion->cliente->nombre !!}</p>
</div>

<!-- Fecha Cotizacion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('fecha_cotizacion', 'Fecha Cotización:') !!}
    <p>{!! $cotizacion->fecha_cotizacion !!}</p>
</div>

<!-- Num Cotizacion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('num_cotizacion', 'Num Cotización:') !!}
    <p>{!! $cotizacion->num_cotizacion !!}</p>
</div>

<!--  -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    <p>{!! $cotizacion->nombre_contacto !!}</p>
</div>
<!--  -->
<div class="form-group col-sm-6">
    {!! Form::label('puesto_contacto', 'Puesto Contacto:') !!}
    <p>{!! $cotizacion->puesto_contacto !!}</p>
</div>
<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('telefono_contacto', 'Teléfono Contacto:') !!}
    <p>{!! $cotizacion->telefono_contacto !!}</p>
</div>
<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('telefono_celular', 'Teléfono Celular:') !!}
    <p>{!! $cotizacion->telefono_celular !!}</p>
</div>
<!--  -->
<div class="form-group col-sm-4">
    {!! Form::label('correo_contacto', 'Correo Contacto:') !!}
    <p>{!! $cotizacion->correo_contacto !!}</p>
</div>

<!-- Forma Pago Field -->
<div class="form-group col-sm-4">
    {!! Form::label('forma_pago', 'Forma Pago:') !!}
    <p id='forma_pago'>{!! $cotizacion->forma_pago !!}</p>
</div>

<!-- Ingreso Factura Field -->
<div class="form-group col-sm-offset-4  col-sm-4">
    {!! Form::label('ingreso_factura', 'Importe antes de IVA:') !!}
    <p id="importe">{!! $cotizacion->ingreso_factura !!}</p>
    {!! Form::hidden('ingreso_factura',  $cotizacion->ingreso_factura, ['id' => 'ingreso_factura'])!!}
</div>
<!--  -->
<div class="form-group col-sm-12">
    {!! Form::label('condiciones_pago', 'Condiciones de pago:') !!}
    <p>{!! nl2br($cotizacion->condiciones_pago) !!}</p>
</div>
