@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>Armado - Conciliación</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table  class="table dataTable table-hover table-bordered"  style="width:100%">
                    <thead>
                        <tr>
                            <th>Orden Trabajo</th>
                            <th>Cliente</th>
                            <th>Fecha arranque</th>
                            <th>Fecha entrega</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                0000006/2019
                            </td>
                            <td>
                                NOVARTIS
                            </td>
                            <td class="text-right">
                                18/01/2020
                            </td>
                            <td class="text-right">
                                18/01/2020
                            </td>
                            <td class="text-center">
                                <a href="http://localhost/vegsa/public/uploads/facturas-conciliacion/factura-00001.pdf"
                                    class="btn btn-default">
                                    <i class="fa fa-download"></i>
                                </a>
                                <a href="{!! url('factura-armado-conciliacion') !!}"
                                    class="btn btn-default ">
                                    <i class="fa fa-cloud-upload"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
