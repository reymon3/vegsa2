@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Conciliacion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('conciliacions.show_fields')
                    <a href="{!! url('descarga-conciliacion') !!}" class="btn btn-primary">Descargar</a>
                    <a href="{!! route('conciliacions.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('conciliacions.detalle')
            </div>
        </div>
    </div>
@endsection
