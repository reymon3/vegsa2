<h3>Caracteristicas Urbanas</h3>
<div class="container">
<form action="{{route('civil.store')}}" method="post">
@csrf
<input type="hidden" name="ordenTrabajo" value="{{ $id_orden_trabajo }}">
        <div class="row">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="clasificacionzona">Clasificación de la Zona:</label>
                    <select class="form-control" id="clasificacionzona" name="clasificacionzona">
                        @foreach($cat_clasificacionzona as $clasificacionzona)
                            <option value="{{ $clasificacionzona->id }}">{{$clasificacionzona->clasificacion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="construcciondominante">Tipo de construccion Dominante:</label>
                    <select class="form-control" id="construcciondominante" name="construcciondominante">
                        @foreach($cat_tipoconstrucciondominante as $constdominante)
                            <option value="{{ $constdominante->id }}">{{$constdominante->tipo}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="indicesaturacion">Indice de Saturacion de la Zona:</label>
                    <input class="form-control" type="number" min="1" max="100" id="indicesaturacion" name="indicesaturacion" required>
                </div>
            </div>
        </div>

        <div class="row" id="poblacion">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="poblacion">Poblacion:</label>
                    <select class="form-control" id="poblacion" name="poblacion">
                        @foreach($cat_poblacion as $poblacion)
                            <option value="{{$poblacion->id}}">{{$poblacion->poblacion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="row" id="usopredio">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="usopredio">Uso de Predio:</label>
                    <select class="form-control" id="usopredio" name="usopredio">
                        @foreach($cat_usopredio as $predio)
                            <option value="{{$predio->id}}">{{$predio->uso}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="row" id="contaminacionambiental">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="contaminacionambiental">Contaminacion Ambiental:</label>
                    <select class="form-control" id="contaminacionambiental" name="contaminacionambiental">
                        @foreach($cat_contaminacionambiental as $contaminacion)
                            <option value="{{$contaminacion->id}}">{{$contaminacion->tipo}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="row" id="principalesviasacceso">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="principalesviasacceso">Uso Principales Vias de Acceso:</label>
                    <textarea class="form-control" rows="5" id="principalesviasacceso" name="principalesviasacceso"></textarea>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="row" id="serviciosmunicipales">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="serviciosmunicipales">Servicios Municipales y Equipamiento Urbano:</label>
                    <select class="form-control" id="serviciosmunicipales" name="serviciosmunicipales">
                        @foreach($cat_serviciosmunicipales as $serviciomunicipal)
                            <option value="{{$serviciomunicipal->id}}">{{$serviciomunicipal->tipo}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                <p>otro</p>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <br>
            <br>
            <button type="submit" name="operacion" value="carUrbanas" class="btn btn-info btn-block">Guardar</button>
        </div>
    </form>
</div>

