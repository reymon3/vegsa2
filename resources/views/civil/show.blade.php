@extends('layouts.app')
<!-- include('partials.custom_datatable.datatable') -->
@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo
        </h1>
    </section>
    <div class="content">
     
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <a href="{!! route('civil.index') !!}" class="btn btn-default">Regresar</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--

          -->
        <div class="container" id="app">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Antecedentes</a></li>
                <li><a data-toggle="tab" href="#menu1">Car. Urbanas</a></li>
                <li><a data-toggle="tab" href="#menu2">Terreno</a></li>
                <li><a data-toggle="tab" href="#menu3">Desc. Gra. del Predio</a></li>
                <li><a data-toggle="tab" href="#menu4">Elem. de Construcción</a></li>
                <li><a data-toggle="tab" href="#menu5">Consideraciones Previas</a></li>
                <li><a data-toggle="tab" href="#menu6">Est. de Mercado</a></li>
                <li><a data-toggle="tab" href="#menu7">Metodo de Costo Dir.</a></li>
                <li><a data-toggle="tab" href="#menu8">Metodo de Mercado</a></li>
                <li><a data-toggle="tab" href="#menu9">Valor Fisico/Directo</a></li>
                <li><a data-toggle="tab" href="#menu10">Resum. de Valores</a></li>
                <li><a data-toggle="tab" href="#menu11">Consid. Previas</a></li>
                <li><a data-toggle="tab" href="#menu12">Conclusiones</a></li>
                <li><a data-toggle="tab" href="#menu13">Anexos</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    @include('civil.antecedentes')
                </div>
                <div id="menu1" class="tab-pane fade">
                
                    @include('civil.caracteristicasUrbanas')
                    
                </div>
                <div id="menu2" class="tab-pane fade">
                    @include('civil.antecedentes')
                    <!-- include('civil.terreno') -->
                </div>
                <div id="menu3" class="tab-pane fade">
                    @include('civil.descripciongralpredio')
                </div>
                <div id="menu4" class="tab-pane fade">
                    <h3>Elementos de Construccion</h3>
                </div>
                <div id="menu5" class="tab-pane fade">
                    @include('civil.Consideracionesprevias')
                </div>
                <div id="menu6" class="tab-pane fade">
                    @include('civil.Estudiomercado')
                </div>
                <div id="menu7" class="tab-pane fade">
                    <h3>Metodo costo directo</h3>
                </div>
                <div id="menu8" class="tab-pane fade">
                    <h3>Metodo de Mercado</h3>
                </div>
                <div id="menu9" class="tab-pane fade">
                    <h3>Valor Fisico Directo</h3>
                </div>
                <div id="menu10" class="tab-pane fade">
                    <h3>Resumen de Valores</h3>
                </div>
                <div id="menu11" class="tab-pane fade">
                    <h3>Consideraciones Previas </h3>
                </div>
                <div id="menu12" class="tab-pane fade">
                    <h3>Conclusiones</h3>
                </div>
                <div id="menu13" class="tab-pane fade">
                    <h3>Anexos</h3>
                </div>
            </div>
        </div>
          <!--  -->
    </div>
@endsection
