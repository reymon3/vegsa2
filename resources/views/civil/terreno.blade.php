<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</html>
<h3>Terreno</h3>
<div class="container">

<div class="row" id="tramodecalles">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="tramodecalles">TRAMO DE CALLES TRANSVERSALES LIMITROFES Y ORIENTACIÓIN:</label>
                    <textarea class="form-control" rows="5" id="tramodecalles" name="tramodecalles"></textarea>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>

        <div class="row" id="superficies">
            <div class="form-group">
                <div class="col-md-10">
                    <label for="superficies">SUPERFICIE MEDIDAS Y COLINDAMCIAS SEGÚN:</label>
                    <textarea class="form-control" rows="10" id="superficies" name="superficies"> LIBRO 1441, INSTRUMENTO 70, 070, EN LA CIUDAD DE MÉXICO A 13 DE JULIODEL 2020, ANTE EL LIC.
                        FRANCISCO DANIEL SÁNCHEZ DOMÍNGUEZ. NOTARIO PÚBLICO NÚMERO 117, HAC CONSTAR EL CONTRATO
                        DE COMPRAVENTA QUE CELEBRAN, POR UNA PARTE Y COMO VENDEDORA "VALCA", SOCIEDAD ANÓNIMA DE
                        CAPITAL VARIABLE REPRESENTADA POR SU ADMINISTRADOR ÚNICO EL SEÑOR CARLOS ANTONIA VALES
                        OCHOA Y POR OTRA PARTE COMO COMPRADORA WYETH S.A DE C.V REPRESENTADA POR SU APODERADO
                        EL SEÑOR CONTADOR PÚBLICO ALEJANDRO VIDAL MIYAMOTO.
                    </textarea>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>

        <!-- CREACIÓN DE PREDIO -->
            <!-- This is the firts form-->
            <h1> Pedido Fraccion </h1>
            <form action="" method="post">                                
                                <table id="pedido" class="table table-striped" type="form">
                                    <thead>
                                        <tr>
                                            <th> Pedido x / Fraccion  </th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td colspan="3"> </td>
                                            <td>
                                                <button type="button" class="btn btn-success button_agregar_pedido"> +</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                
                                                <!--Pedidos x-->
                                                            <!-- This is the second form-->
            <form action="" method="post">
                <table id="lista_productos" class="table table-striped">
                    <thead>
                        <tr>
                            <th> LIMITE </th>
                            <th> MEDIDA (METROS) </th>
                            <th> LIMITANTE </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                            <select type="select" name="limte[]" class="limite" placeholder="Nombre" >
                                <optgroup label="Limite">
                                    <option value="norte">NORTE</option>
                                    <option value="suroeste">SUROESTE</option>
                                    <option value="atras">ATRAS</option>
                                    <option value="suroriente">SURORIENTE</option>
                                    <option value="suroriente">SURORIENTE</option>
                                </optgroup>
                            </select>
                            </td>
                            <td>
                                <input type="number" name="medida[]" class="mdedida" placeholder="Caracteristicas" />
                            </td>
                            <td>
                                <input type="number" name="limitante[]" class="text" placeholder="Cantidad" />
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger button_eliminar_producto"> X
                                </button>
                            </td>
                        </tr>
                    </tbody>
                    <td>
                            <label for="Subtotal">Subtotal:</label>
                            <p>Superficie</p><input type="text" class="form-control" id="Superficie" name="cpinmueble"><p>m2</p>
                            </td>
                    <tfoot>
                    <tr>
                        <td>
                                <button type="button" class="btn btn-success button_agregar_producto"> + Agregar colindancia</button>
                            </td>
                        </tr>
                        </tfoot>
                </table>
            </form>
            <!--End second form-->
                                                <!--Fin Pedidos x-->
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-danger button_eliminar_pedido"> Eliminar
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
            </form>

        <div class="col-md-12">
            <label for="superficietotal">Superficie Total:</label>
            <input type="text" class="form-control" id="suoperficietotal" name="cpinmueble">
        </div>

        <div class="col-md-12">
        <label for="superficietotal">Tipografia y Configuración:</label><br>
        <select type="select" name="" class="limite" placeholder="Escoge una opción" >
                <option value="norte">POLIGONO REGULAR SIN PROMOTORIOS EN LA SUPERFICIE DEL PREDIO.</option>
                <option value="suroeste">POLIGONO IRREGULER SIN PROMOTORIOS EN LA SUOPERFICIE DEL PREDIO.</option>
                <option value="atras">POLIGONO REGULAR CON PEQUEÑOS PROMONTORIOS EN LA SUPERFICIE DEL PREDIO.</option>
                <option value="suroriente">POLIGONO IRREGULAR CON PEQUEÑOS PROMONTORIOS EN LA SUPERFICIE DEL PREDIO</option>
                <option value="otro">OTRO</option>
        </select>
        </div>

        <div class="col-md-12">
        <label for="caracteristicaspanoramicas">Cracterísticas panoramicas:</label><br>
        <select type="select" name="" class="limite" placeholder="Escoge una opción" >
                <option value="norte">AVENIDAS Y CALLES DE LA ZONA CON MEDIANA Y ALTA AFLUENCIA VEHICULAR.</option>
                <option value="suroeste">CALLES DE BAJA AFLUENCIA VEHICULAR.</option>
                <option value="atras">LOTES BALDIOS.</option>
                <option value="suroriente">LOTES AGRICOLAS.</option>
                <option value="otro">OTRO.</option>
        </select>
        </div>

        <div class="col-md-12">
        <label for="caracteristicaspanoramicas">Densidad habitacional permimtida:</label><br>
        <select type="select" name="" class="limite" placeholder="Escoge una opción" >
                <option value="norte">DE ACUERDO AL REGLAMETO DE CONTRUCCIÓN DEL DEPARTAMENTO DE LA CIUDAD DE MÉXICO.</option>
                <option value="suroeste">DE ACUERDO AL REGLAMETO DE CONTRUCCIÓN DE LA IDENTIDAD CORRESPONDIENTE.</option>
                <option value="atras">NO EXISTE.</option>
                <option value="otro">OTRO.</option>
        </select>
        </div>

        <div class="col-md-12">
        <label for="caracteristicaspanoramicas">Intensidad de contrucción permitida:</label><br>
        <select type="select" name="" class="limite" placeholder="Escoge una opción" >
                <option value="norte">DE ACUERDO AL REGLAMETO DE CONTRUCCIÓN DEL DEPARTAMENTO DE LA CIUDAD DE MÉXICO.</option>
                <option value="suroeste">DE ACUERDO AL REGLAMETO DE CONTRUCCIÓN DE LA IDENTIDAD CORRESPONDIENTE.</option>
                <option value="atras">NO EXISTE.</option>
                <option value="otro">OTRO.</option>
        </select>
        </div>

        <div class="row">
    <div class="col-md-12">
    <br>
    <br>
            <button type="button" class="btn btn-info">GUARDADR</button>
            <button type="submit" class="btn btn-default">LIMPIAR FORMULARIO</button>
    </div>
</div>
        <script type="text/javascript">
            $(document).ready(function () {
                productos();
                function productos(){
   var tbody = $('#lista_productos tbody'); 
   var fila_contenido = tbody.find('tr').first().html();
   //Agregar fila nueva. 
   $('#lista_productos .button_agregar_producto').click(function(){ 
      var fila_nueva = $('<tr></tr>');
      fila_nueva.append(fila_contenido); 
      tbody.append(fila_nueva); 
   }); 
   //Eliminar fila. 
   $('#lista_productos').on('click', '.button_eliminar_producto', function(){
      $(this).parents('tr').eq(0).remove();
   });
///////////////////////////////////////////////////
   var oi=0;  //Objeto indicie
   var thisObj;
   var objs = document.getElementById("bloqueID").getElementsByTagName("input");
   for (oi=0;oi<objs.length;oi++) {  
       thisObj = objs[oi];  
       if(thisObj.getAttribute('type') == 'hidden'){ 
           alert(thisObj.value);
       }
   }
}
            }); 
            //Formulario pedido
            $(document).ready(function () {
                pedido();
                function pedido(){
   var tbody = $('#pedido tbody'); 
   var fila_contenido = tbody.find('tr').first().html();
   //Agregar fila nueva. 
   $('#pedido .button_agregar_pedido').click(function(){ 
      var fila_nueva = $('<tr></tr>');
      fila_nueva.append(fila_contenido); 
      tbody.append(fila_nueva); 
   }); 
   //Eliminar fila. 
   $('#pedido').on('click', '.button_eliminar_pedido', function(){
      $(this).parents('tr').eq(0).remove();
   });
///////////////////////////////////////////////////
   var oi=0;  //Objeto indicie
   var thisObj;
   var objs = document.getElementById("bloqueID").getElementsByTagName("input");
   for (oi=0;oi<objs.length;oi++) {  
       thisObj = objs[oi];  
       if(thisObj.getAttribute('type') == 'hidden'){ 
           alert(thisObj.value);
       }
   }
}
            }); 
        </script>