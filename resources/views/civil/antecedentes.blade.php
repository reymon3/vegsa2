<h3>Antecedentes</h3>
<div class="container">
<form action="{{route('civil.store')}}" method="post">
@csrf
<input type="hidden" name="ordenTrabajo" value="{{ $id_orden_trabajo }}">
<label for="solicitante">Solicitante</label>
<div class="radio">
  <label>
    <input type="radio" name="optradio1" value="solicitantefisico" onChange="solicitante(this.value);" required autofocus>Persona Fisica
  </label>
  <label>
    <input type="radio" name="optradio1" value="solicitantemoral" onChange="solicitante(this.value);">Persona Moral
  </label>
</div>

<!-- PERSONA FISICA-->
<div id="solicitantefisico" style="display: none;">
    <div class="form-group">
        <div class="col-md-6">
            <label for="nombresolicitante">Primer Nombre: *</label>
            <input type="text" class="form-control" id="nombresolicitante" name="nombresolicitante">
        </div>
        <div class="col-md-6">
            <label for="segundonombresolicitante">Segundo Nombre:</label>
            <input type="text" class="form-control" id="segundonombresolicitante" name="segundonombresolicitante">
        </div>
        <div class="col-md-6">
            <label for="apasolicitante">Apellido Paterno: * </label>
            <input type="text" class="form-control" id="apasolicitante" name="apasolicitante">
        </div>
        <div class="col-md-6">
            <label for="amasolicitante">Apellido Materno:</label>
            <input type="text" class="form-control" id="amasolicitante" name="amasolicitante">
        </div>
    </div>
</div>
<!-- PERSONA MORAL -->
<div id="solicitantemoral" style="display: none;">
    <div class="form-group">
        <div class="col-md-6">
            <label for="nombrerazonsocialsolicitante">Nombre(s): *</label>
            <input type="text" class="form-control" id="nombrerazonsocialsolicitante" name="nombrerazonsocialsolicitante">
        </div>
        <div class="col-md-6">
            <label for="razonsocialsolicitante">Razon Social:</label>
            <select class="form-control" id="razonsocialsolicitante" name="razonsocialsolicitante">
                @foreach($cat_razonSocial as $razon)
                    <option value="{{ $razon->id }}">{{$razon->descripcion}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<!-- -->
<div class="row p-5">
    <h5>Domicilio del Solicitante</h5>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <label for="calle">Calle</label>
            <input type="text" class="form-control" id="calle" name="calle">
        </div>
        <div class="col-md-2">
            <label for="numero">Numero:</label>
            <input type="text" class="form-control" id="numero" name="numero">
        </div>
        <div class="col-md-4">
            <label for="colonia">Colonia:</label>
            <input type="text" class="form-control" id="colonia" name="colonia">
        </div>
        <div class="col-md-2">
            <label for="cp">CP:</label>
            <input type="text" class="form-control" id="cp" name="cp">
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-3">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" id="estado" name="estado">
        </div>
        <div class="col-md-3">
            <label for="numero">Alcaldia:</label>
            <input type="text" class="form-control" id="alcaldia" name="alcaldia">
        </div>
        <div class="col-md-3">
            <label for="capital">Capital:</label>
            <input type="text" class="form-control" id="capital" name="capital">
        </div>
        <div class="col-md-2">
            <label for="pais">Pais:</label>
            <select class="form-control" id="pais" name="pais">
                @foreach($cat_paises as $pais)
                    <option value="{{ $pais->id }}">{{$pais->nombre}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1 align-left">
            <br>
        </div>  
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-2">
            <label for="fecha">Fecha de Avaluo:</label>
            <input type="date" class="form-control" id="fecha" name="fecha">
        </div>
        <div class="col-md-6">
            <label for="perito">Perito Valuador:</label>
            <select class="form-control" id="perito" name="perito">
                @foreach($cat_perito as $perito)
                    <option value="{{ $perito->id }}">{{$perito->perito}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1 align-left">
            <br>
        </div>  
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-8">
            <label for="inmueble">Inmueble Valuado:</label>
            <select class="form-control" id="inmueble" name="inmueble">
                @foreach($cat_inmuebles as $inmueble)
                    <option value="{{ $inmueble->id }}">{{$inmueble->nombre_inmueble}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-1 align-left">
        <br>
    </div>  
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-8">
            <label for="regimenpromiedad">Regimen de Propiedad:</label>
            <select class="form-control" id="regimenpromiedad" name="regimenpromiedad">
                @foreach($cat_regimenPropiedad as $regimen)
                    <option value="{{ $regimen->id }}">{{$regimen->regimen}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1 align-left">
            <br>
        </div>  
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<label for="propietario">Propietario del Inmueble</label>
<div class="radio">
  <label>
    <input type="radio" name="optradio" value="propietariofisico" onChange="propietario(this.value);" required autofocus>Persona Fisica
  </label>
  <label>
    <input type="radio" name="optradio" value="propietariomoral" onChange="propietario(this.value);">Persona Moral
  </label>
</div>
<!-- PERSONA FISICA-->
<div id="propietariofisico" style="display: none;">
    <div class="form-group">
        <div class="col-md-6">
            <label for="nombrepropietario">Primer Nombre: *</label>
            <input type="text" class="form-control" id="nombrepropietario" name="nombrepropietario">
        </div>
        <div class="col-md-6">
            <label for="segundonombrepropietario">Segundo Nombre:</label>
            <input type="text" class="form-control" id="segundonombrepropietario" name="segundonombrepropietario">
        </div>
        <div class="col-md-6">
            <label for="apapropietario">Apellido Paterno: * </label>
            <input type="text" class="form-control" id="apapropietario" name="apapropietario">
        </div>
        <div class="col-md-6">
            <label for="amapropietario">Apellido Materno:</label>
            <input type="text" class="form-control" id="amapropietario" name="amapropietario">
        </div>
    </div>
</div>
<!-- PERSONA MORAL -->
<div id="propietariomoral" style="display: none;">
    <div class="form-group">
        <div class="col-md-6">
            <label for="nombrerazonsocialpropietario">Nombre(s): *</label>
            <input type="text" class="form-control" id="nombrerazonsocialpropietario" name="nombrerazonsocialpropietario">
        </div>
        <div class="col-md-6">
            <label for="razonsocialpropietario">Razon Social:</label>
            <select class="form-control" id="razonsocialpropietario" name="razonsocialpropietario">
                @foreach($cat_razonSocial as $razon)
                    <option value="{{ $razon->id }}">{{$razon->descripcion}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<!-- -->
<div class="row">
    <div class="form-group">
        <div class="col-md-8">
            <label for="destino">Destino del Avaluo:</label>
            <select class="form-control" id="destino" name="destino">
                @foreach($cat_destinoAvaluo as $destino)
                    <option value="{{ $destino->id }}">{{$destino->destino}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1 align-left">
            <br>
        </div>  
    </div>
</div>
<div class="row">
    <h5>Ubicacion del Inmueble</h5>
    <div class="form-group">
        <div class="col-md-4">
            <label for="calleinmueble">Calle</label>
            <input type="text" class="form-control" id="calleinmueble" name="calleinmueble">
        </div>
        <div class="col-md-2">
            <label for="numeroinmueble">Numero:</label>
            <input type="text" class="form-control" id="numeroinmueble" name="numeroinmueble">
        </div>
        <div class="col-md-4">
            <label for="coloniainmueble">Colonia:</label>
            <input type="text" class="form-control" id="coloniainmueble" name="coloniainmueble">
        </div>
        <div class="col-md-2">
            <label for="cpinmueble">CP:</label>
            <input type="text" class="form-control" id="cpinmueble" name="cpinmueble">
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-3">
            <label for="estadoinmueble">Estado</label>
            <input type="text" class="form-control" id="estadoinmueble" name="estadoinmueble">
        </div>
        <div class="col-md-3">
            <label for="alcaldiainmueble">Alcaldia:</label>
            <input type="text" class="form-control" id="alcaldiainmueble" name="alcaldiainmueble">
        </div>
        <div class="col-md-3">
            <label for="capitalinmueble">Capital:</label>
            <input type="text" class="form-control" id="capitalinmueble" name="capitalinmueble">
        </div>
        <div class="col-md-3">
            <label for="paisinmueble">Pais:</label>
            <select class="form-control" id="paisinmueble" name="paisinmueble">
                @foreach($cat_paises as $pais)
                    <option value="{{ $pais->id }}">{{$pais->nombre}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <label for="clavecatastral">Clave Catastral:</label>
        <input type="text" class="form-control" id="clavecatastral" name="clavecatastral">
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <label for="contratoagua">Contrato de Agua:</label>
        <input type="text" class="form-control" id="contratoagua" name="contratoagua">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <br>
    <br>
    <button type="submit" name="operacion" value="antecedentes" class="btn btn-info btn-block">Guardar</button>
        <!-- <input type="submit" class="btn btn-info m-5" value="Submit Button"> -->
        <!-- <button type="button" class="btn btn-info" value="antecedentes" name="guardad" id="guardar">Info</button> -->
        <!-- <button type="submit" class="btn btn-default">Guardar</button> -->
    </div>
</div>
</form>
</div>

<script type="text/javascript">
function solicitante(id1) {
    if (id1 == "solicitantefisico") {
        $("#solicitantefisico").show();
        $("#solicitantemoral").hide();
    }
    if (id1 == "solicitantemoral") {
        $("#solicitantefisico").hide();
        $("#solicitantemoral").show();
    }
}
function propietario(id) {
    if (id == "propietariofisico") {
        $("#propietariofisico").show();
        $("#propietariomoral").hide();
    }
    if (id == "propietariomoral") {
        $("#propietariofisico").hide();
        $("#propietariomoral").show();
    }
}
</script>