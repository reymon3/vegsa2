
<div class='btn-group'>

    <a href="{!! route('civil.show', ['id' => $id]) !!}"
        class="btn btn-default btn-xs {{ ($asigna_empleado) ? 'disabled' : '' }}"
    >
        <i class="fa fa-bars"></i>
    </a>

</div>
