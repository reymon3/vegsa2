@extends('layouts.app')

@section('content')
<h3>Consideraciones Previas</h3>
<div class="container">
    <form action="{{route('civil.store')}}" method="post">
    @csrf
    <label for="solicitante">Solicitante</label>
    <div class="radio">
    <label>
        <input type="radio" name="optradio" value="fisico" onChange="mostrar(this.value);">Metodo Fisico
    </label>
    <label>
        <input type="radio" name="optradio" value="mercado" onChange="mostrar(this.value);">Metodo Mercado / Capitalizacion de Rentas
    </label>
    </div>
       
        <div class="row">
            <div class="col-md-12">
            <br>
            <br>
            <button type="submit" name="operacion" value="consideracionesPrevias" class="btn btn-info btn-block">Guardar</button>
            </div>
        </div>
    </form>
</div>

@endsection