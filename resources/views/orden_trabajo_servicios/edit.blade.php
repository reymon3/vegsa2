@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Servicio
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoServicio, ['route' => ['ordenTrabajoServicios.update', $ordenTrabajoServicio->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_servicios.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection