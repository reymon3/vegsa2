<!-- Orden Trabajo Id Field -->
{!! Form::text('orden_trabajo_id',  app('request')->input('orden_trabajo_id'))!!}

<!-- Servicio Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('servicio_id', 'Servicio:') !!}
    {!! Form::select('servicio_id', $servicios, null, [
        'class' => 'form-control',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoServicios.index') !!}" class="btn btn-default">Cancelar</a>
</div>
