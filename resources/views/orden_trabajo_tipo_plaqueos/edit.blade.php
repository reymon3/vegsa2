@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Tipo Plaqueo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoTipoPlaqueo, ['route' => ['ordenTrabajoTipoPlaqueos.update', $ordenTrabajoTipoPlaqueo->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_tipo_plaqueos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection