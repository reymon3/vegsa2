<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}

<div class="form-group col-sm-6">
    {!! Form::label('tipo_plaqueo_id', 'Tipo de plaqueo:') !!}
    {!! Form::select('tipo_plaqueo_id', $tiposPlaqueo, null, [
        'class' => 'form-control',
        'id'=>'tipo_plaqueo_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Descripción Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoTipoPlaqueos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
