<!-- Avaluo Id Field -->
<div class="form-group">
    {!! Form::label('avaluo_id', 'Avaluo Id:') !!}
    <p>{!! $civilDetalle->avaluo_id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $civilDetalle->nombre !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $civilDetalle->descripcion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $civilDetalle->blame_id !!}</p>
</div>

