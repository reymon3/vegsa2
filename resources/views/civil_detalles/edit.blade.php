@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Civil Detalle
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($civilDetalle, ['route' => ['civilDetalles.update', $civilDetalle->id], 'method' => 'patch']) !!}

                        @include('civil_detalles.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection