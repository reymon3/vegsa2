<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $tipoCambio->nombre !!}</p>
</div>

<!-- Descripción Field -->
<div class="form-group">
    {!! Form::label('Descripción', 'Descripción:') !!}
    <p>{!! $tipoCambio->descripcion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $tipoCambio->blame_id !!}</p>
</div>

