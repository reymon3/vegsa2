<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $tipoPlaqueo->nombre !!}</p>
</div>

<!-- Descripción Field -->
<div class="form-group">
    {!! Form::label('Descripción', 'Descripción:') !!}
    <p>{!! $tipoPlaqueo->descripcion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $tipoPlaqueo->blame_id !!}</p>
</div>

