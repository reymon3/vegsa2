@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Tipo Plaqueo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoPlaqueo, ['route' => ['tipoPlaqueos.update', $tipoPlaqueo->id], 'method' => 'patch']) !!}

                        @include('tipo_plaqueos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection