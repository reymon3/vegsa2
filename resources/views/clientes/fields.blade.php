<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Razón Social:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>


<!-- Descripción Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Descripción', 'Descripción:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('rfc', 'RFC:') !!}
    {!! Form::text('rfc', null, ['class' => 'form-control']) !!}
</div>
<!-- Domicilio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('domicilio', 'Domicilio:') !!}
    {!! Form::text('domicilio', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    {!! Form::text('nombre_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Puesto Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('puesto_contacto', 'Puesto Contacto:') !!}
    {!! Form::text('puesto_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    {!! Form::text('celular_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Teléfono Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_contacto', 'Teléfono Contacto:') !!}
    {!! Form::text('telefono_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo_contacto', 'Correo Contacto:') !!}
    {!! Form::text('correo_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clientes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
