@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cliente
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('clientes.show_fields')
                </div>
                <div class="row" style="padding-left: 20px">

                    <a href="javascript:;"
                    onclick="fancyboxOpenIframe('{!! route('clienteRazonSocials.create') !!}?pop=1&cliente_id={!! $cliente->id !!}', 650, 550)"
                    class="btn btn-primary">Agregar</a>
                    <a href="{!! route('clientes.index') !!}" class="btn btn-default">Regresar</a>
                </div>
                <div class="row"    >
                    <div class="box-body table-responsive">
                        <table  class="table dataTable table-hover table-bordered"  style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Razón Social</th>
                                    <th>RFC</th>
                                    <th>Domicilio</th>
                                    <th>Nombre Contacto</th>
                                    <th>Puesto Contacto</th>
                                    <th>Celular Contacto</th>
                                    <th>Teléfono Contacto</th>
                                    <th>Correo Contacto</th>
                                    <th>Fecha Alta</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($razonesSociales as $registro)
                                <tr>
                                    <td>{{$registro->nombre}}</td>
                                    <td >{{ $registro->razon_social}}</td>
                                    <td>{{ $registro->rfc }}</td>
                                    <td>{{ $registro->domicilio}} L</td>
                                    <td>{{ $registro->nombre_contacto}}</td>
                                    <td>
                                         {{ $registro->puesto_contacto}}
                                    </td>
                                    <td>
                                        {{ $registro->celular_contacto }}
                                    </td>
                                    <td>
                                        {{ $registro->telefono_contacto }}
                                    </td>
                                    <td>
                                        {{ $registro->email_contacto }}
                                    </td>
                                    <td>
                                        <time
                                            title=" {{
                                                $registro->created_at->formatLocalized('%A, %d de %B del %Y')
                                                . ' - '
                                                . $registro->created_at->format('h:i A')
                                            }}"
                                            tabindex="0"
                                        >
                                            {{ $registro->created_at->format('d/m/Y') }}
                                        </time>
                                    </td>
                                    <td>
                                        <a
                                            href="javascript:;"
                                            onclick="fancyboxOpenIframe('{{ route('clienteRazonSocials.edit', $registro->id) }}?pop=1&cliente_id={!! $cliente->id !!}', 650, 550)"

                                            class='btn btn-default btn-xs'>
                                            <i class="glyphicon glyphicon-edit"></i>
                                        </a>

                                        {!! Form::open(['route' => ['clienteRazonSocials.destroy', $registro->id], 'method' => 'delete']) !!}
                                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'onclick' => "return confirm('¿Está seguro?')"
                                            ]) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="padding-left: 20px">
                    <br />

                </div>

            </div>
        </div>
    </div>
@endsection
