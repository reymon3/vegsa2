<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Razón Social:') !!}
    <p>{!! $cliente->nombre !!}</p>
</div>

<!-- Descripción Field -->
<div class="form-group">
    {!! Form::label('Descripción', 'Descripción:') !!}
    <p>{!! $cliente->descripcion !!}</p>
</div>

<div class="form-group">
    {!! Form::label('RFC', 'RFC:') !!}
    <p>{!! $cliente->rfc !!}</p>
</div>

<!-- Domicilio Field -->
<div class="form-group">
    {!! Form::label('domicilio', 'Domicilio:') !!}
    <p>{!! $cliente->domicilio !!}</p>
</div>

<!-- Nombre Contacto Field -->
<div class="form-group">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    <p>{!! $cliente->nombre_contacto !!}</p>
</div>

<!-- Puesto Contacto Field -->
<div class="form-group">
    {!! Form::label('puesto_contacto', 'Puesto Contacto:') !!}
    <p>{!! $cliente->puesto_contacto !!}</p>
</div>

<!-- Celular Contacto Field -->
<div class="form-group">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    <p>{!! $cliente->celular_contacto !!}</p>
</div>

<!-- Teléfono Contacto Field -->
<div class="form-group">
    {!! Form::label('telefono_contacto', 'Teléfono Contacto:') !!}
    <p>{!! $cliente->telefono_contacto !!}</p>
</div>

<!-- Correo Contacto Field -->
<div class="form-group">
    {!! Form::label('correo_contacto', 'Correo Contacto:') !!}
    <p>{!! $cliente->correo_contacto !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Usuario Alta:') !!}
    <p>{!! $cliente->blame->name !!}</p>
</div>
