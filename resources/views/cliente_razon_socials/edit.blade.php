@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cliente Razon Social
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($clienteRazonSocial, ['route' => ['clienteRazonSocials.update', $clienteRazonSocial->id], 'method' => 'patch']) !!}

                        @include('cliente_razon_socials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection