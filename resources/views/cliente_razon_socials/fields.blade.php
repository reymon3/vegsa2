{!! Form::hidden('cliente_id',  app('request')->input('cliente_id'))!!}

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razon_social', 'Razón Social:') !!}
    {!! Form::text('razon_social', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rfc', 'RFC:') !!}
    {!! Form::text('rfc', null, ['class' => 'form-control']) !!}
</div>


<!-- Domicilio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('domicilio', 'Domicilio:') !!}
    {!! Form::text('domicilio', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    {!! Form::text('nombre_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Puesto Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('puesto_contacto', 'Puesto Contacto:') !!}
    {!! Form::text('puesto_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    {!! Form::text('celular_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Teléfono Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono_contacto', 'Teléfono Contacto:') !!}
    {!! Form::text('telefono_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_contacto', 'Email Contacto:') !!}
    {!! Form::text('email_contacto', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clienteRazonSocials.index') !!}"
    onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar</a>
</div>
