@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cliente Razon Social
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cliente_razon_socials.show_fields')
                    <a href="{!! route('clienteRazonSocials.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
