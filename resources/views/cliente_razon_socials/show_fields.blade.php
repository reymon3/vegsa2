<!-- Cliente Id Field -->
<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente Id:') !!}
    <p>{!! $clienteRazonSocial->cliente_id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $clienteRazonSocial->nombre !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{!! $clienteRazonSocial->rfc !!}</p>
</div>

<!-- Razon Social Field -->
<div class="form-group">
    {!! Form::label('razon_social', 'Razon Social:') !!}
    <p>{!! $clienteRazonSocial->razon_social !!}</p>
</div>

<!-- Domicilio Field -->
<div class="form-group">
    {!! Form::label('domicilio', 'Domicilio:') !!}
    <p>{!! $clienteRazonSocial->domicilio !!}</p>
</div>

<!-- Nombre Contacto Field -->
<div class="form-group">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    <p>{!! $clienteRazonSocial->nombre_contacto !!}</p>
</div>

<!-- Puesto Contacto Field -->
<div class="form-group">
    {!! Form::label('puesto_contacto', 'Puesto Contacto:') !!}
    <p>{!! $clienteRazonSocial->puesto_contacto !!}</p>
</div>

<!-- Celular Contacto Field -->
<div class="form-group">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    <p>{!! $clienteRazonSocial->celular_contacto !!}</p>
</div>

<!-- Teléfono Contacto Field -->
<div class="form-group">
    {!! Form::label('telefono_contacto', 'Teléfono Contacto:') !!}
    <p>{!! $clienteRazonSocial->telefono_contacto !!}</p>
</div>

<!-- Email Contacto Field -->
<div class="form-group">
    {!! Form::label('email_contacto', 'Email Contacto:') !!}
    <p>{!! $clienteRazonSocial->email_contacto !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Usuario Alta:') !!}
    <p>{!! $clienteRazonSocial->blame->name !!}</p>
</div>
