<!-- Avaluo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('avaluo_id', 'Avaluo Id:') !!}
    {!! Form::number('avaluo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Asset Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset', 'Asset:') !!}
    {!! Form::text('asset', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro', 'Rubro:') !!}
    {!! Form::text('rubro', null, ['class' => 'form-control']) !!}
</div>

<!-- Desglose Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desglose', 'Desglose:') !!}
    {!! Form::text('desglose', null, ['class' => 'form-control']) !!}
</div>

<!-- Asset Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset_descripcion', 'Asset Descripcion:') !!}
    {!! Form::text('asset_descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Acquis Val Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acquis_val', 'Acquis Val:') !!}
    {!! Form::number('acquis_val', null, ['class' => 'form-control']) !!}
</div>

<!-- Accum Dep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('accum_dep', 'Accum Dep:') !!}
    {!! Form::number('accum_dep', null, ['class' => 'form-control']) !!}
</div>

<!-- Book Val Field -->
<div class="form-group col-sm-6">
    {!! Form::label('book_val', 'Book Val:') !!}
    {!! Form::number('book_val', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Anterior Homogenea Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_anterior_homogenea', 'Placa Anterior Homogenea:') !!}
    {!! Form::text('placa_anterior_homogenea', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Anterior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_anterior', 'Placa Anterior:') !!}
    {!! Form::text('placa_anterior', null, ['class' => 'form-control']) !!}
</div>

<!-- Edificio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('edificio', 'Edificio:') !!}
    {!! Form::text('edificio', null, ['class' => 'form-control']) !!}
</div>

<!-- Piso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('piso', 'Piso:') !!}
    {!! Form::text('piso', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Vegsa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro_vegsa', 'Rubro Vegsa:') !!}
    {!! Form::text('rubro_vegsa', null, ['class' => 'form-control']) !!}
</div>

<!-- Catalogo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    {!! Form::text('catalogo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Vegsa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion_vegsa', 'Descripcion Vegsa:') !!}
    {!! Form::text('descripcion_vegsa', null, ['class' => 'form-control']) !!}
</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Localidad Field -->
<div class="form-group col-sm-6">
    {!! Form::label('localidad', 'Localidad:') !!}
    {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Vrn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vrn', 'Vrn:') !!}
    {!! Form::number('vrn', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('porcentaje_dep', 0) !!}
        {!! Form::checkbox('porcentaje_dep', '1', null) !!}
    </label>
</div>


<!-- Dep Acum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    {!! Form::number('dep_acum', null, ['class' => 'form-control']) !!}
</div>

<!-- Vnr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vnr', 'Vnr:') !!}
    {!! Form::number('vnr', null, ['class' => 'form-control']) !!}
</div>

<!-- Vur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vur', 'Vur:') !!}
    {!! Form::number('vur', null, ['class' => 'form-control']) !!}
</div>

<!-- Da Field -->
<div class="form-group col-sm-6">
    {!! Form::label('da', 'Da:') !!}
    {!! Form::number('da', null, ['class' => 'form-control']) !!}
</div>

<!-- Vut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vut', 'Vut:') !!}
    {!! Form::number('vut', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('avaluoDetalles.index') !!}" class="btn btn-default">Cancel</a>
</div>
