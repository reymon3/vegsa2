<!-- Avaluo Id Field -->
<div class="form-group">
    {!! Form::label('avaluo_id', 'Avaluo Id:') !!}
    <p>{!! $avaluoDetalle->avaluo_id !!}</p>
</div>

<!-- Asset Field -->
<div class="form-group">
    {!! Form::label('asset', 'Asset:') !!}
    <p>{!! $avaluoDetalle->asset !!}</p>
</div>

<!-- Rubro Field -->
<div class="form-group">
    {!! Form::label('rubro', 'Rubro:') !!}
    <p>{!! $avaluoDetalle->rubro !!}</p>
</div>

<!-- Desglose Field -->
<div class="form-group">
    {!! Form::label('desglose', 'Desglose:') !!}
    <p>{!! $avaluoDetalle->desglose !!}</p>
</div>

<!-- Asset Descripcion Field -->
<div class="form-group">
    {!! Form::label('asset_descripcion', 'Asset Descripcion:') !!}
    <p>{!! $avaluoDetalle->asset_descripcion !!}</p>
</div>

<!-- Acquis Val Field -->
<div class="form-group">
    {!! Form::label('acquis_val', 'Acquis Val:') !!}
    <p>{!! $avaluoDetalle->acquis_val !!}</p>
</div>

<!-- Accum Dep Field -->
<div class="form-group">
    {!! Form::label('accum_dep', 'Accum Dep:') !!}
    <p>{!! $avaluoDetalle->accum_dep !!}</p>
</div>

<!-- Book Val Field -->
<div class="form-group">
    {!! Form::label('book_val', 'Book Val:') !!}
    <p>{!! $avaluoDetalle->book_val !!}</p>
</div>

<!-- Placa Anterior Homogenea Field -->
<div class="form-group">
    {!! Form::label('placa_anterior_homogenea', 'Placa Anterior Homogenea:') !!}
    <p>{!! $avaluoDetalle->placa_anterior_homogenea !!}</p>
</div>

<!-- Placa Anterior Field -->
<div class="form-group">
    {!! Form::label('placa_anterior', 'Placa Anterior:') !!}
    <p>{!! $avaluoDetalle->placa_anterior !!}</p>
</div>

<!-- Edificio Field -->
<div class="form-group">
    {!! Form::label('edificio', 'Edificio:') !!}
    <p>{!! $avaluoDetalle->edificio !!}</p>
</div>

<!-- Piso Field -->
<div class="form-group">
    {!! Form::label('piso', 'Piso:') !!}
    <p>{!! $avaluoDetalle->piso !!}</p>
</div>

<!-- Rubro Vegsa Field -->
<div class="form-group">
    {!! Form::label('rubro_vegsa', 'Rubro Vegsa:') !!}
    <p>{!! $avaluoDetalle->rubro_vegsa !!}</p>
</div>

<!-- Catalogo Field -->
<div class="form-group">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    <p>{!! $avaluoDetalle->catalogo !!}</p>
</div>

<!-- Descripcion Vegsa Field -->
<div class="form-group">
    {!! Form::label('descripcion_vegsa', 'Descripcion Vegsa:') !!}
    <p>{!! $avaluoDetalle->descripcion_vegsa !!}</p>
</div>

<!-- Marca Field -->
<div class="form-group">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $avaluoDetalle->marca !!}</p>
</div>

<!-- Modelo Field -->
<div class="form-group">
    {!! Form::label('modelo', 'Modelo:') !!}
    <p>{!! $avaluoDetalle->modelo !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $avaluoDetalle->serie !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $avaluoDetalle->area !!}</p>
</div>

<!-- Localidad Field -->
<div class="form-group">
    {!! Form::label('localidad', 'Localidad:') !!}
    <p>{!! $avaluoDetalle->localidad !!}</p>
</div>

<!-- Vrn Field -->
<div class="form-group">
    {!! Form::label('vrn', 'Vrn:') !!}
    <p>{!! $avaluoDetalle->vrn !!}</p>
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <p>{!! $avaluoDetalle->porcentaje_dep !!}</p>
</div>

<!-- Dep Acum Field -->
<div class="form-group">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    <p>{!! $avaluoDetalle->dep_acum !!}</p>
</div>

<!-- Vnr Field -->
<div class="form-group">
    {!! Form::label('vnr', 'Vnr:') !!}
    <p>{!! $avaluoDetalle->vnr !!}</p>
</div>

<!-- Vur Field -->
<div class="form-group">
    {!! Form::label('vur', 'Vur:') !!}
    <p>{!! $avaluoDetalle->vur !!}</p>
</div>

<!-- Da Field -->
<div class="form-group">
    {!! Form::label('da', 'Da:') !!}
    <p>{!! $avaluoDetalle->da !!}</p>
</div>

<!-- Vut Field -->
<div class="form-group">
    {!! Form::label('vut', 'Vut:') !!}
    <p>{!! $avaluoDetalle->vut !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $avaluoDetalle->blame_id !!}</p>
</div>

