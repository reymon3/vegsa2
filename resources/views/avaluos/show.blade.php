@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Avaluo
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('avaluos.show_fields')

                    <a href="{!! route('descargar-avaluo', ['id' => $avaluo->id]) !!}" class="btn btn-primary">Descargar</a>
                    <a href="{!! route('avaluos.index') !!}" class="btn btn-default">Regresar</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('avaluos.detalle')
            </div>
        </div>
    </div>
@endsection
