@extends('layouts.app')
@include('partials.custom_datatable.datatable')
@section('content')
    <section class="content-header">
        <h1>
            Avaluo <small>Características urbanas</small>
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <table
                    id="tbl_antecedentes"
                    data-order='[[ 0, "desc" ]]'
                    data-page-length='10'
                    class="table dataTable table-hover"
                    style="width:100%"
                >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Orden Trabajo</th>
                            <th>Nombre</th>
                            <th>Usuario Alta</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($caracteristicas as $registro)
                            <tr>
                                <td>{{ $registro->id }}</td>
                                <td>{{ $registro->ordenTrabajo->num_orden_trabajo }}</td>
                                <td class="text-center">{{ $registro->seccion->nombre }} </td>
                                <td class="text-center">{{ $registro->blame->name }} </td>
                                <td class="text-center">
                                    <a href="{{ route('avaluos.show', $registro->id) }}" class='btn btn-default btn-xs'>
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#tbl_antecedentes').DataTable({
            language: {
                url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                { "orderable": false },
            ]
        });
    });
</script>
@append
