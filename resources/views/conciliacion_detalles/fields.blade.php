<!-- Conciliacion Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('conciliacion_id', 'Conciliacion Id:') !!}
    {!! Form::number('conciliacion_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Folio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::text('folio', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Actual Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_actual', 'Placa Actual:') !!}
    {!! Form::text('placa_actual', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Anterior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_anterior', 'Placa Anterior:') !!}
    {!! Form::text('placa_anterior', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Homologada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_homologada', 'Placa Homologada:') !!}
    {!! Form::text('placa_homologada', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Auxiliar 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_auxiliar_1', 'Placa Auxiliar 1:') !!}
    {!! Form::text('placa_auxiliar_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Placa Auxiliar 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('placa_auxiliar_2', 'Placa Auxiliar 2:') !!}
    {!! Form::text('placa_auxiliar_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Desglose Field -->
<div class="form-group col-sm-6">
    {!! Form::label('desglose', 'Desglose:') !!}
    {!! Form::text('desglose', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro_id', 'Rubro Id:') !!}
    {!! Form::number('rubro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro', 'Rubro:') !!}
    {!! Form::text('rubro', null, ['class' => 'form-control']) !!}
</div>

<!-- Catalogo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    {!! Form::text('catalogo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control']) !!}
</div>

<!-- No Mantenimiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_mantenimiento', 'No Mantenimiento:') !!}
    {!! Form::text('no_mantenimiento', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Ubicacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ubicacion', 'Ubicacion:') !!}
    {!! Form::text('ubicacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Usuario Conteo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usuario_conteo', 'Usuario Conteo:') !!}
    {!! Form::text('usuario_conteo', null, ['class' => 'form-control']) !!}
</div>

<!-- Piso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('piso', 'Piso:') !!}
    {!! Form::text('piso', null, ['class' => 'form-control']) !!}
</div>

<!-- Planta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('planta', 'Planta:') !!}
    {!! Form::text('planta', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Depreciacion Fisica Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_depreciacion_fisica', 'Porcentaje Depreciacion Fisica:') !!}
    {!! Form::number('porcentaje_depreciacion_fisica', null, ['class' => 'form-control']) !!}
</div>

<!-- Anio Fabricacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio_fabricacion', 'Anio Fabricacion:') !!}
    {!! Form::text('anio_fabricacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto_1', 'Foto 1:') !!}
    {!! Form::text('foto_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto_2', 'Foto 2:') !!}
    {!! Form::text('foto_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('foto_3', 'Foto 3:') !!}
    {!! Form::text('foto_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Vrn Anterior Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vrn_anterior', 'Vrn Anterior:') !!}
    {!! Form::number('vrn_anterior', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Dep Ajuste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_dep_ajuste', 'Porcentaje Dep Ajuste:') !!}
    {!! Form::number('porcentaje_dep_ajuste', null, ['class' => 'form-control']) !!}
</div>

<!-- Dep Acum Ajuste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dep_acum_ajuste', 'Dep Acum Ajuste:') !!}
    {!! Form::number('dep_acum_ajuste', null, ['class' => 'form-control']) !!}
</div>

<!-- Vnr Ajuste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vnr_ajuste', 'Vnr Ajuste:') !!}
    {!! Form::number('vnr_ajuste', null, ['class' => 'form-control']) !!}
</div>

<!-- Vur Ajuste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vur_ajuste', 'Vur Ajuste:') !!}
    {!! Form::number('vur_ajuste', null, ['class' => 'form-control']) !!}
</div>

<!-- Da Ajuste Field -->
<div class="form-group col-sm-6">
    {!! Form::label('da_ajuste', 'Da Ajuste:') !!}
    {!! Form::number('da_ajuste', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Ajuste Inst Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_ajuste_inst', 'Porcentaje Ajuste Inst:') !!}
    {!! Form::number('porcentaje_ajuste_inst', null, ['class' => 'form-control']) !!}
</div>

<!-- Vrn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vrn', 'Vrn:') !!}
    {!! Form::number('vrn', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    {!! Form::number('porcentaje_dep', null, ['class' => 'form-control']) !!}
</div>

<!-- Dep Acum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    {!! Form::number('dep_acum', null, ['class' => 'form-control']) !!}
</div>

<!-- Vnr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vnr', 'Vnr:') !!}
    {!! Form::number('vnr', null, ['class' => 'form-control']) !!}
</div>

<!-- Vur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vur', 'Vur:') !!}
    {!! Form::number('vur', null, ['class' => 'form-control']) !!}
</div>

<!-- Da Field -->
<div class="form-group col-sm-6">
    {!! Form::label('da', 'Da:') !!}
    {!! Form::number('da', null, ['class' => 'form-control']) !!}
</div>

<!-- Vut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vut', 'Vut:') !!}
    {!! Form::number('vut', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('conciliacionDetalles.index') !!}" class="btn btn-default">Cancel</a>
</div>
