<!-- Conciliacion Id Field -->
<div class="form-group">
    {!! Form::label('conciliacion_id', 'Conciliacion Id:') !!}
    <p>{!! $conciliacionDetalle->conciliacion_id !!}</p>
</div>

<!-- Folio Field -->
<div class="form-group">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{!! $conciliacionDetalle->folio !!}</p>
</div>

<!-- Placa Actual Field -->
<div class="form-group">
    {!! Form::label('placa_actual', 'Placa Actual:') !!}
    <p>{!! $conciliacionDetalle->placa_actual !!}</p>
</div>

<!-- Placa Anterior Field -->
<div class="form-group">
    {!! Form::label('placa_anterior', 'Placa Anterior:') !!}
    <p>{!! $conciliacionDetalle->placa_anterior !!}</p>
</div>

<!-- Placa Homologada Field -->
<div class="form-group">
    {!! Form::label('placa_homologada', 'Placa Homologada:') !!}
    <p>{!! $conciliacionDetalle->placa_homologada !!}</p>
</div>

<!-- Placa Auxiliar 1 Field -->
<div class="form-group">
    {!! Form::label('placa_auxiliar_1', 'Placa Auxiliar 1:') !!}
    <p>{!! $conciliacionDetalle->placa_auxiliar_1 !!}</p>
</div>

<!-- Placa Auxiliar 2 Field -->
<div class="form-group">
    {!! Form::label('placa_auxiliar_2', 'Placa Auxiliar 2:') !!}
    <p>{!! $conciliacionDetalle->placa_auxiliar_2 !!}</p>
</div>

<!-- Desglose Field -->
<div class="form-group">
    {!! Form::label('desglose', 'Desglose:') !!}
    <p>{!! $conciliacionDetalle->desglose !!}</p>
</div>

<!-- Rubro Id Field -->
<div class="form-group">
    {!! Form::label('rubro_id', 'Rubro Id:') !!}
    <p>{!! $conciliacionDetalle->rubro_id !!}</p>
</div>

<!-- Rubro Field -->
<div class="form-group">
    {!! Form::label('rubro', 'Rubro:') !!}
    <p>{!! $conciliacionDetalle->rubro !!}</p>
</div>

<!-- Catalogo Field -->
<div class="form-group">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    <p>{!! $conciliacionDetalle->catalogo !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $conciliacionDetalle->descripcion !!}</p>
</div>

<!-- Marca Field -->
<div class="form-group">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $conciliacionDetalle->marca !!}</p>
</div>

<!-- Modelo Field -->
<div class="form-group">
    {!! Form::label('modelo', 'Modelo:') !!}
    <p>{!! $conciliacionDetalle->modelo !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $conciliacionDetalle->serie !!}</p>
</div>

<!-- No Mantenimiento Field -->
<div class="form-group">
    {!! Form::label('no_mantenimiento', 'No Mantenimiento:') !!}
    <p>{!! $conciliacionDetalle->no_mantenimiento !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $conciliacionDetalle->area !!}</p>
</div>

<!-- Ubicacion Field -->
<div class="form-group">
    {!! Form::label('ubicacion', 'Ubicacion:') !!}
    <p>{!! $conciliacionDetalle->ubicacion !!}</p>
</div>

<!-- Usuario Conteo Field -->
<div class="form-group">
    {!! Form::label('usuario_conteo', 'Usuario Conteo:') !!}
    <p>{!! $conciliacionDetalle->usuario_conteo !!}</p>
</div>

<!-- Piso Field -->
<div class="form-group">
    {!! Form::label('piso', 'Piso:') !!}
    <p>{!! $conciliacionDetalle->piso !!}</p>
</div>

<!-- Planta Field -->
<div class="form-group">
    {!! Form::label('planta', 'Planta:') !!}
    <p>{!! $conciliacionDetalle->planta !!}</p>
</div>

<!-- Porcentaje Depreciacion Fisica Field -->
<div class="form-group">
    {!! Form::label('porcentaje_depreciacion_fisica', 'Porcentaje Depreciacion Fisica:') !!}
    <p>{!! $conciliacionDetalle->porcentaje_depreciacion_fisica !!}</p>
</div>

<!-- Anio Fabricacion Field -->
<div class="form-group">
    {!! Form::label('anio_fabricacion', 'Anio Fabricacion:') !!}
    <p>{!! $conciliacionDetalle->anio_fabricacion !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $conciliacionDetalle->observaciones !!}</p>
</div>

<!-- Foto 1 Field -->
<div class="form-group">
    {!! Form::label('foto_1', 'Foto 1:') !!}
    <p>{!! $conciliacionDetalle->foto_1 !!}</p>
</div>

<!-- Foto 2 Field -->
<div class="form-group">
    {!! Form::label('foto_2', 'Foto 2:') !!}
    <p>{!! $conciliacionDetalle->foto_2 !!}</p>
</div>

<!-- Foto 3 Field -->
<div class="form-group">
    {!! Form::label('foto_3', 'Foto 3:') !!}
    <p>{!! $conciliacionDetalle->foto_3 !!}</p>
</div>

<!-- Vrn Anterior Field -->
<div class="form-group">
    {!! Form::label('vrn_anterior', 'Vrn Anterior:') !!}
    <p>{!! $conciliacionDetalle->vrn_anterior !!}</p>
</div>

<!-- Porcentaje Dep Ajuste Field -->
<div class="form-group">
    {!! Form::label('porcentaje_dep_ajuste', 'Porcentaje Dep Ajuste:') !!}
    <p>{!! $conciliacionDetalle->porcentaje_dep_ajuste !!}</p>
</div>

<!-- Dep Acum Ajuste Field -->
<div class="form-group">
    {!! Form::label('dep_acum_ajuste', 'Dep Acum Ajuste:') !!}
    <p>{!! $conciliacionDetalle->dep_acum_ajuste !!}</p>
</div>

<!-- Vnr Ajuste Field -->
<div class="form-group">
    {!! Form::label('vnr_ajuste', 'Vnr Ajuste:') !!}
    <p>{!! $conciliacionDetalle->vnr_ajuste !!}</p>
</div>

<!-- Vur Ajuste Field -->
<div class="form-group">
    {!! Form::label('vur_ajuste', 'Vur Ajuste:') !!}
    <p>{!! $conciliacionDetalle->vur_ajuste !!}</p>
</div>

<!-- Da Ajuste Field -->
<div class="form-group">
    {!! Form::label('da_ajuste', 'Da Ajuste:') !!}
    <p>{!! $conciliacionDetalle->da_ajuste !!}</p>
</div>

<!-- Porcentaje Ajuste Inst Field -->
<div class="form-group">
    {!! Form::label('porcentaje_ajuste_inst', 'Porcentaje Ajuste Inst:') !!}
    <p>{!! $conciliacionDetalle->porcentaje_ajuste_inst !!}</p>
</div>

<!-- Vrn Field -->
<div class="form-group">
    {!! Form::label('vrn', 'Vrn:') !!}
    <p>{!! $conciliacionDetalle->vrn !!}</p>
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <p>{!! $conciliacionDetalle->porcentaje_dep !!}</p>
</div>

<!-- Dep Acum Field -->
<div class="form-group">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    <p>{!! $conciliacionDetalle->dep_acum !!}</p>
</div>

<!-- Vnr Field -->
<div class="form-group">
    {!! Form::label('vnr', 'Vnr:') !!}
    <p>{!! $conciliacionDetalle->vnr !!}</p>
</div>

<!-- Vur Field -->
<div class="form-group">
    {!! Form::label('vur', 'Vur:') !!}
    <p>{!! $conciliacionDetalle->vur !!}</p>
</div>

<!-- Da Field -->
<div class="form-group">
    {!! Form::label('da', 'Da:') !!}
    <p>{!! $conciliacionDetalle->da !!}</p>
</div>

<!-- Vut Field -->
<div class="form-group">
    {!! Form::label('vut', 'Vut:') !!}
    <p>{!! $conciliacionDetalle->vut !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $conciliacionDetalle->blame_id !!}</p>
</div>

