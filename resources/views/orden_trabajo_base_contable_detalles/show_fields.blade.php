<!-- Orden Trabajo Base Contable Id Field -->
<div class="form-group">
    {!! Form::label('orden_trabajo_base_contable_id', 'Orden Trabajo Base Contable Id:') !!}
    <p>{!! $ordenTrabajoBaseContableDetalle->orden_trabajo_base_contable_id !!}</p>
</div>

<!-- Json Fila Field -->
<div class="form-group">
    {!! Form::label('json_fila', 'Json Fila:') !!}
    <p>{!! $ordenTrabajoBaseContableDetalle->json_fila !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $ordenTrabajoBaseContableDetalle->blame_id !!}</p>
</div>

