@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Base Contable Detalle
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ordenTrabajoBaseContableDetalles.store']) !!}

                        @include('orden_trabajo_base_contable_detalles.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
