<!-- Orden Trabajo Base Contable Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden_trabajo_base_contable_id', 'Orden Trabajo Base Contable Id:') !!}
    {!! Form::number('orden_trabajo_base_contable_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Json Fila Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('json_fila', 'Json Fila:') !!}
    {!! Form::textarea('json_fila', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoBaseContableDetalles.index') !!}" class="btn btn-default">Cancel</a>
</div>
