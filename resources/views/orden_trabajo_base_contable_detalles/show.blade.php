@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Base Contable Detalle
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('orden_trabajo_base_contable_detalles.show_fields')
                    <a href="{!! route('ordenTrabajoBaseContableDetalles.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
