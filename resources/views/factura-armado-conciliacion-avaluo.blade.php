@extends('layouts.app')

@section('content')

    @if ( isset(session('flash_notification')[0]))
        @php
            $message = session('flash_notification')[0]
        @endphp

        <div class="alta col-md-12">

            <!--COLLAPSER-->
            <div class="col-xs-12 col-md-12 padding-alta">
                <p class="text-alta centers margin-alta">
                    {!! $message['message'] !!}
                </p>
            </div>
            <br />
            <div class="form-group col-sm-12 col-md-12 col-lg-12 padding-tag margin-traspaso">
                <div class="col-xs-12 col-sm-12 col-md-12 margin-validar center-validar margin-10 centers">

                    <button type="button" onclick="closeFancyboxIframe();"
                    class="btn  btn-default" data-dismiss="modal" aria-label="Close">
                        Cerrar
                    </button>

                </div>
            </div>
            <!--COLLAPSER-->
        </div>

    @else
    <section class="content-header">

            <h1>Factura - Armado - Conciliacion y Avaluo</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')

        @if (\Session::has('errores'))
            <div class="alert alert-error">
                <p>Ocurrieron los siguientes errores.</p>
                <ul>
                    @foreach (\Session::get('errores') as $fila => $errores)
                    <br /><li>Fila: {!!$fila !!}
                        @foreach ($errores as $error)
                            <br />{!! $error !!}, <br />
                        @endforeach
                    </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="padding-left: 20px">

                    <div class="form-group">
                        {!! Form::label('orden_trabajo_id', 'Orden Trabajo:') !!}
                        <p>0000006/2019	</p>
                    </div>

                </div>
                <div class="row">
                    <!-- Orden Trabajo Id Field -->
                    <form id="formSubir" name="formSubir" enctype="multipart/form-data" action="{!! route('subirFacturaArmadoConciliacionAvaluo') !!}" method="post">


                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-xs-12 col-lg-12 sinpadding2">
                            <div class="custom-upload-file">
                                <input
                                type="file"
                                name="fileImport"
                                id="fileImport"
                                class="inputfile inputfile-transparent"
                                data-multiple-caption="{count} files selected"
                                />
                                <p class="help-block">Seleccione el archivo a subir.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-12 padding-no">
                            <div class="col-xs-12 col-lg-6 padding-derecho">
                                <input type="submit" class="btn btn-primary">
                                <a href="{!! url('armado-conciliacion') !!}" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @endif

@endsection
