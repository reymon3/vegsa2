<!-- Ficha Tecnica Id Field -->
<div class="form-group">
    {!! Form::label('ficha_tecnica_id', 'Ficha Tecnica Id:') !!}
    <p>{!! $fichaTecnicaDetalle->ficha_tecnica_id !!}</p>
</div>

<!-- Asset Field -->
<div class="form-group">
    {!! Form::label('asset', 'Asset:') !!}
    <p>{!! $fichaTecnicaDetalle->asset !!}</p>
</div>

<!-- Rubro Field -->
<div class="form-group">
    {!! Form::label('rubro', 'Rubro:') !!}
    <p>{!! $fichaTecnicaDetalle->rubro !!}</p>
</div>

<!-- Catalogo Field -->
<div class="form-group">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    <p>{!! $fichaTecnicaDetalle->catalogo !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $fichaTecnicaDetalle->descripcion !!}</p>
</div>

<!-- Marca Field -->
<div class="form-group">
    {!! Form::label('marca', 'Marca:') !!}
    <p>{!! $fichaTecnicaDetalle->marca !!}</p>
</div>

<!-- Modelo Field -->
<div class="form-group">
    {!! Form::label('modelo', 'Modelo:') !!}
    <p>{!! $fichaTecnicaDetalle->modelo !!}</p>
</div>

<!-- Serie Field -->
<div class="form-group">
    {!! Form::label('serie', 'Serie:') !!}
    <p>{!! $fichaTecnicaDetalle->serie !!}</p>
</div>

<!-- Anio Fabricacion Field -->
<div class="form-group">
    {!! Form::label('anio_fabricacion', 'Anio Fabricacion:') !!}
    <p>{!! $fichaTecnicaDetalle->anio_fabricacion !!}</p>
</div>

<!-- Porcentaje Depreciacion Fisica Field -->
<div class="form-group">
    {!! Form::label('porcentaje_depreciacion_fisica', 'Porcentaje Depreciacion Fisica:') !!}
    <p>{!! $fichaTecnicaDetalle->porcentaje_depreciacion_fisica !!}</p>
</div>

<!-- Fecha Adquisicion Field -->
<div class="form-group">
    {!! Form::label('fecha_adquisicion', 'Fecha Adquisicion:') !!}
    <p>{!! $fichaTecnicaDetalle->fecha_adquisicion !!}</p>
</div>

<!-- Area Field -->
<div class="form-group">
    {!! Form::label('area', 'Area:') !!}
    <p>{!! $fichaTecnicaDetalle->area !!}</p>
</div>

<!-- Caso Field -->
<div class="form-group">
    {!! Form::label('caso', 'Caso:') !!}
    <p>{!! $fichaTecnicaDetalle->caso !!}</p>
</div>

<!-- Valor Cotizado 1 Field -->
<div class="form-group">
    {!! Form::label('valor_cotizado_1', 'Valor Cotizado 1:') !!}
    <p>{!! $fichaTecnicaDetalle->valor_cotizado_1 !!}</p>
</div>

<!-- Moneda 1 Field -->
<div class="form-group">
    {!! Form::label('moneda_1', 'Moneda 1:') !!}
    <p>{!! $fichaTecnicaDetalle->moneda_1 !!}</p>
</div>

<!-- Tipo Cambio 1 Field -->
<div class="form-group">
    {!! Form::label('tipo_cambio_1', 'Tipo Cambio 1:') !!}
    <p>{!! $fichaTecnicaDetalle->tipo_cambio_1 !!}</p>
</div>

<!-- No Piezas 1 Field -->
<div class="form-group">
    {!! Form::label('no_piezas_1', 'No Piezas 1:') !!}
    <p>{!! $fichaTecnicaDetalle->no_piezas_1 !!}</p>
</div>

<!-- Valor Cotizado 2 Field -->
<div class="form-group">
    {!! Form::label('valor_cotizado_2', 'Valor Cotizado 2:') !!}
    <p>{!! $fichaTecnicaDetalle->valor_cotizado_2 !!}</p>
</div>

<!-- Moneda 2 Field -->
<div class="form-group">
    {!! Form::label('moneda_2', 'Moneda 2:') !!}
    <p>{!! $fichaTecnicaDetalle->moneda_2 !!}</p>
</div>

<!-- Tipo Cambio 2 Field -->
<div class="form-group">
    {!! Form::label('tipo_cambio_2', 'Tipo Cambio 2:') !!}
    <p>{!! $fichaTecnicaDetalle->tipo_cambio_2 !!}</p>
</div>

<!-- No Piezas 2 Field -->
<div class="form-group">
    {!! Form::label('no_piezas_2', 'No Piezas 2:') !!}
    <p>{!! $fichaTecnicaDetalle->no_piezas_2 !!}</p>
</div>

<!-- Valor Equipo Usado Field -->
<div class="form-group">
    {!! Form::label('valor_equipo_usado', 'Valor Equipo Usado:') !!}
    <p>{!! $fichaTecnicaDetalle->valor_equipo_usado !!}</p>
</div>

<!-- Vrn Field -->
<div class="form-group">
    {!! Form::label('vrn', 'Vrn:') !!}
    <p>{!! $fichaTecnicaDetalle->vrn !!}</p>
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <p>{!! $fichaTecnicaDetalle->porcentaje_dep !!}</p>
</div>

<!-- Dep Acum Field -->
<div class="form-group">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    <p>{!! $fichaTecnicaDetalle->dep_acum !!}</p>
</div>

<!-- Vnr Field -->
<div class="form-group">
    {!! Form::label('vnr', 'Vnr:') !!}
    <p>{!! $fichaTecnicaDetalle->vnr !!}</p>
</div>

<!-- Link 1 Field -->
<div class="form-group">
    {!! Form::label('link_1', 'Link 1:') !!}
    <p>{!! $fichaTecnicaDetalle->link_1 !!}</p>
</div>

<!-- Link 2 Field -->
<div class="form-group">
    {!! Form::label('link_2', 'Link 2:') !!}
    <p>{!! $fichaTecnicaDetalle->link_2 !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $fichaTecnicaDetalle->observaciones !!}</p>
</div>

<!-- Cotizo Field -->
<div class="form-group">
    {!! Form::label('cotizo', 'Cotizo:') !!}
    <p>{!! $fichaTecnicaDetalle->cotizo !!}</p>
</div>

<!-- Vcf Field -->
<div class="form-group">
    {!! Form::label('vcf', 'Vcf:') !!}
    <p>{!! $fichaTecnicaDetalle->vcf !!}</p>
</div>

<!-- Vur Field -->
<div class="form-group">
    {!! Form::label('vur', 'Vur:') !!}
    <p>{!! $fichaTecnicaDetalle->vur !!}</p>
</div>

<!-- Da Field -->
<div class="form-group">
    {!! Form::label('da', 'Da:') !!}
    <p>{!! $fichaTecnicaDetalle->da !!}</p>
</div>

<!-- Vut Field -->
<div class="form-group">
    {!! Form::label('vut', 'Vut:') !!}
    <p>{!! $fichaTecnicaDetalle->vut !!}</p>
</div>

<!-- Gastos Instalacion Field -->
<div class="form-group">
    {!! Form::label('gastos_instalacion', 'Gastos Instalacion:') !!}
    <p>{!! $fichaTecnicaDetalle->gastos_instalacion !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $fichaTecnicaDetalle->blame_id !!}</p>
</div>

