<!-- Ficha Tecnica Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ficha_tecnica_id', 'Ficha Tecnica Id:') !!}
    {!! Form::number('ficha_tecnica_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Asset Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset', 'Asset:') !!}
    {!! Form::text('asset', null, ['class' => 'form-control']) !!}
</div>

<!-- Rubro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rubro', 'Rubro:') !!}
    {!! Form::text('rubro', null, ['class' => 'form-control']) !!}
</div>

<!-- Catalogo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('catalogo', 'Catalogo:') !!}
    {!! Form::text('catalogo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Marca Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marca', 'Marca:') !!}
    {!! Form::text('marca', null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Serie Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serie', 'Serie:') !!}
    {!! Form::text('serie', null, ['class' => 'form-control']) !!}
</div>

<!-- Anio Fabricacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anio_fabricacion', 'Anio Fabricacion:') !!}
    {!! Form::text('anio_fabricacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Depreciacion Fisica Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_depreciacion_fisica', 'Porcentaje Depreciacion Fisica:') !!}
    {!! Form::text('porcentaje_depreciacion_fisica', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Adquisicion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_adquisicion', 'Fecha Adquisicion:') !!}
    {!! Form::date('fecha_adquisicion', null, ['class' => 'form-control','id'=>'fecha_adquisicion']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#fecha_adquisicion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- Caso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('caso', 'Caso:') !!}
    {!! Form::text('caso', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Cotizado 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor_cotizado_1', 'Valor Cotizado 1:') !!}
    {!! Form::number('valor_cotizado_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Moneda 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('moneda_1', 'Moneda 1:') !!}
    {!! Form::text('moneda_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Cambio 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_cambio_1', 'Tipo Cambio 1:') !!}
    {!! Form::number('tipo_cambio_1', null, ['class' => 'form-control']) !!}
</div>

<!-- No Piezas 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_piezas_1', 'No Piezas 1:') !!}
    {!! Form::number('no_piezas_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Cotizado 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor_cotizado_2', 'Valor Cotizado 2:') !!}
    {!! Form::number('valor_cotizado_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Moneda 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('moneda_2', 'Moneda 2:') !!}
    {!! Form::text('moneda_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Cambio 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_cambio_2', 'Tipo Cambio 2:') !!}
    {!! Form::number('tipo_cambio_2', null, ['class' => 'form-control']) !!}
</div>

<!-- No Piezas 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_piezas_2', 'No Piezas 2:') !!}
    {!! Form::number('no_piezas_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Valor Equipo Usado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('valor_equipo_usado', 'Valor Equipo Usado:') !!}
    {!! Form::number('valor_equipo_usado', null, ['class' => 'form-control']) !!}
</div>

<!-- Vrn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vrn', 'Vrn:') !!}
    {!! Form::number('vrn', null, ['class' => 'form-control']) !!}
</div>

<!-- Porcentaje Dep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('porcentaje_dep', 'Porcentaje Dep:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('porcentaje_dep', 0) !!}
        {!! Form::checkbox('porcentaje_dep', '1', null) !!}
    </label>
</div>


<!-- Dep Acum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dep_acum', 'Dep Acum:') !!}
    {!! Form::number('dep_acum', null, ['class' => 'form-control']) !!}
</div>

<!-- Vnr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vnr', 'Vnr:') !!}
    {!! Form::number('vnr', null, ['class' => 'form-control']) !!}
</div>

<!-- Link 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link_1', 'Link 1:') !!}
    {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Link 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link_2', 'Link 2:') !!}
    {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Cotizo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cotizo', 'Cotizo:') !!}
    {!! Form::text('cotizo', null, ['class' => 'form-control']) !!}
</div>

<!-- Vcf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vcf', 'Vcf:') !!}
    {!! Form::number('vcf', null, ['class' => 'form-control']) !!}
</div>

<!-- Vur Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vur', 'Vur:') !!}
    {!! Form::number('vur', null, ['class' => 'form-control']) !!}
</div>

<!-- Da Field -->
<div class="form-group col-sm-6">
    {!! Form::label('da', 'Da:') !!}
    {!! Form::number('da', null, ['class' => 'form-control']) !!}
</div>

<!-- Vut Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vut', 'Vut:') !!}
    {!! Form::number('vut', null, ['class' => 'form-control']) !!}
</div>

<!-- Gastos Instalacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gastos_instalacion', 'Gastos Instalacion:') !!}
    {!! Form::number('gastos_instalacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('fichaTecnicaDetalles.index') !!}" class="btn btn-default">Cancel</a>
</div>
