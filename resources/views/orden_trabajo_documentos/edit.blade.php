@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Documento
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoDocumento, ['route' => ['ordenTrabajoDocumentos.update', $ordenTrabajoDocumento->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_documentos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection