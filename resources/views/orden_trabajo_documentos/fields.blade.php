<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Ruta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ruta', 'Ruta:') !!}
    {!! Form::text('ruta', null, ['class' => 'form-control']) !!}
</div>

<!-- Orden Trabajo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden_trabajo_id', 'Orden Trabajo Id:') !!}
    {!! Form::number('orden_trabajo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoDocumentos.index') !!}" class="btn btn-default">Cancel</a>
</div>
