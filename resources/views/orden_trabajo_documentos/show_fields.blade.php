<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $ordenTrabajoDocumento->nombre !!}</p>
</div>

<!-- Ruta Field -->
<div class="form-group">
    {!! Form::label('ruta', 'Ruta:') !!}
    <p>{!! $ordenTrabajoDocumento->ruta !!}</p>
</div>

<!-- Orden Trabajo Id Field -->
<div class="form-group">
    {!! Form::label('orden_trabajo_id', 'Orden Trabajo Id:') !!}
    <p>{!! $ordenTrabajoDocumento->orden_trabajo_id !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $ordenTrabajoDocumento->blame_id !!}</p>
</div>

