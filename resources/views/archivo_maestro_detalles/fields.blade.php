<!-- Archivo Maestro Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('archivo_maestro_id', 'Archivo Maestro Id:') !!}
    {!! Form::number('archivo_maestro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company_code', 'Company Code:') !!}
    {!! Form::text('company_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Asset Class Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset_class', 'Asset Class:') !!}
    {!! Form::text('asset_class', null, ['class' => 'form-control']) !!}
</div>

<!-- Asset Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset', 'Asset:') !!}
    {!! Form::text('asset', null, ['class' => 'form-control']) !!}
</div>

<!-- Subnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subnumber', 'Subnumber:') !!}
    {!! Form::text('subnumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Additional Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('additional_description', 'Additional Description:') !!}
    {!! Form::text('additional_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Serial Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serial_number', 'Serial Number:') !!}
    {!! Form::text('serial_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Inventory Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('inventory_number', 'Inventory Number:') !!}
    {!! Form::text('inventory_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Capitalized Field -->
<div class="form-group col-sm-6">
    {!! Form::label('capitalized', 'Capitalized:') !!}
    {!! Form::date('capitalized', null, ['class' => 'form-control','id'=>'capitalized']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#capitalized').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Deactivation On Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deactivation_on', 'Deactivation On:') !!}
    {!! Form::date('deactivation_on', null, ['class' => 'form-control','id'=>'deactivation_on']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#deactivation_on').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Cost Center Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_center', 'Cost Center:') !!}
    {!! Form::text('cost_center', null, ['class' => 'form-control']) !!}
</div>

<!-- Evaluation Group 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('evaluation_group_1', 'Evaluation Group 1:') !!}
    {!! Form::text('evaluation_group_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Evaluation Group 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('evaluation_group_2', 'Evaluation Group 2:') !!}
    {!! Form::text('evaluation_group_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Key Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_key', 'Country Key:') !!}
    {!! Form::text('country_key', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_name', 'Country Name:') !!}
    {!! Form::text('country_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Ad Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ad_value', 'Ad Value:') !!}
    {!! Form::number('ad_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Rate 01 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_rate_01', 'Dprn Rate 01:') !!}
    {!! Form::number('dprn_rate_01', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Value 01 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_value_01', 'Dprn Value 01:') !!}
    {!! Form::number('dprn_value_01', null, ['class' => 'form-control']) !!}
</div>

<!-- Depreciacion Acumulada 01 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depreciacion_acumulada_01', 'Depreciacion Acumulada 01:') !!}
    {!! Form::number('depreciacion_acumulada_01', null, ['class' => 'form-control']) !!}
</div>

<!-- Book Value For Area 01 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('book_value_for_area_01', 'Book Value For Area 01:') !!}
    {!! Form::number('book_value_for_area_01', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Rate 02 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_rate_02', 'Dprn Rate 02:') !!}
    {!! Form::number('dprn_rate_02', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Value 02 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_value_02', 'Dprn Value 02:') !!}
    {!! Form::number('dprn_value_02', null, ['class' => 'form-control']) !!}
</div>

<!-- Depreciacion Acumulada 02 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depreciacion_acumulada_02', 'Depreciacion Acumulada 02:') !!}
    {!! Form::number('depreciacion_acumulada_02', null, ['class' => 'form-control']) !!}
</div>

<!-- Book Value For Area 02 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('book_value_for_area_02', 'Book Value For Area 02:') !!}
    {!! Form::number('book_value_for_area_02', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Rate 03 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_rate_03', 'Dprn Rate 03:') !!}
    {!! Form::number('dprn_rate_03', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Value 03 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_value_03', 'Dprn Value 03:') !!}
    {!! Form::number('dprn_value_03', null, ['class' => 'form-control']) !!}
</div>

<!-- Depreciacion Acumulada 03 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depreciacion_acumulada_03', 'Depreciacion Acumulada 03:') !!}
    {!! Form::number('depreciacion_acumulada_03', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Rate 04 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_rate_04', 'Dprn Rate 04:') !!}
    {!! Form::number('dprn_rate_04', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Value 04 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_value_04', 'Dprn Value 04:') !!}
    {!! Form::number('dprn_value_04', null, ['class' => 'form-control']) !!}
</div>

<!-- Depreciacion Acumulada 04 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depreciacion_acumulada_04', 'Depreciacion Acumulada 04:') !!}
    {!! Form::number('depreciacion_acumulada_04', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Rate 05 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_rate_05', 'Dprn Rate 05:') !!}
    {!! Form::number('dprn_rate_05', null, ['class' => 'form-control']) !!}
</div>

<!-- Dprn Value 05 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dprn_value_05', 'Dprn Value 05:') !!}
    {!! Form::number('dprn_value_05', null, ['class' => 'form-control']) !!}
</div>

<!-- Depreciacion Acumulada 05 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('depreciacion_acumulada_05', 'Depreciacion Acumulada 05:') !!}
    {!! Form::number('depreciacion_acumulada_05', null, ['class' => 'form-control']) !!}
</div>

<!-- Blame Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    {!! Form::number('blame_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('archivoMaestroDetalles.index') !!}" class="btn btn-default">Cancel</a>
</div>
