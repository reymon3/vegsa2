<!-- Archivo Maestro Id Field -->
<div class="form-group">
    {!! Form::label('archivo_maestro_id', 'Archivo Maestro Id:') !!}
    <p>{!! $archivoMaestroDetalle->archivo_maestro_id !!}</p>
</div>

<!-- Company Code Field -->
<div class="form-group">
    {!! Form::label('company_code', 'Company Code:') !!}
    <p>{!! $archivoMaestroDetalle->company_code !!}</p>
</div>

<!-- Asset Class Field -->
<div class="form-group">
    {!! Form::label('asset_class', 'Asset Class:') !!}
    <p>{!! $archivoMaestroDetalle->asset_class !!}</p>
</div>

<!-- Asset Field -->
<div class="form-group">
    {!! Form::label('asset', 'Asset:') !!}
    <p>{!! $archivoMaestroDetalle->asset !!}</p>
</div>

<!-- Subnumber Field -->
<div class="form-group">
    {!! Form::label('subnumber', 'Subnumber:') !!}
    <p>{!! $archivoMaestroDetalle->subnumber !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $archivoMaestroDetalle->name !!}</p>
</div>

<!-- Additional Description Field -->
<div class="form-group">
    {!! Form::label('additional_description', 'Additional Description:') !!}
    <p>{!! $archivoMaestroDetalle->additional_description !!}</p>
</div>

<!-- Serial Number Field -->
<div class="form-group">
    {!! Form::label('serial_number', 'Serial Number:') !!}
    <p>{!! $archivoMaestroDetalle->serial_number !!}</p>
</div>

<!-- Inventory Number Field -->
<div class="form-group">
    {!! Form::label('inventory_number', 'Inventory Number:') !!}
    <p>{!! $archivoMaestroDetalle->inventory_number !!}</p>
</div>

<!-- Capitalized Field -->
<div class="form-group">
    {!! Form::label('capitalized', 'Capitalized:') !!}
    <p>{!! $archivoMaestroDetalle->capitalized !!}</p>
</div>

<!-- Deactivation On Field -->
<div class="form-group">
    {!! Form::label('deactivation_on', 'Deactivation On:') !!}
    <p>{!! $archivoMaestroDetalle->deactivation_on !!}</p>
</div>

<!-- Cost Center Field -->
<div class="form-group">
    {!! Form::label('cost_center', 'Cost Center:') !!}
    <p>{!! $archivoMaestroDetalle->cost_center !!}</p>
</div>

<!-- Evaluation Group 1 Field -->
<div class="form-group">
    {!! Form::label('evaluation_group_1', 'Evaluation Group 1:') !!}
    <p>{!! $archivoMaestroDetalle->evaluation_group_1 !!}</p>
</div>

<!-- Evaluation Group 2 Field -->
<div class="form-group">
    {!! Form::label('evaluation_group_2', 'Evaluation Group 2:') !!}
    <p>{!! $archivoMaestroDetalle->evaluation_group_2 !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $archivoMaestroDetalle->text !!}</p>
</div>

<!-- Country Key Field -->
<div class="form-group">
    {!! Form::label('country_key', 'Country Key:') !!}
    <p>{!! $archivoMaestroDetalle->country_key !!}</p>
</div>

<!-- Country Name Field -->
<div class="form-group">
    {!! Form::label('country_name', 'Country Name:') !!}
    <p>{!! $archivoMaestroDetalle->country_name !!}</p>
</div>

<!-- Ad Value Field -->
<div class="form-group">
    {!! Form::label('ad_value', 'Ad Value:') !!}
    <p>{!! $archivoMaestroDetalle->ad_value !!}</p>
</div>

<!-- Dprn Rate 01 Field -->
<div class="form-group">
    {!! Form::label('dprn_rate_01', 'Dprn Rate 01:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_rate_01 !!}</p>
</div>

<!-- Dprn Value 01 Field -->
<div class="form-group">
    {!! Form::label('dprn_value_01', 'Dprn Value 01:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_value_01 !!}</p>
</div>

<!-- Depreciacion Acumulada 01 Field -->
<div class="form-group">
    {!! Form::label('depreciacion_acumulada_01', 'Depreciacion Acumulada 01:') !!}
    <p>{!! $archivoMaestroDetalle->depreciacion_acumulada_01 !!}</p>
</div>

<!-- Book Value For Area 01 Field -->
<div class="form-group">
    {!! Form::label('book_value_for_area_01', 'Book Value For Area 01:') !!}
    <p>{!! $archivoMaestroDetalle->book_value_for_area_01 !!}</p>
</div>

<!-- Dprn Rate 02 Field -->
<div class="form-group">
    {!! Form::label('dprn_rate_02', 'Dprn Rate 02:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_rate_02 !!}</p>
</div>

<!-- Dprn Value 02 Field -->
<div class="form-group">
    {!! Form::label('dprn_value_02', 'Dprn Value 02:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_value_02 !!}</p>
</div>

<!-- Depreciacion Acumulada 02 Field -->
<div class="form-group">
    {!! Form::label('depreciacion_acumulada_02', 'Depreciacion Acumulada 02:') !!}
    <p>{!! $archivoMaestroDetalle->depreciacion_acumulada_02 !!}</p>
</div>

<!-- Book Value For Area 02 Field -->
<div class="form-group">
    {!! Form::label('book_value_for_area_02', 'Book Value For Area 02:') !!}
    <p>{!! $archivoMaestroDetalle->book_value_for_area_02 !!}</p>
</div>

<!-- Dprn Rate 03 Field -->
<div class="form-group">
    {!! Form::label('dprn_rate_03', 'Dprn Rate 03:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_rate_03 !!}</p>
</div>

<!-- Dprn Value 03 Field -->
<div class="form-group">
    {!! Form::label('dprn_value_03', 'Dprn Value 03:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_value_03 !!}</p>
</div>

<!-- Depreciacion Acumulada 03 Field -->
<div class="form-group">
    {!! Form::label('depreciacion_acumulada_03', 'Depreciacion Acumulada 03:') !!}
    <p>{!! $archivoMaestroDetalle->depreciacion_acumulada_03 !!}</p>
</div>

<!-- Dprn Rate 04 Field -->
<div class="form-group">
    {!! Form::label('dprn_rate_04', 'Dprn Rate 04:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_rate_04 !!}</p>
</div>

<!-- Dprn Value 04 Field -->
<div class="form-group">
    {!! Form::label('dprn_value_04', 'Dprn Value 04:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_value_04 !!}</p>
</div>

<!-- Depreciacion Acumulada 04 Field -->
<div class="form-group">
    {!! Form::label('depreciacion_acumulada_04', 'Depreciacion Acumulada 04:') !!}
    <p>{!! $archivoMaestroDetalle->depreciacion_acumulada_04 !!}</p>
</div>

<!-- Dprn Rate 05 Field -->
<div class="form-group">
    {!! Form::label('dprn_rate_05', 'Dprn Rate 05:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_rate_05 !!}</p>
</div>

<!-- Dprn Value 05 Field -->
<div class="form-group">
    {!! Form::label('dprn_value_05', 'Dprn Value 05:') !!}
    <p>{!! $archivoMaestroDetalle->dprn_value_05 !!}</p>
</div>

<!-- Depreciacion Acumulada 05 Field -->
<div class="form-group">
    {!! Form::label('depreciacion_acumulada_05', 'Depreciacion Acumulada 05:') !!}
    <p>{!! $archivoMaestroDetalle->depreciacion_acumulada_05 !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $archivoMaestroDetalle->blame_id !!}</p>
</div>

