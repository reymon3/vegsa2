@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Archivo Maestro Detalle
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($archivoMaestroDetalle, ['route' => ['archivoMaestroDetalles.update', $archivoMaestroDetalle->id], 'method' => 'patch']) !!}

                        @include('archivo_maestro_detalles.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection