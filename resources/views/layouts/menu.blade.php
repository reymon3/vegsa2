<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('areas.index') !!}"><i class="fa fa-edit"></i><span>Usuarios</span></a>
</li>
<li class="{{ Request::is('areas*') ? 'active' : '' }}">
    <a href="{!! route('areas.index') !!}"><i class="fa fa-edit"></i><span>Areas</span></a>
</li>

<li class="{{ Request::is('empleados*') ? 'active' : '' }}">
    <a href="{!! route('empleados.index') !!}"><i class="fa fa-edit"></i><span>Empleados</span></a>
</li>

<li class="{{ Request::is('puestos*') ? 'active' : '' }}">
    <a href="{!! route('puestos.index') !!}"><i class="fa fa-edit"></i><span>Puestos</span></a>
</li>

<li class="{{ Request::is('rubros*') ? 'active' : '' }}">
    <a href="{!! route('rubros.index') !!}"><i class="fa fa-edit"></i><span>Rubros</span></a>
</li>

<li class="{{ Request::is('tipoAvaluos*') ? 'active' : '' }}">
    <a href="{!! route('tipoAvaluos.index') !!}"><i class="fa fa-edit"></i><span>Tipo Avaluos</span></a>
</li>

<li class="{{ Request::is('tipoCambios*') ? 'active' : '' }}">
    <a href="{!! route('tipoCambios.index') !!}"><i class="fa fa-edit"></i><span>Tipo Cambios</span></a>
</li>

<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{!! route('clientes.index') !!}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>
<li class="{{ Request::is('servicios*') ? 'active' : '' }}">
    <a href="{!! route('servicios.index') !!}"><i class="fa fa-edit"></i><span>Servicios</span></a>
</li>

<li class="{{ Request::is('normas*') ? 'active' : '' }}">
    <a href="{!! route('normas.index') !!}"><i class="fa fa-edit"></i><span>Normas</span></a>
</li>

<li class="{{ Request::is('propositoAvaluos*') ? 'active' : '' }}">
    <a href="{!! route('propositoAvaluos.index') !!}"><i class="fa fa-edit"></i><span>Proposito Avaluos</span></a>
</li>

<li class="{{ Request::is('tipoPlaqueos*') ? 'active' : '' }}">
    <a href="{!! route('tipoPlaqueos.index') !!}"><i class="fa fa-edit"></i><span>Tipo Plaqueos</span></a>
</li>

<li class="{{ Request::is('cotizacions*') ? 'active' : '' }}">
    <a href="{!! route('cotizacions.index') !!}"><i class="fa fa-edit"></i><span>Cotizaciones</span></a>
</li>
<li class="{{ Request::is('ordenTrabajos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoRazonSocials*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoRazonSocials.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Razon Socials</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoBaseContables*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoBaseContables.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Base Contables</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoServicios*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoServicios.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Servicios</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoNormas*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoNormas.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Normas</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoEmpleados*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoEmpleados.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Empleados</span></a>
</li>

<li class="{{ Request::is('avaluos*') ? 'active' : '' }}">
    <a href="{!! route('avaluos.index') !!}"><i class="fa fa-edit"></i><span>Avaluos</span></a>
</li>

<li class="{{ Request::is('avaluoDetalles*') ? 'active' : '' }}">
    <a href="{!! route('avaluoDetalles.index') !!}"><i class="fa fa-edit"></i><span>Avaluo Detalles</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoPropositoAvaluos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoPropositoAvaluos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Proposito Avaluos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoRubroAlcances*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoRubroAlcances.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Rubro Alcances</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoTipoPlaqueos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoTipoPlaqueos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Tipo Plaqueos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoTipoAvaluos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoTipoAvaluos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Tipo Avaluos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoDocumentos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoDocumentos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Documentos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoConteos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoConteos.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Conteos</span></a>
</li>

<li class="{{ Request::is('ordenTrabajoBaseContableDetalles*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajoBaseContableDetalles.index') !!}"><i class="fa fa-edit"></i><span>Orden Trabajo Base Contable Detalles</span></a>
</li>

<li class="{{ Request::is('civils*') ? 'active' : '' }}">
    <a href="{!! route('civils.index') !!}"><i class="fa fa-edit"></i><span>Civils</span></a>
</li>

<li class="{{ Request::is('civilDetalles*') ? 'active' : '' }}">
    <a href="{!! route('civilDetalles.index') !!}"><i class="fa fa-edit"></i><span>Civil Detalles</span></a>
</li>

<li class="{{ Request::is('avaluos*') ? 'active' : '' }}">
    <a href="{!! route('avaluos.index') !!}"><i class="fa fa-edit"></i><span>Avaluos</span></a>
</li>

<li class="{{ Request::is('avaluoDetalles*') ? 'active' : '' }}">
    <a href="{!! route('avaluoDetalles.index') !!}"><i class="fa fa-edit"></i><span>Avaluo Detalles</span></a>
</li>

<li class="{{ Request::is('archivoMaestros*') ? 'active' : '' }}">
    <a href="{!! route('archivoMaestros.index') !!}"><i class="fa fa-edit"></i><span>Archivo Maestros</span></a>
</li>


<li class="{{ Request::is('archivoMaestroDetalles*') ? 'active' : '' }}">
    <a href="{!! route('archivoMaestroDetalles.index') !!}"><i class="fa fa-edit"></i><span>Archivo Maestro Detalles</span></a>
</li>

<li class="{{ Request::is('activoFijos*') ? 'active' : '' }}">
    <a href="{!! route('activoFijos.index') !!}"><i class="fa fa-edit"></i><span>Activo Fijos</span></a>
</li>

<li class="{{ Request::is('conciliacions*') ? 'active' : '' }}">
    <a href="{!! route('conciliacions.index') !!}"><i class="fa fa-edit"></i><span>Conciliacions</span></a>
</li>

<li class="{{ Request::is('conciliacionDetalles*') ? 'active' : '' }}">
    <a href="{!! route('conciliacionDetalles.index') !!}"><i class="fa fa-edit"></i><span>Conciliacion Detalles</span></a>
</li>

<li class="{{ Request::is('fichaTecnicas*') ? 'active' : '' }}">
    <a href="{!! route('fichaTecnicas.index') !!}"><i class="fa fa-edit"></i><span>Ficha Tecnicas</span></a>
</li>

<li class="{{ Request::is('fichaTecnicaDetalles*') ? 'active' : '' }}">
    <a href="{!! route('fichaTecnicaDetalles.index') !!}"><i class="fa fa-edit"></i><span>Ficha Tecnica Detalles</span></a>
</li>

