<li class="{{ Request::is('/') ? 'active' : '' }} ">
    <a href="{!! url('/') !!}" class="">
        <i class="fa fa-dashboard" aria-hidden="true"></i>
        <span>Home</span>
    </a>
</li>

<li class="treeview
    {{ Request::is('ordenTrabajos*') ? 'active' : '' }}
    {{ Request::is('cotizacions*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-money"></i>
        <span>Ventas</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('cotizacions*') ? 'active' : '' }}">
            <a href="{!! route('cotizacions.index') !!}"><i class="fa fa-circle-o"></i><span>Cotizaciones</span></a>
        </li>
        <li class="{{ Request::is('ordenTrabajos*') ? 'active' : '' }}">
            <a href="{!! route('ordenTrabajos.index') !!}">
                <i class="fa fa-circle-o"></i>Orden de trabajo
            </a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('asignacion-proyecto') ? 'active' : '' }} ">
    <a href="{!! url('asignacion-proyecto') !!}" class="">
        <i class="fa fa-cog" aria-hidden="true"></i>
        <span>Asignación proyecto</span>
    </a>
</li>

<li class="treeview

    {{ Request::is('ordenTrabajoBaseContables*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-truck"></i>
        <span>Maquinaria</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ordenTrabajoBaseContables*') ? 'active' : '' }}">
            <a href="{!! url('ordenTrabajoBaseContables') !!}">
            <i class="fa fa-circle-o"></i>Inventario</a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('avaluos') ? 'active' : '' }} ">
    <a href="{!! route('avaluos.index') !!}" class="">
        <i class="fa fa-dollar" aria-hidden="true"></i>
        <span>Avalúos</span>
    </a>
</li>

<li class="{{ Request::is('conciliacions') ? 'active' : '' }} ">
    <a href="{!! route('conciliacions.index') !!}" class="">
        <i class="fa fa-tasks" aria-hidden="true"></i>
        <span>Conciliacion</span>
    </a>
</li>

<li class="{{ Request::is('civil') ? 'active' : '' }} ">
    <a href="{!! route('civil.index') !!}" class="">
        <i class="fa fa-cubes" aria-hidden="true"></i>
        <span>Civil</span>
    </a>
</li>

<li class="treeview
    {{ Request::is('armado*') ? 'active' : '' }}
    {{ Request::is('factura*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-puzzle-piece"></i>
        <span>Armado</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="
            {{ Request::is('armado-conciliacion') ? 'active' : '' }}
            {{ Request::is('factura-armado-conciliacion') ? 'active' : '' }}
        ">
            <a href="{!! url('armado-conciliacion') !!}">
                <i class="fa fa-circle-o"></i>
                <span>Conciliación </span>
            </a>
        </li>
        <li class="
            {{ Request::is('armado-avaluo') ? 'active' : '' }}
            {{ Request::is('factura-armado-avaluo') ? 'active' : '' }}
        ">
            <a href="{!! url('armado-avaluo') !!}">
                <i class="fa fa-circle-o"></i>
                <span>Avaluo</span>
            </a>
        </li>
        <li class="
            {{ Request::is('armado-conciliacion-avaluo') ? 'active' : '' }}
            {{ Request::is('factura-armado-conciliacion-avaluo') ? 'active' : '' }}
        ">
            <a href="{!! url('armado-conciliacion-avaluo') !!}">
                <i class="fa fa-circle-o"></i>
                <span>Conciliación y Avaluo</span>
            </a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('/') ? 'active' : '' }} ">
    <a href="{!! route('fichaTecnicas.index') !!}" class="">
        <i class="fa fa-file-text-o" aria-hidden="true"></i>
        <span>Ficha Técnica</span>
    </a>
</li>

<li class="treeview
    {{ Request::is('users*') ? 'active' : '' }}
    {{ Request::is('areas*') ? 'active' : '' }}
    {{ Request::is('empleados*') ? 'active' : '' }}
    {{ Request::is('puestos*') ? 'active' : '' }}
    {{ Request::is('rubros*') ? 'active' : '' }}
    {{ Request::is('tipoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoCambios*') ? 'active' : '' }}
    {{ Request::is('clientes*') ? 'active' : '' }}
    {{ Request::is('servicios*') ? 'active' : '' }}
    {{ Request::is('normas*') ? 'active' : '' }}
    {{ Request::is('propositoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoPlaqueos*') ? 'active' : '' }}
    {{ Request::is('activoFijos*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-folder"></i>
        <span>Catálogos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="{!! route('users.index') !!}"><i class="fa fa-circle-o"></i><span>Usuarios</span></a>
        </li>
        <li class="{{ Request::is('activoFijos*') ? 'active' : '' }}">
            <a href="{!! route('activoFijos.index') !!}"><i class="fa fa-circle-o"></i><span>Activo Fijo</span></a>
        </li>
        <li class="{{ Request::is('areas*') ? 'active' : '' }}">
            <a href="{!! route('areas.index') !!}"><i class="fa fa-circle-o"></i><span>Areas</span></a>
        </li>

        <li class="{{ Request::is('empleados*') ? 'active' : '' }}">
            <a href="{!! route('empleados.index') !!}"><i class="fa fa-circle-o"></i><span>Empleados</span></a>
        </li>

        <li class="{{ Request::is('puestos*') ? 'active' : '' }}">
            <a href="{!! route('puestos.index') !!}"><i class="fa fa-circle-o"></i><span>Puestos</span></a>
        </li>

        <li class="{{ Request::is('rubros*') ? 'active' : '' }}">
            <a href="{!! route('rubros.index') !!}"><i class="fa fa-circle-o"></i><span>Rubros</span></a>
        </li>

        <li class="{{ Request::is('tipoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('tipoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Avaluos</span></a>
        </li>

        <li class="{{ Request::is('tipoCambios*') ? 'active' : '' }}">
            <a href="{!! route('tipoCambios.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Cambios</span></a>
        </li>

        <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
            <a href="{!! route('clientes.index') !!}"><i class="fa fa-circle-o"></i><span>Clientes</span></a>
        </li>
        <li class="{{ Request::is('servicios*') ? 'active' : '' }}">
            <a href="{!! route('servicios.index') !!}"><i class="fa fa-circle-o"></i><span>Servicios</span></a>
        </li>

        <li class="{{ Request::is('normas*') ? 'active' : '' }}">
            <a href="{!! route('normas.index') !!}"><i class="fa fa-circle-o"></i><span>Normas</span></a>
        </li>

        <li class="{{ Request::is('propositoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('propositoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Proposito Avaluos</span></a>
        </li>

        <li class="{{ Request::is('tipoPlaqueos*') ? 'active' : '' }}">
            <a href="{!! route('tipoPlaqueos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Plaqueos</span></a>
        </li>

    </ul>
</li>

<li class="{{ Request::is('/') ? 'active' : '' }} ">
    <a href="{!! route('archivoMaestros.index') !!}" class="">
        <i class="fa fa-file-text-o" aria-hidden="true"></i>
        <span>Reportes</span>
    </a>
</li>

<li class="{{ Request::is('placas*') ? 'active' : '' }}">
    <a href="{!! route('placas.index') !!}"><i class="fa fa-circle-o"></i><span>Placas</span></a>
</li>
<li class="{{ Request::is('cartas*') ? 'active' : '' }}">
    <a href="{!! route('cartas.index') !!}"><i class="fa fa-circle-o"></i><span>Cartas</span></a>
</li>
<li class="{{ Request::is('reporteador*') ? 'active' : '' }}">
    <a href="{!! route('reporteador.index') !!}"><i class="fa fa-circle-o"></i><span>Reporteador</span></a>
</li>
<li class="{{ Request::is('cuadernillo*') ? 'active' : '' }}">
    <a href="{!! route('cuadernillo.index') !!}"><i class="fa fa-building"></i><span>Cuadernillo</span></a>
</li>