<li class="treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i>
        <span>Reportes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resúmen</a>
            </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Físico
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Contable
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resumen Detallado
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Ajuste depreciacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Archivo Maestro
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Orden de Trabajo
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Componetizacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Avaluos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Civil
            </a>
        </li>
    </ul>
</li>
