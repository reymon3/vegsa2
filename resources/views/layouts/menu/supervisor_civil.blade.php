<li class="{{ Request::is('home') ? 'active' : '' }} nuno">
    <a href="{!! url('/') !!}" class="nuno">
        <i class="fa fa-dashboard fa-lg" aria-hidden="true"></i>
        <span>Home</span>
    </a>
</li>

<li class="treeview
    {{ Request::is('avaluos-antecedentes') ? 'active' : '' }}
    {{ Request::is('avaluos-caracteristicas') ? 'active' : '' }}
    {{ Request::is('avaluos-terreno') ? 'active' : '' }}
    {{ Request::is('avaluos-descripcion') ? 'active' : '' }}
    {{ Request::is('avaluos-elementos') ? 'active' : '' }}
    {{ Request::is('avaluos-conclusiones') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-cubes"></i>
        <span>Civil</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('avaluos-antecedentes') ? 'active' : '' }}">
            <a href="{!! url('avaluos-antecedentes') !!}">
                <i class="fa fa-circle-o"></i><span>Antecedentes</span>
            </a>
        </li>
        <li class="{{ Request::is('avaluos-caracteristicas') ? 'active' : '' }}">
            <a href="{!! url('avaluos-caracteristicas') !!}">
                <i class="fa fa-circle-o"></i><span>Caracteristicas urbanas</span>
            </a>
        </li>
        <li class="{{ Request::is('avaluos-terreno') ? 'active' : '' }}">
            <a href="{!! url('avaluos-terreno') !!}">
                <i class="fa fa-circle-o"></i><span>Caracteristicas del terreno</span>
            </a>
        </li>

        <li class="{{ Request::is('avaluos-descripcion') ? 'active' : '' }}">
            <a href="{!! url('avaluos-descripcion') !!}">
                <i class="fa fa-circle-o"></i><span>Descripción General del Predio</span>
            </a>
        </li>

        <li class="{{ Request::is('avaluos-elementos') ? 'active' : '' }}">
            <a href="{!! url('avaluos-elementos') !!}">
                <i class="fa fa-circle-o"></i><span>Elementos de Construccion</span>
            </a>
        </li>

        <li class="{{ Request::is('avaluos-conclusiones') ? 'active' : '' }}">
            <a href="{!! url('avaluos-conclusiones') !!}">
                <i class="fa fa-circle-o"></i><span>Conclusiones Previas</span>
            </a>
        </li>

    </ul>
</li>

<li class="treeview
    {{ Request::is('users*') ? 'active' : '' }}
    {{ Request::is('areas*') ? 'active' : '' }}
    {{ Request::is('empleados*') ? 'active' : '' }}
    {{ Request::is('puestos*') ? 'active' : '' }}
    {{ Request::is('rubros*') ? 'active' : '' }}
    {{ Request::is('tipoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoCambios*') ? 'active' : '' }}
    {{ Request::is('clientes*') ? 'active' : '' }}
    {{ Request::is('servicios*') ? 'active' : '' }}
    {{ Request::is('normas*') ? 'active' : '' }}
    {{ Request::is('propositoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoPlaqueos*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-folder"></i>
        <span>Catalogos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('areas*') ? 'active' : '' }}">
            <a href="{!! route('areas.index') !!}"><i class="fa fa-circle-o"></i><span>Areas</span></a>
        </li>

        <li class="{{ Request::is('empleados*') ? 'active' : '' }}">
            <a href="{!! route('empleados.index') !!}"><i class="fa fa-circle-o"></i><span>Empleados</span></a>
        </li>

        <li class="{{ Request::is('puestos*') ? 'active' : '' }}">
            <a href="{!! route('puestos.index') !!}"><i class="fa fa-circle-o"></i><span>Puestos</span></a>
        </li>

        <li class="{{ Request::is('rubros*') ? 'active' : '' }}">
            <a href="{!! route('rubros.index') !!}"><i class="fa fa-circle-o"></i><span>Rubros</span></a>
        </li>

        <li class="{{ Request::is('tipoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('tipoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Avaluos</span></a>
        </li>

        <li class="{{ Request::is('tipoCambios*') ? 'active' : '' }}">
            <a href="{!! route('tipoCambios.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Cambios</span></a>
        </li>

        <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
            <a href="{!! route('clientes.index') !!}"><i class="fa fa-circle-o"></i><span>Clientes</span></a>
        </li>
        <li class="{{ Request::is('servicios*') ? 'active' : '' }}">
            <a href="{!! route('servicios.index') !!}"><i class="fa fa-circle-o"></i><span>Servicios</span></a>
        </li>

        <li class="{{ Request::is('normas*') ? 'active' : '' }}">
            <a href="{!! route('normas.index') !!}"><i class="fa fa-circle-o"></i><span>Normas</span></a>
        </li>

        <li class="{{ Request::is('propositoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('propositoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Proposito Avaluos</span></a>
        </li>

        <li class="{{ Request::is('tipoPlaqueos*') ? 'active' : '' }}">
            <a href="{!! route('tipoPlaqueos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Plaqueos</span></a>
        </li>

    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i>
        <span>Reportes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="pages/tables/simple.html">
                <i class="fa fa-circle-o"></i>Resúmen</a>
            </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Físico
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Contable
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resumen Detallado
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Ajuste depreciacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Archivo Maestro
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Orden de Trabajo
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Componetizacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Catalogos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Avaluos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Civil
            </a>
        </li>
    </ul>
</li>
