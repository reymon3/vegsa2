<li class="{{ Request::is('home') ? 'active' : '' }} nuno">
    <a href="{!! url('/') !!}" class="nuno">
        <i class="fa fa-dashboard" aria-hidden="true"></i>
        <span>Home</span>
    </a>
</li>

<li class="treeview
    {{ Request::is('ordenTrabajos') ? 'active' : '' }}
    {{ Request::is('cotizacions') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-money"></i>
        <span>Ventas</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('cotizacions*') ? 'active' : '' }}">
            <a href="{!! route('cotizacions.index') !!}"><i class="fa fa-circle-o"></i><span>Cotizaciones</span></a>
        </li>
        <li class="{{ Request::is('ordenTrabajos*') ? 'active' : '' }}">
            <a href="{!! route('ordenTrabajos.index') !!}">
                <i class="fa fa-circle-o"></i>Orden de trabajo
            </a>
        </li>
    </ul>
</li>

<li class="treeview
    {{ Request::is('users*') ? 'active' : '' }}
    {{ Request::is('areas*') ? 'active' : '' }}
    {{ Request::is('empleados*') ? 'active' : '' }}
    {{ Request::is('puestos*') ? 'active' : '' }}
    {{ Request::is('rubros*') ? 'active' : '' }}
    {{ Request::is('tipoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoCambios*') ? 'active' : '' }}
    {{ Request::is('clientes*') ? 'active' : '' }}
    {{ Request::is('servicios*') ? 'active' : '' }}
    {{ Request::is('normas*') ? 'active' : '' }}
    {{ Request::is('propositoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoPlaqueos*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-folder"></i>
        <span>Catalogos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('clientes*') ? 'active' : '' }}">
            <a href="{!! route('clientes.index') !!}"><i class="fa fa-circle-o"></i><span>Clientes</span></a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Servicios Requeridos
            </a>
        </li>
        <li class="{{ Request::is('tipoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('tipoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Avaluos</span></a>
        </li>

        <li class="{{ Request::is('tipoCambios*') ? 'active' : '' }}">
            <a href="{!! route('tipoCambios.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Cambios</span></a>
        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i>
        <span>Reportes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resúmen</a>
            </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Físico
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Contable
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resumen Detallado
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Ajuste depreciacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Archivo Maestro
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Orden de Trabajo
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Componetizacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Catalogos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Avaluos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Civil
            </a>
        </li>
    </ul>
</li>
