<li class="{{ Request::is('home') ? 'active' : '' }} nuno">
    <a href="{!! url('/') !!}" class="nuno">
        <i class="fa fa-dashboard" aria-hidden="true"></i>
        <span>Home</span>
    </a>
</li>

<li class="{{ Request::is('ordenTrabajos*') ? 'active' : '' }}">
    <a href="{!! route('ordenTrabajos.index') !!}">
        <i class="fa fa-money"></i>
        <span>Orden de trabajo</span>

    </a>
</li>


<li class="treeview

    {{ Request::is('ordenTrabajoBaseContables*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-truck"></i>
        <span>Maquinaria</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ordenTrabajoBaseContables*') ? 'active' : '' }}">
            <a href="{!! url('ordenTrabajoBaseContables') !!}">
            <i class="fa fa-circle-o"></i>Inventario</a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Conciliación
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Componetización
            </a>
        </li>
    </ul>
</li>

<li class="treeview
    {{ Request::is('users*') ? 'active' : '' }}
    {{ Request::is('areas*') ? 'active' : '' }}
    {{ Request::is('empleados*') ? 'active' : '' }}
    {{ Request::is('puestos*') ? 'active' : '' }}
    {{ Request::is('rubros*') ? 'active' : '' }}
    {{ Request::is('tipoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoCambios*') ? 'active' : '' }}
    {{ Request::is('clientes*') ? 'active' : '' }}
    {{ Request::is('servicios*') ? 'active' : '' }}
    {{ Request::is('normas*') ? 'active' : '' }}
    {{ Request::is('propositoAvaluos*') ? 'active' : '' }}
    {{ Request::is('tipoPlaqueos*') ? 'active' : '' }}
">
    <a href="#">
        <i class="fa fa-folder"></i>
        <span>Catalogos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('tipoPlaqueos*') ? 'active' : '' }}">
            <a href="{!! route('tipoPlaqueos.index') !!}"><i class="fa fa-circle-o"></i><span>Tipo Plaqueos</span></a>
        </li>
        <li class="{{ Request::is('propositoAvaluos*') ? 'active' : '' }}">
            <a href="{!! route('propositoAvaluos.index') !!}"><i class="fa fa-circle-o"></i><span>Proposito Avaluos</span></a>
        </li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i>
        <span>Reportes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resúmen</a>
            </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Físico
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Sobrante Contable
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Resumen Detallado
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Ajuste depreciacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Archivo Maestro
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Orden de Trabajo
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Componetizacion
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Catalogos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Avaluos
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>Civil
            </a>
        </li>
    </ul>
</li>
