<aside class="main-sidebar" id="sidebar-wrapper">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <br />
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{!! route('empleados.index') !!}">
                    <i class="fa fa-shield fa-lg"></i><span> ROL: {{ @Auth::user()->roles[0]->name}}</span>
                </a>
            </li>
        </ul>
        <hr />

        <ul class="sidebar-menu" data-widget="tree">
            @role('admin')
                @include('layouts.menu.admin')
            @endrole
            @role('gerente_asignador')
                @include('layouts.menu.gerente_asignador')
            @endrole
            @role('ejecutivo')
                @include('layouts.menu.ejecutivo')
            @endrole
            @role('supervisor_avaluos')
                @include('layouts.menu.supervisor_avaluos')
            @endrole
            @role('supervisor_civil')
                @include('layouts.menu.supervisor_civil')
            @endrole
            @role('supervisor_maquinaria')
                @include('layouts.menu.supervisor_maquinaria')
            @endrole
            @role('supervisor_ventas')
                @include('layouts.menu.supervisor_ventas')
            @endrole

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
