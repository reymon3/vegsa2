@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Norma
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoNorma, ['route' => ['ordenTrabajoNormas.update', $ordenTrabajoNorma->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_normas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection