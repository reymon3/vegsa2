<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}
<!-- Norma Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('norma_id', 'Norma:') !!}
    {!! Form::select('norma_id', $normas, null, [
        'class' => 'form-control',
        'id'=>'norma_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoNormas.index') !!}" onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar</a>
</div>
