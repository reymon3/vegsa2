@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Proposito Avaluo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoPropositoAvaluo, ['route' => ['ordenTrabajoPropositoAvaluos.update', $ordenTrabajoPropositoAvaluo->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_proposito_avaluos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection