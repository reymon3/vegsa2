{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}
<!-- Orden Trabajo Id Field -->


<!-- Proposito Avaluo Id Field -->


<div class="form-group col-sm-6">
    {!! Form::label('proposito_avaluo_id', 'Proposito Avaluo:') !!}
    {!! Form::select('proposito_avaluo_id', $propositos, null, [
        'class' => 'form-control',
        'id'=>'proposito_avaluo_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoPropositoAvaluos.index') !!}"  onclick="closeFancyboxIframe(); " class="btn btn-default">Cancelar</a>
</div>
