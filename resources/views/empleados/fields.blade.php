<!-- Area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area:') !!}
    <?php $area = isset($empleado->area_id) ? $empleado->area_id : null; ?>
    {!!
        Form::select('area_id', $areas, $area, [
            'class' => 'form-control',
            'placeholder' => 'Seleccione...'
        ])
    !!}
</div>

<!-- Puesto Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('puesto_id', 'Puesto:') !!}
    <?php $puesto = isset($empleado->puesto_id) ? $empleado->puesto_id : null; ?>
    {!!
        Form::select('puesto_id', $puestos, $puesto, [
            'class' => 'form-control',
            'placeholder' => 'Seleccione...'
        ])
    !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::text('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Rfc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rfc', 'RFC:') !!}
    {!! Form::text('rfc', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('empleados.index') !!}" class="btn btn-default">Cancelar</a>
</div>
