<!-- Area Id Field -->
<div class="form-group">
    {!! Form::label('area_id', 'Area Id:') !!}
    <p>{!! $empleados->area_id !!}</p>
</div>

<!-- Puesto Id Field -->
<div class="form-group">
    {!! Form::label('puesto_id', 'Puesto Id:') !!}
    <p>{!! $empleados->puesto_id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $empleados->nombre !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $empleados->correo !!}</p>
</div>

<!-- Rfc Field -->
<div class="form-group">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{!! $empleados->rfc !!}</p>
</div>

<!-- Blame Id Field -->
<div class="form-group">
    {!! Form::label('blame_id', 'Blame Id:') !!}
    <p>{!! $empleados->blame_id !!}</p>
</div>

