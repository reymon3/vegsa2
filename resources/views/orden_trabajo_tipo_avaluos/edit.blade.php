@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Orden Trabajo Tipo Avaluo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ordenTrabajoTipoAvaluo, ['route' => ['ordenTrabajoTipoAvaluos.update', $ordenTrabajoTipoAvaluo->id], 'method' => 'patch']) !!}

                        @include('orden_trabajo_tipo_avaluos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection