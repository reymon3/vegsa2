<!-- Orden Trabajo Id Field -->
{!! Form::hidden('orden_trabajo_id', app('request')->input('orden_trabajo_id')) !!}

<div class="form-group col-sm-6">
    {!! Form::label('tipo_avaluo_id', 'Tipo de Avalúo:') !!}
    {!! Form::select('tipo_avaluo_id', $tiposAvaluo, null, [
        'class' => 'form-control',
        'id'=>'tipo_avaluo_id',
        'placeholder' => 'Seleccione...'
    ])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ordenTrabajoTipoAvaluos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
