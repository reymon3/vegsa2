<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoTipoAvaluo;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoTipoAvaluoRepository
 * @package App\Repositories
 * @version December 5, 2019, 8:17 am CST
*/

class OrdenTrabajoTipoAvaluoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'tipo_avaluo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoTipoAvaluo::class;
    }
}
