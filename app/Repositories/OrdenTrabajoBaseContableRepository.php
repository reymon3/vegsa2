<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoBaseContable;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoBaseContableRepository
 * @package App\Repositories
 * @version November 14, 2019, 4:18 pm CST
*/

class OrdenTrabajoBaseContableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'json_cabeceras',
        'json_excel',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoBaseContable::class;
    }
}
