<?php

namespace App\Repositories;

use App\Models\Civil;
use App\Repositories\BaseRepository;

/**
 * Class CivilRepository
 * @package App\Repositories
 * @version April 23, 2020, 10:24 am CDT
*/

class CivilRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'seccion_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Civil::class;
    }
}
