<?php

namespace App\Repositories;

use App\Models\Avaluo;
use App\Repositories\BaseRepository;

/**
 * Class AvaluoRepository
 * @package App\Repositories
 * @version April 23, 2020, 10:27 am CDT
*/

class AvaluoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Avaluo::class;
    }
}
