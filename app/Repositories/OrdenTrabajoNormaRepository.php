<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoNorma;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoNormaRepository
 * @package App\Repositories
 * @version November 27, 2019, 2:03 pm CST
*/

class OrdenTrabajoNormaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'norma_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoNorma::class;
    }
}
