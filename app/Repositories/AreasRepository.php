<?php

namespace App\Repositories;

use App\Models\Areas;
use App\Repositories\BaseRepository;

/**
 * Class AreasRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:23 am UTC
*/

class AreasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Areas::class;
    }
}
