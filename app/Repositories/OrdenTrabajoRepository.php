<?php

namespace App\Repositories;

use App\Models\OrdenTrabajo;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoRepository
 * @package App\Repositories
 * @version November 8, 2019, 10:54 am CST
*/

class OrdenTrabajoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cotizacion_id',
        'fecha_orden_trabajo',
        'fecha_arranque',
        'fecha_estimada_entrega',
        'num_orden_trabajo',
        'localidades',
        'observaciones',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajo::class;
    }
}
