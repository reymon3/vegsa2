<?php

namespace App\Repositories;

use App\Models\Conciliacion;
use App\Repositories\BaseRepository;

/**
 * Class ConciliacionRepository
 * @package App\Repositories
 * @version August 6, 2020, 9:34 am CDT
*/

class ConciliacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Conciliacion::class;
    }
}
