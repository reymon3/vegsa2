<?php

namespace App\Repositories;

use App\Models\Normas;
use App\Repositories\BaseRepository;

/**
 * Class NormasRepository
 * @package App\Repositories
 * @version November 5, 2019, 7:01 am CST
*/

class NormasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Normas::class;
    }
}
