<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoServicio;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoServicioRepository
 * @package App\Repositories
 * @version November 27, 2019, 10:43 am CST
*/

class OrdenTrabajoServicioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'servicio_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoServicio::class;
    }
}
