<?php

namespace App\Repositories;

use App\Models\ConciliacionDetalle;
use App\Repositories\BaseRepository;

/**
 * Class ConciliacionDetalleRepository
 * @package App\Repositories
 * @version August 6, 2020, 9:35 am CDT
*/

class ConciliacionDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'conciliacion_id',
        'folio',
        'placa_actual',
        'placa_anterior',
        'placa_homologada',
        'placa_auxiliar_1',
        'placa_auxiliar_2',
        'desglose',
        'rubro_id',
        'rubro',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'no_mantenimiento',
        'area',
        'ubicacion',
        'usuario_conteo',
        'piso',
        'planta',
        'porcentaje_depreciacion_fisica',
        'anio_fabricacion',
        'observaciones',
        'foto_1',
        'foto_2',
        'foto_3',
        'vrn_anterior',
        'porcentaje_dep_ajuste',
        'dep_acum_ajuste',
        'vnr_ajuste',
        'vur_ajuste',
        'da_ajuste',
        'porcentaje_ajuste_inst',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'vur',
        'da',
        'vut',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ConciliacionDetalle::class;
    }
}
