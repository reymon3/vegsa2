<?php

namespace App\Repositories;

use App\Models\Puestos;
use App\Repositories\BaseRepository;

/**
 * Class PuestosRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:29 am UTC
*/

class PuestosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Puestos::class;
    }
}
