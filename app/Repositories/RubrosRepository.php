<?php

namespace App\Repositories;

use App\Models\Rubros;
use App\Repositories\BaseRepository;

/**
 * Class RubrosRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:31 am UTC
*/

class RubrosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id',
        'update_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rubros::class;
    }
}
