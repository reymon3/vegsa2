<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoBaseContableDetalle;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoBaseContableDetalleRepository
 * @package App\Repositories
 * @version February 24, 2020, 5:46 pm CST
*/

class OrdenTrabajoBaseContableDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_base_contable_id',
        'json_fila',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoBaseContableDetalle::class;
    }
}
