<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoRubroAlcance;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoRubroAlcanceRepository
 * @package App\Repositories
 * @version December 5, 2019, 8:11 am CST
*/

class OrdenTrabajoRubroAlcanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'rubro_alcance_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoRubroAlcance::class;
    }
}
