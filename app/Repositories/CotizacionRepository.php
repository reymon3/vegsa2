<?php

namespace App\Repositories;

use App\Models\Cotizacion;
use App\Repositories\BaseRepository;

/**
 * Class CotizacionRepository
 * @package App\Repositories
 * @version November 5, 2019, 7:26 pm CST
*/

class CotizacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'vendedor_id',
        'num_cotizacion',
        'fecha_cotizacion',
        'ingreso_factura',
        'forma_pago'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cotizacion::class;
    }
}
