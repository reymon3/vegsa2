<?php

namespace App\Repositories;

use App\Models\ArchivoMaestro;
use App\Repositories\BaseRepository;

/**
 * Class ArchivoMaestroRepository
 * @package App\Repositories
 * @version April 29, 2020, 7:58 am CDT
*/

class ArchivoMaestroRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ArchivoMaestro::class;
    }
}
