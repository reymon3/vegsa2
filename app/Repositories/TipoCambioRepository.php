<?php

namespace App\Repositories;

use App\Models\TipoCambio;
use App\Repositories\BaseRepository;

/**
 * Class TipoCambioRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:34 am UTC
*/

class TipoCambioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoCambio::class;
    }
}
