<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoTipoPlaqueo;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoTipoPlaqueoRepository
 * @package App\Repositories
 * @version December 5, 2019, 8:15 am CST
*/

class OrdenTrabajoTipoPlaqueoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'tipo_plaqueo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoTipoPlaqueo::class;
    }
}
