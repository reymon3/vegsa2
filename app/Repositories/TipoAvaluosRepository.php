<?php

namespace App\Repositories;

use App\Models\TipoAvaluos;
use App\Repositories\BaseRepository;

/**
 * Class TipoAvaluosRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:32 am UTC
*/

class TipoAvaluosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoAvaluos::class;
    }
}
