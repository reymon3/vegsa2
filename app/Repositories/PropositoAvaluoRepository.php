<?php

namespace App\Repositories;

use App\Models\PropositoAvaluo;
use App\Repositories\BaseRepository;

/**
 * Class PropositoAvaluoRepository
 * @package App\Repositories
 * @version November 5, 2019, 7:05 am CST
*/

class PropositoAvaluoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropositoAvaluo::class;
    }
}
