<?php

namespace App\Repositories;

use App\Models\Cartas;
use App\Repositories\BaseRepository;

/**
 * Class CartasRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:31 am UTC
*/

class CartasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'nombre',
        'blame_id',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cartas::class;
    }
}
