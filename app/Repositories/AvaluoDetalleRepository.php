<?php

namespace App\Repositories;

use App\Models\AvaluoDetalle;
use App\Repositories\BaseRepository;

/**
 * Class AvaluoDetalleRepository
 * @package App\Repositories
 * @version April 23, 2020, 10:29 am CDT
*/

class AvaluoDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'avaluo_id',
        'asset',
        'rubro',
        'desglose',
        'asset_descripcion',
        'acquis_val',
        'accum_dep',
        'book_val',
        'placa_anterior_homogenea',
        'placa_anterior',
        'edificio',
        'piso',
        'rubro_vegsa',
        'catalogo',
        'descripcion_vegsa',
        'marca',
        'modelo',
        'serie',
        'area',
        'localidad',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'vur',
        'da',
        'vut',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AvaluoDetalle::class;
    }
}
