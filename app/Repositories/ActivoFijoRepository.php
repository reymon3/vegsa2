<?php

namespace App\Repositories;

use App\Models\ActivoFijo;
use App\Repositories\BaseRepository;

/**
 * Class ActivoFijoRepository
 * @package App\Repositories
 * @version July 26, 2020, 9:57 pm CDT
*/

class ActivoFijoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rubro_id',
        'nombre',
        'vut',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActivoFijo::class;
    }
}
