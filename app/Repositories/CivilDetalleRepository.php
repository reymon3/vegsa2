<?php

namespace App\Repositories;

use App\Models\CivilDetalle;
use App\Repositories\BaseRepository;

/**
 * Class CivilDetalleRepository
 * @package App\Repositories
 * @version April 23, 2020, 10:25 am CDT
*/

class CivilDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'avaluo_id',
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CivilDetalle::class;
    }
}
