<?php

namespace App\Repositories;

use App\Models\ArchivoMaestroDetalle;
use App\Repositories\BaseRepository;

/**
 * Class ArchivoMaestroDetalleRepository
 * @package App\Repositories
 * @version April 29, 2020, 11:18 am CDT
*/

class ArchivoMaestroDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'archivo_maestro_id',
        'company_code',
        'asset_class',
        'asset',
        'subnumber',
        'name',
        'additional_description',
        'serial_number',
        'inventory_number',
        'capitalized',
        'deactivation_on',
        'cost_center',
        'evaluation_group_1',
        'evaluation_group_2',
        'text',
        'country_key',
        'country_name',
        'ad_value',
        'dprn_rate_01',
        'dprn_value_01',
        'depreciacion_acumulada_01',
        'book_value_for_area_01',
        'dprn_rate_02',
        'dprn_value_02',
        'depreciacion_acumulada_02',
        'book_value_for_area_02',
        'dprn_rate_03',
        'dprn_value_03',
        'depreciacion_acumulada_03',
        'dprn_rate_04',
        'dprn_value_04',
        'depreciacion_acumulada_04',
        'dprn_rate_05',
        'dprn_value_05',
        'depreciacion_acumulada_05',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ArchivoMaestroDetalle::class;
    }
}
