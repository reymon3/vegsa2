<?php

namespace App\Repositories;

use App\Models\FichaTecnica;
use App\Repositories\BaseRepository;

/**
 * Class FichaTecnicaRepository
 * @package App\Repositories
 * @version August 20, 2020, 2:23 am CDT
*/

class FichaTecnicaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FichaTecnica::class;
    }
}
