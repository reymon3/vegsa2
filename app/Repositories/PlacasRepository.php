<?php

namespace App\Repositories;

use App\Models\Placas;
use App\Repositories\BaseRepository;

/**
 * Class RubrosRepository
 * @package App\Repositories
 * @version November 4, 2019, 5:31 am UTC
*/

class PlacasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'placa',
        'placa_pivote',
        'blame_id',
        'update_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Placas::class;
    }
}
