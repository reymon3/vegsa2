<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoRazonSocial;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoRazonSocialRepository
 * @package App\Repositories
 * @version November 14, 2019, 10:43 am CST
*/

class OrdenTrabajoRazonSocialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'cliente_razon_social_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoRazonSocial::class;
    }
}
