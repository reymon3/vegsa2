<?php

namespace App\Repositories;

use App\Models\FichaTecnicaDetalle;
use App\Repositories\BaseRepository;

/**
 * Class FichaTecnicaDetalleRepository
 * @package App\Repositories
 * @version August 20, 2020, 2:23 am CDT
*/

class FichaTecnicaDetalleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ficha_tecnica_id',
        'asset',
        'rubro',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'anio_fabricacion',
        'porcentaje_depreciacion_fisica',
        'fecha_adquisicion',
        'area',
        'caso',
        'valor_cotizado_1',
        'moneda_1',
        'tipo_cambio_1',
        'no_piezas_1',
        'valor_cotizado_2',
        'moneda_2',
        'tipo_cambio_2',
        'no_piezas_2',
        'valor_equipo_usado',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'link_1',
        'link_2',
        'observaciones',
        'cotizo',
        'vcf',
        'vur',
        'da',
        'vut',
        'gastos_instalacion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FichaTecnicaDetalle::class;
    }
}
