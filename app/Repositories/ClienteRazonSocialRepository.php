<?php

namespace App\Repositories;

use App\Models\ClienteRazonSocial;
use App\Repositories\BaseRepository;

/**
 * Class ClienteRazonSocialRepository
 * @package App\Repositories
 * @version November 5, 2019, 12:03 am UTC
*/

class ClienteRazonSocialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cliente_id',
        'nombre',
        'rfc',
        'razon_social',
        'domicilio',
        'nombre_contacto',
        'puesto_contacto',
        'celular_contacto',
        'telefono_contacto',
        'email_contacto',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ClienteRazonSocial::class;
    }
}
