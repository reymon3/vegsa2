<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoConteo;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoConteoRepository
 * @package App\Repositories
 * @version February 10, 2020, 4:29 pm CST
*/

class OrdenTrabajoConteoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'folio',
        'rubro_id',
        'activo',
        'marca',
        'modelo',
        'serie',
        'area',
        'ubicacion',
        'piso',
        'observaciones',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'vur',
        'da',
        'vut',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoConteo::class;
    }
}
