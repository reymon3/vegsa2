<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoEmpleado;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoEmpleadoRepository
 * @package App\Repositories
 * @version November 29, 2019, 10:30 am CST
*/

class OrdenTrabajoEmpleadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'empleado_id',
        'rol_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoEmpleado::class;
    }
}
