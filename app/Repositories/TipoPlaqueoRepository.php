<?php

namespace App\Repositories;

use App\Models\TipoPlaqueo;
use App\Repositories\BaseRepository;

/**
 * Class TipoPlaqueoRepository
 * @package App\Repositories
 * @version November 5, 2019, 7:24 am CST
*/

class TipoPlaqueoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoPlaqueo::class;
    }
}
