<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoDocumento;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoDocumentoRepository
 * @package App\Repositories
 * @version January 7, 2020, 9:34 am CST
*/

class OrdenTrabajoDocumentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'ruta',
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoDocumento::class;
    }
}
