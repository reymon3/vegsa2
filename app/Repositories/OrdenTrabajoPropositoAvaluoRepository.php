<?php

namespace App\Repositories;

use App\Models\OrdenTrabajoPropositoAvaluo;
use App\Repositories\BaseRepository;

/**
 * Class OrdenTrabajoPropositoAvaluoRepository
 * @package App\Repositories
 * @version December 5, 2019, 8:02 am CST
*/

class OrdenTrabajoPropositoAvaluoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'orden_trabajo_id',
        'proposito_avaluo_id',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdenTrabajoPropositoAvaluo::class;
    }
}
