<?php

namespace App\Repositories;

use App\Models\CotizacionServicio;
use App\Repositories\BaseRepository;

/**
 * Class CotizacionServicioRepository
 * @package App\Repositories
 * @version November 7, 2019, 2:07 am CST
*/

class CotizacionServicioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'cotizacion_id',
        'servicio_id',
        'importe',
        'iva',
        'blame_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CotizacionServicio::class;
    }
}
