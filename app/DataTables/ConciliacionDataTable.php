<?php

namespace App\DataTables;

use App\Models\Conciliacion;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ConciliacionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'conciliacions.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Conciliacion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Conciliacion $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[0, 'asc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [ 'title' => 'ID'],
            // 'orden_trabajo_id',
            'ordenTrabajo'=> [
                'title' =>  'Num. Orden Trabajo',
                'data' => 'orden_trabajo.num_orden_trabajo',
                'name'=>'ordenTrabajo.num_orden_trabajo',
                'orderable' => false,
                'searchable' => true,
            ],
            // 'blame_id',
            'blame'=> [
                'title' =>  'Usuario Alta',
                'name'=>'blame.name',
                'data' => 'blame.name',
                'orderable' => true,
                'searchable' => true,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'conciliacionsdatatable_' . time();
    }
}
