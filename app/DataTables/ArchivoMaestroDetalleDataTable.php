<?php

namespace App\DataTables;

use App\Models\ArchivoMaestroDetalle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ArchivoMaestroDetalleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'archivo_maestro_detalles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ArchivoMaestroDetalle $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ArchivoMaestroDetalle $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[0, 'desc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'archivo_maestro_id',
            'text' => ['title' => 'Estatus' ],
            'company_code',
            'asset_class',
            'asset',
            'subnumber',
            'name',
            'additional_description',
            'serial_number',
            'inventory_number',
            'capitalized',
            'deactivation_on',
            'cost_center',
            'evaluation_group_1',
            'evaluation_group_2',
            'country_key',
            'country_name',
            'ad_value',
            'dprn_rate_01',
            'dprn_value_01',
            'depreciacion_acumulada_01',
            'book_value_for_area_01',
            // 'dprn_rate_02',
            // 'dprn_value_02',
            // 'depreciacion_acumulada_02',
            // 'book_value_for_area_02',
            // 'dprn_rate_03',
            // 'dprn_value_03',
            // 'depreciacion_acumulada_03',
            // 'dprn_rate_04',
            // 'dprn_value_04',
            // 'depreciacion_acumulada_04',
            'dprn_rate_05',
            'dprn_value_05',
            'depreciacion_acumulada_05',
            // 'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'archivo_maestro_detallesdatatable_' . time();
    }
}
