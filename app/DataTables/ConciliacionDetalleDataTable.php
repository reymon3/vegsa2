<?php

namespace App\DataTables;

use App\Models\ConciliacionDetalle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ConciliacionDetalleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'conciliacion_detalles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ConciliacionDetalle $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ConciliacionDetalle $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[0, 'asc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'conciliacion_id',
            'folio',
            'placa_actual',
            'placa_anterior',
            'placa_homologada',
            'placa_auxiliar_1',
            'placa_auxiliar_2',
            'desglose',
            // 'rubro_id',
            'rubro',
            'catalogo',
            'descripcion',
            'marca',
            'modelo',
            'serie',
            'no_mantenimiento',
            'area',
            'ubicacion',
            'usuario_conteo',
            'piso',
            'planta',
            'porcentaje_depreciacion_fisica',
            'anio_fabricacion',
            'observaciones',
            'foto_1',
            'foto_2',
            'foto_3',
            'vrn_anterior',
            'porcentaje_dep_ajuste',
            'dep_acum_ajuste',
            'vnr_ajuste',
            'vur_ajuste',
            'da_ajuste',
            'porcentaje_ajuste_inst',
            'vrn',
            'porcentaje_dep',
            'dep_acum',
            'vnr',
            'vur',
            'da',
            'vut',
            // 'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'conciliacion_detallesdatatable_' . time();
    }
}
