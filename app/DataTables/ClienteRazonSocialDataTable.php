<?php

namespace App\DataTables;

use App\Models\ClienteRazonSocial;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ClienteRazonSocialDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'cliente_razon_socials.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ClienteRazonSocial $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ClienteRazonSocial $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[0, 'asc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'cliente_id',
            'nombre',
            'rfc',
            'razon_social',
            'domicilio',
            'nombre_contacto',
            'puesto_contacto',
            'celular_contacto',
            'telefono_contacto',
            'email_contacto',
            'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cliente_razon_socialsdatatable_' . time();
    }
}
