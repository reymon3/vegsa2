<?php

namespace App\DataTables;

use App\Models\OrdenTrabajo;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Carbon\Carbon;

class OrdenTrabajoCivilDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->filterColumn('fecha_orden_trabajo', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha_orden_trabajo,'%d/%m/%y') like ?", ["%$keyword%"]);
        });

        $dataTable->filterColumn('fecha_arranque', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha_arranque,'%d/%m/%y') like ?", ["%$keyword%"]);
        });

        $dataTable->filterColumn('fecha_estimada_entrega', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha_estimada_entrega,'%d/%m/%y') like ?", ["%$keyword%"]);
        });

        $dataTable->editColumn('created_at', function ($model) {
            return $model->created_at
                ? with(new Carbon($model->created_at))->format('d/m/Y')
                : '';
        })->filterColumn('created_at', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%y') like ?", ["%$keyword%"]);
        });

        return $dataTable->addColumn('action', 'civil.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\OrdenTrabajo $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(OrdenTrabajo $model)
    {
        // dd($model->newQuery());
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[5, 'desc'], [0, 'desc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'cotizacion_id',
            // 'id',
            'num_orden_trabajo',
            // 'cotizacion'=> [
            //     'title' =>  'Cotización',
            //     'name'=>'cotizacion.num_cotizacion',
            //     'data' => 'cotizacion.num_cotizacion',
            //     'orderable' => false,
            //     'searchable' => false,
            // ],
            'cliente'=> [
                'title' =>  'Cliente',
                'name'=>'cotizacion.cliente.nombre',
                'data' => 'cotizacion.cliente.nombre',
                'orderable' => false,
                'searchable' => false,
            ],
            // 'vendedor'=> [
            //     'title' =>  'Vendedor',
            //     'name'=>'cotizacion.vendedor.name',
            //     'data' => 'cotizacion.vendedor.name',
            //     'orderable' => false,
            //     'searchable' => false,
            // ],
            'fecha_orden_trabajo' => [
                'class' => 'alineacion-derecha'
            ],
            'fecha_arranque' => [
                'class' => 'alineacion-derecha'
            ],
            'fecha_estimada_entrega' => [
                'class' => 'alineacion-derecha'
            ],
            'created_at' => [
                'title' => 'Fecha Alta',
                'class' => 'alineacion-derecha'
            ],
            // 'localidades',
            // 'observaciones',
            // 'blame_id'
            // 'blame'=> [
            //     'title' =>  'Usuario Alta',
            //     'name'=>'blame.name',
            //     'data' => 'blame.name',
            //     'orderable' => false,
            //     'searchable' => false,
            // ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orden_trabajosdatatable_' . time();
    }
}
