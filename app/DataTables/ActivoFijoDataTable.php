<?php

namespace App\DataTables;

use App\Models\ActivoFijo;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ActivoFijoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'activo_fijos.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ActivoFijo $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ActivoFijo $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[1, 'asc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => [
                'title' =>  'ID',
            ],
            // 'rubro_id',
            'rubro' => [
                'title' =>  'Rubro',
                'name'=>'rubro.nombre',
                'data' => 'rubro.nombre',
                'orderable' => false,
                'searchable' => true,
            ],
            'nombre' => [
                'title' =>  'Catálogo',
            ],
            'vut' => [
                'title' =>  'VUT',
                'width' => '70px',
            ],
            'porcentaje_gasto_instalacion' => [
                'title' =>  '% de Gastos e Instalación',
                'width' => '70px',
            ],
            'blame' => [
                'title' =>  'Usuario Alta',
                'name'=>'blame.name',
                'data' => 'blame.name',
                'orderable' => false,
                'searchable' => false,
            ],
            // 'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'activo_fijosdatatable_' . time();
    }
}
