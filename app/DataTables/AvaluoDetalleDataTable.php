<?php

namespace App\DataTables;

use App\Models\AvaluoDetalle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class AvaluoDetalleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'avaluo_detalles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\AvaluoDetalle $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AvaluoDetalle $model)
    {
        if( !is_null($this->avaluo_id)){

            $model = AvaluoDetalle::where('avaluo_id', $this->avaluo_id);
            return $this->applyScopes($model);

        }

        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            // ->addAction([
            //     'width' => '70px',
            //     'title' => '',
            //     'class' => 'text-center',
            //     'printable' => false
            // ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                // 'order'   => [[0, 'asc']],
                'order'   => [],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'avaluo_id',
            'asset',
            'rubro',
            // 'catalogo',
            'catalogo'=> [
                'title' =>  'Catálogo',
                // 'name'=>'blame.name',
                // 'data' => 'blame.name',
                // 'orderable' => true,
                // 'searchable' => true,
            ],
            'marca',
            'modelo',
            'serie',
            // 'vrn',
            'vrn' => [
                'title' =>  'VRN',
            ],
            'porcentaje_dep' => [
                'title' =>  '% Dep',
            ],
            'dep_acum',
            'vnr' => [
                'title' =>  'VNR',
            ],
            'vut' => [
                'title' =>  'VUT',
            ],
            'gastos_instalacion' => [
                'title' =>  '% de Gastos de Instalacion',
            ],
            'vur' => [
                'title' =>  'VUR',
            ],
            'da' => [
                'title' =>  'DA',
            ],
            'vcf' => [
                'title' =>  'VCF',
            ],
            // 'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'avaluo_detallesdatatable_' . time();
    }
}
