<?php

namespace App\DataTables;

use App\Models\FichaTecnicaDetalle;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class FichaTecnicaDetalleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'ficha_tecnica_detalles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\FichaTecnicaDetalle $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(FichaTecnicaDetalle $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'ficha_tecnica_id',
            'asset',
            'rubro',
            'catalogo',
            'descripcion',
            'marca',
            'modelo',
            'serie',
            'anio_fabricacion',
            'porcentaje_depreciacion_fisica',
            'fecha_adquisicion',
            'area',
            'caso',
            'valor_cotizado_1',
            'moneda_1',
            'tipo_cambio_1',
            'no_piezas_1',
            'valor_cotizado_2',
            'moneda_2',
            'tipo_cambio_2',
            'no_piezas_2',
            'valor_equipo_usado',
            'vrn',
            'porcentaje_dep',
            'dep_acum',
            'vnr',
            'link_1',
            'link_2',
            'observaciones',
            'cotizo',
            'vcf',
            'vur',
            'da',
            'vut',
            'gastos_instalacion',
            'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ficha_tecnica_detallesdatatable_' . time();
    }
}
