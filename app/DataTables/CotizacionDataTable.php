<?php

namespace App\DataTables;

use App\Models\Cotizacion;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Carbon\Carbon;

class CotizacionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->editColumn('ingreso_factura', function ($model) {
            return $model->ingreso_factura
                ? '$ ' . number_format($model->ingreso_factura, 2, '.', ',')
                : '';
        });

        $dataTable->filterColumn('fecha_cotizacion', function ($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha_cotizacion,'%d/%m/%y') like ?", ["%$keyword%"]);
        });

        return $dataTable->addColumn('action', 'cotizacions.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Cotizacion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Cotizacion $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[2, 'desc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // 'id',
            // 'cliente_id',
            'cliente'=> [
                'title' =>  'Cliente',
                'name'=>'cliente.nombre',
                'data' => 'cliente.nombre',
                'orderable' => true,
                'searchable' => true,
            ],
            // 'vendedor_id',
            'vendedor'=> [
                'title' =>  'Vendedor',
                'name'=>'vendedor.name',
                'data' => 'vendedor.name',
                'orderable' => true,
                'searchable' => true,
            ],
            'num_cotizacion' => [
                'title' => '# Cotización'
            ],
            'fecha_cotizacion' => [
                'class' => 'alineacion-derecha'
            ],
            'ingreso_factura' => [
                'title' => 'Importe',
                'class' => 'alineacion-derecha'
            ],
            'forma_pago',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cotizacionsdatatable_' . time();
    }
}
