<?php

namespace App\DataTables;

use App\Models\Reporteador;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

use Barryvdh\Snappy;

class ReporteadorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        // return $dataTable->addColumn('action', 'cartas.datatables_actions');
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Reporteador $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Reporteador $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            // ->columns($this->getColumns(), ':visible' )
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[1, 'asc']],
                'dom'       => 'Bfrtip',
                'buttons'   => [
                    [
                        'extend'=> 'print',
                        'exportOptions'=> [
                            'columns'=> 'visible'
                        ]
                        
                    ],
                    // 'excel','print','csv','colvis'
                    'csv','colvis'
                ],
                'columnDefs'=> [ [
                    'targets'=> -1,
                    'visible'=> false
                ]
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'archivo_maestro_id'=>[
                'title' => 'ID archivo Maestro'
            ],
            'company_code'=>[
                'title' => 'company_code'
            ],
            'asset_class'=>[
                'title' => 'asset_class'
            ],
            'asset'=>[
                'title' => 'asset'
            ],
            'subnumber'=>[
                'title' => 'subnumber'
            ],
            'name'=>[
                'title' => 'name'
            ],
            'additional_description'=>[
                'title' => 'additional_description'
            ],
            'serial_number'=>[
                'title' => 'serial_number'
            ],
            'blame'=> [
                'title' =>  'Usuario Alta',
                'name'=>'blame.name',
                'data' => 'blame.name',
                'orderable' => false,
                'searchable' => false,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reporteadordatatable_' . time();
    }
}
