<?php

namespace App\DataTables;

use App\Models\Servicio;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ServicioDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'servicios.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Servicio $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Servicio $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[0, 'asc']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'nombre',
            'descripcion'=> [
                'title' =>  'Descripción',
            ],
            'blame'=> [
                'title' =>  'Usuario Alta',
                'name'=>'blame.name',
                'data' => 'blame.name',
                'orderable' => false,
                'searchable' => false,
            ],
            // 'blame_id'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'serviciosdatatable_' . time();
    }
}
