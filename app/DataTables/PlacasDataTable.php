<?php

namespace App\DataTables;

use App\Models\Placas;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PlacasDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        // return $dataTable->addColumn('action', 'tablas.datatables_actions');
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Placas $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Placas $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction([
                'width' => '70px',
                'title' => '',
                'class' => 'text-center',
                'printable' => false
            ])
            ->parameters([
                "language" => [
                    "url" => "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
                ],
                'order'   => [[1, 'asc']],
                'dom'       => 'Bfrtip',
                'buttons'   => [
                    'excel',
                ],
                'scrollX' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'placa'=>[
                'title' => 'No. de Placa'
            ],
            'tipo'=>[
                'title' => 'Tipo'
            ],
            'estatus'=>[
                'title' => 'Estatus'
            ],
            'planta'=>[
                'title' => 'Planta'
            ],
            'placa_pivote'=> [
                'title' =>  'Cliente',
            ],
            // 'blame_id',
            // 'update_at'
            'blame'=> [
                'title' =>  'Usuario Alta',
                'name'=>'blame.name',
                'data' => 'blame.name',
                'orderable' => false,
                'searchable' => false,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'placasdatatable_' . time();
    }
}
