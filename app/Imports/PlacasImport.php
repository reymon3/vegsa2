<?php

namespace App\Imports;

use App\Models\PLacas;
use Maatwebsite\Excel\Concerns\ToModel;

class PlacasImport implements ToModel
{
   
    public function model(array $row)
    {
        return new Placas([
            'placa' => $row[0],
            'placa_pivote' => session('pivote'),
            'tipo' => $row[1],
            'estatus' => $row[2],
            'planta' => $row[3],

        ]);
        // return new Placas([
        //     'placa' => $row[0],
        //     'placa_pivote' => $row[1],
        //     'tipo' => $row[2],
        //     'estatus' => $row[3],
        //     'planta' => $row[4],

        // ]);
    }
}
