<?php

namespace App\Http\Controllers;

use App\DataTables\ArchivoMaestroDataTable;
use App\DataTables\ArchivoMaestroDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArchivoMaestroRequest;
use App\Http\Requests\UpdateArchivoMaestroRequest;
use App\Repositories\ArchivoMaestroRepository;
use App\Repositories\ArchivoMaestroDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use App\Models\ArchivoMaestro;
use App\Models\ArchivoMaestroDetalle;

class ArchivoMaestroController extends AppBaseController
{
    /** @var  ArchivoMaestroRepository */
    private $archivoMaestroRepository;

    public function __construct(
        ArchivoMaestroRepository $archivoMaestroRepo,
        ArchivoMaestroDetalleRepository $archivoMaestroDetalleRepository
    ){
        $this->archivoMaestroRepository = $archivoMaestroRepo;
        $this->archivoMaestroDetalleRepository = $archivoMaestroDetalleRepository;
    }

    /**
     * Display a listing of the ArchivoMaestro.
     *
     * @param ArchivoMaestroDataTable $archivoMaestroDataTable
     * @return Response
     */
    public function index(ArchivoMaestroDataTable $archivoMaestroDataTable)
    {
        return $archivoMaestroDataTable->render('archivo_maestros.index');
    }

    /**
     * Show the form for creating a new ArchivoMaestro.
     *
     * @return Response
     */
    public function create()
    {
        $ordenesTrabajo = \App\Models\OrdenTrabajo::doesntHave('archivoMaestro')->pluck('num_orden_trabajo', 'id');

        return view('archivo_maestros.create', compact('ordenesTrabajo'));
    }

    /**
     * Store a newly created ArchivoMaestro in storage.
     *
     * @param CreateArchivoMaestroRequest $request
     *
     * @return Response
     */
    public function store(CreateArchivoMaestroRequest $request)
    {
        $input = $request->all();

        $archivoMaestro = $this->archivoMaestroRepository->create($input);

        Flash::success('Archivo Maestro saved successfully.');

        return redirect(route('archivoMaestros.index'));
    }

    /**
     * Display the specified ArchivoMaestro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show(
        ArchivoMaestroDetalleDataTable $archivoMaestroDetalleDataTable,
        $id = null
    ){
        $archivoMaestro = $this->archivoMaestroRepository->find($id);

        if (empty($archivoMaestro)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('archivoMaestros.index'));
        }

        // return view('archivo_maestros.show')->with('archivoMaestro', $archivoMaestro);

        return $archivoMaestroDetalleDataTable->with([
                // Envia parametro
                'archivo_maestro_id' => $id
            ])->render('archivo_maestros.show', compact('archivoMaestro', 'archivoMaestroDetalleDataTable'));
    }

    /**
     * Show the form for editing the specified ArchivoMaestro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $archivoMaestro = $this->archivoMaestroRepository->find($id);

        if (empty($archivoMaestro)) {
            Flash::error('Archivo Maestro not found');

            return redirect(route('archivoMaestros.index'));
        }

        return view('archivo_maestros.edit')->with('archivoMaestro', $archivoMaestro);
    }

    /**
     * Update the specified ArchivoMaestro in storage.
     *
     * @param  int              $id
     * @param UpdateArchivoMaestroRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArchivoMaestroRequest $request)
    {
        $archivoMaestro = $this->archivoMaestroRepository->find($id);

        if (empty($archivoMaestro)) {
            Flash::error('Archivo Maestro not found');

            return redirect(route('archivoMaestros.index'));
        }

        $archivoMaestro = $this->archivoMaestroRepository->update($request->all(), $id);

        Flash::success('Archivo Maestro updated successfully.');

        return redirect(route('archivoMaestros.index'));
    }

    /**
     * Remove the specified ArchivoMaestro from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $archivoMaestro = $this->archivoMaestroRepository->find($id);

        if (empty($archivoMaestro)) {
            Flash::error('Archivo Maestro not found');

            return redirect(route('archivoMaestros.index'));
        }

        $this->archivoMaestroRepository->delete($id);

        Flash::success('Archivo Maestro deleted successfully.');

        return redirect(route('archivoMaestros.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function descargarArchivoMaestro($id)
    {

        // $input = $request->all();
        $archivo = ArchivoMaestro::find($id);
        $start = microtime(true);
        // dd($archivo);

        if (empty($archivo)) {
            Flash::error('Registro no encontrado');

            return redirect(route('avaluos.index'));
        }

        $file_name = 'Archivo-Maestro_'.$start.'.xls';

        // $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
        $aBody = ArchivoMaestroDetalle::where(
            'archivo_maestro_id', $archivo->id
            )->get();
        // dd($id, $avaluo, $aBody);

        $aEncabezado = [
            "company_code" => "company_code" ,
            "asset_class" => "asset_class" ,
            "asset" => "asset" ,
            "subnumber" => "subnumber" ,
            "name" => "name" ,
            "additional_description" => "additional_description" ,
            "inventory_number" => "inventory_number" ,
            "capitalized" => "capitalized" ,
            "deactivation_on" => "deactivation_on" ,
            "cost_center" => "cost_center" ,
            "evaluation_group_1" => "evaluation_group_1" ,
            "evaluation_group_2" => "evaluation_group_2" ,
            "text" => "text" ,
            "country_key" => "country_key" ,
            "country_name" => "country_name" ,
            "ad_value" => "ad_value" ,
            "dprn_rate_01" => "dprn_rate_01" ,
            "dprn_value_01" => "dprn_value_01" ,
            "depreciacion_acumulada_01" => "depreciacion_acumulada_01" ,
            "book_value_for_area_01" => "book_value_for_area_01" ,
            "dprn_rate_02" => "dprn_rate_02" ,
            "dprn_value_02" => "dprn_value_02" ,
            "depreciacion_acumulada_02" => "depreciacion_acumulada_02" ,
            "book_value_for_area_02" => "book_value_for_area_02" ,
            "dprn_rate_03" => "dprn_rate_03" ,
            "dprn_value_03" => "dprn_value_03" ,
            "depreciacion_acumulada_03" => "depreciacion_acumulada_03" ,
            "dprn_rate_04" => "dprn_rate_04" ,
            "dprn_value_04" => "dprn_value_04" ,
            "depreciacion_acumulada_04" => "depreciacion_acumulada_04" ,
            "dprn_rate_05" => "dprn_rate_05" ,
            "dprn_value_05" => "dprn_value_05",
            "depreciacion_acumulada_05" => "depreciacion_acumulada_05"
        ];

        $excel_file  = '<table border="1">';
        $excel_file .= '    <thead>';
        $excel_file .= '        <tr>';
            foreach ($aEncabezado as $clave => $valor) {
                $excel_file .= "<th>{$valor}</th>";
            }
        $excel_file .= '        </tr>';
        $excel_file .= '    </thead>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($aBody as $clave) {

            $excel_file .= '        <tr>';
            $excel_file .= "<th>{$clave->company_code}</th>";
            $excel_file .= "<th>{$clave->asset_class}</th>";
            $excel_file .= "<th>{$clave->asset}</th>";
            $excel_file .= "<th>{$clave->subnumber}</th>";
            $excel_file .= "<th>{$clave->name}</th>";
            $excel_file .= "<th>{$clave->additional_description}</th>";
            $excel_file .= "<th>{$clave->inventory_number}</th>";
            $excel_file .= "<th>{$clave->capitalized}</th>";
            $excel_file .= "<th>{$clave->deactivation_on}</th>";
            $excel_file .= "<th>{$clave->cost_center}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_2}</th>";
            $excel_file .= "<th>{$clave->text}</th>";
            $excel_file .= "<th>{$clave->country_key}</th>";
            $excel_file .= "<th>{$clave->country_name}</th>";
            $excel_file .= "<th>{$clave->ad_value}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_01}</th>";
            $excel_file .= "<th>{$clave->dprn_value_01}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_01}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_01}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";

            $excel_file .= '        </tr>';

        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

            // echo $excel_file;
            // die();

        return  $excel_file;

        return redirect(route('archivoMaestros.show', ['id' => $avaluo->id]));

    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function descargarSobranteFisico($id)
    {
        // $input = $request->all();
        $archivo = ArchivoMaestro::find($id);
        $start = microtime(true);
        // dd($archivo);

        if (empty($archivo)) {
            Flash::error('Registro no encontrado');

            return redirect(route('avaluos.index'));
        }

        $file_name = 'Archivo-Maestro_'.$start.'.xls';

        // $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
        $aBody = ArchivoMaestroDetalle::where([
                'archivo_maestro_id' => $archivo->id,
                'text' => 'Sobrante Fisico',
            ])->get();
        // dd($id, $avaluo, $aBody);

        $aEncabezado = [
            "company_code" => "company_code" ,
            "asset_class" => "asset_class" ,
            "asset" => "asset" ,
            "subnumber" => "subnumber" ,
            "name" => "name" ,
            "additional_description" => "additional_description" ,
            "inventory_number" => "inventory_number" ,
            "capitalized" => "capitalized" ,
            "deactivation_on" => "deactivation_on" ,
            "cost_center" => "cost_center" ,
            "evaluation_group_1" => "evaluation_group_1" ,
            "evaluation_group_2" => "evaluation_group_2" ,
            "text" => "text" ,
            "country_key" => "country_key" ,
            "country_name" => "country_name" ,
            "ad_value" => "ad_value" ,
            "dprn_rate_01" => "dprn_rate_01" ,
            "dprn_value_01" => "dprn_value_01" ,
            "depreciacion_acumulada_01" => "depreciacion_acumulada_01" ,
            "book_value_for_area_01" => "book_value_for_area_01" ,
            "dprn_rate_02" => "dprn_rate_02" ,
            "dprn_value_02" => "dprn_value_02" ,
            "depreciacion_acumulada_02" => "depreciacion_acumulada_02" ,
            "book_value_for_area_02" => "book_value_for_area_02" ,
            "dprn_rate_03" => "dprn_rate_03" ,
            "dprn_value_03" => "dprn_value_03" ,
            "depreciacion_acumulada_03" => "depreciacion_acumulada_03" ,
            "dprn_rate_04" => "dprn_rate_04" ,
            "dprn_value_04" => "dprn_value_04" ,
            "depreciacion_acumulada_04" => "depreciacion_acumulada_04" ,
            "dprn_rate_05" => "dprn_rate_05" ,
            "dprn_value_05" => "dprn_value_05",
            "depreciacion_acumulada_05" => "depreciacion_acumulada_05"
        ];

        $excel_file  = '<table border="1">';
        $excel_file .= '    <thead>';
        $excel_file .= '        <tr>';
            foreach ($aEncabezado as $clave => $valor) {
                $excel_file .= "<th>{$valor}</th>";
            }
        $excel_file .= '        </tr>';
        $excel_file .= '    </thead>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($aBody as $clave) {

            $excel_file .= '        <tr>';
            $excel_file .= "<th>{$clave->company_code}</th>";
            $excel_file .= "<th>{$clave->asset_class}</th>";
            $excel_file .= "<th>{$clave->asset}</th>";
            $excel_file .= "<th>{$clave->subnumber}</th>";
            $excel_file .= "<th>{$clave->name}</th>";
            $excel_file .= "<th>{$clave->additional_description}</th>";
            $excel_file .= "<th>{$clave->inventory_number}</th>";
            $excel_file .= "<th>{$clave->capitalized}</th>";
            $excel_file .= "<th>{$clave->deactivation_on}</th>";
            $excel_file .= "<th>{$clave->cost_center}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_2}</th>";
            $excel_file .= "<th>{$clave->text}</th>";
            $excel_file .= "<th>{$clave->country_key}</th>";
            $excel_file .= "<th>{$clave->country_name}</th>";
            $excel_file .= "<th>{$clave->ad_value}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_01}</th>";
            $excel_file .= "<th>{$clave->dprn_value_01}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_01}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_01}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";

            $excel_file .= '        </tr>';

        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

            // echo $excel_file;
            // die();

        return  $excel_file;

        return redirect(route('archivoMaestros.show', ['id' => $avaluo->id]));
        dd('sobrante fisico');
        //
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function descargarSobranteContable($id)
    {
        // $input = $request->all();
        $archivo = ArchivoMaestro::find($id);
        $start = microtime(true);
        // dd($archivo);

        if (empty($archivo)) {
            Flash::error('Registro no encontrado');

            return redirect(route('avaluos.index'));
        }

        $file_name = 'Archivo-Maestro_'.$start.'.xls';

        // $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
        $aBody = ArchivoMaestroDetalle::where([
            'archivo_maestro_id' => $archivo->id,
            'text' => 'Sobrante Contable',
            ])->get();
        // dd($id, $avaluo, $aBody);

        $aEncabezado = [
            "company_code" => "company_code" ,
            "asset_class" => "asset_class" ,
            "asset" => "asset" ,
            "subnumber" => "subnumber" ,
            "name" => "name" ,
            "additional_description" => "additional_description" ,
            "inventory_number" => "inventory_number" ,
            "capitalized" => "capitalized" ,
            "deactivation_on" => "deactivation_on" ,
            "cost_center" => "cost_center" ,
            "evaluation_group_1" => "evaluation_group_1" ,
            "evaluation_group_2" => "evaluation_group_2" ,
            "text" => "text" ,
            "country_key" => "country_key" ,
            "country_name" => "country_name" ,
            "ad_value" => "ad_value" ,
            "dprn_rate_01" => "dprn_rate_01" ,
            "dprn_value_01" => "dprn_value_01" ,
            "depreciacion_acumulada_01" => "depreciacion_acumulada_01" ,
            "book_value_for_area_01" => "book_value_for_area_01" ,
            "dprn_rate_02" => "dprn_rate_02" ,
            "dprn_value_02" => "dprn_value_02" ,
            "depreciacion_acumulada_02" => "depreciacion_acumulada_02" ,
            "book_value_for_area_02" => "book_value_for_area_02" ,
            "dprn_rate_03" => "dprn_rate_03" ,
            "dprn_value_03" => "dprn_value_03" ,
            "depreciacion_acumulada_03" => "depreciacion_acumulada_03" ,
            "dprn_rate_04" => "dprn_rate_04" ,
            "dprn_value_04" => "dprn_value_04" ,
            "depreciacion_acumulada_04" => "depreciacion_acumulada_04" ,
            "dprn_rate_05" => "dprn_rate_05" ,
            "dprn_value_05" => "dprn_value_05",
            "depreciacion_acumulada_05" => "depreciacion_acumulada_05"
        ];

        $excel_file  = '<table border="1">';
        $excel_file .= '    <thead>';
        $excel_file .= '        <tr>';
            foreach ($aEncabezado as $clave => $valor) {
                $excel_file .= "<th>{$valor}</th>";
            }
        $excel_file .= '        </tr>';
        $excel_file .= '    </thead>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($aBody as $clave) {

            $excel_file .= '        <tr>';
            $excel_file .= "<th>{$clave->company_code}</th>";
            $excel_file .= "<th>{$clave->asset_class}</th>";
            $excel_file .= "<th>{$clave->asset}</th>";
            $excel_file .= "<th>{$clave->subnumber}</th>";
            $excel_file .= "<th>{$clave->name}</th>";
            $excel_file .= "<th>{$clave->additional_description}</th>";
            $excel_file .= "<th>{$clave->inventory_number}</th>";
            $excel_file .= "<th>{$clave->capitalized}</th>";
            $excel_file .= "<th>{$clave->deactivation_on}</th>";
            $excel_file .= "<th>{$clave->cost_center}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_1}</th>";
            $excel_file .= "<th>{$clave->evaluation_group_2}</th>";
            $excel_file .= "<th>{$clave->text}</th>";
            $excel_file .= "<th>{$clave->country_key}</th>";
            $excel_file .= "<th>{$clave->country_name}</th>";
            $excel_file .= "<th>{$clave->ad_value}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_01}</th>";
            $excel_file .= "<th>{$clave->dprn_value_01}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_01}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_01}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";
            $excel_file .= "<th>{$clave->dprn_rate_02}</th>";
            $excel_file .= "<th>{$clave->dprn_value_02}</th>";
            $excel_file .= "<th>{$clave->depreciacion_acumulada_02}</th>";
            $excel_file .= "<th>{$clave->book_value_for_area_02}</th>";

            $excel_file .= '        </tr>';

        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

            // echo $excel_file;
            // die();

        return  $excel_file;

        return redirect(route('archivoMaestros.show', ['id' => $avaluo->id]));
        dd('sobrante Contable');
        //
    }


    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirArchivoMaestro(Request $request)
    {

        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');

        // dd($request, $start, $input);

        if (!$request->hasFile('fileImport')) {
            // Flash::error('Favor de seleccionar un archivo válido');
            return \Redirect::back()->withErrors( 'Favor de seleccionar un archivo válido');

        }

        $avatar = $request->file('fileImport');
        $excelFilepath = $avatar->getPathName();
        $filename = time().'_'.$avatar->getClientOriginalName();


        $spreadsheetxlsx = \PhpOffice\PhpSpreadsheet\IOFactory::load($excelFilepath);
        $worksheet = $spreadsheetxlsx->getActiveSheet();
        // lee datos de Excel y los almacena en una matriz
        $xls_data = $worksheet->toArray(null, true, true, true);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

        $mapeo = [
            "A" => "company_code",
            "B" => "asset_class",
            "C" => "asset",
            "D" => "subnumber",
            "E" => "name",
            "F" => "additional_description",
            "G" => "serial_number",
            "H" => "inventory_number",
            "I" => "capitalized",
            "J" => "deactivation_on",
            "K" => "cost_center",
            "L" => "evaluation_group_1",
            "M" => "evaluation_group_2",
            "N" => "text",
            "O" => "country_key",
            "P" => "country_name",
            "Q" => "ad_value",
            "R" => "dprn_rate_01",
            "S" => "dprn_value_01",
            "T" => "depreciacion_acumulada_01",
            "U" => "book_value_for_area_01",
            "V" => "dprn_rate_02",
            "W" => "dprn_value_02",
            "X" => "depreciacion_acumulada_02",
            "Y" => "book_value_for_area_02",
            "Z" => "dprn_rate_03",

            "AA" => "dprn_value_03",
            "AB" => "depreciacion_acumulada_03",



            "AC" => "dprn_rate_04",
            "AD" => "dprn_value_04",
            "AE" => "depreciacion_acumulada_04",

            "AF" => "dprn_rate_05",
            "AG" => "dprn_value_05",
            "AH" => "depreciacion_acumulada_05",

        ];

        $errores = [];
        $blameId =\Auth::id();
        $createdAt = date("Y-m-d H:i:s");

        $avaluo = ArchivoMaestro::create($input);

        for ($indiceHoja = 0; $indiceHoja < 1; $indiceHoja++) {
            # Obtener hoja en el índice que vaya del ciclo
            $hojaActual = $spreadsheetxlsx->getSheet($indiceHoja);
            // echo "<h3>Vamos en la hoja con índice $indiceHoja</h3>";

            $registros = [];
            # Iterar filas
            foreach ($hojaActual->getRowIterator() as $fila) {

                // $data = new OrdenTrabajoConteo();
                foreach ($fila->getCellIterator() as $celda) {

                    # Fila, que comienza en 1, luego 2 y así...
                    $fila = $celda->getRow();

                    # Columna, que es la A, B, C y así...
                    $columna = $celda->getColumn();

                    if($fila < 2 || $columna > 'Y' ){
                        break;
                    }

                    if($columna == 'A' ){
                        // $registros[$fila]['avaluo_id'] = $a + $b + $c ;
                        $registros[$fila]['archivo_maestro_id'] = $avaluo->id;
                        $registros[$fila]['blame_id'] = $blameId;
                        $registros[$fila]['created_at'] = $createdAt;
                        $registros[$fila]['updated_at'] = $createdAt;

                    }

                    // Aquí podemos obtener varias cosas interesantes
                    #https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Cell/Cell.html

                    # El valor, así como está en el documento
                    if(isset($mapeo[$columna])){

                        $valorRaw = $celda->getValue();

                        $registros[$fila][$mapeo[$columna]] = (string)$valorRaw;
                    }

                    // # Formateado por ejemplo como dinero o con decimales
                    // $valorFormateado = $celda->getFormattedValue();
                    //
                    // # Si es una fórmula y necesitamos su valor, llamamos a:
                    // $valorCalculado = $celda->getCalculatedValue();

                    // echo "En <strong>$columna$fila</strong> tenemos el valor <strong>$valorRaw</strong>. ";
                    // echo "Formateado es: <strong>$valorFormateado</strong>. ";
                    // echo "Calculado es: <strong>$valorCalculado</strong><br><br>";
                }

                if($fila > 1  ){

                    // dd($registros);

                    $validator = \Validator::make($registros[$fila], ArchivoMaestroDetalle::$rules);

                    if ($validator->fails()) {
                        $errores[$fila] = $validator->messages()->all();
                    }

                    if (count($errores) > ArchivoMaestroDetalle::MAX_ERROR_ROWS) {

                        // dd(
                        //     // 'fila: '. $fila,
                        //     $registros,
                        //     $errores,
                        //     // $validator->errors(),
                        //     count($errores)
                        // );
                        return \Redirect::back()->with('errores', $errores);
                    }
                }
            }


        }


        // dd('Algodon', $input['orden_trabajo_id'], $avaluo,  $registros);
        \DB::beginTransaction();

        try {
            // ArchivoMaestroDetalle::where('orden_trabajo_id', $input['orden_trabajo_id'])->forceDelete();
            ArchivoMaestroDetalle::insert($registros);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }

        Flash::success('Registros importados');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('archivoMaestros.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('archivoMaestros.show', ['id' => $avaluo->id]));

    }

}
