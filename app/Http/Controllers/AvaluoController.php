<?php

namespace App\Http\Controllers;

use App\DataTables\AvaluoDataTable;
use App\DataTables\AvaluoDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAvaluoRequest;
use App\Http\Requests\UpdateAvaluoRequest;
use App\Repositories\AvaluoRepository;
use App\Repositories\AvaluoDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Avaluo;
use App\Models\AvaluoDetalle;
use App\Models\Rubros;
use App\Models\ActivoFijo;

use Illuminate\Http\Request;

class AvaluoController extends AppBaseController
{
    /** @var  AvaluoRepository */
    private $avaluoRepository;

    public function __construct(
        AvaluoRepository $avaluoRepo,
        AvaluoDetalleRepository $avaluoDetalleRepo
    )
    {
        $this->avaluoRepository = $avaluoRepo;
        $this->avaluoDetalleRepository = $avaluoDetalleRepo;
    }

    /**
     * Display a listing of the Avaluo.
     *
     * @param AvaluoDataTable $avaluoDataTable
     * @return Response
     */
    public function index(AvaluoDataTable $avaluoDataTable)
    {
        return $avaluoDataTable->render('avaluos.index');
    }

    /**
     * Show the form for creating a new Avaluo.
     *
     * @return Response
     */
    public function create()
    {

        $ordenesTrabajo = \App\Models\OrdenTrabajo::doesntHave('avaluo')->pluck('num_orden_trabajo', 'id');

        return view('avaluos.create', compact('ordenesTrabajo'));
    }

    /**
     * Store a newly created Avaluo in storage.
     *
     * @param CreateAvaluoRequest $request
     *
     * @return Response
     */
    public function store(CreateAvaluoRequest $request)
    {
        $input = $request->all();

        $avaluo = $this->avaluoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('avaluos.index'));
    }

    /**
     * Display the specified Avaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show(
        AvaluoDetalleDataTable $avaluoDetalleDataTable,
        $id = null
    ){
        $avaluo = $this->avaluoRepository->find($id);

        if (empty($avaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('avaluos.index'));
        }

        // dd($id);
        return $avaluoDetalleDataTable->with([
                // Envia parametro
                'avaluo_id' => $id
            ])->render('avaluos.show', compact('avaluo', 'avaluoDetalleDataTable'));
    }

    /**
     * Show the form for editing the specified Avaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $avaluo = $this->avaluoRepository->find($id);

        if (empty($avaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('avaluos.index'));
        }

        return view('avaluos.edit')->with('avaluo', $avaluo);
    }

    /**
     * Update the specified Avaluo in storage.
     *
     * @param  int              $id
     * @param UpdateAvaluoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvaluoRequest $request)
    {
        $avaluo = $this->avaluoRepository->find($id);

        if (empty($avaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('avaluos.index'));
        }

        $avaluo = $this->avaluoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('avaluos.index'));
    }

    /**
     * Remove the specified Avaluo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $avaluo = $this->avaluoRepository->find($id);

        if (empty($avaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('avaluos.index'));
        }

        $this->avaluoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('avaluos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirAvaluo(Request $request)
    {
        // dd($request);
        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');

        if (!$request->hasFile('fileImport')) {
            // Flash::error('Favor de seleccionar un archivo válido');
            return \Redirect::back()->withErrors( 'Favor de seleccionar un archivo válido');

        }

        $avatar = $request->file('fileImport');
        $excelFilepath = $avatar->getPathName();
        $filename = time().'_'.$avatar->getClientOriginalName();

        $spreadsheetxlsx = \PhpOffice\PhpSpreadsheet\IOFactory::load($excelFilepath);
        $worksheet = $spreadsheetxlsx->getActiveSheet();
        // lee datos de Excel y los almacena en una matriz
        $xls_data = $worksheet->toArray(null, true, true, true);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

        $mapeo = [
            "A" => "asset",
            "B" => "catalogo",
            "C" => "descripcion",
            "D" => "marca",
            "E" => "modelo",
            "F" => "serie",
            "G" => "anio_fabricacion",
            "H" => "porcentaje_depreciacion_fisica",
            "I" => "vrn",
            "J" => "fecha_adquisicion",
            "K" => "valor_cotizado_1",
            "L" => "moneda_1",
            "M" => "tipo_cambio_1",
            "N" => "no_piezas_1",
            "O" => "valor_cotizado_2",
            "P" => "moneda_2",
            "Q" => "tipo_cambio_2",
            "R" => "no_piezas_2",
            "S" => "valor_equipo_usado",
            "T" => "vnr",
            "U" => "link_1",
            "V" => "link_2",
            "W" => "observaciones",
            // "X" =>
            // "Y" =>
            // "Z" =>
        ];

        $errores = [];
        $blameId =\Auth::id();
        $createdAt = date("Y-m-d H:i:s");
        $anioActual = date('Y');

        $avaluo = Avaluo::create($input);

        $rubros = ActivoFijo::pluck( 'rubro_id', 'nombre' );
        $rubrosIds = Rubros::pluck( 'nombre',  'id' );
        $vut = ActivoFijo::pluck( 'vut', 'nombre');
        $porcentajeGastosInstalacion = ActivoFijo::pluck( 'porcentaje_gasto_instalacion', 'nombre');

        // dd($rubros, $rubrosIds, $vut, $porcentajeGastosInstalacion);

        for ($indiceHoja = 0; $indiceHoja < 1; $indiceHoja++) {
            # Obtener hoja en el índice que vaya del ciclo
            $hojaActual = $spreadsheetxlsx->getSheet($indiceHoja);
            // echo "<h3>Vamos en la hoja con índice $indiceHoja</h3>";

            $registros = [];
            # Iterar filas
            foreach ($hojaActual->getRowIterator() as $fila) {

                // $data = new OrdenTrabajoConteo();
                foreach ($fila->getCellIterator() as $celda) {

                    # Fila, comienza en 1
                    $fila = $celda->getRow();

                    # Columna, que es la A, B, C y así...
                    $columna = $celda->getColumn();

                    if($fila < 2 || $columna > 'Z' ){
                        break;
                    }

                    if($columna == 'A' ){

                        $registros[$fila]['avaluo_id']  = $avaluo->id;
                        $registros[$fila]['blame_id']   = $blameId;
                        $registros[$fila]['created_at'] = $createdAt;
                        $registros[$fila]['updated_at'] = $createdAt;

                    }

                    if($columna == 'W' ){

                        // VCF = Valor Cotizado Final

                        // dump($rubros, $rubrosIds, $registros[$fila], $mapeo[$columna], $rubros[$registros[$fila]['catalogo']], );
                        // dd($rubros, $rubrosIds, $registros[$fila], $mapeo[$columna], $rubros[$registros[$fila]['catalogo']]);
                        // dd($vut[$registros[$fila]['catalogo']] );

                        $registros[$fila]['rubro_id'] = isset ($rubros[$registros[$fila]['catalogo']]) ? $rubros[$registros[$fila]['catalogo']] : 0;
                        $registros[$fila]['rubro'] = isset ($rubrosIds[$registros[$fila]['rubro_id']]) ? $rubrosIds[$registros[$fila]['rubro_id']] : 0;
                        $registros[$fila]['vut'] = isset ($vut[$registros[$fila]['catalogo']]) ? $vut[$registros[$fila]['catalogo']] : 0;
                        $registros[$fila]['gastos_instalacion'] = isset ($porcentajeGastosInstalacion[$registros[$fila]['catalogo']]) ? $porcentajeGastosInstalacion[$registros[$fila]['catalogo']] : 0;

                        // dd($registros[$fila]);

                        $registros[$fila]['vcf'] =
                        (
                            (
                                $registros[$fila]['valor_cotizado_1']
                                * $registros[$fila]['no_piezas_1']
                            )
                            * $registros[$fila]['tipo_cambio_1']
                        )
                        +
                        (
                            (
                                $registros[$fila]['valor_cotizado_2']
                                * $registros[$fila]['no_piezas_2']
                            )
                            * $registros[$fila]['tipo_cambio_2']
                        );


                        if(
                            $registros[$fila]['rubro'] == AvaluoDetalle::EQUIPO_TRANSPORTE
                        ){

                            // VRN => Valor de reposición nuevo
                            $registros[$fila]['vrn'] =  $registros[$fila]['vcf'];

                            // Dep. Acum. -|- V.R.N - V.N.R
                            $registros[$fila]['dep_acum'] = $registros[$fila]['vrn'] - $registros[$fila]['vnr']  ;

                            // % Depresiación -|- (DEP.ACUM/V.R.N.)*100
                            // (((2020-2015)/5) * 100)
                            $registros[$fila]['porcentaje_dep'] = ($registros[$fila]['dep_acum'] / $registros[$fila]['vrn']) * 100 ;

                            // VNR

                                // Se toma el valor del layout

                        } else {

                            // VRN => Valor de reposición nuevo -|-  VCF * (1 + Gasto de instalacion)

                            $registros[$fila]['vrn'] = $registros[$fila]['vcf'] * (1 + $registros[$fila]['gastos_instalacion']) ;

                            // % Depresiación -|- ((año de avaluo - año de fab/ADQ) / VUT) * 100   XXX

                            /*
                            "Si el rubro es diferente de TRANSPORTE
                                1.- si no existe año de fabricación tomar año de Adquisicion extraido de la fecha de Adquisicion
                                2.- si no existe año de fabricación ni año de adquisición tomar el %dep Fisica"

                            */

                            $anioFabAdq = null;


                            // año fabricacion -> año adquisición -> %dep Fisica
                            if ( empty($registros[$fila]['anio_fabricacion']) && empty($registros[$fila]['fecha_adquisicion']) ){

                                $registros[$fila]['porcentaje_dep'] = $registros[$fila]['porcentaje_depreciacion_fisica'];

                            } else {

                                // si año fabricacion no es vacio
                                if(!empty($registros[$fila]['anio_fabricacion'])){
                                    $anioFabAdq = $registros[$fila]['anio_fabricacion'];
                                }
                                // si año fabricacion es vacio pero existe fecha de adquisición
                                if(empty($registros[$fila]['anio_fabricacion']) && !empty($registros[$fila]['fecha_adquisicion'])){

                                    // getAnioOfDate();
                                    // dd($registros[$fila]['fecha_adquisicion'], getAnioOfDate($registros[$fila]['fecha_adquisicion']));

                                    $anioFabAdq = getAnioOfDate($registros[$fila]['fecha_adquisicion']);
                                    // dd($anioFabAdq);
                                }

                                // ((año de avaluo - año de fab/ADQ) / VUT) * 100
                                $registros[$fila]['porcentaje_dep'] = (($anioActual - $anioFabAdq) / $registros[$fila]['vut'] )* 100;

                            }



                            // Dep. Acum. -|- (VRN * %Depresiación) / 100
                            $registros[$fila]['dep_acum'] = (($registros[$fila]['vrn'] * $registros[$fila]['porcentaje_dep']) / 100);

                            // VNR => Valor neto de reposición -|-  V.N.R.= V.R.N. - DEP. ACUM
                            $registros[$fila]['vnr'] = $registros[$fila]['vrn'] - $registros[$fila]['dep_acum'];
                            // 4 - 4

                        }

                        // VUR => Vida util remanente -|- V.U.R.=((100 - %DEP)*V.U.T.)/100)
                        // 100 - 4 * 5 / 100


                        $registros[$fila]['vur'] = ((100 - $registros[$fila]['porcentaje_dep']) * $registros[$fila]['vut']  / 100) ;
                        // ((100 - 100) * 5 / 100
                        // 100 - 100 * 5 / 100

                        // DA => Depresiacion anual -|- D.A.=V.N.R / V.U.R
                        // if($registros[$fila]['vnr'] == 0 || $registros[$fila]['vur'] == 0){
                        //
                        //     dd( $fila , $registros[$fila] , 'VNR:' . $registros[$fila]['vnr'] . ' VUR:' .$registros[$fila]['vur']);
                        // }
                        // dump( $fila . ' - ' .$registros[$fila]['vnr'] . ' - ' .$registros[$fila]['vur']);
                        // Fix division 0
                        $registros[$fila]['da'] = ($registros[$fila]['vur'] <= 0) ?  0 : $registros[$fila]['vnr'] / $registros[$fila]['vur'] ;
                        // 0 / 0


                    }

                    // Aquí podemos obtener varias cosas interesantes
                    #https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Cell/Cell.html

                    # El valor, así como está en el documento
                    if(isset($mapeo[$columna])){

                        $valorRaw = $celda->getValue();

                        $registros[$fila][$mapeo[$columna]] = is_null($valorRaw) ? $valorRaw :(string)$valorRaw;
                    }

                }

                if($fila > 1  ){

                    // dd($registros);

                    $validator = \Validator::make($registros[$fila], AvaluoDetalle::$rules);

                    if ($validator->fails()) {
                        $errores[$fila] = $validator->messages()->all();
                    }

                    if (count($errores) > AvaluoDetalle::MAX_ERROR_ROWS) {

                        // dd(
                        //     // 'fila: '. $fila,
                        //     $registros,
                        //     $errores,
                        //     // $validator->errors(),
                        //     count($errores)
                        // );
                        return \Redirect::back()->with('errores', $errores);
                    }
                }
            }


        }

        if (count($errores)) {
            return \Redirect::back()->with('errores', $errores);
        }

        // dd('Algodon', $input['orden_trabajo_id'], $avaluo,  $registros);
        \DB::beginTransaction();

        try {
            // AvaluoDetalle::where('orden_trabajo_id', $input['orden_trabajo_id'])->forceDelete();
            AvaluoDetalle::insert($registros);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }

        Flash::success('Registros importados');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('avaluos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('avaluos.show', ['id' => $avaluo->id]));



    }
    /**
     * Descargar avaluo
     *
     * @param int $id
     *
     * @return Response
     */
    public function descargarAvaluo($id, Request $request)
    {
        $input = $request->all();
        $avaluo = Avaluo::find($id);
        $start = microtime(true);

        if (empty($avaluo)) {
            Flash::error('Registro no encontrado');

            return redirect(route('avaluos.index'));
        }


        $file_name = 'Avaluo_'.$start.'.xls';

        // $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
        $aBody = AvaluoDetalle::where(
            'avaluo_id', $avaluo->id
            )->get();


        // dd($id, $avaluo, $aBody);

        $aEncabezado = [
            "asset" => "Asset" ,
            "rubro" => "rubro" ,
            "catalogo" => "catalogo" ,
            "descripcion" => "descripcion" ,
            "marca" => "marca" ,
            "modelo" => "modelo" ,
            "serie" => "serie" ,
            "area" => "area" ,
            "localidad" => "localidad" ,
            "valor_cotizado_1" => "valor_cotizado_1" ,
            "no_piezas_1" => "no_piezas_1" ,
            "tipo_cambio_1" => "tipo_cambio_1" ,
            "valor_cotizado_2" => "valor_cotizado_2" ,
            "no_piezas_2" => "no_piezas_2" ,
            "tipo_cambio_2" => "tipo_cambio_2" ,
            "vcf" => "vcf" ,
            "vrn" => "vrn" ,
            "porcentaje_dep" => "porcentaje_dep" ,
            "dep_acum" => "dep_acum" ,
            "vnr" => "vnr" ,
            "vur" => "vur" ,
            "da" => "da" ,
            "vut" => "vut"
        ];

        $excel_file  = '<table border="1">';
        $excel_file .= '    <thead>';
        $excel_file .= '        <tr>';
            foreach ($aEncabezado as $clave => $valor) {
                $excel_file .= "<th>{$valor}</th>";
            }
        $excel_file .= '        </tr>';
        $excel_file .= '    </thead>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($aBody as $clave) {

            $excel_file .= '        <tr>';
            $excel_file .= "<th>{$clave->asset}</th>";
            $excel_file .= "<th>{$clave->rubro}</th>";
            $excel_file .= "<th>{$clave->catalogo}</th>";
            $excel_file .= "<th>{$clave->descripcion}</th>";
            $excel_file .= "<th>{$clave->marca}</th>";
            $excel_file .= "<th>{$clave->modelo}</th>";
            $excel_file .= "<th>{$clave->serie}</th>";
            $excel_file .= "<th>{$clave->area}</th>";
            $excel_file .= "<th>{$clave->localidad}</th>";
            $excel_file .= "<th>{$clave->valor_cotizado_1}</th>";
            $excel_file .= "<th>{$clave->no_piezas_1}</th>";
            $excel_file .= "<th>{$clave->tipo_cambio_1}</th>";
            $excel_file .= "<th>{$clave->valor_cotizado_2}</th>";
            $excel_file .= "<th>{$clave->no_piezas_2}</th>";
            $excel_file .= "<th>{$clave->tipo_cambio_2}</th>";
            $excel_file .= "<th>{$clave->vcf}</th>";
            $excel_file .= "<th>{$clave->vrn}</th>";
            $excel_file .= "<th>{$clave->porcentaje_dep}</th>";
            $excel_file .= "<th>{$clave->dep_acum}</th>";
            $excel_file .= "<th>{$clave->vnr}</th>";
            $excel_file .= "<th>{$clave->vur}</th>";
            $excel_file .= "<th>{$clave->da}</th>";
            $excel_file .= "<th>{$clave->vut}</th>";

            $excel_file .= '        </tr>';

        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

        // echo $excel_file;

        echo  $excel_file;
        die();
    }

    /**
     * Descargar avaluo
     *
     * @param int $id
     *
     * @return Response
     */
    public function armadoConciliacion()
    {
return view('armado-conciliacion');
        dd('Armado conciliacion');

    }
    /**
     * Descargar avaluo
     *
     * @param int $id
     *
     * @return Response
     */
    public function armadoConciliacionAvaluo()
    {
        return view('armado-conciliacion-avaluo');

        // dd('Armado conciliacion y Avaluo');

    }



        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function facturaArmadoConciliacion()
        {
            $ordenesTrabajo = \App\Models\OrdenTrabajo::doesntHave('avaluo')->pluck('num_orden_trabajo', 'id');

            return view('factura-armado-conciliacion', compact('ordenesTrabajo'));
            // return view();
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function facturaArmadoAvaluo()
        {
            return view('factura-armado-avaluo');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function facturaArmadoConciliacionAvaluo()
        {
            return view('factura-armado-conciliacion-avaluo');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function subirFacturaArmadoConciliacion(Request $request)
        {
            //obtenemos el campo file definido en el formulario
           $file = $request->file('fileImport');

           //obtenemos el nombre del archivo
           $nombre = $file->getClientOriginalName();

           //indicamos que queremos guardar un nuevo archivo en el disco local
           \Storage::disk('facturas-conciliacion')->put($nombre,  \File::get($file));

           Flash::success('Registro insertado.');
           // dd($request, $file, $nombre);
           return redirect(url('armado-conciliacion'));
            // return view('factura-armado-conciliacion-avaluo');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function subirFacturaArmadoAvaluo(Request $request)
        {
            //obtenemos el campo file definido en el formulario
           $file = $request->file('fileImport');

           //obtenemos el nombre del archivo
           $nombre = $file->getClientOriginalName();

           //indicamos que queremos guardar un nuevo archivo en el disco local
           \Storage::disk('facturas-avaluo')->put($nombre,  \File::get($file));

           Flash::success('Registro insertado.');
            // dd($request);
             return redirect(url('armado-avaluo'));
            // return view('factura-armado-conciliacion-avaluo');
        }

        /**
         * Show the application dashboard.
         *
         * @return \Illuminate\Http\Response
         */
        public function subirFacturaArmadoConciliacionAvaluo(Request $request)
        {

            //obtenemos el campo file definido en el formulario
           $file = $request->file('fileImport');

           //obtenemos el nombre del archivo
           $nombre = $file->getClientOriginalName();

           //indicamos que queremos guardar un nuevo archivo en el disco local
           \Storage::disk('facturas-conciliacion-avaluo')->put($nombre,  \File::get($file));

           Flash::success('Registro insertado.');

            return redirect(url('armado-conciliacion-avaluo'));

        }


}
