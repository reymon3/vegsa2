<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

use App\DataTables\PlacasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePlacasRequest;
use App\Http\Requests\UpdatePlacasRequest;
use App\Repositories\PLacasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\Cliente;
use App\Models\Placas;
use Illuminate\Http\Request;

use App\Imports\PlacasImport;
use App\imports\ExcelImport;
use DB;



class PlacasController extends Controller
{
    private $placasRepository;

    public function __construct(PlacasRepository $placasRepo)
    {
        $this->placasRepository = $placasRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PlacasDataTable $placasDataTable)
    {
        return $placasDataTable->render('placas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $clientes = Cliente::all()->pluck('nombre', 'id')->toArray();
        $clientes = Cliente::all();
        // dd($clientes);
        return view('placas.create')->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clienteDelete = $request->input('cliente');
        session(['pivote' => $clienteDelete]);
        // dd($clienteDelete);
        // $query = DB::select('select * from relacion_placas WHERE placa_pivote LIKE "'.$clienteDelete.'%"');
        DB::delete('delete from relacion_placas WHERE placa_pivote LIKE "'.$clienteDelete.'"');
        // $query = DB::select('select * from relacion_placas WHERE placa_pivote LIKE "BIMBO S.A. DE C%"');

        

        // dd("ok");
        Excel::import(new PlacasImport, request()->file('Archivo'));
        session()->forget('pivote');
        return redirect('/placas')->with('success', 'All good!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
