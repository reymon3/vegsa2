<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoRequest;
use App\Http\Requests\UpdateOrdenTrabajoRequest;
use App\Models\Cotizacion;
use App\Models\OrdenTrabajo;
use App\Models\OrdenTrabajoServicio;
use App\Models\CotizacionServicio;
use App\Repositories\OrdenTrabajoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use DB;

class OrdenTrabajoController extends AppBaseController
{
    /** @var  OrdenTrabajoRepository */
    private $ordenTrabajoRepository;

    public function __construct(OrdenTrabajoRepository $ordenTrabajoRepo)
    {
        $this->ordenTrabajoRepository = $ordenTrabajoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajo.
     *
     * @param OrdenTrabajoDataTable $ordenTrabajoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoDataTable $ordenTrabajoDataTable)
    {
        return $ordenTrabajoDataTable->render('orden_trabajos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajo.
     *
     * @return Response
     */
    public function create()
    {
        // return view('orden_trabajos.create');
    }

    /**
     * Store a newly created OrdenTrabajo in storage.
     *
     * @param CreateOrdenTrabajoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajo = $this->ordenTrabajoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('ordenTrabajos.index'));
    }

    /**
     * Display the specified OrdenTrabajo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajo = $this->ordenTrabajoRepository->find($id);
        $servicios = \App\Models\OrdenTrabajoServicio::where('orden_trabajo_id', $id)->get();
        $normas = \App\Models\OrdenTrabajoNorma::where('orden_trabajo_id', $id)->get();
        $propositos = \App\Models\OrdenTrabajoPropositoAvaluo::where('orden_trabajo_id', $id)->get();
        $rubros = \App\Models\OrdenTrabajoRubroAlcance::where('orden_trabajo_id', $id)->get();
        $tiposPlaqueo= \App\Models\OrdenTrabajoTipoPlaqueo::where('orden_trabajo_id', $id)->get();
        $tiposAvaluo = \App\Models\OrdenTrabajoTipoAvaluo::where('orden_trabajo_id', $id)->get();
        $documentos = \App\Models\OrdenTrabajoDocumento::where('orden_trabajo_id', $id)->get();
        $conteo = DB::table('ordenes_trabajo_conteos')
            ->select(DB::raw(
                'rubro_id,'
                .'rubros.nombre,'
                .'COUNT(*) registros,'
                .'SUM(vrn) vrn,'
                .'SUM(dep_acum) dep_acum,'
                .'SUM(vnr) vnr,'
                .'AVG(vur) vur,'
                .'SUM(da) da')
            )
            ->leftJoin('rubros', 'ordenes_trabajo_conteos.rubro_id', '=', 'rubros.id')
            ->where('orden_trabajo_id', $id)
            ->groupBy('rubro_id')
            ->get();
        // dd($conteo);

        // dd($documentos);

        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
        }

        return view('orden_trabajos.show', compact(
            'ordenTrabajo',
            'servicios',
            'normas',
            'propositos',
            'rubros',
            'tiposPlaqueo',
            'tiposAvaluo',
            'documentos',
            'conteo'
        ));
    }

    /**
     * Show the form for editing the specified OrdenTrabajo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajo = $this->ordenTrabajoRepository->find($id);

        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
        }

        return view('orden_trabajos.edit')->with('ordenTrabajo', $ordenTrabajo);
    }

    /**
     * Update the specified OrdenTrabajo in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoRequest $request)
    {
        $ordenTrabajo = $this->ordenTrabajoRepository->find($id);

        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
        }

        $ordenTrabajo = $this->ordenTrabajoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajos.index'));
    }

    /**
     * Remove the specified OrdenTrabajo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajo = $this->ordenTrabajoRepository->find($id);

        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
        }

        $this->ordenTrabajoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajos.index'));
    }

    /**
     * Generar orden.
     *
     * @param  int $cotizacionId
     *
     * @return Response
     */
    public function generaOrden($cotizacionId)
    {
        $cotizacion = Cotizacion::find($cotizacionId);

        if (empty($cotizacion)) {

            Flash::error('Registro no encontrado.');
            return redirect(route('cotizacions.index'));

        }

        if (OrdenTrabajo::where('cotizacion_id', $cotizacionId)->exists()){

            Flash::error('Esta cotización, ya está relacionada a una orden de trabajo.');
            $cotizacion->bloqueado = 1;
            $cotizacion->save();
            return redirect(route('cotizacions.index'));

        };

        $cotizacionServicios = CotizacionServicio::where('cotizacion_id', $cotizacion->id)->get();

        $ordenTrabajo = new OrdenTrabajo([
            'cotizacion_id' => $cotizacion->id,
            'fecha_orden_trabajo' => date('d/m/Y'),
            'num_orden_trabajo' => $cotizacion->num_cotizacion
        ]);

        // dd($cotizacion, $ordenTrabajo);
        $cotizacion->bloqueado = 1;

        $cotizacion->save();
        $ordenTrabajo->save();

        foreach ($cotizacionServicios as $registro) {

            $ordenTrabajoServicio = new OrdenTrabajoServicio([
                'orden_trabajo_id' => $ordenTrabajo->id,
                'servicio_id' => $registro->servicio_id
            ]);

            $ordenTrabajoServicio->save();
            // echo " <br /> {$registro->servicio_id} <br />";
        }

        Flash::success('Orden de trabajo creada.');

        return redirect(route('ordenTrabajos.edit', ['id' => $ordenTrabajo->id]));
    }


    public function asignacionProyecto(){

        $ordenesTrabajo = OrdenTrabajo::where('asigna_empleado', '>', 0)->get();

        return view('orden_trabajos.detalles.asignacion-proyecto', compact('ordenesTrabajo'));
    }

    public function asignacionProyectoDetalle($ordenTrabajoId){

        // dd('asignacionDetalle', $ordenTrabajoId);

        $ordenTrabajo = OrdenTrabajo::find($ordenTrabajoId);
        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('orden_trabajos.detalles.asignacion-proyecto'));
        }

        $ordenTrabajoEmpleado = \App\Models\OrdenTrabajoEmpleado::with(['empleados', 'roles'])
            ->where('orden_trabajo_id', $ordenTrabajoId)
            ->get();
        // dd($ordenTrabajoEmpleado);

        // dd($ordenTrabajoEmpleado);

        return view('orden_trabajos.detalles.asignacion-proyecto-detalle', compact('ordenTrabajo', 'ordenTrabajoEmpleado'));
    }

    public function actualizaAsignacionEmpleado($id){

        $ordenTrabajo = OrdenTrabajo::find($id);
        if (empty($ordenTrabajo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
        }

        $ordenTrabajo->asigna_empleado = true;
        // dd($id, $ordenTrabajo, $ordenTrabajo->asigna_empleado);
        $ordenTrabajo->save();

        Flash::success('Registro en asignación de proyectos.');

        return redirect(route('ordenTrabajos.index'));

    }
}
