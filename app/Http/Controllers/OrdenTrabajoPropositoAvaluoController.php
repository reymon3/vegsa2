<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoPropositoAvaluoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoPropositoAvaluoRequest;
use App\Http\Requests\UpdateOrdenTrabajoPropositoAvaluoRequest;
use App\Repositories\OrdenTrabajoPropositoAvaluoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoPropositoAvaluoController extends AppBaseController
{
    /** @var  OrdenTrabajoPropositoAvaluoRepository */
    private $ordenTrabajoPropositoAvaluoRepository;

    public function __construct(OrdenTrabajoPropositoAvaluoRepository $ordenTrabajoPropositoAvaluoRepo)
    {
        $this->ordenTrabajoPropositoAvaluoRepository = $ordenTrabajoPropositoAvaluoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoPropositoAvaluo.
     *
     * @param OrdenTrabajoPropositoAvaluoDataTable $ordenTrabajoPropositoAvaluoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoPropositoAvaluoDataTable $ordenTrabajoPropositoAvaluoDataTable)
    {
        return $ordenTrabajoPropositoAvaluoDataTable->render('orden_trabajo_proposito_avaluos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoPropositoAvaluo.
     *
     * @return Response
     */
    public function create()
    {
        $propositos = \App\Models\PropositoAvaluo::pluck('nombre', 'id');
        // dd($propositos);
        return view('orden_trabajo_proposito_avaluos.create', compact('propositos'));
    }

    /**
     * Store a newly created OrdenTrabajoPropositoAvaluo in storage.
     *
     * @param CreateOrdenTrabajoPropositoAvaluoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoPropositoAvaluoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoPropositoAvaluos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoPropositoAvaluos.index'));
    }

    /**
     * Display the specified OrdenTrabajoPropositoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->find($id);

        if (empty($ordenTrabajoPropositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoPropositoAvaluos.index'));
        }

        return view('orden_trabajo_proposito_avaluos.show')->with('ordenTrabajoPropositoAvaluo', $ordenTrabajoPropositoAvaluo);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoPropositoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->find($id);

        if (empty($ordenTrabajoPropositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoPropositoAvaluos.index'));
        }

        return view('orden_trabajo_proposito_avaluos.edit')->with('ordenTrabajoPropositoAvaluo', $ordenTrabajoPropositoAvaluo);
    }

    /**
     * Update the specified OrdenTrabajoPropositoAvaluo in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoPropositoAvaluoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoPropositoAvaluoRequest $request)
    {
        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->find($id);

        if (empty($ordenTrabajoPropositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoPropositoAvaluos.index'));
        }

        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->update($request->all(), $id);

        Flash::success('registro actualizado.');

        return redirect(route('ordenTrabajoPropositoAvaluos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoPropositoAvaluo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoPropositoAvaluo = $this->ordenTrabajoPropositoAvaluoRepository->find($id);

        if (empty($ordenTrabajoPropositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoPropositoAvaluos.index'));
        }

        $this->ordenTrabajoPropositoAvaluoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoPropositoAvaluo->orden_trabajo_id]));
        // return redirect(route('ordenTrabajoPropositoAvaluos.index'));
    }
}
