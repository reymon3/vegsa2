<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 300); //300 seconds = 5 minutes
set_time_limit(300);
ini_set('memory_limit', '512M');

use App\DataTables\OrdenTrabajoBaseContableDataTable;
use App\Http\Requests\CreateOrdenTrabajoBaseContableRequest;
use App\Http\Requests\UpdateOrdenTrabajoBaseContableRequest;
use App\Models\OrdenTrabajoBaseContable;
use App\Models\OrdenTrabajoBaseContableDetalle;
use App\Models\OrdenTrabajoConteo;
use App\Repositories\OrdenTrabajoBaseContableRepository;
use Flash;
use Illuminate\Http\Request;
use Response;

class OrdenTrabajoBaseContableController extends AppBaseController
{
    /** @var OrdenTrabajoBaseContableRepository */
    private $ordenTrabajoBaseContableRepository;

    public function __construct(OrdenTrabajoBaseContableRepository $ordenTrabajoBaseContableRepo)
    {
        $this->ordenTrabajoBaseContableRepository = $ordenTrabajoBaseContableRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoBaseContable.
     *
     * @return Response
     */
    public function index(OrdenTrabajoBaseContableDataTable $ordenTrabajoBaseContableDataTable)
    {
        return $ordenTrabajoBaseContableDataTable->render('orden_trabajo_base_contables.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoBaseContable.
     *
     * @return Response
     */
    public function create()
    {

        $ordenesTrabajo = \App\Models\OrdenTrabajo::doesntHave('ordenTrabajo')->pluck('num_orden_trabajo', 'id');
        return view('orden_trabajo_base_contables.create',  compact('ordenesTrabajo'));
    }

    /**
     * Store a newly created OrdenTrabajoBaseContable in storage.
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoBaseContableRequest $request)
    {
        $input = $request->all();
        dd($request, $input);

        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('ordenTrabajoBaseContables.index'));
    }

    /**
     * Display the specified OrdenTrabajoBaseContable.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');
            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $basesContables = OrdenTrabajoBaseContable::where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)->get();

        $registrosRepetidos['placa_actual']['repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_actual, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('placa_actual')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_actual');

        $registrosRepetidos['placa_actual']['no_repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_actual, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('placa_actual')
            ->havingRaw("COUNT(*) > 1")
            ->get();

        $registrosRepetidos['placa_actual']['detalle_repetidos'] = OrdenTrabajoConteo::select('folio',  'placa_actual')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereIn('placa_actual', $registrosRepetidos['placa_actual']['repetidos'])
            ->get();

        $registrosRepetidos['serie']['repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('serie, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('serie')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('serie');

        $registrosRepetidos['serie']['no_repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('serie, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('serie')
            ->havingRaw("COUNT(*) > 1")
            ->get();

        $registrosRepetidos['serie']['detalle_repetidos'] = OrdenTrabajoConteo::select('folio',  'serie')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereIn('serie', $registrosRepetidos['serie']['repetidos'])
            ->get();

        $registrosRepetidos['placa_anterior']['repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_anterior, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('placa_anterior')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_anterior');

        $registrosRepetidos['placa_anterior']['no_repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_anterior, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('placa_anterior')
            ->havingRaw("COUNT(*) > 1")
            ->get();

        $registrosRepetidos['placa_anterior']['detalle_repetidos'] = OrdenTrabajoConteo::select('folio',  'placa_anterior')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereIn('placa_anterior', $registrosRepetidos['placa_anterior']['repetidos'])
            ->get();

        $registrosRepetidos['placa_auxiliar_1']['repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_1, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_1 is not null")
            ->groupBy('placa_auxiliar_1')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_auxiliar_1');

        $registrosRepetidos['placa_auxiliar_1']['no_repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_1, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_1 is not null")
            ->groupBy('placa_auxiliar_1')
            ->havingRaw("COUNT(*) > 1")
            ->get();

        $registrosRepetidos['placa_auxiliar_1']['detalle_repetidos'] = OrdenTrabajoConteo::select('folio',  'placa_auxiliar_1')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_1 is not null")
            ->whereIn('placa_auxiliar_1', $registrosRepetidos['placa_auxiliar_1']['repetidos'])
            ->get();

        $registrosRepetidos['placa_auxiliar_2']['repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_2, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_2 is not null")
            ->groupBy('placa_auxiliar_2')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_auxiliar_2');

        $registrosRepetidos['placa_auxiliar_2']['no_repetidos'] = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_2, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_2 is not null")
            ->groupBy('placa_auxiliar_2')
            ->havingRaw("COUNT(*) > 1")
            ->get();

        $registrosRepetidos['placa_auxiliar_2']['detalle_repetidos'] = OrdenTrabajoConteo::select('folio',  'placa_auxiliar_2')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->whereRaw("placa_auxiliar_2 is not null")
            ->whereIn('placa_auxiliar_2', $registrosRepetidos['placa_auxiliar_2']['repetidos'])
            ->get();

        //     dd(
        //         $registrosRepetidos['placa_actual']['no_repetidos'][0]->no_repetidos,
        //         $registrosRepetidos['serie']['no_repetidos'][0]->no_repetidos,
        //         $registrosRepetidos['placa_anterior']['no_repetidos'][0]->no_repetidos,
        //         $registrosRepetidos['placa_auxiliar_1']['no_repetidos'][0]->no_repetidos
        //         // ,
        //         // $registrosRepetidos['placa_auxiliar_2']['no_repetidos'][0]->no_repetidos
        // );

        $conteo = \DB::table('ordenes_trabajo_conteos')
            ->select(\DB::raw(
                // 'rubro_id,'
                 'planta,'
                .'rubro,'
                .'COUNT(*) registros'
            ))
            // ->leftJoin('rubros', 'ordenes_trabajo_conteos.rubro_id', '=', 'rubros.id')
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            ->groupBy('planta', 'rubro')
            ->get();

        $plantas = \DB::table('ordenes_trabajo_conteos')
            ->select(\DB::raw(
                 'planta,'
                .'COUNT(*) registros'
            ))
            ->where('orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id)
            // ->whereNotNull('planta')
            ->groupBy('planta')
            ->orderBy('planta')
            ->get();

        // dd(count($plantas), $plantas);


        return view('orden_trabajo_base_contables.show',[
            'ordenTrabajoBaseContable' => $ordenTrabajoBaseContable,
            'conteo' => $conteo,
            'plantas' => $plantas,
            'ordenTrabajo' => $ordenTrabajoBaseContable->ordenTrabajo,
            'basesContables' => $basesContables,
            'registrosRepetidos' => $registrosRepetidos,
        ]);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoBaseContable.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        return view('orden_trabajo_base_contables.edit')->with('ordenTrabajoBaseContable', $ordenTrabajoBaseContable);
    }

    /**
     * Update the specified OrdenTrabajoBaseContable in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoBaseContableRequest $request)
    {
        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoBaseContables.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $this->ordenTrabajoBaseContableRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajoBaseContables.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirDocumento(Request $request)
    {
        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');
        // dd($input, $request);
        if ($request->hasFile('fileImport')) {
            $avatar = $request->file('fileImport');
            $excelFilepath = $avatar->getPathName();
            $filename = time().'_'.$avatar->getClientOriginalName();
            $data = new OrdenTrabajoBaseContable();

            $spreadsheetxlsx = \PhpOffice\PhpSpreadsheet\IOFactory::load($excelFilepath);
            $worksheet = $spreadsheetxlsx->getActiveSheet();
            // lee datos de Excel y los almacena en una matriz
            $xls_data = $worksheet->toArray(null, true, true, true);
            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

            foreach ($spreadsheetxlsx->getSheetNames() as $ceja) {
                //OBTIENE LOS TITULOS DE LAS COLUMNAS DEL EXCEL
                for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                    $value = $worksheet->getCellByColumnAndRow($col, 1)->getValue();
                    // Si el encabezado es nulo, detiene el ciclo
                    // dd($col);
                    if (is_null($value) || $col > 60 ){
                        break;
                    }

                    $arrayCabeceras[$worksheet->getCellByColumnAndRow($col, 1)->getColumn()] = $value;
                    // echo $arrayCabeceras[$worksheet->getCellByColumnAndRow($col, 1)->getColumn() ]."<br />";
                    // dd($worksheet->getCellByColumnAndRow($col, 1)->getColumn(), $value);
                }
            }

            $contador = 0;
            $registros = [];

            $ultimaColumna = array_key_last($arrayCabeceras);

            // Eliminamos la primer fila (cabeceras)
            unset($xls_data[array_key_first($xls_data)]);

            foreach ($xls_data as $fila) {
                // code...
                $contador++;

                foreach ($fila as $columna => $valor) {
                    // echo "Columna: $columna - Ultima Columna: $ultimaColumna - es mayor: " . ( $ultimaColumna > $columna). '<br />';
                    $registros[$contador][$arrayCabeceras[$columna]] = $valor;

                    // Evita guardar mas columnas de las que se tienen en cabeceras
                    if ($columna == $ultimaColumna){
                        break;
                    }
                }
            }

            // dd($xls_data, $arrayCabeceras,  $registros, $highestRow, $highestColumn, $highestColumnIndex);
            $data->nombre = $filename;
            $data->orden_trabajo_id = $input['orden_trabajo_id'];
            $data->json_cabeceras = json_encode($arrayCabeceras);
            // $data->json_excel = json_encode($xls_data);

            $data->blame_id =\Auth::id();
            $data->save();

            $dataDir = public_path('uploads/ordenes-trabajo/' . $input['orden_trabajo_id'] .'/base-contable');
            //$temporal = \SleekDB\SleekDB::store('temporal', $dataDir);

            // Insert the data.

            foreach ($registros as $registro) {

                //  orden_trabajo_base_contable_id	json_fila	blame_id	created_at	updated_at	deleted_at

                $filaJSON = OrdenTrabajoBaseContableDetalle::create([
                    'orden_trabajo_base_contable_id' => $data->id,
                    'json_fila' => json_encode($registro),
                    'blame_id' => 1,
                    'created_at' =>  date('Y-m-d')
                ]);

                //$temporal->insert( $registro );

            }

            // foreach ($registros as $registro) {
            //     $temporal->insert( $registro );
            // }



        }

        Flash::success('Registro insertado.');
        return redirect(route('ordenTrabajoBaseContables.show', ['id' => $data->id]));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function descargaPlantilla($id, Request $request)
    {
        $data = OrdenTrabajoBaseContable::find($id);
        $file_name = 'plantilla_'.$id.'.xls';
        $aEncabezado = json_decode($data->json_cabeceras);

        $excel_file = '
            <table>
                <thead>
                    <tr>';
        foreach ($aEncabezado as $clave => $valor) {
            $excel_file .= "<th>{$valor}</th>";
        }

        $excel_file .= '
                    </tr>
                </thead>
            </table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

        echo $excel_file;
        die();
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function obtenerDatosAgrupados($id, $columna)
    {

        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $filasJSON = OrdenTrabajoBaseContableDetalle::where(
            'orden_trabajo_base_contable_id', $ordenTrabajoBaseContable->id
        )->get();

        // $aFilas = array_filter(json_decode($ordenTrabajoBaseContable->json_excel, true));


        $registros = [];
        foreach ($filasJSON as $clave => $fila) {
            // echo '<tr  class="text-center">';
            foreach (json_decode($fila->json_fila) as $celda => $contenido) {
                // dd($fila, $celda, $contenido, $columna);
                if($celda != $columna){
                    continue;
                }

                // dd($registros, $contenido, 'pelas');
                if (!in_array($contenido, $registros)) {
                    // echo "Existe Irix";
                    $registros[]=$contenido;
                }
                // echo "{$celda} => {$contenido}<br />";
            }
            // echo '</tr>';
        }

        // https://www.php.net/manual/es/array.sorting.php
        natsort($registros);
        // https://www.php.net/manual/es/function.array-values.php
        $registros = array_values($registros);

        return json_encode($registros);

    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function homologarColumnas($id)
    {
        // dd('pelas');
        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $dataDir = public_path('uploads/ordenes-trabajo/' . $ordenTrabajoBaseContable->orden_trabajo_id .'/base-contable');
        //$temporal = \SleekDB\SleekDB::store('temporal', $dataDir);

        // dd($temporal->fetch());

        return view('orden_trabajo_base_contables.homologar-columnas', compact('ordenTrabajoBaseContable'));

    }

    //
    // /**
    //  * Remove the specified OrdenTrabajoBaseContable from storage.
    //  *
    //  * @param int $id
    //  *
    //  * @return Response
    //  */
    // public function devuelveDatosPorColumna($id, $columna )
    // {
    //     $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);
    //
    // }
    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function reemplazaDatosPorColumna(Request $request )
    {
        $input = $request->all();

        // registros[]: TRANSFERENCIA
        // registros[]: Transferencia
        // registros[]: transferencia
        // valor: TRANSFERENCIA
        // nombre_columna: RUBRO
        // orden_trabajo_base_contable_id: 41

        // ======================================================================
        $filas = OrdenTrabajoBaseContableDetalle::where(
            'orden_trabajo_base_contable_id', $input['orden_trabajo_base_contable_id']
        )->get();

        if (empty($filas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $registros = [];
        $contador = 0;
        foreach ($filas as $clave => $fila) {
            $contador++;
            foreach (json_decode($fila->json_fila) as $celda => $contenido) {

                $registros[$contador][$celda] = $contenido;

                if($celda == $input['nombre_columna']
                    && in_array($contenido, $input['registros'])
                ){
                    // dump("reemplazar: $contenido por " . $input['valor'] );
                    $registros[$contador][$celda] = $input['valor'];
                }

            }
        }

        OrdenTrabajoBaseContableDetalle::where(
            'orden_trabajo_base_contable_id', $input['orden_trabajo_base_contable_id']
        )->forceDelete();

        foreach ($registros as $registro) {

            //  orden_trabajo_base_contable_id	json_fila	blame_id	created_at	updated_at	deleted_at

            OrdenTrabajoBaseContableDetalle::create([
                'orden_trabajo_base_contable_id' => $input['orden_trabajo_base_contable_id'],
                'json_fila' => json_encode($registro),
                'blame_id' => 1,
                'created_at' =>  date('Y-m-d')
            ]);

            // $temporal->insert( $registro );

        }


        return json_encode(['estatus' => 'ok']);

        return json_encode($registros);
        // ======================================================================


    }

    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaCruce($id ){



        $ordenTrabajoBaseContable = $this->ordenTrabajoBaseContableRepository->find($id);

        if (empty($ordenTrabajoBaseContable)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name = 'cruce_'.$ordenTrabajoBaseContable->nombre.'.xls';

        $aEncabezado = json_decode($ordenTrabajoBaseContable->json_cabeceras, true);
        $aBody = OrdenTrabajoBaseContableDetalle::where(
            'orden_trabajo_base_contable_id', $ordenTrabajoBaseContable->id
            )->get();

        $series = OrdenTrabajoConteo::where(
            'orden_trabajo_id', $ordenTrabajoBaseContable->orden_trabajo_id
            )
            ->pluck('serie')
            ->all();

        // dd($series,  $ordenTrabajoBaseContable->id, $ordenTrabajoBaseContable->orden_trabajo_id);

        $excel_file  = '<table border="1">';
        $excel_file .= '    <thead>';
        $excel_file .= '        <tr>';
        $excel_file .= '        <th>Cruce</th>';
            foreach ($aEncabezado as $clave => $valor) {
                $excel_file .= "<th>{$valor}</th>";
            }
        $excel_file .= '        </tr>';
        $excel_file .= '    </thead>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;
        // $cruce = '';
        foreach ($aBody as $clave => $fila) {

            $noFila ++;

            foreach (json_decode($fila->json_fila) as $celda => $contenido) {

                $registros[$noFila][] = $contenido;
            }

            if (in_array($registros[$noFila][20], $series)) {
                array_unshift($registros[$noFila], "ENCONTRADO");
            } else {
                array_unshift($registros[$noFila], "NO ENCONTRADO");
            }

        }

        foreach ($registros as $clave => $fila) {

            $excel_file .= '<tr >';
            foreach ($fila as $celda => $contenido) {
                if($celda == 0 && $contenido != 'NO ENCONTRADO'){
                    $excel_file .= "<td bgcolor='green'>$contenido</td>";
                } else {
                    $excel_file .= "<td >$contenido</td>";
                }
            }

            $excel_file .= '</tr>';
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';

        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");

        echo $excel_file;
        die();

        return  $excel_file;

    }

    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaRepetidosPlacaActual($id ){

        // $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')->select(
        //         \DB::raw('serie, COUNT(*) no_repetidos')
        //     )
        //     ->where('orden_trabajo_id', $id)
        //     ->groupBy('serie')
        //     ->havingRaw("COUNT(*) > 1")
        //     ->pluck('serie');

        $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_actual, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $id)
            ->groupBy('placa_actual')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_actual');

        $componetizacion = OrdenTrabajoConteo::where('orden_trabajo_id', $id)

        ->whereIn('placa_actual', $componetizacionRepetidos)
            ->orderBy('placa_actual', 'asc')
            ->get();

        if (empty($componetizacionRepetidos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name =  date("Y-m-d_H:i:s").'_Placas-Actuales-Repetidas.xls';

        $excel_file  = '<table border="1">';
        $thead  = '        <tr>';
        $thead .= '             <th>Serie</th>';
        $thead .= '             <th>Folio</th>';
        $thead .= '             <th>Placa Anterior</th>';
        $thead .= '             <th>Placa Actual</th>';
        $thead .= '             <th>Placa Homologada</th>';
        $thead .= '             <th>Placa Auxiliar 1</th>';
        $thead .= '             <th>Placa Auxiliar 2</th>';
        $thead .= '             <th>Descripcion</th>';
        $thead .= '        </tr>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($componetizacion as $registro) {

            $registroActual = $registro->placa_actual;

            if(!isset($registroAnterior)){

                $registroAnterior = $registroActual;
                $excel_file .= $thead;

            }

            if($registroActual != $registroAnterior){
                // Pinta encabezados
                $excel_file .= $thead;
            }

            $excel_file .= '<tr >';
            $excel_file .= '<td >' . $registro->serie .' </td>';
            $excel_file .= '<td >\'' . $registro->folio .' </td>';
            $excel_file .= '<td >' . $registro->placa_anterior.' </td>';
            $excel_file .= '<td >' . $registro->placa_actual.' </td>';
            $excel_file .= '<td >' . $registro->placa_homologada.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_1.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_2.' </td>';
            $excel_file .= '<td >';
            $excel_file .= $registro->catalogo . ', ';
            $excel_file .= $registro->marca . ', ';
            $excel_file .= $registro->modelo . ', ';
            $excel_file .= $registro->observaciones . ', ';
            $excel_file .= '</td>';

            $excel_file .= '</tr>';
            $registroAnterior = $registroActual;
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';
        // echo $excel_file;
        // dd();
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");


        echo $excel_file;
        die();

        return  $excel_file;

    }
    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaRepetidosSerie($id ){

        $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')->select(
                \DB::raw('serie, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $id)
            ->groupBy('serie')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('serie');

        $componetizacion = OrdenTrabajoConteo::where('orden_trabajo_id', $id)

        ->whereIn('serie', $componetizacionRepetidos)
            ->orderBy('serie', 'asc')
            ->get();

        if (empty($componetizacionRepetidos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name =  date("Y-m-d_H:i:s").'_No-Serie-Repetidos.xls';

        $excel_file  = '<table border="1">';
        $thead  = '        <tr>';
        $thead .= '             <th>Serie</th>';
        $thead .= '             <th>Folio</th>';
        $thead .= '             <th>Placa Anterior</th>';
        $thead .= '             <th>Placa Actual</th>';
        $thead .= '             <th>Placa Homologada</th>';
        $thead .= '             <th>Placa Auxiliar 1</th>';
        $thead .= '             <th>Placa Auxiliar 2</th>';
        $thead .= '             <th>Descripcion</th>';
        $thead .= '        </tr>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($componetizacion as $registro) {

            $registroActual = $registro->serie;

            if(!isset($registroAnterior)){

                $registroAnterior = $registroActual;
                $excel_file .= $thead;

            }

            if($registroActual != $registroAnterior){
                // Pinta encabezados
                $excel_file .= $thead;
            }

            $excel_file .= '<tr >';
            $excel_file .= '<td >' . $registro->serie .' </td>';
            $excel_file .= '<td >\'' . $registro->folio .' </td>';
            $excel_file .= '<td >' . $registro->placa_anterior.' </td>';
            $excel_file .= '<td >' . $registro->placa_actual.' </td>';
            $excel_file .= '<td >' . $registro->placa_homologada.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_1.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_2.' </td>';
            $excel_file .= '<td >';
            $excel_file .= $registro->catalogo . ', ';
            $excel_file .= $registro->marca . ', ';
            $excel_file .= $registro->modelo . ', ';
            $excel_file .= $registro->observaciones . ', ';
            $excel_file .= '</td>';

            $excel_file .= '</tr>';
            $registroAnterior = $registroActual;
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';
        // echo $excel_file;
        // dd();
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");


        echo $excel_file;
        die();

        return  $excel_file;

    }
    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaRepetidosPlacaAnterior($id ){

        $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_anterior, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $id)
            ->groupBy('placa_anterior')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_anterior');

        $componetizacion = OrdenTrabajoConteo::where('orden_trabajo_id', $id)

        ->whereIn('placa_anterior', $componetizacionRepetidos)
            ->orderBy('placa_anterior', 'asc')
            ->get();

        if (empty($componetizacionRepetidos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name =  date("Y-m-d_H:i:s").'_Placas-Anteriores-Repetidas.xls';

        $excel_file  = '<table border="1">';
        $thead  = '        <tr>';
        $thead .= '             <th>Serie</th>';
        $thead .= '             <th>Folio</th>';
        $thead .= '             <th>Placa Anterior</th>';
        $thead .= '             <th>Placa Actual</th>';
        $thead .= '             <th>Placa Homologada</th>';
        $thead .= '             <th>Placa Auxiliar 1</th>';
        $thead .= '             <th>Placa Auxiliar 2</th>';
        $thead .= '             <th>Descripcion</th>';
        $thead .= '        </tr>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($componetizacion as $registro) {

            $registroActual = $registro->placa_anterior;

            if(!isset($registroAnterior)){

                $registroAnterior = $registroActual;
                $excel_file .= $thead;

            }

            if($registroActual != $registroAnterior){
                // Pinta encabezados
                $excel_file .= $thead;
            }

            $excel_file .= '<tr >';
            $excel_file .= '<td >' . $registro->serie .' </td>';
            $excel_file .= '<td >\'' . $registro->folio .' </td>';
            $excel_file .= '<td >' . $registro->placa_anterior.' </td>';
            $excel_file .= '<td >' . $registro->placa_actual.' </td>';
            $excel_file .= '<td >' . $registro->placa_homologada.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_1.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_2.' </td>';
            $excel_file .= '<td >';
            $excel_file .= $registro->catalogo . ', ';
            $excel_file .= $registro->marca . ', ';
            $excel_file .= $registro->modelo . ', ';
            $excel_file .= $registro->observaciones . ', ';
            $excel_file .= '</td>';

            $excel_file .= '</tr>';
            $registroAnterior = $registroActual;
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';
        // echo $excel_file;
        // dd();
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");


        echo $excel_file;
        die();

        return  $excel_file;

    }

    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaRepetidosPlacaAuxiliar1($id ){

        $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_1, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $id)
            ->groupBy('placa_auxiliar_1')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_auxiliar_1');

        $componetizacion = OrdenTrabajoConteo::where('orden_trabajo_id', $id)

        ->whereIn('placa_auxiliar_1', $componetizacionRepetidos)
            ->orderBy('placa_auxiliar_1', 'asc')
            ->get();

        if (empty($componetizacionRepetidos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name =  date("Y-m-d_H:i:s").'_Placas-Auxiliares-1-Repetidas.xls';

        $excel_file  = '<table border="1">';
        $thead  = '        <tr>';
        $thead .= '             <th>Serie</th>';
        $thead .= '             <th>Folio</th>';
        $thead .= '             <th>Placa Anterior</th>';
        $thead .= '             <th>Placa Actual</th>';
        $thead .= '             <th>Placa Homologada</th>';
        $thead .= '             <th>Placa Auxiliar 1</th>';
        $thead .= '             <th>Placa Auxiliar 2</th>';
        $thead .= '             <th>Descripcion</th>';
        $thead .= '        </tr>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($componetizacion as $registro) {

            $registroActual = $registro->placa_auxiliar_1;

            if(!isset($registroAnterior)){

                $registroAnterior = $registroActual;
                $excel_file .= $thead;

            }

            if($registroActual != $registroAnterior){
                // Pinta encabezados
                $excel_file .= $thead;
            }

            $excel_file .= '<tr >';
            $excel_file .= '<td >' . $registro->serie .' </td>';
            $excel_file .= '<td >\'' . $registro->folio .' </td>';
            $excel_file .= '<td >' . $registro->placa_anterior.' </td>';
            $excel_file .= '<td >' . $registro->placa_actual.' </td>';
            $excel_file .= '<td >' . $registro->placa_homologada.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_1.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_2.' </td>';
            $excel_file .= '<td >';
            $excel_file .= $registro->catalogo . ', ';
            $excel_file .= $registro->marca . ', ';
            $excel_file .= $registro->modelo . ', ';
            $excel_file .= $registro->observaciones . ', ';
            $excel_file .= '</td>';

            $excel_file .= '</tr>';
            $registroAnterior = $registroActual;
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';
        // echo $excel_file;
        // dd();
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");


        echo $excel_file;
        die();

        return  $excel_file;

    }
    /**
    * Remove the specified OrdenTrabajoBaseContable from storage.
    *
    * @param int $id
    *
    * @return Response
    */
    public function descargaRepetidosPlacaAuxiliar2($id ){

        $componetizacionRepetidos = \DB::table('ordenes_trabajo_conteos')
            ->select(
                \DB::raw('placa_auxiliar_2, COUNT(*) no_repetidos')
            )
            ->where('orden_trabajo_id', $id)
            ->groupBy('placa_auxiliar_2')
            ->havingRaw("COUNT(*) > 1")
            ->pluck('placa_auxiliar_2');

        $componetizacion = OrdenTrabajoConteo::where('orden_trabajo_id', $id)

        ->whereIn('placa_auxiliar_2', $componetizacionRepetidos)
            ->orderBy('placa_auxiliar_2', 'asc')
            ->get();

        if (empty($componetizacionRepetidos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoBaseContables.index'));
        }

        $file_name =  date("Y-m-d_H:i:s").'_Placas-Auxiliares-2-Repetidas.xls';

        $excel_file  = '<table border="1">';
        $thead  = '        <tr>';
        $thead .= '             <th>Serie</th>';
        $thead .= '             <th>Folio</th>';
        $thead .= '             <th>Placa Anterior</th>';
        $thead .= '             <th>Placa Actual</th>';
        $thead .= '             <th>Placa Homologada</th>';
        $thead .= '             <th>Placa Auxiliar 1</th>';
        $thead .= '             <th>Placa Auxiliar 2</th>';
        $thead .= '             <th>Descripcion</th>';
        $thead .= '        </tr>';
        $excel_file .= '    <tbody>';

        $registros = [];
        $noFila = 0;

        foreach ($componetizacion as $registro) {

            $registroActual = $registro->placa_auxiliar_2;

            if(!isset($registroAnterior)){

                $registroAnterior = $registroActual;
                $excel_file .= $thead;

            }

            if($registroActual != $registroAnterior){
                // Pinta encabezados
                $excel_file .= $thead;
            }

            $excel_file .= '<tr >';
            $excel_file .= '<td >' . $registro->serie .' </td>';
            $excel_file .= '<td >\'' . $registro->folio .' </td>';
            $excel_file .= '<td >' . $registro->placa_anterior.' </td>';
            $excel_file .= '<td >' . $registro->placa_actual.' </td>';
            $excel_file .= '<td >' . $registro->placa_homologada.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_1.' </td>';
            $excel_file .= '<td >' . $registro->placa_auxiliar_2.' </td>';
            $excel_file .= '<td >';
            $excel_file .= $registro->catalogo . ', ';
            $excel_file .= $registro->marca . ', ';
            $excel_file .= $registro->modelo . ', ';
            $excel_file .= $registro->observaciones . ', ';
            $excel_file .= '</td>';

            $excel_file .= '</tr>';
            $registroAnterior = $registroActual;
        }

        $excel_file .= '    </tbody>';
        $excel_file .= '</table>';
        // echo $excel_file;
        // dd();
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$file_name");


        echo $excel_file;
        die();

        return  $excel_file;

    }

}
