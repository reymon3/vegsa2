<?php

namespace App\Http\Controllers;

use App\DataTables\CivilDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCivilDetalleRequest;
use App\Http\Requests\UpdateCivilDetalleRequest;
use App\Repositories\CivilDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CivilDetalleController extends AppBaseController
{
    /** @var  CivilDetalleRepository */
    private $civilDetalleRepository;

    public function __construct(CivilDetalleRepository $civilDetalleRepo)
    {
        $this->civilDetalleRepository = $civilDetalleRepo;
    }

    /**
     * Display a listing of the CivilDetalle.
     *
     * @param CivilDetalleDataTable $civilDetalleDataTable
     * @return Response
     */
    public function index(CivilDetalleDataTable $civilDetalleDataTable)
    {
        return $civilDetalleDataTable->render('civil_detalles.index');
    }

    /**
     * Show the form for creating a new CivilDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('civil_detalles.create');
    }

    /**
     * Store a newly created CivilDetalle in storage.
     *
     * @param CreateCivilDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateCivilDetalleRequest $request)
    {
        $input = $request->all();

        $civilDetalle = $this->civilDetalleRepository->create($input);

        Flash::success('Civil Detalle saved successfully.');

        return redirect(route('civilDetalles.index'));
    }

    /**
     * Display the specified CivilDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $civilDetalle = $this->civilDetalleRepository->find($id);

        if (empty($civilDetalle)) {
            Flash::error('Civil Detalle not found');

            return redirect(route('civilDetalles.index'));
        }

        return view('civil_detalles.show')->with('civilDetalle', $civilDetalle);
    }

    /**
     * Show the form for editing the specified CivilDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $civilDetalle = $this->civilDetalleRepository->find($id);

        if (empty($civilDetalle)) {
            Flash::error('Civil Detalle not found');

            return redirect(route('civilDetalles.index'));
        }

        return view('civil_detalles.edit')->with('civilDetalle', $civilDetalle);
    }

    /**
     * Update the specified CivilDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateCivilDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCivilDetalleRequest $request)
    {
        $civilDetalle = $this->civilDetalleRepository->find($id);

        if (empty($civilDetalle)) {
            Flash::error('Civil Detalle not found');

            return redirect(route('civilDetalles.index'));
        }

        $civilDetalle = $this->civilDetalleRepository->update($request->all(), $id);

        Flash::success('Civil Detalle updated successfully.');

        return redirect(route('civilDetalles.index'));
    }

    /**
     * Remove the specified CivilDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $civilDetalle = $this->civilDetalleRepository->find($id);

        if (empty($civilDetalle)) {
            Flash::error('Civil Detalle not found');

            return redirect(route('civilDetalles.index'));
        }

        $this->civilDetalleRepository->delete($id);

        Flash::success('Civil Detalle deleted successfully.');

        return redirect(route('civilDetalles.index'));
    }
}
