<?php

namespace App\Http\Controllers;

use App\DataTables\FichaTecnicaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFichaTecnicaRequest;
use App\Http\Requests\UpdateFichaTecnicaRequest;
use App\Repositories\FichaTecnicaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FichaTecnicaController extends AppBaseController
{
    /** @var  FichaTecnicaRepository */
    private $fichaTecnicaRepository;

    public function __construct(FichaTecnicaRepository $fichaTecnicaRepo)
    {
        $this->fichaTecnicaRepository = $fichaTecnicaRepo;
    }

    /**
     * Display a listing of the FichaTecnica.
     *
     * @param FichaTecnicaDataTable $fichaTecnicaDataTable
     * @return Response
     */
    public function index(FichaTecnicaDataTable $fichaTecnicaDataTable)
    {
        return $fichaTecnicaDataTable->render('ficha_tecnicas.index');
    }

    /**
     * Show the form for creating a new FichaTecnica.
     *
     * @return Response
     */
    public function create()
    {
        return view('ficha_tecnicas.create');
    }

    /**
     * Store a newly created FichaTecnica in storage.
     *
     * @param CreateFichaTecnicaRequest $request
     *
     * @return Response
     */
    public function store(CreateFichaTecnicaRequest $request)
    {
        $input = $request->all();

        $fichaTecnica = $this->fichaTecnicaRepository->create($input);

        Flash::success('Ficha Tecnica saved successfully.');

        return redirect(route('fichaTecnicas.index'));
    }

    /**
     * Display the specified FichaTecnica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fichaTecnica = $this->fichaTecnicaRepository->find($id);

        if (empty($fichaTecnica)) {
            Flash::error('Ficha Tecnica not found');

            return redirect(route('fichaTecnicas.index'));
        }

        return view('ficha_tecnicas.show')->with('fichaTecnica', $fichaTecnica);
    }

    /**
     * Show the form for editing the specified FichaTecnica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fichaTecnica = $this->fichaTecnicaRepository->find($id);

        if (empty($fichaTecnica)) {
            Flash::error('Ficha Tecnica not found');

            return redirect(route('fichaTecnicas.index'));
        }

        return view('ficha_tecnicas.edit')->with('fichaTecnica', $fichaTecnica);
    }

    /**
     * Update the specified FichaTecnica in storage.
     *
     * @param  int              $id
     * @param UpdateFichaTecnicaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFichaTecnicaRequest $request)
    {
        $fichaTecnica = $this->fichaTecnicaRepository->find($id);

        if (empty($fichaTecnica)) {
            Flash::error('Ficha Tecnica not found');

            return redirect(route('fichaTecnicas.index'));
        }

        $fichaTecnica = $this->fichaTecnicaRepository->update($request->all(), $id);

        Flash::success('Ficha Tecnica updated successfully.');

        return redirect(route('fichaTecnicas.index'));
    }

    /**
     * Remove the specified FichaTecnica from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fichaTecnica = $this->fichaTecnicaRepository->find($id);

        if (empty($fichaTecnica)) {
            Flash::error('Ficha Tecnica not found');

            return redirect(route('fichaTecnicas.index'));
        }

        $this->fichaTecnicaRepository->delete($id);

        Flash::success('Ficha Tecnica deleted successfully.');

        return redirect(route('fichaTecnicas.index'));
    }
}
