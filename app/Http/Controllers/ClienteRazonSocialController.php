<?php

namespace App\Http\Controllers;

use App\DataTables\ClienteRazonSocialDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateClienteRazonSocialRequest;
use App\Http\Requests\UpdateClienteRazonSocialRequest;
use App\Repositories\ClienteRazonSocialRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ClienteRazonSocialController extends AppBaseController
{
    /** @var  ClienteRazonSocialRepository */
    private $clienteRazonSocialRepository;

    public function __construct(ClienteRazonSocialRepository $clienteRazonSocialRepo)
    {
        $this->clienteRazonSocialRepository = $clienteRazonSocialRepo;
    }

    /**
     * Display a listing of the ClienteRazonSocial.
     *
     * @param ClienteRazonSocialDataTable $clienteRazonSocialDataTable
     * @return Response
     */
    public function index(ClienteRazonSocialDataTable $clienteRazonSocialDataTable)
    {
        return $clienteRazonSocialDataTable->render('cliente_razon_socials.index');
    }

    /**
     * Show the form for creating a new ClienteRazonSocial.
     *
     * @return Response
     */
    public function create()
    {
        return view('cliente_razon_socials.create');
    }

    /**
     * Store a newly created ClienteRazonSocial in storage.
     *
     * @param CreateClienteRazonSocialRequest $request
     *
     * @return Response
     */
    public function store(CreateClienteRazonSocialRequest $request)
    {
        $input = $request->all();

        $clienteRazonSocial = $this->clienteRazonSocialRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('clienteRazonSocials.create').'?pop=1&close=1');
        }

        return redirect(route('clienteRazonSocials.index'));
    }

    /**
     * Display the specified ClienteRazonSocial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clienteRazonSocial = $this->clienteRazonSocialRepository->find($id);

        if (empty($clienteRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('clienteRazonSocials.index'));
        }

        return view('cliente_razon_socials.show')->with('clienteRazonSocial', $clienteRazonSocial);
    }

    /**
     * Show the form for editing the specified ClienteRazonSocial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clienteRazonSocial = $this->clienteRazonSocialRepository->find($id);

        if (empty($clienteRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('clienteRazonSocials.index'));
        }

        return view('cliente_razon_socials.edit')->with('clienteRazonSocial', $clienteRazonSocial);
    }

    /**
     * Update the specified ClienteRazonSocial in storage.
     *
     * @param  int              $id
     * @param UpdateClienteRazonSocialRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClienteRazonSocialRequest $request)
    {
        $clienteRazonSocial = $this->clienteRazonSocialRepository->find($id);

        if (empty($clienteRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('clienteRazonSocials.index'));
        }

        $clienteRazonSocial = $this->clienteRazonSocialRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('clienteRazonSocials.create').'?pop=1&close=1');
        }

        return redirect(route('clienteRazonSocials.index'));
    }

    /**
     * Remove the specified ClienteRazonSocial from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clienteRazonSocial = $this->clienteRazonSocialRepository->find($id);

        if (empty($clienteRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('clienteRazonSocials.index'));
        }

        $this->clienteRazonSocialRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('clientes.show', ['id'=> $clienteRazonSocial->cliente_id]));
        // return redirect(route('clienteRazonSocials.index'));
    }
}
