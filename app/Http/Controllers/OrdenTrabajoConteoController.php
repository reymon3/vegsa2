<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoConteoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoConteoRequest;
use App\Http\Requests\UpdateOrdenTrabajoConteoRequest;
use App\Repositories\OrdenTrabajoConteoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\OrdenTrabajoConteo;
use App\Models\Rubros;
use App\Models\ActivoFijo;

use Illuminate\Http\Request;
use Response;

class OrdenTrabajoConteoController extends AppBaseController
{
    /** @var  OrdenTrabajoConteoRepository */
    private $ordenTrabajoConteoRepository;

    public function __construct(OrdenTrabajoConteoRepository $ordenTrabajoConteoRepo)
    {
        $this->ordenTrabajoConteoRepository = $ordenTrabajoConteoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoConteo.
     *
     * @param OrdenTrabajoConteoDataTable $ordenTrabajoConteoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoConteoDataTable $ordenTrabajoConteoDataTable)
    {
        return $ordenTrabajoConteoDataTable->render('orden_trabajo_conteos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoConteo.
     *
     * @return Response
     */
    public function create()
    {
        return view('orden_trabajo_conteos.create');
    }

    /**
     * Store a newly created OrdenTrabajoConteo in storage.
     *
     * @param CreateOrdenTrabajoConteoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoConteoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->create($input);

        Flash::success('Orden Trabajo Conteo saved successfully.');

        return redirect(route('ordenTrabajoConteos.index'));
    }

    /**
     * Display the specified OrdenTrabajoConteo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->find($id);

        if (empty($ordenTrabajoConteo)) {
            Flash::error('Orden Trabajo Conteo not found');

            return redirect(route('ordenTrabajoConteos.index'));
        }

        return view('orden_trabajo_conteos.show')->with('ordenTrabajoConteo', $ordenTrabajoConteo);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoConteo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->find($id);

        if (empty($ordenTrabajoConteo)) {
            Flash::error('Orden Trabajo Conteo not found');

            return redirect(route('ordenTrabajoConteos.index'));
        }

        return view('orden_trabajo_conteos.edit')->with('ordenTrabajoConteo', $ordenTrabajoConteo);
    }

    /**
     * Update the specified OrdenTrabajoConteo in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoConteoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoConteoRequest $request)
    {
        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->find($id);

        if (empty($ordenTrabajoConteo)) {
            Flash::error('Orden Trabajo Conteo not found');

            return redirect(route('ordenTrabajoConteos.index'));
        }

        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->update($request->all(), $id);

        Flash::success('Orden Trabajo Conteo updated successfully.');

        return redirect(route('ordenTrabajoConteos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoConteo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoConteo = $this->ordenTrabajoConteoRepository->find($id);

        if (empty($ordenTrabajoConteo)) {
            Flash::error('Orden Trabajo Conteo not found');

            return redirect(route('ordenTrabajoConteos.index'));
        }

        $this->ordenTrabajoConteoRepository->delete($id);

        Flash::success('Orden Trabajo Conteo deleted successfully.');

        return redirect(route('ordenTrabajoConteos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirConteo(Request $request)
    {
        if (!$request->hasFile('fileImport')) {
            return \Redirect::back()->withErrors( 'Favor de seleccionar un archivo válido');
        }

        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');
        $avatar = $request->file('fileImport');
        $excelFilepath = $avatar->getPathName();
        $filename = time().'_'.$avatar->getClientOriginalName();

        $spreadsheetxlsx = \PhpOffice\PhpSpreadsheet\IOFactory::load($excelFilepath);
        $worksheet = $spreadsheetxlsx->getActiveSheet();
        // lee datos de Excel y los almacena en una matriz
        $xls_data = $worksheet->toArray(null, true, true, true);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

        $mapeo = [
            "A" => "folio",
            "B" => "placa_anterior",
            "C" => "placa_actual",
            "D" => "placa_homologada",
            "E" => "placa_auxiliar_1",
            "F" => "placa_auxiliar_2",
            "G" => "catalogo",
            "H" => "descripcion",
            "I" => "marca",
            "J" => "modelo",
            "K" => "serie",
            "L" => "no_mantenimiento",
            "M" => "area",
            "N" => "ubicacion",
            "O" => "usuario_conteo",
            "P" => "piso",
            "Q" => "planta",
            "R" => "porcentaje_depreciacion_fisica",
            "S" => "anio_fabricacion",
            "T" => "observaciones",
            // "U" =>
        ];

        $errores = [];
        $blameId =\Auth::id();
        $createdAt = date("Y-m-d H:i:s");

        $rubros = ActivoFijo::pluck( 'rubro_id', 'nombre' );
        $rubrosIds = Rubros::pluck( 'nombre',  'id' );
        // dd($rubros, $rubrosIds);

        for ($indiceHoja = 0; $indiceHoja < 1; $indiceHoja++) {
            # Obtener hoja en el índice que vaya del ciclo
            $hojaActual = $spreadsheetxlsx->getSheet($indiceHoja);
            // echo "<h3>Vamos en la hoja con índice $indiceHoja</h3>";

            $registros = [];
            # Iterar filas
            foreach ($hojaActual->getRowIterator() as $fila) {

                // $data = new OrdenTrabajoConteo();
                foreach ($fila->getCellIterator() as $celda) {

                    # Fila, que comienza en 1, luego 2 y así...
                    $fila = $celda->getRow();

                    # Columna, que es la A, B, C y así...
                    $columna = $celda->getColumn();

                    if($fila < 2 || $columna > 'T' ){
                        break;
                    }

                    if($columna == 'A' ){
                        $registros[$fila]['orden_trabajo_id'] = $input['orden_trabajo_id'];
                        $registros[$fila]['blame_id'] = $blameId;
                        $registros[$fila]['created_at'] = $createdAt;
                        $registros[$fila]['updated_at'] = $createdAt;

                    }

                    // Aquí podemos obtener varias cosas interesantes
                    #https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Cell/Cell.html

                    # El valor, así como está en el documento
                    $valorRaw = $celda->getValue();

                    $registros[$fila][$mapeo[$columna]] = is_null($valorRaw) ? $valorRaw: (string)$valorRaw;

                    if($columna == 'G'){

                        // $key = array_search('green', $rubros); // $key = 2;
//                         dd(
//                         $registros[$fila][$mapeo[$columna]],
//                         $rubros[$registros[$fila][$mapeo[$columna]]],
//                         $rubrosIds[$rubros[$registros[$fila][$mapeo[$columna]]]],
// $rubrosIds
//                         );

                        $registros[$fila]['rubro_id'] = isset ($rubros[$registros[$fila][$mapeo[$columna]]]) ? $rubros[$registros[$fila][$mapeo[$columna]]] : 0;

                        $registros[$fila]['rubro'] = isset ($rubrosIds[$registros[$fila]['rubro_id']]) ? $rubrosIds[$registros[$fila]['rubro_id']] : 0;

                        // dd($registros[$fila]['rubro_id']);

                    }

                    // # Formateado por ejemplo como dinero o con decimales
                    // $valorFormateado = $celda->getFormattedValue();
                    //
                    // # Si es una fórmula y necesitamos su valor, llamamos a:
                    // $valorCalculado = $celda->getCalculatedValue();

                    // echo "En <strong>$columna$fila</strong> tenemos el valor <strong>$valorRaw</strong>. ";
                    // echo "Formateado es: <strong>$valorFormateado</strong>. ";
                    // echo "Calculado es: <strong>$valorCalculado</strong><br><br>";
                }

                if($fila > 1  ){

                    // dd($registros);

                    $validator = \Validator::make($registros[$fila], OrdenTrabajoConteo::$rules);

                    if ($validator->fails()) {
                        $errores[$registros[$fila]['folio']] = $validator->messages()->all();
                    }

                    if (count($errores) > OrdenTrabajoConteo::MAX_ERROR_ROWS) {

                        // dd(
                        //     // 'fila: '. $fila,
                        //     // $registros[$fila],
                        //     $errores,
                        //     // $validator->errors(),
                        //     count($errores)
                        // );
                        return \Redirect::back()->with('errores', $errores);
                    }
                }
            }
        }

        // dd($registros);
        if (count($errores)) {
            return \Redirect::back()->with('errores', $errores);
        }

        \DB::beginTransaction();

        try {
            // OrdenTrabajoConteo::where('orden_trabajo_id', $input['orden_trabajo_id'])->delete();
            OrdenTrabajoConteo::where('orden_trabajo_id', $input['orden_trabajo_id'])->forceDelete();
            OrdenTrabajoConteo::insert($registros);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }

        Flash::success('Registros importados');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoConteos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoConteos.show', ['id' => $input['orden_trabajo_id']]));
}

/**
 * Remove the specified OrdenTrabajoBaseContable from storage.
 *
 * @param int $id
 *
 * @return Response
 */
public function descargaConteo($id, Request $request)
{

    $conteo = \DB::table('ordenes_trabajo_conteos')
        ->select(\DB::raw(
            'rubro_id,'
            .'rubros.nombre,'
            .'COUNT(*) registros'
            // .'SUM(vrn) vrn,'
            // .'SUM(dep_acum) dep_acum,'
            // .'SUM(vnr) vnr,'
            // .'AVG(vur) vur,'
            // .'SUM(da) da'
            )
        )
        ->leftJoin('rubros', 'ordenes_trabajo_conteos.rubro_id', '=', 'rubros.id')
        ->where('orden_trabajo_id', $id)
        ->groupBy('rubro_id')
        ->get();

    // $data = OrdenTrabajoBaseContable::find($id);
    $file_name = date("Y-m-d_H:i:s").'_conteo_'.'.xls';
    // $aEncabezado = array_filter(json_decode($data->json_cabeceras));

    $contador = 0;

    $totalRegistros = 0;
    $totalVRN = 0;
    $totalDepAcum = 0;
    $totalVNR = 0;
    $totalVUR = 0;
    $totalDA = 0;

    $excel_file = '<table>
        <thead>
            <tr>
                <th>Rubros</th>
                <th>Registros</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($conteo as $registro){

            if(!isset($registro->nombre)){
                break;
            }

            $contador++;
            $totalRegistros += $registro->registros;

        $excel_file .= '<tr>
                    <td>'.$registro->nombre.'</td>
                    <td>'.$registro->registros.'</td>
                </tr>';
            }
        $excel_file .= '</tbody>';

        $excel_file .= "
        <tfoot>
            <tr>
                <th>Total general</th>
                <th>$totalRegistros</th>
            </tr>
        </tfoot>
    </table>";

    // $excel_file .= '
    //     <table>
    //         <thead>
    //             <tr>';
    // foreach ($aEncabezado as $clave => $valor) {
    //     $excel_file .= "<th>{$valor}</th>";
    // }
    //
    // $excel_file .= '
    //             </tr>
    //         </thead>
    //     </table>';

    header('Content-type: application/vnd.ms-excel');
    header("Content-Disposition: attachment; filename=$file_name");

    echo $excel_file;
    die();
}



}
