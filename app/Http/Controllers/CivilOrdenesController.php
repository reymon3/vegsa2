<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\OrdenTrabajoCivilDataTable;
use App\Repositories\OrdenTrabajoRepository;
use DB;
use App\Models\Direccion;
use App\Models\Antecedente;
use App\Models\CaracteristicaUrbana;
use App\Models\ConsideracionPrevia;

class CivilOrdenesController extends Controller
{
    /** @var  OrdenTrabajoRepository */
    private $ordenTrabajoRepository;

    public function __construct(OrdenTrabajoRepository $ordenTrabajoRepo)
    {
        $this->ordenTrabajoRepository = $ordenTrabajoRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrdenTrabajoCivilDataTable $ordenTrabajoDataTable)
    {
        return $ordenTrabajoDataTable->render('civil.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_orden_trabajo = intval($request->get('ordenTrabajo'));

        if($request->operacion == "antecedentes") {
            // Direccion Solicitante
            $direccion = new Direccion();
            $direccion->calle       = $request->get('calle');
            $direccion->numero      = $request->get('numero');
            $direccion->colonia     = $request->get('colonia'); 
            $direccion->cp          = $request->get('cp');
            $direccion->estado      = $request->get('estado');
            $direccion->alcaldia    = $request->get('alcaldia');
            $direccion->capital     = $request->get('capital');
            $direccion->id_pais     = $request->get('pais');
            $direccion->save();
            $iddireccionSolicitante = DB::select('select id_direccion from direccion order by id_direccion desc limit 1');
            // Direccion Propietario
            $direccionInmueble = new Direccion();
            $direccionInmueble->calle       = $request->get('calleinmueble');
            $direccionInmueble->numero      = $request->get('numeroinmueble');
            $direccionInmueble->colonia     = $request->get('coloniainmueble'); 
            $direccionInmueble->cp          = $request->get('cpinmueble');
            $direccionInmueble->estado      = $request->get('estadoinmueble');
            $direccionInmueble->alcaldia    = $request->get('alcaldiainmueble');
            $direccionInmueble->capital     = $request->get('capitalinmueble');
            $direccionInmueble->id_pais     = $request->get('paisinmueble');
            $direccionInmueble->save();
            $iddireccionInmueble = DB::select('select id_direccion from direccion order by id_direccion desc limit 1');
            // Antecedentes
            $request->get('optradio1') == "solicitantemoral" ? $regSol = 2 : $regSol = 1;
            $request->get('optradio') == "propietariomoral" ? $regPro = 2 : $regPro = 1;
            $request->get('nombresolicitante') != null ? $nom = $request->get('nombresolicitante') : $nom = $nom = $request->get('nombrerazonsocialsolicitante');
            $request->get('nombrepropietario') != null ? $nompro = $request->get('nombrepropietario') : $nompro = $nom = $request->get('nombrerazonsocialsolicitante');
            $request->get('apasolicitante') != null ? $apaS = $request->get('apasolicitante') : $apaS = "NA";
            $request->get('amasolicitante') != null ? $amaS = $request->get('amasolicitante') : $amaS = "NA";
            $request->get('razonsocialsolicitante') != null ? $razonSol = $request->get('razonsocialsolicitante') : $razonSol = "NA";
            $request->get('segundonombrepropietario') ? $segundoNomP = $request->get('segundonombrepropietario') : $segundoNomP = "NA";
            $request->get('apapropietario') != null ? $apaP = $request->get('apapropietario') : $apaP = "NA";
            $request->get('amapropietario') != null ? $amaP = $request->get('amapropietario') : $amaP = "NA";
            $request->get('razonsocialpropietario') != null ? $razonPro = $request->get('razonsocialpropietario') : $razonPro = "NA";

            $antecedente = new Antecedente();
            $antecedente->id_ordentrabajo              = $id_orden_trabajo;
            $antecedente->id_regimensolicitante        = $regSol;
            $antecedente->segundo_nombre               = $nom;
            $antecedente->apa                          = $apaS;
            $antecedente->ama                          = $amaS;
            $antecedente->id_razon_social              = $razonSol;
            $antecedente->id_domicilio_solicitante     = $iddireccionSolicitante[0]->id_direccion;
            $antecedente->fecha_avaluo                 = $request->get('fecha');
            $antecedente->id_perito_valuador           = $request->get('perito');
            $antecedente->id_inmueble_valuado          = $request->get('inmueble');
            $antecedente->id_regimen_propiedad         = $request->get('regimenpromiedad');
            $antecedente->id_regimenpropietario        = $regPro;
            $antecedente->nombre_propietario           = $nompro;
            $antecedente->segundo_nombre_propietario   = $segundoNomP;
            $antecedente->apa_propietario              = $apaP;
            $antecedente->ama_propietario              = $amaP;
            $antecedente->id_razon_social_propietario  = $razonPro;
            $antecedente->id_destino_avaluo            = $request->get('destino');
            $antecedente->id_direccion_ubicacion_inmueble  = $iddireccionInmueble[0]->id_direccion;
            $antecedente->clave_catastral              = $request->get('clavecatastral');
            $antecedente->contrato_agua                = $request->get('contratoagua');
            $antecedente->creado                       = date('Y-m-d');
            $antecedente->modificado                   = date('Y-m-d');
            $antecedente->id_usuario_alta              = 1;
            $antecedente->save();

            return back();
        }elseif($request->operacion == "carUrbanas"){
            // dd($request);
            $request->get('principalesviasacceso') != null ? $vias = $request->get('principalesviasacceso') : $vias = "NA"; 
            $caracteristicaUrbana = new CaracteristicaUrbana();
            $caracteristicaUrbana->id_orden_trabajo                = $id_orden_trabajo;
            $caracteristicaUrbana->id_clasificacion_zona           = $request->get('clasificacionzona');
            $caracteristicaUrbana->id_tipo_contruccion_dominante   = $request->get('construcciondominante');
            $caracteristicaUrbana->indice_saturacion_zona          = $request->get('indicesaturacion');
            $caracteristicaUrbana->id_poblacion                    = $request->get('poblacion');
            $caracteristicaUrbana->id_uso_predio                   = $request->get('usopredio');
            $caracteristicaUrbana->id_contaminacion_ambiental      = $request->get('contaminacionambiental');
            $caracteristicaUrbana->uso_principales_vias_acceso     = $vias;
            $caracteristicaUrbana->id_servicios_municipales        = $request->get('serviciosmunicipales');
            $caracteristicaUrbana->creado                          = date('Y-m-d');
            $caracteristicaUrbana->id_creador                      = 1;
            $caracteristicaUrbana->save();
            return back();

        }elseif($request->operacion == "terreno"){
        }elseif($request->operacion == "descGralPredio"){
        }elseif($request->operacion == "elemConstruccion"){
        }elseif($request->operacion == "consideracionesPrevias"){
            // dd($request);
            $request->get('optradio') == "fisico" ? $metodo = 1 : $metodo = 2;
            // dd($metodo);
            $metodo == 1 ? $metodocomparativomercado = "NA" : $metodocomparativomercado = $request->get('metodocomparativomercado');
            $metodo == 1 ? $metodocapitalizacionrentas = "NA" : $metodocapitalizacionrentas = $request->get('metodocapitalizacionrentas');

            // dd($metodocapitalizacionrentas);
            $consideracion = new ConsideracionPrevia();
            $consideracion->id_orden_trabajo        = $id_orden_trabajo;
            $consideracion->id_metodo               = $metodo;
            $consideracion->costos                  = $request->get('metododecostos');
            $consideracion->comparativo_mercado     = $metodocomparativomercado;
            $consideracion->capitalizacion_rentas   = $metodocapitalizacionrentas;
            $consideracion->valor_comercial         = $request->get('valorcomercial');
            $consideracion->condiciones_limitantes  = $request->get('condicioneslimitantes');
            $consideracion->creado                  = date('Y-m-d');
            $consideracion->save();

            return back();
        }elseif($request->operacion == "estudioMercado"){
        }elseif($request->operacion == "metodoCostoDirecto"){
        }elseif($request->operacion == "metodoMercado"){
        }elseif($request->operacion == "valorFisisco"){
        }elseif($request->operacion == "resumenValores"){
        }elseif($request->operacion == "considPrevias"){
        }elseif($request->operacion == "conclusiones"){
        }elseif($request->operacion == "anexos"){
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Id
        $id_orden_trabajo = $id;
        //Antecedentes
        $cat_razonSocial = DB::select('SELECT * FROM cat_razonSocial');
        $cat_paises = DB::select('SELECT * FROM cat_pais');
        $cat_perito = DB::select('SELECT * FROM cat_peritovaluador');
        $cat_inmuebles = DB::select('SELECT * FROM cat_inmueble');
        $cat_regimenPropiedad = DB::select('SELECT * FROM cat_regimen_propiedad');
        $cat_destinoAvaluo = DB::select('SELECT * FROM cat_destino_avaluo');

        //Caracteristicas Urbanas
        $cat_clasificacionzona = DB::select('SELECT * FROM cat_clasificacionzona');
        $cat_tipoconstrucciondominante = DB::select('SELECT * FROM cat_tipoconstrucciondominante');
        $cat_poblacion = DB::select('SELECT * FROM cat_poblacion');
        $cat_usopredio = DB::select('SELECT * FROM cat_usopredio');
        $cat_contaminacionambiental = DB::select('SELECT * FROM cat_contaminacionambiental');
        $cat_serviciosmunicipales = DB::select('SELECT * FROM cat_serviciosmunicipalesequipmientourbano');


        // Terreno
        $cat_topografiaconfiguracion = DB::select('SELECT * FROM ca_topografiaconfiguracion');
        $cat_caracteristicaspanoramicas = DB::select('SELECT * FROM cat_caracteristicaspanoramicas');
        $cat_densidadhabitacional = DB::select('SELECT * FROM cat_densidadhabitacional');
        $cat_intensidadconstruccion = DB::select('SELECT * FROM cat_intensidadcontruccionpermitido');


        $orden = DB::select('
            SELECT a.*, c.nombre
            FROM ordenes_trabajo a, cotizaciones b, clientes c
            WHERE a.id = '.$id.' 
            AND a.cotizacion_id = b.id 
            AND b.cliente_id = c.id
            ORDER BY id DESC
        ');
        return view('civil.show')
            ->with('id_orden_trabajo', $id_orden_trabajo)
            ->with('cat_razonSocial', $cat_razonSocial)
            ->with('cat_paises', $cat_paises)
            ->with('cat_perito', $cat_perito)
            ->with('cat_inmuebles', $cat_inmuebles)
            ->with('cat_regimenPropiedad', $cat_regimenPropiedad)
            ->with('cat_destinoAvaluo', $cat_destinoAvaluo)
            ->with('orden', $orden)
            ->with('cat_clasificacionzona', $cat_clasificacionzona) //carUrbanas
            ->with('cat_tipoconstrucciondominante', $cat_tipoconstrucciondominante)
            ->with('cat_poblacion', $cat_poblacion)
            ->with('cat_usopredio', $cat_usopredio)
            ->with('cat_contaminacionambiental', $cat_contaminacionambiental)
            ->with('cat_serviciosmunicipales', $cat_serviciosmunicipales)
            ->with('cat_topografiaconfiguracion' , $cat_topografiaconfiguracion)//terreno
            ->with('cat_caracteristicaspanoramicas' , $cat_caracteristicaspanoramicas)
            ->with('cat_densidadhabitacional' , $cat_densidadhabitacional)
            ->with('cat_intensidadconstruccion' , $cat_intensidadconstruccion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
