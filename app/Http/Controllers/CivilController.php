<?php

namespace App\Http\Controllers;

use App\DataTables\CivilDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCivilRequest;
use App\Http\Requests\UpdateCivilRequest;
use App\Repositories\CivilRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CivilController extends AppBaseController
{
    /** @var  CivilRepository */
    private $civilRepository;

    public function __construct(CivilRepository $civilRepo)
    {
        $this->civilRepository = $civilRepo;
    }

    /**
     * Display a listing of the Civil.
     *
     * @param CivilDataTable $civilDataTable
     * @return Response
     */
    public function index(CivilDataTable $civilDataTable)
    {
        return $civilDataTable->render('civils.index');
    }

    /**
     * Show the form for creating a new Civil.
     *
     * @return Response
     */
    public function create()
    {
        return view('civils.create');
    }

    /**
     * Store a newly created Civil in storage.
     *
     * @param CreateCivilRequest $request
     *
     * @return Response
     */
    public function store(CreateCivilRequest $request)
    {
        $input = $request->all();

        $civil = $this->civilRepository->create($input);

        Flash::success('Civil saved successfully.');

        return redirect(route('civils.index'));
    }

    /**
     * Display the specified Civil.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $civil = $this->civilRepository->find($id);

        if (empty($civil)) {
            Flash::error('Civil not found');

            return redirect(route('civils.index'));
        }

        return view('civils.show')->with('civil', $civil);
    }

    /**
     * Show the form for editing the specified Civil.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $civil = $this->civilRepository->find($id);

        if (empty($civil)) {
            Flash::error('Civil not found');

            return redirect(route('civils.index'));
        }

        return view('civils.edit')->with('civil', $civil);
    }

    /**
     * Update the specified Civil in storage.
     *
     * @param  int              $id
     * @param UpdateCivilRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCivilRequest $request)
    {
        $civil = $this->civilRepository->find($id);

        if (empty($civil)) {
            Flash::error('Civil not found');

            return redirect(route('civils.index'));
        }

        $civil = $this->civilRepository->update($request->all(), $id);

        Flash::success('Civil updated successfully.');

        return redirect(route('civils.index'));
    }

    /**
     * Remove the specified Civil from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $civil = $this->civilRepository->find($id);

        if (empty($civil)) {
            Flash::error('Civil not found');

            return redirect(route('civils.index'));
        }

        $this->civilRepository->delete($id);

        Flash::success('Civil deleted successfully.');

        return redirect(route('civils.index'));
    }
}
