<?php

namespace App\Http\Controllers;

use App\DataTables\ReporteadorDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateReporteadorRequest;
use App\Http\Requests\UpdateReporteadorRequest;
use App\Repositories\ReporteadorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\Reporteador;
use App\Exports\ReporteadorExport;
use Maatwebsite\Excel\Facades\Excel;


class ReporteadorController extends Controller
{
    private $reporteadorRepository;

  public function __construct(ReporteadorRepository $reporteadorRepo)
  {
      $this->reporteadorRepository = $reporteadorRepo;
  }

    public function index(ReporteadorDataTable $reporteadorDataTable)
    {
        return $reporteadorDataTable->render('reporteador.index');
    }

    public function download()
    {
        return Excel::download(new ReporteadorExport, 'users.xlsx');
        // $datos = new ReporteadorExport;
        // dd($datos);
        // Excel::create('Laravel Excel', function($excel) {
        //     $excel->sheet('Excel sheet', function($sheet) {
        //         //otra opción -> $products = Product::select('name')->get();
        //         $products = Reporteador::all();                
        //         $sheet->fromArray($products);
        //         $sheet->setOrientation('landscape');
        //     });
        // })->export('xls');
    }
}
