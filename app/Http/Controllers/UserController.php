<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Models\Empleados;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {

        $roles = \jeremykenedy\LaravelRoles\Models\Role::pluck('name', 'id');
        $empleados = Empleados::doesntHave('usuario')->pluck('nombre', 'id');

        return view('users.create', compact('roles', 'empleados'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $request->validate([
            'rol_id' => 'bail|required',
        ]);

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = $this->userRepository->create($input);
        $rolUser = config('roles.models.defaultUser')::find($user->id);
        $rolUser->attachRole($input['rol_id']);
        Flash::success('Registro insertado.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('users.index'));
        }

        $roles = \jeremykenedy\LaravelRoles\Models\Role::pluck('name', 'id');
        $rolUser = config('roles.models.defaultUser')::find($user->id);

        if( $user->empleado_id > 0){
            $empleados = Empleados::where('id', $user->empleado_id)->pluck('nombre', 'id');
        } else {
            $empleados = Empleados::doesntHave('usuario')->pluck('nombre', 'id');
        }

// dd($empleados);
        return view('users.edit', compact('user', 'roles', 'rolUser', 'empleados'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = User::find($id);
        $input  = unsetEmptyInput($request->all());
        if (is_null($user)) {
            Flash::error('Registro no encontrado.');
            return redirect(route('users.edit'));
        }

        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }

        $user = $this->userRepository->update($input, $id);

        $rolUser = config('roles.models.defaultUser')::find($user->id);

        $rolUser->detachAllRoles();
        $rolUser->attachRole($input['rol_id']);

        Flash::success('Registro actualizado.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('users.index'));
    }
}
