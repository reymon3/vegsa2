<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoNormaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoNormaRequest;
use App\Http\Requests\UpdateOrdenTrabajoNormaRequest;
use App\Repositories\OrdenTrabajoNormaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoNormaController extends AppBaseController
{
    /** @var  OrdenTrabajoNormaRepository */
    private $ordenTrabajoNormaRepository;

    public function __construct(OrdenTrabajoNormaRepository $ordenTrabajoNormaRepo)
    {
        $this->ordenTrabajoNormaRepository = $ordenTrabajoNormaRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoNorma.
     *
     * @param OrdenTrabajoNormaDataTable $ordenTrabajoNormaDataTable
     * @return Response
     */
    public function index(OrdenTrabajoNormaDataTable $ordenTrabajoNormaDataTable)
    {
        return $ordenTrabajoNormaDataTable->render('orden_trabajo_normas.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoNorma.
     *
     * @return Response
     */
    public function create()
    {
        $normas = \App\Models\Normas::pluck('nombre', 'id');
        return view('orden_trabajo_normas.create', compact('normas'));
    }

    /**
     * Store a newly created OrdenTrabajoNorma in storage.
     *
     * @param CreateOrdenTrabajoNormaRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoNormaRequest $request)
    {
        $input = $request->all();
        // dd($input);
        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoNormas.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoNormas.index'));
    }

    /**
     * Display the specified OrdenTrabajoNorma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->find($id);

        if (empty($ordenTrabajoNorma)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoNormas.index'));
        }

        return view('orden_trabajo_normas.show')->with('ordenTrabajoNorma', $ordenTrabajoNorma);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoNorma.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->find($id);

        if (empty($ordenTrabajoNorma)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoNormas.index'));
        }

        return view('orden_trabajo_normas.edit')->with('ordenTrabajoNorma', $ordenTrabajoNorma);
    }

    /**
     * Update the specified OrdenTrabajoNorma in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoNormaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoNormaRequest $request)
    {
        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->find($id);

        if (empty($ordenTrabajoNorma)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoNormas.index'));
        }

        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoNormas.index'));
    }

    /**
     * Remove the specified OrdenTrabajoNorma from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoNorma = $this->ordenTrabajoNormaRepository->find($id);

        if (empty($ordenTrabajoNorma)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoNormas.index'));
        }

        $this->ordenTrabajoNormaRepository->delete($id);

        Flash::success('Registro eliminado.');


        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoNorma->orden_trabajo_id]));
        // return redirect(route('ordenTrabajoNormas.show'));
    }
}
