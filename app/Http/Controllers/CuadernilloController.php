<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Indice;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

use DB;

class CuadernilloController extends Controller
{
    public function index(){
        $clientes = Cliente::all();
        $indices = DB::select('select * from indice');
        return view('cuadernillo.index')
                ->with('indices', $indices)
                ->with('clientes', $clientes);
    }

    public function portada(){
        $pathtoFile = public_path().'\downloads\cuadernillo\portada.pdf';
        return response()->download($pathtoFile);
    }

    public function contenidoCrear(){
        $clientes = Cliente::all();
        return view('cuadernillo.crearContenido')
            ->with('clientes', $clientes);
    }

    public function contenidoCrearIndice(){
        $clientes = Cliente::all();
        $indices = DB::select('select * from indice');
        return view('cuadernillo.crearIndice')
            ->with('indices', $indices)
            ->with('clientes', $clientes);
    }
    
    public function crearIndice(Request $request){
        // $cliente = Cliente::find($request->cliente);

        if ($request->operacion == "indice") {
            $indice = $request->indices;
            $pdf = PDF::loadView('cuadernillo.indice', compact('indice'))->setPaper('letter', 'portrait');
            return $pdf->download('indice.pdf');
        }else{
            $cliente = Cliente::find($request->cliente);
            $indice = $request->indices;
            $pdf = PDF::loadView('cuadernillo.separadores', compact('indice', 'cliente'))->setPaper('letter', 'portrait');
            return $pdf->download('separadores.pdf');
        }
    }

    public function store(Request $request){
        $array = array("2F5597", "FFC000", "7CE2B9", "2091CA", "BFBFBF");
        if($request->operacion == "portada") {
            $pathtoFile = public_path().'\downloads\cuadernillo\portada.pdf';
            return response()->download($pathtoFile);
        }elseif($request->operacion == "contenido"){
            $cliente = Cliente::find($request->cliente);
            $descripcion = $request->descripcion;
            $pdf = PDF::loadView('cuadernillo.contenido', compact('cliente','descripcion'))->setPaper('letter', 'portrait');
            return $pdf->download('contenido.pdf');
        }elseif($request->operacion == "indice"){
            $indice = $request->indices;
            $pdf = PDF::loadView('cuadernillo.indice', compact('indice'))->setPaper('letter', 'portrait');
            return $pdf->download('indice.pdf');
        }elseif($request->operacion == "separadores"){
            $cliente = Cliente::find($request->cliente);
            $indice = $request->indices;
            $nombreCorto = $request->corto;
            $descripcion = $request->descripcion;
            $pdf = PDF::loadView('cuadernillo.separadores', compact('indice', 'cliente', 'descripcion', 'nombreCorto'))->setPaper('letter', 'portrait');
            return $pdf->download('separadores.pdf');
        }elseif($request->operacion == "certificado"){
            $fecha = $request->fecha;
            $moneda = $request->moneda;
            $pdf = PDF::loadView('cuadernillo.certificado', compact('fecha', 'moneda'))->setPaper('letter', 'portrait');
            return $pdf->download('certificado.pdf');
        }elseif($request->operacion == "limitantes"){
            $cliente = $request->cliente;
            $clienteInfo = Cliente::find($request->cliente);
            $consulta = DB::select('
            select a.* from avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            where a.avaluo_id = b.id
            and b.orden_trabajo_id = c.id
            and c.cotizacion_id = d.id 
            and d.cliente_id = e.id
            and e.id = '.$cliente.' ');
            $pdf = PDF::loadView('cuadernillo.limitantes',compact('consulta','clienteInfo'))->setPaper('letter', 'portrait');
            return $pdf->download('limitantes.pdf');
        }elseif($request->operacion == "reporte"){
            $file = $request->file('logotipo');
            $nombreArchivo = $file->getClientOriginalName();
            \Storage::disk('logotipo')->put($nombreArchivo,  \File::get($file));
            $cliente = $request->cliente;
            $maquirariaEquipo = DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            $equipoLaboratorio = DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            $mobiliarioIndustrial = DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            $equipoTransporte = DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            $equipoComputo = DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            $mobiliarioEquipo =DB::select('
            SELECT a.* FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            $clienteInfo = Cliente::find($request->cliente);
            $pdf = PDF::loadView('cuadernillo.reporte',
            compact(
                'maquirariaEquipo',
                'equipoLaboratorio',
                'mobiliarioIndustrial',
                'equipoTransporte',
                'equipoComputo',
                'mobiliarioEquipo',
                'clienteInfo'
                ))->setPaper('letter', 'landscape');
            return $pdf->download('reporte.pdf');
        }elseif($request->operacion == "valores"){
            $pdf = PDF::loadView('cuadernillo.valores')->setPaper('letter', 'portrait');
            return $pdf->download('valores.pdf');
        }elseif($request->operacion == "resumen"){
            $file = $request->file('logotipo');
            $nombreArchivo = $file->getClientOriginalName();
            \Storage::disk('logotipo')->put($nombreArchivo,  \File::get($file));
            $clienteInfo = Cliente::find($request->cliente);
            // 
            $cliente = $request->cliente;
            // 
            $maquirariaEquipo = DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            $maquirariaEquipoSUM = DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            $maquirariaEquipoDEP = DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            $maquirariaEquipoVNR = DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            $maquirariaEquipoVUR = DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            $maquirariaEquipoDA = DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MAQUINARIA Y EQUIPO%"');
            // 
            // 
            $equipoLaboratorio = DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            // 
            $equipoLaboratorioSUM = DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            $equipoLaboratorioDEP = DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            $equipoLaboratorioVNR = DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            $equipoLaboratorioVUR = DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            $equipoLaboratorioDA = DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE LABORATORIO%"');
            // 
            // 
            $mobiliarioIndustrial = DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            // 
            $mobiliarioIndustrialSUM = DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            $mobiliarioIndustrialDEP = DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            $mobiliarioIndustrialVNR = DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            $mobiliarioIndustrialVUR = DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            $mobiliarioIndustrialDA = DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO INDUSTRIAL%"');
            // 
            // 
            $equipoTransporte = DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            // 
            $equipoTransporteSUM = DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            $equipoTransporteDEP = DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            $equipoTransporteVNR = DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            $equipoTransporteVUR = DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            $equipoTransporteDA = DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE TRANSPORTE%"');
            // 
            // 
            $equipoComputo = DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            // 
            $equipoComputoSUM = DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            $equipoComputoDEP = DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            $equipoComputoVNR = DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            $equipoComputoVUR = DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            $equipoComputoDA = DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%EQUIPO DE CÓMPUTO%"');
            // 
            // 
            $mobiliarioEquipo =DB::select('
            SELECT COUNT(a.id) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            // 
            $mobiliarioEquipoSUM =DB::select('
            SELECT SUM(a.vrn) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            $mobiliarioEquipoDEP =DB::select('
            SELECT SUM(a.dep_acum) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            $mobiliarioEquipoVNR =DB::select('
            SELECT SUM(a.vnr) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            $mobiliarioEquipoVUR =DB::select('
            SELECT SUM(a.vur) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            $mobiliarioEquipoDA =DB::select('
            SELECT SUM(a.da) as valores FROM avaluos_detalle a, avaluos b, ordenes_trabajo c, cotizaciones d, clientes e
            WHERE a.avaluo_id = b.id
            AND b.orden_trabajo_id = c.id
            AND c.cotizacion_id = d.id 
            AND d.cliente_id = e.id
            AND e.id = '.$cliente.'
            AND  a.rubro LIKE "%MOBILIARIO Y EQUIPO%"');
            // 
            // 
            // dd($maquirariaEquipo);
            $pdf = PDF::loadView('cuadernillo.cuadroResumen', 
            compact(
                'nombreArchivo',
                'maquirariaEquipo',
                'equipoLaboratorio',
                'mobiliarioIndustrial',
                'equipoTransporte',
                'equipoComputo',
                'mobiliarioEquipo',
                'maquirariaEquipoSUM',
                'equipoLaboratorioSUM',
                'mobiliarioIndustrialSUM',
                'equipoTransporteSUM',
                'equipoComputoSUM',

                'maquirariaEquipoDEP',
                'equipoLaboratorioDEP',
                'mobiliarioIndustrialDEP',
                'equipoTransporteDEP',
                'equipoComputoDEP',

                'maquirariaEquipoVNR',
                'equipoLaboratorioVNR',
                'mobiliarioIndustrialVNR',
                'equipoTransporteVNR',
                'equipoComputoVNR',
                'mobiliarioEquipoVNR',

                'maquirariaEquipoVUR',
                'equipoLaboratorioVUR',
                'mobiliarioIndustrialVUR',
                'equipoTransporteVUR',
                'equipoComputoVUR',
                'mobiliarioEquipoVUR',

                'maquirariaEquipoDA',
                'equipoLaboratorioDA',
                'mobiliarioIndustrialDA',
                'equipoTransporteDA',
                'equipoComputoDA',
                'mobiliarioEquipoDA',
                
                'clienteInfo'
            ))->setPaper('letter', 'landscape');
            return $pdf->download('cuadroResumen.pdf');
        }
        
    }
}
