<?php

namespace App\Http\Controllers;

use App\DataTables\AreasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAreasRequest;
use App\Http\Requests\UpdateAreasRequest;
use App\Repositories\AreasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AreasController extends AppBaseController
{
    /** @var  AreasRepository */
    private $areasRepository;

    public function __construct(AreasRepository $areasRepo)
    {
        $this->areasRepository = $areasRepo;
    }

    /**
     * Display a listing of the Areas.
     *
     * @param AreasDataTable $areasDataTable
     * @return Response
     */
    public function index(AreasDataTable $areasDataTable)
    {
        return $areasDataTable->render('areas.index');
    }

    /**
     * Show the form for creating a new Areas.
     *
     * @return Response
     */
    public function create()
    {
        return view('areas.create');
    }

    /**
     * Store a newly created Areas in storage.
     *
     * @param CreateAreasRequest $request
     *
     * @return Response
     */
    public function store(CreateAreasRequest $request)
    {
        $input = $request->all();

        $areas = $this->areasRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('areas.index'));
    }

    /**
     * Display the specified Areas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $areas = $this->areasRepository->find($id);

        if (empty($areas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('areas.index'));
        }

        return view('areas.show')->with('areas', $areas);
    }

    /**
     * Show the form for editing the specified Areas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $areas = $this->areasRepository->find($id);

        if (empty($areas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('areas.index'));
        }

        return view('areas.edit')->with('areas', $areas);
    }

    /**
     * Update the specified Areas in storage.
     *
     * @param  int              $id
     * @param UpdateAreasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAreasRequest $request)
    {
        $areas = $this->areasRepository->find($id);

        if (empty($areas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('areas.index'));
        }

        $areas = $this->areasRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('areas.index'));
    }

    /**
     * Remove the specified Areas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $areas = $this->areasRepository->find($id);

        if (empty($areas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('areas.index'));
        }

        $this->areasRepository->delete($id);

        Flash::success('Registro eliminado');

        return redirect(route('areas.index'));
    }
}
