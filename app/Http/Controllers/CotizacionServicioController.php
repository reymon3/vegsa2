<?php

namespace App\Http\Controllers;

use App\DataTables\CotizacionServicioDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCotizacionServicioRequest;
use App\Http\Requests\UpdateCotizacionServicioRequest;
use App\Repositories\CotizacionServicioRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CotizacionServicioController extends AppBaseController
{
    /** @var  CotizacionServicioRepository */
    private $cotizacionServicioRepository;

    public function __construct(CotizacionServicioRepository $cotizacionServicioRepo)
    {
        $this->cotizacionServicioRepository = $cotizacionServicioRepo;
    }

    /**
     * Display a listing of the CotizacionServicio.
     *
     * @param CotizacionServicioDataTable $cotizacionServicioDataTable
     * @return Response
     */
    public function index(CotizacionServicioDataTable $cotizacionServicioDataTable)
    {
        return $cotizacionServicioDataTable->render('cotizacion_servicios.index');
    }

    /**
     * Show the form for creating a new CotizacionServicio.
     *
     * @return Response
     */
    public function create()
    {
        $servicios = \App\Models\Servicio::pluck('nombre', 'id');
        return view('cotizacion_servicios.create', compact('servicios'));
    }

    /**
     * Store a newly created CotizacionServicio in storage.
     *
     * @param CreateCotizacionServicioRequest $request
     *
     * @return Response
     */
    public function store(CreateCotizacionServicioRequest $request)
    {
        $input = $request->all();

        $cotizacionServicio = $this->cotizacionServicioRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('cotizacionServicios.create').'?pop=1&close=1');
        }

        return redirect(route('cotizacionServicios.index'));
    }

    /**
     * Display the specified CotizacionServicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cotizacionServicio = $this->cotizacionServicioRepository->find($id);

        if (empty($cotizacionServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacionServicios.index'));
        }

        return view('cotizacion_servicios.show')->with('cotizacionServicio', $cotizacionServicio);
    }

    /**
     * Show the form for editing the specified CotizacionServicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cotizacionServicio = $this->cotizacionServicioRepository->find($id);

        if (empty($cotizacionServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacionServicios.index'));
        }

        return view('cotizacion_servicios.edit')->with('cotizacionServicio', $cotizacionServicio);
    }

    /**
     * Update the specified CotizacionServicio in storage.
     *
     * @param  int              $id
     * @param UpdateCotizacionServicioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCotizacionServicioRequest $request)
    {
        $cotizacionServicio = $this->cotizacionServicioRepository->find($id);

        if (empty($cotizacionServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacionServicios.index'));
        }

        $cotizacionServicio = $this->cotizacionServicioRepository->update($request->all(), $id);

        Flash::success('Cotizacion Servicio updated successfully.');

        return redirect(route('cotizacionServicios.index'));
    }

    /**
     * Remove the specified CotizacionServicio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cotizacionServicio = $this->cotizacionServicioRepository->find($id);

        if (empty($cotizacionServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacionServicios.index'));
        }

        $this->cotizacionServicioRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('cotizacions.show', ['id'=> $cotizacionServicio->cotizacion_id]));
    }
}
