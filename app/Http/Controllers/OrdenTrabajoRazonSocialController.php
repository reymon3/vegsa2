<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoRazonSocialDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoRazonSocialRequest;
use App\Http\Requests\UpdateOrdenTrabajoRazonSocialRequest;
use App\Repositories\OrdenTrabajoRazonSocialRepository;
use Illuminate\Http\Request;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoRazonSocialController extends AppBaseController
{
    /** @var  OrdenTrabajoRazonSocialRepository */
    private $ordenTrabajoRazonSocialRepository;

    public function __construct(OrdenTrabajoRazonSocialRepository $ordenTrabajoRazonSocialRepo)
    {
        $this->ordenTrabajoRazonSocialRepository = $ordenTrabajoRazonSocialRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoRazonSocial.
     *
     * @param OrdenTrabajoRazonSocialDataTable $ordenTrabajoRazonSocialDataTable
     * @return Response
     */
    public function index(OrdenTrabajoRazonSocialDataTable $ordenTrabajoRazonSocialDataTable)
    {
        return $ordenTrabajoRazonSocialDataTable->render('orden_trabajo_razon_socials.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoRazonSocial.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $input = $request->only('cliente_id', 'orden_trabajo_id');
        $razonesSociales = \App\Models\ClienteRazonSocial::where(
            'cliente_id',
             $input['cliente_id']
        )
        ->pluck('nombre', 'id');
        // dd($razonesSociales, $request->all());
        return view('orden_trabajo_razon_socials.create', compact('razonesSociales'));
    }

    /**
     * Store a newly created OrdenTrabajoRazonSocial in storage.
     *
     * @param CreateOrdenTrabajoRazonSocialRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoRazonSocialRequest $request)
    {
        $input = $request->all();
        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->create($input);
        // dd($input, $ordenTrabajoRazonSocial);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoRazonSocials.create').'?pop=1&close=1&cliente_id=0');
        }

        return redirect(route('ordenTrabajoRazonSocials.index'));
    }

    /**
     * Display the specified OrdenTrabajoRazonSocial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->find($id);

        if (empty($ordenTrabajoRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRazonSocials.index'));
        }

        return view('orden_trabajo_razon_socials.show')->with('ordenTrabajoRazonSocial', $ordenTrabajoRazonSocial);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoRazonSocial.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->find($id);

        if (empty($ordenTrabajoRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRazonSocials.index'));
        }

        return view('orden_trabajo_razon_socials.edit')->with('ordenTrabajoRazonSocial', $ordenTrabajoRazonSocial);
    }

    /**
     * Update the specified OrdenTrabajoRazonSocial in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoRazonSocialRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoRazonSocialRequest $request)
    {
        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->find($id);

        if (empty($ordenTrabajoRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRazonSocials.index'));
        }

        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoRazonSocials.index'));
    }

    /**
     * Remove the specified OrdenTrabajoRazonSocial from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        $ordenTrabajoRazonSocial = $this->ordenTrabajoRazonSocialRepository->find($id);
        // dd($id, $ordenTrabajoRazonSocial);

        if (empty($ordenTrabajoRazonSocial)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajos.index'));
            // return redirect(route('ordenTrabajoRazonSocials.index'));
        }

        $this->ordenTrabajoRazonSocialRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoRazonSocial->orden_trabajo_id]));
    }
}
