<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoDocumentoDataTable;
use App\Http\Requests\CreateOrdenTrabajoDocumentoRequest;
use App\Http\Requests\UpdateOrdenTrabajoDocumentoRequest;
use App\Models\OrdenTrabajoDocumento;
use App\Repositories\OrdenTrabajoDocumentoRepository;
use Flash;
use Illuminate\Http\Request;
use Response;

class OrdenTrabajoDocumentoController extends AppBaseController
{
    /** @var OrdenTrabajoDocumentoRepository */
    private $ordenTrabajoDocumentoRepository;

    public function __construct(OrdenTrabajoDocumentoRepository $ordenTrabajoDocumentoRepo)
    {
        $this->ordenTrabajoDocumentoRepository = $ordenTrabajoDocumentoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoDocumento.
     *
     * @return Response
     */
    public function index(OrdenTrabajoDocumentoDataTable $ordenTrabajoDocumentoDataTable)
    {
        return $ordenTrabajoDocumentoDataTable->render('orden_trabajo_documentos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoDocumento.
     *
     * @return Response
     */
    public function create()
    {
        return view('orden_trabajo_documentos.create');
    }

    /**
     * Store a newly created OrdenTrabajoDocumento in storage.
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoDocumentoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('ordenTrabajoDocumentos.index'));
    }

    /**
     * Display the specified OrdenTrabajoDocumento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->find($id);

        if (empty($ordenTrabajoDocumento)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoDocumentos.index'));
        }

        return view('orden_trabajo_documentos.show')->with('ordenTrabajoDocumento', $ordenTrabajoDocumento);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoDocumento.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->find($id);

        if (empty($ordenTrabajoDocumento)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoDocumentos.index'));
        }

        return view('orden_trabajo_documentos.edit')->with('ordenTrabajoDocumento', $ordenTrabajoDocumento);
    }

    /**
     * Update the specified OrdenTrabajoDocumento in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoDocumentoRequest $request)
    {
        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->find($id);

        if (empty($ordenTrabajoDocumento)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoDocumentos.index'));
        }

        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoDocumentos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoDocumento from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoDocumento = $this->ordenTrabajoDocumentoRepository->find($id);

        if (empty($ordenTrabajoDocumento)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoDocumentos.index'));
        }

        $this->ordenTrabajoDocumentoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoDocumento->orden_trabajo_id]));    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirArchivo(Request $request)
    {
        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');
        if ($request->hasFile('fileImport')) {
            $file = $request->file('fileImport');
            $fileOriginalName = $file->getClientOriginalName();
            $fileExtension = $file->getClientOriginalExtension();
            $fileRealPath = $file->getRealPath();
            $fileType = $file->getMimeType();
            $excelFilepath = $file->getPathName();
            $data = new OrdenTrabajoDocumento();
            $relativeOrdenPath = '/'. $input['orden_trabajo_id'] .'/documentos/' ;
            $destinationPath = OrdenTrabajoDocumento::CARPETA_ORDEN_TRABAJO . $relativeOrdenPath;
            $fileName = secureFileName($fileOriginalName, $relativeOrdenPath);

            $file->move(public_path($destinationPath), $fileName);
            // $file->move($destinationPath, $fileName);

            // dd(
            //     $file,
            //     $fileOriginalName,
            //     $fileExtension,
            //     $fileRealPath,
            //     $fileType,
            //     $excelFilepath,
            //     $fileName,
            //     $destinationPath,
            //     $input,
            //     $data
            // );

            $data->nombre = $fileName;
            $data->ruta = str_replace('\\', "/", $destinationPath) . $fileName; //$destinationPath . $fileName;
            $data->extension = $fileExtension;
            $data->orden_trabajo_id = $input['orden_trabajo_id'];
            $data->save();
        }

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoDocumentos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajos.show', ['id' => $input['orden_trabajo_id']]));
    }
}
