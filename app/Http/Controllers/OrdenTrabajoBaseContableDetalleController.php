<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoBaseContableDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoBaseContableDetalleRequest;
use App\Http\Requests\UpdateOrdenTrabajoBaseContableDetalleRequest;
use App\Repositories\OrdenTrabajoBaseContableDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoBaseContableDetalleController extends AppBaseController
{
    /** @var  OrdenTrabajoBaseContableDetalleRepository */
    private $ordenTrabajoBaseContableDetalleRepository;

    public function __construct(OrdenTrabajoBaseContableDetalleRepository $ordenTrabajoBaseContableDetalleRepo)
    {
        $this->ordenTrabajoBaseContableDetalleRepository = $ordenTrabajoBaseContableDetalleRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoBaseContableDetalle.
     *
     * @param OrdenTrabajoBaseContableDetalleDataTable $ordenTrabajoBaseContableDetalleDataTable
     * @return Response
     */
    public function index(OrdenTrabajoBaseContableDetalleDataTable $ordenTrabajoBaseContableDetalleDataTable)
    {
        return $ordenTrabajoBaseContableDetalleDataTable->render('orden_trabajo_base_contable_detalles.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoBaseContableDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('orden_trabajo_base_contable_detalles.create');
    }

    /**
     * Store a newly created OrdenTrabajoBaseContableDetalle in storage.
     *
     * @param CreateOrdenTrabajoBaseContableDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoBaseContableDetalleRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->create($input);

        Flash::success('Orden Trabajo Base Contable Detalle saved successfully.');

        return redirect(route('ordenTrabajoBaseContableDetalles.index'));
    }

    /**
     * Display the specified OrdenTrabajoBaseContableDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->find($id);

        if (empty($ordenTrabajoBaseContableDetalle)) {
            Flash::error('Orden Trabajo Base Contable Detalle not found');

            return redirect(route('ordenTrabajoBaseContableDetalles.index'));
        }

        return view('orden_trabajo_base_contable_detalles.show')->with('ordenTrabajoBaseContableDetalle', $ordenTrabajoBaseContableDetalle);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoBaseContableDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->find($id);

        if (empty($ordenTrabajoBaseContableDetalle)) {
            Flash::error('Orden Trabajo Base Contable Detalle not found');

            return redirect(route('ordenTrabajoBaseContableDetalles.index'));
        }

        return view('orden_trabajo_base_contable_detalles.edit')->with('ordenTrabajoBaseContableDetalle', $ordenTrabajoBaseContableDetalle);
    }

    /**
     * Update the specified OrdenTrabajoBaseContableDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoBaseContableDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoBaseContableDetalleRequest $request)
    {
        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->find($id);

        if (empty($ordenTrabajoBaseContableDetalle)) {
            Flash::error('Orden Trabajo Base Contable Detalle not found');

            return redirect(route('ordenTrabajoBaseContableDetalles.index'));
        }

        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->update($request->all(), $id);

        Flash::success('Orden Trabajo Base Contable Detalle updated successfully.');

        return redirect(route('ordenTrabajoBaseContableDetalles.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContableDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoBaseContableDetalle = $this->ordenTrabajoBaseContableDetalleRepository->find($id);

        if (empty($ordenTrabajoBaseContableDetalle)) {
            Flash::error('Orden Trabajo Base Contable Detalle not found');

            return redirect(route('ordenTrabajoBaseContableDetalles.index'));
        }

        $this->ordenTrabajoBaseContableDetalleRepository->delete($id);

        Flash::success('Orden Trabajo Base Contable Detalle deleted successfully.');

        return redirect(route('ordenTrabajoBaseContableDetalles.index'));
    }
}
