<?php

namespace App\Http\Controllers;

use App\DataTables\ArchivoMaestroDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArchivoMaestroDetalleRequest;
use App\Http\Requests\UpdateArchivoMaestroDetalleRequest;
use App\Repositories\ArchivoMaestroDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ArchivoMaestroDetalleController extends AppBaseController
{
    /** @var  ArchivoMaestroDetalleRepository */
    private $archivoMaestroDetalleRepository;

    public function __construct(ArchivoMaestroDetalleRepository $archivoMaestroDetalleRepo)
    {
        $this->archivoMaestroDetalleRepository = $archivoMaestroDetalleRepo;
    }

    /**
     * Display a listing of the ArchivoMaestroDetalle.
     *
     * @param ArchivoMaestroDetalleDataTable $archivoMaestroDetalleDataTable
     * @return Response
     */
    public function index(ArchivoMaestroDetalleDataTable $archivoMaestroDetalleDataTable)
    {
        return $archivoMaestroDetalleDataTable->render('archivo_maestro_detalles.index');
    }

    /**
     * Show the form for creating a new ArchivoMaestroDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('archivo_maestro_detalles.create');
    }

    /**
     * Store a newly created ArchivoMaestroDetalle in storage.
     *
     * @param CreateArchivoMaestroDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateArchivoMaestroDetalleRequest $request)
    {
        $input = $request->all();

        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->create($input);

        Flash::success('Archivo Maestro Detalle saved successfully.');

        return redirect(route('archivoMaestroDetalles.index'));
    }

    /**
     * Display the specified ArchivoMaestroDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->find($id);

        if (empty($archivoMaestroDetalle)) {
            Flash::error('Archivo Maestro Detalle not found');

            return redirect(route('archivoMaestroDetalles.index'));
        }

        return view('archivo_maestro_detalles.show')->with('archivoMaestroDetalle', $archivoMaestroDetalle);
    }

    /**
     * Show the form for editing the specified ArchivoMaestroDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->find($id);

        if (empty($archivoMaestroDetalle)) {
            Flash::error('Archivo Maestro Detalle not found');

            return redirect(route('archivoMaestroDetalles.index'));
        }

        return view('archivo_maestro_detalles.edit')->with('archivoMaestroDetalle', $archivoMaestroDetalle);
    }

    /**
     * Update the specified ArchivoMaestroDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateArchivoMaestroDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArchivoMaestroDetalleRequest $request)
    {
        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->find($id);

        if (empty($archivoMaestroDetalle)) {
            Flash::error('Archivo Maestro Detalle not found');

            return redirect(route('archivoMaestroDetalles.index'));
        }

        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->update($request->all(), $id);

        Flash::success('Archivo Maestro Detalle updated successfully.');

        return redirect(route('archivoMaestroDetalles.index'));
    }

    /**
     * Remove the specified ArchivoMaestroDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $archivoMaestroDetalle = $this->archivoMaestroDetalleRepository->find($id);

        if (empty($archivoMaestroDetalle)) {
            Flash::error('Archivo Maestro Detalle not found');

            return redirect(route('archivoMaestroDetalles.index'));
        }

        $this->archivoMaestroDetalleRepository->delete($id);

        Flash::success('Archivo Maestro Detalle deleted successfully.');

        return redirect(route('archivoMaestroDetalles.index'));
    }
}
