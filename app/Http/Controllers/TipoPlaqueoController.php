<?php

namespace App\Http\Controllers;

use App\DataTables\TipoPlaqueoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoPlaqueoRequest;
use App\Http\Requests\UpdateTipoPlaqueoRequest;
use App\Repositories\TipoPlaqueoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TipoPlaqueoController extends AppBaseController
{
    /** @var  TipoPlaqueoRepository */
    private $tipoPlaqueoRepository;

    public function __construct(TipoPlaqueoRepository $tipoPlaqueoRepo)
    {
        $this->tipoPlaqueoRepository = $tipoPlaqueoRepo;
    }

    /**
     * Display a listing of the TipoPlaqueo.
     *
     * @param TipoPlaqueoDataTable $tipoPlaqueoDataTable
     * @return Response
     */
    public function index(TipoPlaqueoDataTable $tipoPlaqueoDataTable)
    {
        return $tipoPlaqueoDataTable->render('tipo_plaqueos.index');
    }

    /**
     * Show the form for creating a new TipoPlaqueo.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_plaqueos.create');
    }

    /**
     * Store a newly created TipoPlaqueo in storage.
     *
     * @param CreateTipoPlaqueoRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoPlaqueoRequest $request)
    {
        $input = $request->all();

        $tipoPlaqueo = $this->tipoPlaqueoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('tipoPlaqueos.index'));
    }

    /**
     * Display the specified TipoPlaqueo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoPlaqueo = $this->tipoPlaqueoRepository->find($id);

        if (empty($tipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoPlaqueos.index'));
        }

        return view('tipo_plaqueos.show')->with('tipoPlaqueo', $tipoPlaqueo);
    }

    /**
     * Show the form for editing the specified TipoPlaqueo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoPlaqueo = $this->tipoPlaqueoRepository->find($id);

        if (empty($tipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoPlaqueos.index'));
        }

        return view('tipo_plaqueos.edit')->with('tipoPlaqueo', $tipoPlaqueo);
    }

    /**
     * Update the specified TipoPlaqueo in storage.
     *
     * @param  int              $id
     * @param UpdateTipoPlaqueoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoPlaqueoRequest $request)
    {
        $tipoPlaqueo = $this->tipoPlaqueoRepository->find($id);

        if (empty($tipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoPlaqueos.index'));
        }

        $tipoPlaqueo = $this->tipoPlaqueoRepository->update($request->all(), $id);

        Flash::success('Regsitro actualizado.');

        return redirect(route('tipoPlaqueos.index'));
    }

    /**
     * Remove the specified TipoPlaqueo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoPlaqueo = $this->tipoPlaqueoRepository->find($id);

        if (empty($tipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoPlaqueos.index'));
        }

        $this->tipoPlaqueoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('tipoPlaqueos.index'));
    }
}
