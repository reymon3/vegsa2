<?php

namespace App\Http\Controllers;

use App\DataTables\ConciliacionDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateConciliacionDetalleRequest;
use App\Http\Requests\UpdateConciliacionDetalleRequest;
use App\Repositories\ConciliacionDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ConciliacionDetalleController extends AppBaseController
{
    /** @var  ConciliacionDetalleRepository */
    private $conciliacionDetalleRepository;

    public function __construct(ConciliacionDetalleRepository $conciliacionDetalleRepo)
    {
        $this->conciliacionDetalleRepository = $conciliacionDetalleRepo;
    }

    /**
     * Display a listing of the ConciliacionDetalle.
     *
     * @param ConciliacionDetalleDataTable $conciliacionDetalleDataTable
     * @return Response
     */
    public function index(ConciliacionDetalleDataTable $conciliacionDetalleDataTable)
    {
        return $conciliacionDetalleDataTable->render('conciliacion_detalles.index');
    }

    /**
     * Show the form for creating a new ConciliacionDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('conciliacion_detalles.create');
    }

    /**
     * Store a newly created ConciliacionDetalle in storage.
     *
     * @param CreateConciliacionDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateConciliacionDetalleRequest $request)
    {
        $input = $request->all();

        $conciliacionDetalle = $this->conciliacionDetalleRepository->create($input);

        Flash::success('Conciliacion Detalle saved successfully.');

        return redirect(route('conciliacionDetalles.index'));
    }

    /**
     * Display the specified ConciliacionDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $conciliacionDetalle = $this->conciliacionDetalleRepository->find($id);

        if (empty($conciliacionDetalle)) {
            Flash::error('Conciliacion Detalle not found');

            return redirect(route('conciliacionDetalles.index'));
        }

        return view('conciliacion_detalles.show')->with('conciliacionDetalle', $conciliacionDetalle);
    }

    /**
     * Show the form for editing the specified ConciliacionDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $conciliacionDetalle = $this->conciliacionDetalleRepository->find($id);

        if (empty($conciliacionDetalle)) {
            Flash::error('Conciliacion Detalle not found');

            return redirect(route('conciliacionDetalles.index'));
        }

        return view('conciliacion_detalles.edit')->with('conciliacionDetalle', $conciliacionDetalle);
    }

    /**
     * Update the specified ConciliacionDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateConciliacionDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConciliacionDetalleRequest $request)
    {
        $conciliacionDetalle = $this->conciliacionDetalleRepository->find($id);

        if (empty($conciliacionDetalle)) {
            Flash::error('Conciliacion Detalle not found');

            return redirect(route('conciliacionDetalles.index'));
        }

        $conciliacionDetalle = $this->conciliacionDetalleRepository->update($request->all(), $id);

        Flash::success('Conciliacion Detalle updated successfully.');

        return redirect(route('conciliacionDetalles.index'));
    }

    /**
     * Remove the specified ConciliacionDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $conciliacionDetalle = $this->conciliacionDetalleRepository->find($id);

        if (empty($conciliacionDetalle)) {
            Flash::error('Conciliacion Detalle not found');

            return redirect(route('conciliacionDetalles.index'));
        }

        $this->conciliacionDetalleRepository->delete($id);

        Flash::success('Conciliacion Detalle deleted successfully.');

        return redirect(route('conciliacionDetalles.index'));
    }
}
