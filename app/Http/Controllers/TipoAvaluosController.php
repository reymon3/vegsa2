<?php

namespace App\Http\Controllers;

use App\DataTables\TipoAvaluosDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoAvaluosRequest;
use App\Http\Requests\UpdateTipoAvaluosRequest;
use App\Repositories\TipoAvaluosRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TipoAvaluosController extends AppBaseController
{
    /** @var  TipoAvaluosRepository */
    private $tipoAvaluosRepository;

    public function __construct(TipoAvaluosRepository $tipoAvaluosRepo)
    {
        $this->tipoAvaluosRepository = $tipoAvaluosRepo;
    }

    /**
     * Display a listing of the TipoAvaluos.
     *
     * @param TipoAvaluosDataTable $tipoAvaluosDataTable
     * @return Response
     */
    public function index(TipoAvaluosDataTable $tipoAvaluosDataTable)
    {
        return $tipoAvaluosDataTable->render('tipo_avaluos.index');
    }

    /**
     * Show the form for creating a new TipoAvaluos.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_avaluos.create');
    }

    /**
     * Store a newly created TipoAvaluos in storage.
     *
     * @param CreateTipoAvaluosRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoAvaluosRequest $request)
    {
        $input = $request->all();

        $tipoAvaluos = $this->tipoAvaluosRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('tipoAvaluos.index'));
    }

    /**
     * Display the specified TipoAvaluos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoAvaluos = $this->tipoAvaluosRepository->find($id);

        if (empty($tipoAvaluos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoAvaluos.index'));
        }

        return view('tipo_avaluos.show')->with('tipoAvaluos', $tipoAvaluos);
    }

    /**
     * Show the form for editing the specified TipoAvaluos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoAvaluos = $this->tipoAvaluosRepository->find($id);

        if (empty($tipoAvaluos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoAvaluos.index'));
        }

        return view('tipo_avaluos.edit')->with('tipoAvaluos', $tipoAvaluos);
    }

    /**
     * Update the specified TipoAvaluos in storage.
     *
     * @param  int              $id
     * @param UpdateTipoAvaluosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoAvaluosRequest $request)
    {
        $tipoAvaluos = $this->tipoAvaluosRepository->find($id);

        if (empty($tipoAvaluos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoAvaluos.index'));
        }

        $tipoAvaluos = $this->tipoAvaluosRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('tipoAvaluos.index'));
    }

    /**
     * Remove the specified TipoAvaluos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoAvaluos = $this->tipoAvaluosRepository->find($id);

        if (empty($tipoAvaluos)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoAvaluos.index'));
        }

        $this->tipoAvaluosRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('tipoAvaluos.index'));
    }
}
