<?php

namespace App\Http\Controllers;

use App\DataTables\AvaluoDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAvaluoDetalleRequest;
use App\Http\Requests\UpdateAvaluoDetalleRequest;
use App\Repositories\AvaluoDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AvaluoDetalleController extends AppBaseController
{
    /** @var  AvaluoDetalleRepository */
    private $avaluoDetalleRepository;

    public function __construct(AvaluoDetalleRepository $avaluoDetalleRepo)
    {
        $this->avaluoDetalleRepository = $avaluoDetalleRepo;
    }

    /**
     * Display a listing of the AvaluoDetalle.
     *
     * @param AvaluoDetalleDataTable $avaluoDetalleDataTable
     * @return Response
     */
    public function index(AvaluoDetalleDataTable $avaluoDetalleDataTable)
    {
        return $avaluoDetalleDataTable->render('avaluo_detalles.index');
    }

    /**
     * Show the form for creating a new AvaluoDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('avaluo_detalles.create');
    }

    /**
     * Store a newly created AvaluoDetalle in storage.
     *
     * @param CreateAvaluoDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateAvaluoDetalleRequest $request)
    {
        $input = $request->all();

        $avaluoDetalle = $this->avaluoDetalleRepository->create($input);

        Flash::success('Avaluo Detalle saved successfully.');

        return redirect(route('avaluoDetalles.index'));
    }

    /**
     * Display the specified AvaluoDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $avaluoDetalle = $this->avaluoDetalleRepository->find($id);

        if (empty($avaluoDetalle)) {
            Flash::error('Avaluo Detalle not found');

            return redirect(route('avaluoDetalles.index'));
        }

        return view('avaluo_detalles.show')->with('avaluoDetalle', $avaluoDetalle);
    }

    /**
     * Show the form for editing the specified AvaluoDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $avaluoDetalle = $this->avaluoDetalleRepository->find($id);

        if (empty($avaluoDetalle)) {
            Flash::error('Avaluo Detalle not found');

            return redirect(route('avaluoDetalles.index'));
        }

        return view('avaluo_detalles.edit')->with('avaluoDetalle', $avaluoDetalle);
    }

    /**
     * Update the specified AvaluoDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateAvaluoDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvaluoDetalleRequest $request)
    {
        $avaluoDetalle = $this->avaluoDetalleRepository->find($id);

        if (empty($avaluoDetalle)) {
            Flash::error('Avaluo Detalle not found');

            return redirect(route('avaluoDetalles.index'));
        }

        $avaluoDetalle = $this->avaluoDetalleRepository->update($request->all(), $id);

        Flash::success('Avaluo Detalle updated successfully.');

        return redirect(route('avaluoDetalles.index'));
    }

    /**
     * Remove the specified AvaluoDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $avaluoDetalle = $this->avaluoDetalleRepository->find($id);

        if (empty($avaluoDetalle)) {
            Flash::error('Avaluo Detalle not found');

            return redirect(route('avaluoDetalles.index'));
        }

        $this->avaluoDetalleRepository->delete($id);

        Flash::success('Avaluo Detalle deleted successfully.');

        return redirect(route('avaluoDetalles.index'));
    }
}
