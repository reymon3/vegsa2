<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoTipoPlaqueoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoTipoPlaqueoRequest;
use App\Http\Requests\UpdateOrdenTrabajoTipoPlaqueoRequest;
use App\Repositories\OrdenTrabajoTipoPlaqueoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoTipoPlaqueoController extends AppBaseController
{
    /** @var  OrdenTrabajoTipoPlaqueoRepository */
    private $ordenTrabajoTipoPlaqueoRepository;

    public function __construct(OrdenTrabajoTipoPlaqueoRepository $ordenTrabajoTipoPlaqueoRepo)
    {
        $this->ordenTrabajoTipoPlaqueoRepository = $ordenTrabajoTipoPlaqueoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoTipoPlaqueo.
     *
     * @param OrdenTrabajoTipoPlaqueoDataTable $ordenTrabajoTipoPlaqueoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoTipoPlaqueoDataTable $ordenTrabajoTipoPlaqueoDataTable)
    {
        return $ordenTrabajoTipoPlaqueoDataTable->render('orden_trabajo_tipo_plaqueos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoTipoPlaqueo.
     *
     * @return Response
     */
    public function create()
    {
        $tiposPlaqueo = \App\Models\TipoPlaqueo::pluck('nombre', 'id');
        // dd($tiposAvaluo);
        return view('orden_trabajo_tipo_plaqueos.create', compact('tiposPlaqueo'));
    }

    /**
     * Store a newly created OrdenTrabajoTipoPlaqueo in storage.
     *
     * @param CreateOrdenTrabajoTipoPlaqueoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoTipoPlaqueoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoTipoPlaqueos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoTipoPlaqueos.index'));
    }

    /**
     * Display the specified OrdenTrabajoTipoPlaqueo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->find($id);

        if (empty($ordenTrabajoTipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoPlaqueos.index'));
        }

        return view('orden_trabajo_tipo_plaqueos.show')->with('ordenTrabajoTipoPlaqueo', $ordenTrabajoTipoPlaqueo);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoTipoPlaqueo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->find($id);

        if (empty($ordenTrabajoTipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoPlaqueos.index'));
        }

        return view('orden_trabajo_tipo_plaqueos.edit')->with('ordenTrabajoTipoPlaqueo', $ordenTrabajoTipoPlaqueo);
    }

    /**
     * Update the specified OrdenTrabajoTipoPlaqueo in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoTipoPlaqueoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoTipoPlaqueoRequest $request)
    {
        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->find($id);

        if (empty($ordenTrabajoTipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoPlaqueos.index'));
        }

        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoTipoPlaqueos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoTipoPlaqueo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoTipoPlaqueo = $this->ordenTrabajoTipoPlaqueoRepository->find($id);

        if (empty($ordenTrabajoTipoPlaqueo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoPlaqueos.index'));
        }

        $this->ordenTrabajoTipoPlaqueoRepository->delete($id);

        Flash::success('Registro eliminado.');
        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoTipoPlaqueo->orden_trabajo_id]));

        // return redirect(route('ordenTrabajoTipoPlaqueos.index'));
    }
}
