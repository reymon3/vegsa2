<?php

namespace App\Http\Controllers;

use App\DataTables\PropositoAvaluoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePropositoAvaluoRequest;
use App\Http\Requests\UpdatePropositoAvaluoRequest;
use App\Repositories\PropositoAvaluoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PropositoAvaluoController extends AppBaseController
{
    /** @var  PropositoAvaluoRepository */
    private $propositoAvaluoRepository;

    public function __construct(PropositoAvaluoRepository $propositoAvaluoRepo)
    {
        $this->propositoAvaluoRepository = $propositoAvaluoRepo;
    }

    /**
     * Display a listing of the PropositoAvaluo.
     *
     * @param PropositoAvaluoDataTable $propositoAvaluoDataTable
     * @return Response
     */
    public function index(PropositoAvaluoDataTable $propositoAvaluoDataTable)
    {
        return $propositoAvaluoDataTable->render('proposito_avaluos.index');
    }

    /**
     * Show the form for creating a new PropositoAvaluo.
     *
     * @return Response
     */
    public function create()
    {
        return view('proposito_avaluos.create');
    }

    /**
     * Store a newly created PropositoAvaluo in storage.
     *
     * @param CreatePropositoAvaluoRequest $request
     *
     * @return Response
     */
    public function store(CreatePropositoAvaluoRequest $request)
    {
        $input = $request->all();

        $propositoAvaluo = $this->propositoAvaluoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('propositoAvaluos.index'));
    }

    /**
     * Display the specified PropositoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propositoAvaluo = $this->propositoAvaluoRepository->find($id);

        if (empty($propositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('propositoAvaluos.index'));
        }

        return view('proposito_avaluos.show')->with('propositoAvaluo', $propositoAvaluo);
    }

    /**
     * Show the form for editing the specified PropositoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propositoAvaluo = $this->propositoAvaluoRepository->find($id);

        if (empty($propositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('propositoAvaluos.index'));
        }

        return view('proposito_avaluos.edit')->with('propositoAvaluo', $propositoAvaluo);
    }

    /**
     * Update the specified PropositoAvaluo in storage.
     *
     * @param  int              $id
     * @param UpdatePropositoAvaluoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropositoAvaluoRequest $request)
    {
        $propositoAvaluo = $this->propositoAvaluoRepository->find($id);

        if (empty($propositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('propositoAvaluos.index'));
        }

        $propositoAvaluo = $this->propositoAvaluoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('propositoAvaluos.index'));
    }

    /**
     * Remove the specified PropositoAvaluo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $propositoAvaluo = $this->propositoAvaluoRepository->find($id);

        if (empty($propositoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('propositoAvaluos.index'));
        }

        $this->propositoAvaluoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('propositoAvaluos.index'));
    }
}
