<?php

namespace App\Http\Controllers;

use App\DataTables\ActivoFijoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateActivoFijoRequest;
use App\Http\Requests\UpdateActivoFijoRequest;
use App\Repositories\ActivoFijoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Rubros;
use Response;

class ActivoFijoController extends AppBaseController
{
    /** @var  ActivoFijoRepository */
    private $activoFijoRepository;

    public function __construct(ActivoFijoRepository $activoFijoRepo)
    {
        $this->activoFijoRepository = $activoFijoRepo;
    }

    /**
     * Display a listing of the ActivoFijo.
     *
     * @param ActivoFijoDataTable $activoFijoDataTable
     * @return Response
     */
    public function index(ActivoFijoDataTable $activoFijoDataTable)
    {
        return $activoFijoDataTable->render('activo_fijos.index');
    }

    /**
     * Show the form for creating a new ActivoFijo.
     *
     * @return Response
     */
    public function create()
    {
        $rubros = Rubros::pluck('nombre', 'id');

        return view('activo_fijos.create', compact('rubros'));
    }

    /**
     * Store a newly created ActivoFijo in storage.
     *
     * @param CreateActivoFijoRequest $request
     *
     * @return Response
     */
    public function store(CreateActivoFijoRequest $request)
    {
        $input = $request->all();

        $activoFijo = $this->activoFijoRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('activoFijos.index'));
    }

    /**
     * Display the specified ActivoFijo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $activoFijo = $this->activoFijoRepository->find($id);

        if (empty($activoFijo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('activoFijos.index'));
        }

        return view('activo_fijos.show')->with('activoFijo', $activoFijo);
    }

    /**
     * Show the form for editing the specified ActivoFijo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $activoFijo = $this->activoFijoRepository->find($id);

        if (empty($activoFijo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('activoFijos.index'));
        }

        $rubros = Rubros::pluck('nombre', 'id');

        return view('activo_fijos.edit', compact('activoFijo', 'rubros'));
    }

    /**
     * Update the specified ActivoFijo in storage.
     *
     * @param  int              $id
     * @param UpdateActivoFijoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivoFijoRequest $request)
    {
        $activoFijo = $this->activoFijoRepository->find($id);

        if (empty($activoFijo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('activoFijos.index'));
        }

        $activoFijo = $this->activoFijoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('activoFijos.index'));
    }

    /**
     * Remove the specified ActivoFijo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activoFijo = $this->activoFijoRepository->find($id);

        if (empty($activoFijo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('activoFijos.index'));
        }

        $this->activoFijoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('activoFijos.index'));
    }
}
