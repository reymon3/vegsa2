<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoServicioDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoServicioRequest;
use App\Http\Requests\UpdateOrdenTrabajoServicioRequest;
use App\Repositories\OrdenTrabajoServicioRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoServicioController extends AppBaseController
{
    /** @var  OrdenTrabajoServicioRepository */
    private $ordenTrabajoServicioRepository;

    public function __construct(OrdenTrabajoServicioRepository $ordenTrabajoServicioRepo)
    {
        $this->ordenTrabajoServicioRepository = $ordenTrabajoServicioRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoServicio.
     *
     * @param OrdenTrabajoServicioDataTable $ordenTrabajoServicioDataTable
     * @return Response
     */
    public function index(OrdenTrabajoServicioDataTable $ordenTrabajoServicioDataTable)
    {
        return $ordenTrabajoServicioDataTable->render('orden_trabajo_servicios.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoServicio.
     *
     * @return Response
     */
    public function create()
    {
        $servicios = \App\Models\Servicio::pluck('nombre', 'id');
        return view('orden_trabajo_servicios.create', compact('servicios'));
    }

    /**
     * Store a newly created OrdenTrabajoServicio in storage.
     *
     * @param CreateOrdenTrabajoServicioRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoServicioRequest $request)
    {
        $input = $request->all();
        // dd($input);

        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoServicios.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoServicios.index'));
    }

    /**
     * Display the specified OrdenTrabajoServicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->find($id);

        if (empty($ordenTrabajoServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoServicios.index'));
        }

        return view('orden_trabajo_servicios.show')->with('ordenTrabajoServicio', $ordenTrabajoServicio);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoServicio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->find($id);

        if (empty($ordenTrabajoServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoServicios.index'));
        }

        return view('orden_trabajo_servicios.edit')->with('ordenTrabajoServicio', $ordenTrabajoServicio);
    }

    /**
     * Update the specified OrdenTrabajoServicio in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoServicioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoServicioRequest $request)
    {
        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->find($id);

        if (empty($ordenTrabajoServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoServicios.index'));
        }

        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoServicios.index'));
    }

    /**
     * Remove the specified OrdenTrabajoServicio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoServicio = $this->ordenTrabajoServicioRepository->find($id);

        if (empty($ordenTrabajoServicio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoServicios.index'));
        }

        $this->ordenTrabajoServicioRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajoServicios.index'));
    }
}
