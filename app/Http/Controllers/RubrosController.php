<?php

namespace App\Http\Controllers;

use App\DataTables\RubrosDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRubrosRequest;
use App\Http\Requests\UpdateRubrosRequest;
use App\Repositories\RubrosRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RubrosController extends AppBaseController
{
    /** @var  RubrosRepository */
    private $rubrosRepository;

    public function __construct(RubrosRepository $rubrosRepo)
    {
        $this->rubrosRepository = $rubrosRepo;
    }

    /**
     * Display a listing of the Rubros.
     *
     * @param RubrosDataTable $rubrosDataTable
     * @return Response
     */
    public function index(RubrosDataTable $rubrosDataTable)
    {
        return $rubrosDataTable->render('rubros.index');
    }

    /**
     * Show the form for creating a new Rubros.
     *
     * @return Response
     */
    public function create()
    {
        return view('rubros.create');
    }

    /**
     * Store a newly created Rubros in storage.
     *
     * @param CreateRubrosRequest $request
     *
     * @return Response
     */
    public function store(CreateRubrosRequest $request)
    {
        $input = $request->all();

        $rubros = $this->rubrosRepository->create($input);

        Flash::success('Registro actializado.');

        return redirect(route('rubros.index'));
    }

    /**
     * Display the specified Rubros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rubros = $this->rubrosRepository->find($id);

        if (empty($rubros)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('rubros.index'));
        }

        return view('rubros.show')->with('rubros', $rubros);
    }

    /**
     * Show the form for editing the specified Rubros.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rubros = $this->rubrosRepository->find($id);

        if (empty($rubros)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('rubros.index'));
        }

        return view('rubros.edit')->with('rubros', $rubros);
    }

    /**
     * Update the specified Rubros in storage.
     *
     * @param  int              $id
     * @param UpdateRubrosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRubrosRequest $request)
    {
        $rubros = $this->rubrosRepository->find($id);

        if (empty($rubros)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('rubros.index'));
        }

        $rubros = $this->rubrosRepository->update($request->all(), $id);

        Flash::success('Registro actualizado');

        return redirect(route('rubros.index'));
    }

    /**
     * Remove the specified Rubros from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rubros = $this->rubrosRepository->find($id);

        if (empty($rubros)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('rubros.index'));
        }

        $this->rubrosRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('rubros.index'));
    }
}
