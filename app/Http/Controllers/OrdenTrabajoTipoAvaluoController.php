<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoTipoAvaluoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoTipoAvaluoRequest;
use App\Http\Requests\UpdateOrdenTrabajoTipoAvaluoRequest;
use App\Repositories\OrdenTrabajoTipoAvaluoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoTipoAvaluoController extends AppBaseController
{
    /** @var  OrdenTrabajoTipoAvaluoRepository */
    private $ordenTrabajoTipoAvaluoRepository;

    public function __construct(OrdenTrabajoTipoAvaluoRepository $ordenTrabajoTipoAvaluoRepo)
    {
        $this->ordenTrabajoTipoAvaluoRepository = $ordenTrabajoTipoAvaluoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoTipoAvaluo.
     *
     * @param OrdenTrabajoTipoAvaluoDataTable $ordenTrabajoTipoAvaluoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoTipoAvaluoDataTable $ordenTrabajoTipoAvaluoDataTable)
    {
        return $ordenTrabajoTipoAvaluoDataTable->render('orden_trabajo_tipo_avaluos.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoTipoAvaluo.
     *
     * @return Response
     */
    public function create()
    {
        $tiposAvaluo = \App\Models\TipoAvaluos::pluck('nombre', 'id');
        return view('orden_trabajo_tipo_avaluos.create', compact('tiposAvaluo'));
    }

    /**
     * Store a newly created OrdenTrabajoTipoAvaluo in storage.
     *
     * @param CreateOrdenTrabajoTipoAvaluoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoTipoAvaluoRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoTipoAvaluos.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoTipoAvaluos.index'));
    }

    /**
     * Display the specified OrdenTrabajoTipoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->find($id);

        if (empty($ordenTrabajoTipoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoAvaluos.index'));
        }

        return view('orden_trabajo_tipo_avaluos.show')->with('ordenTrabajoTipoAvaluo', $ordenTrabajoTipoAvaluo);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoTipoAvaluo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->find($id);

        if (empty($ordenTrabajoTipoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoAvaluos.index'));
        }

        return view('orden_trabajo_tipo_avaluos.edit')->with('ordenTrabajoTipoAvaluo', $ordenTrabajoTipoAvaluo);
    }

    /**
     * Update the specified OrdenTrabajoTipoAvaluo in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoTipoAvaluoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoTipoAvaluoRequest $request)
    {
        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->find($id);

        if (empty($ordenTrabajoTipoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoAvaluos.index'));
        }

        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoTipoAvaluos.index'));
    }

    /**
     * Remove the specified OrdenTrabajoTipoAvaluo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoTipoAvaluo = $this->ordenTrabajoTipoAvaluoRepository->find($id);

        if (empty($ordenTrabajoTipoAvaluo)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoTipoAvaluos.index'));
        }

        $this->ordenTrabajoTipoAvaluoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoTipoAvaluo->orden_trabajo_id]));

        // return redirect(route('ordenTrabajoTipoAvaluos.index'));
    }
}
