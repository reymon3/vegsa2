<?php

namespace App\Http\Controllers;

use App\DataTables\CartasDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCartasRequest;
use App\Http\Requests\UpdateCartasRequest;
use App\Repositories\CartasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

// use App\Models\Cliente;
use App\Models\OrdenTrabajo;
use App\Models\Cartas;

class CartasController extends Controller
{
  private $cartasRepository;

  public function __construct(CartasRepository $cartasRepo)
  {
      $this->cartasRepository = $cartasRepo;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CartasDataTable $cartasDataTable)
    {
        return $cartasDataTable->render('cartas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('cartas.upload'); ###TODO-cambiar la ruta para actualizar carta 
        $ordenes = OrdenTrabajo::all()->pluck('num_orden_trabajo','id')->toArray();
        return view('cartas.create')->withOrdenes($ordenes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #Store
        $file = $request->file('nombre');
        $nombre = $file->getClientOriginalName();

        $input = $request->all();
        $input['nombre'] = $nombre;
        // dd($input);
        $carta = $this->cartasRepository->create($input);
        #Upload
        // $file = $request->file('nombre');
        // $nombre = $file->getClientOriginalName();
        \Storage::disk('cartas-entrega')->put($nombre,  \File::get($file));
        

        Flash::success('Registro insertado.');
        return redirect(route('cartas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtengo el nombre del Archivo
        $file = $request->file('nombre');
        $nombre = $file->getClientOriginalName();

        // Busca algun registro
        $carta = $this->cartasRepository->find($id);

        // No existe algun registro
        if (empty($carta)) {
            Flash::error('Registro no encontrado.');
            return redirect(route('cartas.index'));
        }
        // Reemplazo el valor del input
        $input = $request->all();
        $input['nombre'] = $nombre;
        
        // Actualizo el registro
        $carta = $this->cartasRepository->update($input, $id);
        // Cargo el archivo
        \Storage::disk('cartas-entrega')->put($nombre,  \File::get($file));

        Flash::success('Registro actualizado.');
        return redirect(route('cartas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($file)
    {
        $pathtoFile = public_path().'\uploads\cartas-entrega'.'/'.$file;
        return response()->download($pathtoFile);
    }

    public function actualizarCarta($id_p)
    {
        // dd($id);
        $id = $id_p;
        return view('cartas.upload')->with('id', $id);
    }

    
}
