<?php

namespace App\Http\Controllers;

use App\DataTables\CotizacionDataTable;
use App\Http\Requests;
use App\Models\CotizacionServicio;
use App\Models\Cotizacion;
use App\Http\Requests\CreateCotizacionRequest;
use App\Http\Requests\UpdateCotizacionRequest;
use App\Repositories\CotizacionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;

class CotizacionController extends AppBaseController
{
    /** @var  CotizacionRepository */
    private $cotizacionRepository;

    public function __construct(CotizacionRepository $cotizacionRepo)
    {
        $this->cotizacionRepository = $cotizacionRepo;
    }

    /**
     * Display a listing of the Cotizacion.
     *
     * @param CotizacionDataTable $cotizacionDataTable
     * @return Response
     */
    public function index(CotizacionDataTable $cotizacionDataTable)
    {
        return $cotizacionDataTable->render('cotizacions.index');
    }

    /**
     * Show the form for creating a new Cotizacion.
     *
     * @return Response
     */
    public function create()
    {
        $folio = generaFolio('cotizacion', 7);
        if(!$folio){
            die('error al calcular folio');
        }

        return view('cotizacions.create', compact('folio'));
    }

    /**
     * Store a newly created Cotizacion in storage.
     *
     * @param CreateCotizacionRequest $request
     *
     * @return Response
     */
    public function store(CreateCotizacionRequest $request)
    {
        $input = $request->all();
        $cotizacion = $this->cotizacionRepository->create($input);

        Flash::success('Cotizacion saved successfully.');

        return redirect(route('cotizacions.show', ['id' => $cotizacion->id]));
    }

    /**
     * Display the specified Cotizacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id = null)
    {
        $cotizacion = $this->cotizacionRepository->find($id);
        // dd($cotizacion);

        if (empty($cotizacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacions.index'));
        }

        $servicios = CotizacionServicio::where('cotizacion_id', $id)->get();


        return view('cotizacions.show')->with([
            'cotizacion' => $cotizacion,
            'servicios' => $servicios,
        ]);
    }

    /**
     * Show the form for editing the specified Cotizacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cotizacion = $this->cotizacionRepository->find($id);

        if (empty($cotizacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacions.index'));
        }

        return view('cotizacions.edit')->with('cotizacion', $cotizacion);
    }

    /**
     * Update the specified Cotizacion in storage.
     *
     * @param  int              $id
     * @param UpdateCotizacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCotizacionRequest $request)
    {
        $cotizacion = $this->cotizacionRepository->find($id);

        if (empty($cotizacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacions.index'));
        }

        $cotizacion = $this->cotizacionRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('cotizacions.index'));
    }

    /**
     * Remove the specified Cotizacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cotizacion = $this->cotizacionRepository->find($id);

        if (empty($cotizacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('cotizacions.index'));
        }

        $this->cotizacionRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('cotizacions.index'));
    }

}
