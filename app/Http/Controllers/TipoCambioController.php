<?php

namespace App\Http\Controllers;

use App\DataTables\TipoCambioDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoCambioRequest;
use App\Http\Requests\UpdateTipoCambioRequest;
use App\Repositories\TipoCambioRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TipoCambioController extends AppBaseController
{
    /** @var  TipoCambioRepository */
    private $tipoCambioRepository;

    public function __construct(TipoCambioRepository $tipoCambioRepo)
    {
        $this->tipoCambioRepository = $tipoCambioRepo;
    }

    /**
     * Display a listing of the TipoCambio.
     *
     * @param TipoCambioDataTable $tipoCambioDataTable
     * @return Response
     */
    public function index(TipoCambioDataTable $tipoCambioDataTable)
    {
        return $tipoCambioDataTable->render('tipo_cambios.index');
    }

    /**
     * Show the form for creating a new TipoCambio.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipo_cambios.create');
    }

    /**
     * Store a newly created TipoCambio in storage.
     *
     * @param CreateTipoCambioRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoCambioRequest $request)
    {
        $input = $request->all();

        $tipoCambio = $this->tipoCambioRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('tipoCambios.index'));
    }

    /**
     * Display the specified TipoCambio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoCambio = $this->tipoCambioRepository->find($id);

        if (empty($tipoCambio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoCambios.index'));
        }

        return view('tipo_cambios.show')->with('tipoCambio', $tipoCambio);
    }

    /**
     * Show the form for editing the specified TipoCambio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoCambio = $this->tipoCambioRepository->find($id);

        if (empty($tipoCambio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoCambios.index'));
        }

        return view('tipo_cambios.edit')->with('tipoCambio', $tipoCambio);
    }

    /**
     * Update the specified TipoCambio in storage.
     *
     * @param  int              $id
     * @param UpdateTipoCambioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoCambioRequest $request)
    {
        $tipoCambio = $this->tipoCambioRepository->find($id);

        if (empty($tipoCambio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoCambios.index'));
        }

        $tipoCambio = $this->tipoCambioRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('tipoCambios.index'));
    }

    /**
     * Remove the specified TipoCambio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoCambio = $this->tipoCambioRepository->find($id);

        if (empty($tipoCambio)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('tipoCambios.index'));
        }

        $this->tipoCambioRepository->delete($id);

        Flash::success('registro eliminado.');

        return redirect(route('tipoCambios.index'));
    }
}
