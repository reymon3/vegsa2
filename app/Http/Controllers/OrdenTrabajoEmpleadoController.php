<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoEmpleadoDataTable;
use App\Http\Requests;
use App\Models\Empleados;
use App\Http\Requests\CreateOrdenTrabajoEmpleadoRequest;
use App\Http\Requests\UpdateOrdenTrabajoEmpleadoRequest;
use App\Repositories\OrdenTrabajoEmpleadoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoEmpleadoController extends AppBaseController
{
    /** @var  OrdenTrabajoEmpleadoRepository */
    private $ordenTrabajoEmpleadoRepository;

    public function __construct(OrdenTrabajoEmpleadoRepository $ordenTrabajoEmpleadoRepo)
    {
        $this->ordenTrabajoEmpleadoRepository = $ordenTrabajoEmpleadoRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoEmpleado.
     *
     * @param OrdenTrabajoEmpleadoDataTable $ordenTrabajoEmpleadoDataTable
     * @return Response
     */
    public function index(OrdenTrabajoEmpleadoDataTable $ordenTrabajoEmpleadoDataTable)
    {
        return $ordenTrabajoEmpleadoDataTable->render('orden_trabajo_empleados.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoEmpleado.
     *
     * @return Response
     */
    public function create()
    {
        // $empleados = \DB::table('users')
        //     ->join('role_user', 'users.id', '=', 'role_user.user_id')
        //     ->whereIn('role_user.role_id', [2,3,4,5,6,7,8,9])
        //     ->orderBy('users.name', 'asc')
        //     ->pluck('users.name', 'users.id');

        // $empleados = \DB::table('empleados')
        //     ->join('users', 'empleados.id', '=', 'users.empleado_id')
        //     ->join('role_user', 'users.id', '=', 'role_user.user_id')
        //     ->whereIn('role_user.role_id', [2,3,4,5,6,7,8,9])
        //     ->orderBy('empleados.nombre', 'asc')
        //     ->pluck('empleados.nombre', 'empleados.id');

        $empleados = Empleados::pluck('nombre', 'id');

        // dd($empleados);

        $roles = \DB::table('roles')->whereIn('id', [2,3,4,5,6,7,8,9])
        ->orderBy('name', 'asc')
        ->pluck('name', 'id');

        return view('orden_trabajo_empleados.create', compact('empleados', 'roles'));
    }

    /**
     * Store a newly created OrdenTrabajoEmpleado in storage.
     *
     * @param CreateOrdenTrabajoEmpleadoRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoEmpleadoRequest $request)
    {
        $input = $request->all();

        // dd($input);
        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoNormas.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('ordenTrabajoEmpleados.index'));
    }

    /**
     * Display the specified OrdenTrabajoEmpleado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->find($id);

        if (empty($ordenTrabajoEmpleado)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoEmpleados.index'));
        }

        return view('orden_trabajo_empleados.show')->with('ordenTrabajoEmpleado', $ordenTrabajoEmpleado);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoEmpleado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->find($id);

        if (empty($ordenTrabajoEmpleado)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoEmpleados.index'));
        }

        return view('orden_trabajo_empleados.edit')->with('ordenTrabajoEmpleado', $ordenTrabajoEmpleado);
    }

    /**
     * Update the specified OrdenTrabajoEmpleado in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoEmpleadoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoEmpleadoRequest $request)
    {
        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->find($id);

        if (empty($ordenTrabajoEmpleado)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoEmpleados.index'));
        }

        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->update($request->all(), $id);

        Flash::success('registro actualizado.');

        return redirect(route('ordenTrabajoEmpleados.index'));
    }

    /**
     * Remove the specified OrdenTrabajoEmpleado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoEmpleado = $this->ordenTrabajoEmpleadoRepository->find($id);

        if (empty($ordenTrabajoEmpleado)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoEmpleados.index'));
        }

        $this->ordenTrabajoEmpleadoRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(url('asignacion-proyecto-detalle', ['id' => $ordenTrabajoEmpleado->orden_trabajo_id]));
        // return redirect(route('ordenTrabajoEmpleados.index'));

    }
}
