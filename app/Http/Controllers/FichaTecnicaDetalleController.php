<?php

namespace App\Http\Controllers;

use App\DataTables\FichaTecnicaDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFichaTecnicaDetalleRequest;
use App\Http\Requests\UpdateFichaTecnicaDetalleRequest;
use App\Repositories\FichaTecnicaDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FichaTecnicaDetalleController extends AppBaseController
{
    /** @var  FichaTecnicaDetalleRepository */
    private $fichaTecnicaDetalleRepository;

    public function __construct(FichaTecnicaDetalleRepository $fichaTecnicaDetalleRepo)
    {
        $this->fichaTecnicaDetalleRepository = $fichaTecnicaDetalleRepo;
    }

    /**
     * Display a listing of the FichaTecnicaDetalle.
     *
     * @param FichaTecnicaDetalleDataTable $fichaTecnicaDetalleDataTable
     * @return Response
     */
    public function index(FichaTecnicaDetalleDataTable $fichaTecnicaDetalleDataTable)
    {
        return $fichaTecnicaDetalleDataTable->render('ficha_tecnica_detalles.index');
    }

    /**
     * Show the form for creating a new FichaTecnicaDetalle.
     *
     * @return Response
     */
    public function create()
    {
        return view('ficha_tecnica_detalles.create');
    }

    /**
     * Store a newly created FichaTecnicaDetalle in storage.
     *
     * @param CreateFichaTecnicaDetalleRequest $request
     *
     * @return Response
     */
    public function store(CreateFichaTecnicaDetalleRequest $request)
    {
        $input = $request->all();

        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->create($input);

        Flash::success('Ficha Tecnica Detalle saved successfully.');

        return redirect(route('fichaTecnicaDetalles.index'));
    }

    /**
     * Display the specified FichaTecnicaDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->find($id);

        if (empty($fichaTecnicaDetalle)) {
            Flash::error('Ficha Tecnica Detalle not found');

            return redirect(route('fichaTecnicaDetalles.index'));
        }

        return view('ficha_tecnica_detalles.show')->with('fichaTecnicaDetalle', $fichaTecnicaDetalle);
    }

    /**
     * Show the form for editing the specified FichaTecnicaDetalle.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->find($id);

        if (empty($fichaTecnicaDetalle)) {
            Flash::error('Ficha Tecnica Detalle not found');

            return redirect(route('fichaTecnicaDetalles.index'));
        }

        return view('ficha_tecnica_detalles.edit')->with('fichaTecnicaDetalle', $fichaTecnicaDetalle);
    }

    /**
     * Update the specified FichaTecnicaDetalle in storage.
     *
     * @param  int              $id
     * @param UpdateFichaTecnicaDetalleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFichaTecnicaDetalleRequest $request)
    {
        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->find($id);

        if (empty($fichaTecnicaDetalle)) {
            Flash::error('Ficha Tecnica Detalle not found');

            return redirect(route('fichaTecnicaDetalles.index'));
        }

        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->update($request->all(), $id);

        Flash::success('Ficha Tecnica Detalle updated successfully.');

        return redirect(route('fichaTecnicaDetalles.index'));
    }

    /**
     * Remove the specified FichaTecnicaDetalle from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fichaTecnicaDetalle = $this->fichaTecnicaDetalleRepository->find($id);

        if (empty($fichaTecnicaDetalle)) {
            Flash::error('Ficha Tecnica Detalle not found');

            return redirect(route('fichaTecnicaDetalles.index'));
        }

        $this->fichaTecnicaDetalleRepository->delete($id);

        Flash::success('Ficha Tecnica Detalle deleted successfully.');

        return redirect(route('fichaTecnicaDetalles.index'));
    }
}
