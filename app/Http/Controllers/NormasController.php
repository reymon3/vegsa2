<?php

namespace App\Http\Controllers;

use App\DataTables\NormasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNormasRequest;
use App\Http\Requests\UpdateNormasRequest;
use App\Repositories\NormasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class NormasController extends AppBaseController
{
    /** @var  NormasRepository */
    private $normasRepository;

    public function __construct(NormasRepository $normasRepo)
    {
        $this->normasRepository = $normasRepo;
    }

    /**
     * Display a listing of the Normas.
     *
     * @param NormasDataTable $normasDataTable
     * @return Response
     */
    public function index(NormasDataTable $normasDataTable)
    {
        return $normasDataTable->render('normas.index');
    }

    /**
     * Show the form for creating a new Normas.
     *
     * @return Response
     */
    public function create()
    {
        return view('normas.create');
    }

    /**
     * Store a newly created Normas in storage.
     *
     * @param CreateNormasRequest $request
     *
     * @return Response
     */
    public function store(CreateNormasRequest $request)
    {
        $input = $request->all();

        $normas = $this->normasRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('normas.index'));
    }

    /**
     * Display the specified Normas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $normas = $this->normasRepository->find($id);

        if (empty($normas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('normas.index'));
        }

        return view('normas.show')->with('normas', $normas);
    }

    /**
     * Show the form for editing the specified Normas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $normas = $this->normasRepository->find($id);

        if (empty($normas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('normas.index'));
        }

        return view('normas.edit')->with('normas', $normas);
    }

    /**
     * Update the specified Normas in storage.
     *
     * @param  int              $id
     * @param UpdateNormasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNormasRequest $request)
    {
        $normas = $this->normasRepository->find($id);

        if (empty($normas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('normas.index'));
        }

        $normas = $this->normasRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('normas.index'));
    }

    /**
     * Remove the specified Normas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $normas = $this->normasRepository->find($id);

        if (empty($normas)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('normas.index'));
        }

        $this->normasRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('normas.index'));
    }
}
