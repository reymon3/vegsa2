<?php

namespace App\Http\Controllers;

use App\DataTables\ConciliacionDataTable;
use App\DataTables\ConciliacionDetalleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateConciliacionRequest;
use App\Http\Requests\UpdateConciliacionRequest;
use App\Repositories\ConciliacionRepository;
use App\Repositories\ConciliacionDetalleRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Conciliacion;
use App\Models\ConciliacionDetalle;

use Illuminate\Http\Request;

class ConciliacionController extends AppBaseController
{
    /** @var  ConciliacionRepository */
    private $conciliacionRepository;

    public function __construct(
        ConciliacionRepository $conciliacionRepo,
        ConciliacionDetalleRepository $conciliacionDetalleRepo

    ){
        $this->conciliacionRepository = $conciliacionRepo;
        $this->conciliacionDetalleRepository = $conciliacionDetalleRepo;
    }

    /**
     * Display a listing of the Conciliacion.
     *
     * @param ConciliacionDataTable $conciliacionDataTable
     * @return Response
     */
    public function index(ConciliacionDataTable $conciliacionDataTable)
    {
        return $conciliacionDataTable->render('conciliacions.index');
    }

    /**
     * Show the form for creating a new Conciliacion.
     *
     * @return Response
     */
    public function create()
    {
        $ordenesTrabajo = \App\Models\OrdenTrabajo::doesntHave('conciliacion')->pluck('num_orden_trabajo', 'id');
        return view('conciliacions.create',  compact('ordenesTrabajo'));
        // return view('conciliacions.create');
    }

    /**
     * Store a newly created Conciliacion in storage.
     *
     * @param CreateConciliacionRequest $request
     *
     * @return Response
     */
    public function store(CreateConciliacionRequest $request)
    {
        $input = $request->all();

        $conciliacion = $this->conciliacionRepository->create($input);

        Flash::success('Registro insertado.');

        return redirect(route('conciliacions.index'));
    }

    /**
     * Display the specified Conciliacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show(
        ConciliacionDetalleDataTable $conciliacionDetalleDataTable,
        $id = null
    ){
        $conciliacion = $this->conciliacionRepository->find($id);

        if (empty($conciliacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('conciliacions.index'));
        }

// dd($id, $conciliacion, $conciliacionDetalleDataTable);

        return $conciliacionDetalleDataTable->with([
            // Envia parametro
            'avaluo_id' => $id
        ])->render('conciliacions.show', compact(
            'conciliacion',
            'conciliacionDetalleDataTable'
        ));

    }



    /**
     * Show the form for editing the specified Conciliacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $conciliacion = $this->conciliacionRepository->find($id);

        if (empty($conciliacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('conciliacions.index'));
        }

        return view('conciliacions.edit')->with('conciliacion', $conciliacion);
    }

    /**
     * Update the specified Conciliacion in storage.
     *
     * @param  int              $id
     * @param UpdateConciliacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConciliacionRequest $request)
    {
        $conciliacion = $this->conciliacionRepository->find($id);

        if (empty($conciliacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('conciliacions.index'));
        }

        $conciliacion = $this->conciliacionRepository->update($request->all(), $id);

        Flash::success('Registro no encontrado.');

        return redirect(route('conciliacions.index'));
    }

    /**
     * Remove the specified Conciliacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $conciliacion = $this->conciliacionRepository->find($id);

        if (empty($conciliacion)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('conciliacions.index'));
        }

        $this->conciliacionRepository->delete($id);

        Flash::success('Registro eliminado.');

        return redirect(route('conciliacions.index'));
    }

    /**
     * Remove the specified OrdenTrabajoBaseContable from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function subirConciliacion(Request $request)
    {

        $start = microtime(true);
        $input = $request->only('orden_trabajo_id');

        if (!$request->hasFile('fileImport')) {
            // Flash::error('Favor de seleccionar un archivo válido');
            return \Redirect::back()->withErrors( 'Favor de seleccionar un archivo válido');

        }

        $avatar = $request->file('fileImport');
        $excelFilepath = $avatar->getPathName();
        $filename = time().'_'.$avatar->getClientOriginalName();

        $spreadsheetxlsx = \PhpOffice\PhpSpreadsheet\IOFactory::load($excelFilepath);
        $worksheet = $spreadsheetxlsx->getActiveSheet();
        // lee datos de Excel y los almacena en una matriz
        $xls_data = $worksheet->toArray(null, true, true, true);
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn);

        $mapeo = [
            "A" => "placa_anterior",
            "B" => "folio",
            "C" => "placa_actual",
            "D" => "placa_homologada",
            "E" => "placa_auxiliar_1",
            "F" => "placa_auxiliar_2",
            "G" => "catalogo",
            "H" => "descripcion",
            "I" => "marca",
            "J" => "catalogo",
            "K" => "serie",
            "L" => "no_mantenimiento",
            "M" => "area",
            "N" => "ubicacion",
            "O" => "descripcion",
            "P" => "piso",
            "Q" => "planta",
            "R" => "porcentaje_depreciacion_fisica",
            "S" => "anio_fabricacion",
            "T" => "observaciones",
            // "U" =>
        ];

        $errores = [];
        $blameId =\Auth::id();
        $createdAt = date("Y-m-d H:i:s");
        $anioActual = date('Y');

        $input['blame_id'] = $blameId;
        $avaluo = Conciliacion::create($input);

        for ($indiceHoja = 0; $indiceHoja < 1; $indiceHoja++) {
            # Obtener hoja en el índice que vaya del ciclo
            $hojaActual = $spreadsheetxlsx->getSheet($indiceHoja);
            // echo "<h3>Vamos en la hoja con índice $indiceHoja</h3>";

            $registros = [];
            # Iterar filas
            foreach ($hojaActual->getRowIterator() as $fila) {

                // $data = new OrdenTrabajoConteo();
                foreach ($fila->getCellIterator() as $celda) {

                    # Fila, comienza en 1
                    $fila = $celda->getRow();

                    # Columna, que es la A, B, C y así...
                    $columna = $celda->getColumn();

                    if($fila < 2 || $columna > 'Z' ){
                        break;
                    }

                    if($columna == 'A' ){

                        $registros[$fila]['conciliacion_id']  = $avaluo->id;
                        $registros[$fila]['blame_id']   = $blameId ? $blameId: 1;
                        $registros[$fila]['created_at'] = $createdAt;
                        $registros[$fila]['updated_at'] = $createdAt;

                    }


                    // Aquí podemos obtener varias cosas interesantes
                    #https://phpoffice.github.io/PhpSpreadsheet/master/PhpOffice/PhpSpreadsheet/Cell/Cell.html

                    # El valor, así como está en el documento
                    if(isset($mapeo[$columna])){

                        $valorRaw = $celda->getValue();

                        $registros[$fila][$mapeo[$columna]] = is_null($valorRaw) ? $valorRaw :(string)$valorRaw;
                    }

                }

                if($fila > 1  ){

                    // dd($registros);

                    $validator = \Validator::make($registros[$fila], ConciliacionDetalle::$rules);

                    if ($validator->fails()) {
                        $errores[$fila] = $validator->messages()->all();
                    }

                    if (count($errores) > ConciliacionDetalle::MAX_ERROR_ROWS) {

                        // dd(
                        //     // 'fila: '. $fila,
                        //     $registros,
                        //     $errores,
                        //     // $validator->errors(),
                        //     count($errores)
                        // );
                        return \Redirect::back()->with('errores', $errores);
                    }
                }
            }


        }

        if (count($errores)) {
            return \Redirect::back()->with('errores', $errores);
        }

        // dd('Algodon', $input['orden_trabajo_id'], $avaluo,  $registros);
        \DB::beginTransaction();

        try {
            // ConciliacionDetalle::where('orden_trabajo_id', $input['orden_trabajo_id'])->forceDelete();
            ConciliacionDetalle::insert($registros);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }

        Flash::success('Registros importados');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('conciliacions.create').'?pop=1&close=1&orden_trabajo_id=0');
        }

        return redirect(route('conciliacions.show', ['id' => $avaluo->id]));
    }

    public function getConciliacion(){


            $file= public_path("Conciliacion.xlsx");
            return Response::download($file);
    }

    public function armadoAvaluo(){

    return view('armado-avaluo');
        dd('Armado Conciliacion');
    }
}
