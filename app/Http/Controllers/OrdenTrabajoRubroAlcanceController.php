<?php

namespace App\Http\Controllers;

use App\DataTables\OrdenTrabajoRubroAlcanceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOrdenTrabajoRubroAlcanceRequest;
use App\Http\Requests\UpdateOrdenTrabajoRubroAlcanceRequest;
use App\Repositories\OrdenTrabajoRubroAlcanceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OrdenTrabajoRubroAlcanceController extends AppBaseController
{
    /** @var  OrdenTrabajoRubroAlcanceRepository */
    private $ordenTrabajoRubroAlcanceRepository;

    public function __construct(OrdenTrabajoRubroAlcanceRepository $ordenTrabajoRubroAlcanceRepo)
    {
        $this->ordenTrabajoRubroAlcanceRepository = $ordenTrabajoRubroAlcanceRepo;
    }

    /**
     * Display a listing of the OrdenTrabajoRubroAlcance.
     *
     * @param OrdenTrabajoRubroAlcanceDataTable $ordenTrabajoRubroAlcanceDataTable
     * @return Response
     */
    public function index(OrdenTrabajoRubroAlcanceDataTable $ordenTrabajoRubroAlcanceDataTable)
    {
        return $ordenTrabajoRubroAlcanceDataTable->render('orden_trabajo_rubro_alcances.index');
    }

    /**
     * Show the form for creating a new OrdenTrabajoRubroAlcance.
     *
     * @return Response
     */
    public function create()
    {
        $rubros = \App\Models\Rubros::pluck('nombre', 'id');
        // dd($rubros);
        return view('orden_trabajo_rubro_alcances.create', compact('rubros'));
    }

    /**
     * Store a newly created OrdenTrabajoRubroAlcance in storage.
     *
     * @param CreateOrdenTrabajoRubroAlcanceRequest $request
     *
     * @return Response
     */
    public function store(CreateOrdenTrabajoRubroAlcanceRequest $request)
    {
        $input = $request->all();

        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->create($input);

        Flash::success('Registro insertado.');

        if (strpos($request->server("HTTP_REFERER"), "pop=1")) {
            return redirect(route('ordenTrabajoRubroAlcances.create').'?pop=1&close=1&orden_trabajo_id=0');
        }
        return redirect(route('ordenTrabajoRubroAlcances.index'));
    }

    /**
     * Display the specified OrdenTrabajoRubroAlcance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->find($id);

        if (empty($ordenTrabajoRubroAlcance)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRubroAlcances.index'));
        }

        return view('orden_trabajo_rubro_alcances.show')->with('ordenTrabajoRubroAlcance', $ordenTrabajoRubroAlcance);
    }

    /**
     * Show the form for editing the specified OrdenTrabajoRubroAlcance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->find($id);

        if (empty($ordenTrabajoRubroAlcance)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRubroAlcances.index'));
        }

        return view('orden_trabajo_rubro_alcances.edit')->with('ordenTrabajoRubroAlcance', $ordenTrabajoRubroAlcance);
    }

    /**
     * Update the specified OrdenTrabajoRubroAlcance in storage.
     *
     * @param  int              $id
     * @param UpdateOrdenTrabajoRubroAlcanceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrdenTrabajoRubroAlcanceRequest $request)
    {
        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->find($id);

        if (empty($ordenTrabajoRubroAlcance)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRubroAlcances.index'));
        }

        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->update($request->all(), $id);

        Flash::success('Registro actualizado.');

        return redirect(route('ordenTrabajoRubroAlcances.index'));
    }

    /**
     * Remove the specified OrdenTrabajoRubroAlcance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ordenTrabajoRubroAlcance = $this->ordenTrabajoRubroAlcanceRepository->find($id);

        if (empty($ordenTrabajoRubroAlcance)) {
            Flash::error('Registro no encontrado.');

            return redirect(route('ordenTrabajoRubroAlcances.index'));
        }

        $this->ordenTrabajoRubroAlcanceRepository->delete($id);

        Flash::success('Registro eliminado.');
        return redirect(route('ordenTrabajos.show', ['id' => $ordenTrabajoRubroAlcance->orden_trabajo_id]));
        // return redirect(route('ordenTrabajoRubroAlcances.index'));
    }
}
