<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        date_default_timezone_set(config('app.timezone'));
        setlocale(LC_TIME, 'spanish');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        \Schema::defaultStringLength(191);
        Carbon::setUtf8(true);
        Carbon::setLocale(config('app.locale'));
    }
}
