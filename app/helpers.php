<?php

/**
 * Obtiene el nombre de la accion actual.
 *
 * @see https://laravel.com/docs/5.5/routing#accessing-the-current-route
 *
 * @return string
 **/
function getAction()
{
    return Route::getCurrentRoute()->getActionMethod();
}

function generaFolio($campo, $espacios = 7)
{
    $registro = \App\Models\Folio::find(1);
    if (is_null($registro)) {
        return false;
    }

    $anioActual = (int) date('Y');
    $anioCotizacion = (int) date('Y', strtotime($registro->ultimo_reinicio));

    $registro->$campo = $registro->$campo + 1;

    if ($anioActual > $anioCotizacion) {
        // reiniciar Contador y actualizar ultimo_reinicio
        $registro->$campo = 1;
    }

    $registro->ultimo_reinicio = date('Y-m-d');
    $registro->save();

    $folio = str_pad($registro->$campo, $espacios, '0', STR_PAD_LEFT);
    $folio = $folio.'/'.$anioActual;

    return $folio;
}

/**
 * Limpia variables con valor vacio.
 *
 * @var array
 *
 * @return array
 **/
function unsetEmptyInput($input)
{
    foreach ($input as $campo => $valor) {
        if (is_null($valor)) {
            unset($input[$campo]);
            continue;
        }
    }

    return $input;
}

function getRecord($tabla = null, $llavePrimaria = null, $registroId = null)
{
    if (is_null($tabla) || is_null($llavePrimaria) || is_null($registroId)) {
        return false;
    }

    $result = \DB::table($tabla)->where($llavePrimaria, $registroId)->first();

    if (is_null($result)) {
        return false;
    }

    return $result;
}

function enviaCorreo(
    $nombreEnvia = '',
    $correoEnvia = '',
    $asunto = '',
    $correoDestino = '',
    $mensaje = '',
    $template = ''
) {
    \Mail::send($template, compact('mensaje'), function ($msg) use (
        $nombreEnvia,
        $correoEnvia,
        $mensaje,
        $correoDestino,
        $asunto
    ) {
        $msg->to($correoDestino);
        $msg->from($correoEnvia, $nombreEnvia);
        $msg->subject($asunto);
    });
}

function formateaFecha($fecha)
{
    $fecha = str_replace(' ', '', $fecha);
    $aFecha = explode('/', $fecha);

    return  $aFecha[2].'-'.$aFecha[1].'-'.$aFecha[0];
}

function secureFileName($fileName, $relativePathPrefix = '')
{
    // check file exists in directory or not
    if(!\Storage::disk('ordenes-trabajo')->exists($relativePathPrefix . $fileName)) {
        return $fileName;
    }

    $fullPath  = public_path(\App\Models\OrdenTrabajoDocumento::CARPETA_ORDEN_TRABAJO);
    $fullPath .= $relativePathPrefix ;

    return newFileName($fullPath, $fileName);

}


function newFileName($path, $fileName, $limitIterator = 10){

    if ($pos = strrpos($fileName, '.')) {
        $name = substr($fileName, 0, $pos);
        $ext = substr($fileName, $pos);
    } else {
        $name = $fileName;
    }

    $newpath = $path.'/'.$fileName;
    $newname = $fileName;
    $counter = 0;

    while (file_exists($newpath)) {
        $counter++;
        if ($counter > $limitIterator) {
            throw new Exception('Error iterator limit renaming `'.$name.'` to `'.$newname.'`');
        }
        $newname = $name .'_'. $counter . $ext;
        $newpath = $path.'/'.$newname;
    }

    return $newname;
}

function getIcon($type = null){

    if (is_null($type)) {
        return "<img width='40px' src='../images/file-types/desconocido.png' />";
    }

    return "<img width='40px' src='../images/file-types/".$type.".png' />";
}

function getAnioOfDate( $date = null, $limiter = '/'){

    if(is_null($date)){
        throw new Exception('Fecha no valida `'.$date.'`');
    }

    $aFecha = explode($limiter, $date);

    return $aFecha[2];
}
