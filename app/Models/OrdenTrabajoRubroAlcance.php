<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class OrdenTrabajoRubroAlcance
 * @package App\Models
 * @version December 5, 2019, 8:11 am CST
 *
 * @property integer orden_trabajo_id
 * @property integer rubro_alcance_id
 * @property integer blame_id
 */
class OrdenTrabajoRubroAlcance extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo_rubros_alcance';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $with = ['blame', 'rubro'];


    public $fillable = [
        'orden_trabajo_id',
        'rubro_alcance_id',
        'observaciones',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'rubro_alcance_id' => 'integer',
        'observaciones' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'rubro_alcance_id' => 'required'
    ];


        public function blameable()
        {
            return [
                'user' => \App\User::class,
                'createdBy' => 'blame_id',
                'updatedBy' => null,
                'deletedBy' => null
            ];
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         **/
        public function blame()
        {
            return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         **/
        public function rubro()
        {
            return $this->belongsTo(\App\Models\Rubros::class, 'rubro_alcance_id')->withTrashed();
        }

}
