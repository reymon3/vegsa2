<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class CotizacionServicio
 * @package App\Models
 * @version November 7, 2019, 2:07 am CST
 *
 * @property integer cotizacion_id
 * @property integer servicio_id
 * @property number importe
 * @property number iva
 * @property integer blame_id
 */
class CotizacionServicio extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'cotizaciones_servicios';
    public $with = ['cotizacion', 'servicio'];
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cotizacion_id',
        'servicio_id',
        'importe',
        'iva',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cotizacion_id' => 'integer',
        'servicio_id' => 'integer',
        'importe' => 'float',
        'iva' => 'float',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cotizacion_id' => 'required',
        'servicio_id' => 'required',
        'importe' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cotizacion()
    {
        return $this->belongsTo(\App\Models\Cotizacion::class, 'cotizacion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function servicio()
    {
        return $this->belongsTo(\App\Models\Servicio::class, 'servicio_id')->withTrashed();
    }

}
