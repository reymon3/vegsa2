<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ConciliacionDetalle
 * @package App\Models
 * @version August 6, 2020, 9:35 am CDT
 *
 * @property integer conciliacion_id
 * @property string folio
 * @property string placa_actual
 * @property string placa_anterior
 * @property string placa_homologada
 * @property string placa_auxiliar_1
 * @property string placa_auxiliar_2
 * @property string desglose
 * @property integer rubro_id
 * @property string rubro
 * @property string catalogo
 * @property string descripcion
 * @property string marca
 * @property string modelo
 * @property string serie
 * @property string no_mantenimiento
 * @property string area
 * @property string ubicacion
 * @property string usuario_conteo
 * @property string piso
 * @property string planta
 * @property integer porcentaje_depreciacion_fisica
 * @property string anio_fabricacion
 * @property string observaciones
 * @property string foto_1
 * @property string foto_2
 * @property string foto_3
 * @property number vrn_anterior
 * @property number porcentaje_dep_ajuste
 * @property number dep_acum_ajuste
 * @property number vnr_ajuste
 * @property number vur_ajuste
 * @property number da_ajuste
 * @property number porcentaje_ajuste_inst
 * @property number vrn
 * @property number porcentaje_dep
 * @property number dep_acum
 * @property number vnr
 * @property number vur
 * @property number da
 * @property number vut
 * @property integer blame_id
 */
class ConciliacionDetalle extends Model
{
    use SoftDeletes;

    public $table = 'conciliacion_detalle';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const MAX_ERROR_ROWS = 10;

    protected $dates = ['deleted_at'];



    public $fillable = [
        'conciliacion_id',
        'folio',
        'placa_actual',
        'placa_anterior',
        'placa_homologada',
        'placa_auxiliar_1',
        'placa_auxiliar_2',
        'desglose',
        'rubro_id',
        'rubro',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'no_mantenimiento',
        'area',
        'ubicacion',
        'usuario_conteo',
        'piso',
        'planta',
        'porcentaje_depreciacion_fisica',
        'anio_fabricacion',
        'observaciones',
        'foto_1',
        'foto_2',
        'foto_3',
        'vrn_anterior',
        'porcentaje_dep_ajuste',
        'dep_acum_ajuste',
        'vnr_ajuste',
        'vur_ajuste',
        'da_ajuste',
        'porcentaje_ajuste_inst',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'vur',
        'da',
        'vut',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'conciliacion_id' => 'integer',
        'folio' => 'string',
        'placa_actual' => 'string',
        'placa_anterior' => 'string',
        'placa_homologada' => 'string',
        'placa_auxiliar_1' => 'string',
        'placa_auxiliar_2' => 'string',
        'desglose' => 'string',
        'rubro_id' => 'integer',
        'rubro' => 'string',
        'catalogo' => 'string',
        'descripcion' => 'string',
        'marca' => 'string',
        'modelo' => 'string',
        'serie' => 'string',
        'no_mantenimiento' => 'string',
        'area' => 'string',
        'ubicacion' => 'string',
        'usuario_conteo' => 'string',
        'piso' => 'string',
        'planta' => 'string',
        'porcentaje_depreciacion_fisica' => 'integer',
        'anio_fabricacion' => 'string',
        'observaciones' => 'string',
        'foto_1' => 'string',
        'foto_2' => 'string',
        'foto_3' => 'string',
        'vrn_anterior' => 'float',
        'porcentaje_dep_ajuste' => 'float',
        'dep_acum_ajuste' => 'float',
        'vnr_ajuste' => 'float',
        'vur_ajuste' => 'float',
        'da_ajuste' => 'float',
        'porcentaje_ajuste_inst' => 'float',
        'vrn' => 'float',
        'porcentaje_dep' => 'float',
        'dep_acum' => 'float',
        'vnr' => 'float',
        'vur' => 'float',
        'da' => 'float',
        'vut' => 'float',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'conciliacion_id' => 'required',
        'folio' => 'required',
        // 'rubro_id' => 'required',
        'catalogo' => 'required',
        'descripcion' => 'required',
        // 'no_mantenimiento' => 'required',
        // 'usuario_conteo' => 'required',
        'porcentaje_depreciacion_fisica' => 'required',
        // 'vrn_anterior' => 'required',
        // 'porcentaje_dep_ajuste' => 'required',
        // 'dep_acum_ajuste' => 'required',
        // 'vnr_ajuste' => 'required',
        // 'vur_ajuste' => 'required',
        // 'da_ajuste' => 'required',
        // 'porcentaje_ajuste_inst' => 'required',
        // 'vrn' => 'required',
        // 'porcentaje_dep' => 'required',
        // 'dep_acum' => 'required',
        // 'vnr' => 'required',
        // 'vur' => 'required',
        // 'da' => 'required',
        // 'vut' => 'required'
    ];


}
