<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AvaluoDetalle
 * @package App\Models
 * @version April 23, 2020, 10:29 am CDT
 *
 * @property integer avaluo_id
 * @property string asset
 * @property string rubro
 * @property string desglose
 * @property string asset_descripcion
 * @property number acquis_val
 * @property number accum_dep
 * @property number book_val
 * @property string placa_anterior_homogenea
 * @property string placa_anterior
 * @property string edificio
 * @property string piso
 * @property string rubro_vegsa
 * @property string catalogo
 * @property string descripcion
 * @property string marca
 * @property string modelo
 * @property string serie
 * @property string area
 * @property string caso
 * @property number vrn
 * @property boolean porcentaje_dep
 * @property number dep_acum
 * @property number vnr
 * @property number vur
 * @property number da
 * @property number vut
 * @property integer blame_id
 */
class AvaluoDetalle extends Model
{
    use SoftDeletes;

    public $table = 'avaluos_detalle';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const MAX_ERROR_ROWS = 10;
    const EQUIPO_TRANSPORTE = 'EQUIPO DE TRANSPORTE';

    protected $dates = ['deleted_at'];



    public $fillable = [
        'avaluo_id',
        'asset',
        'rubro_id',
        'rubro',
        // 'desglose',
        // 'asset_descripcion',
        // 'acquis_val',
        // 'accum_dep',
        // 'book_val',
        // 'placa_anterior_homogenea',
        // 'placa_anterior',
        // 'edificio',
        // 'piso',
        // 'rubro_vegsa',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'anio_fabricacion',
        'porcentaje_depreciacion_fisica',
        'fecha_adquisicion',
        'area',
        'caso',

        'valor_cotizado_1',
        'moneda_1',
        'tipo_cambio_1',
        'no_piezas_1',

        'valor_cotizado_2',
        'moneda_2',
        'tipo_cambio_2',
        'no_piezas_2',
        'valor_equipo_usado',

        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',

        'link_1',
        'link_2',
        'observaciones',
        'cotizo',
        'vcf',

        'vur',
        'da',
        'vut',
        'gastos_instalacion',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'avaluo_id' => 'integer',
        'asset' => 'string',
        'rubro_id' => 'integer',
        'rubro' => 'string',
        // 'desglose' => 'string',
        // 'asset_descripcion' => 'string',
        // 'acquis_val' => 'float',
        // 'accum_dep' => 'float',
        // 'book_val' => 'float',
        // 'placa_anterior_homogenea' => 'string',
        // 'placa_anterior' => 'string',
        // 'edificio' => 'string',
        // 'piso' => 'string',
        // 'rubro_vegsa' => 'string',
        'catalogo' => 'string',
        'descripcion' => 'string',
        'marca' => 'string',
        'modelo' => 'string',
        'serie' => 'string',
        'anio_fabricacion' => 'string',
        'fecha_adquisicion' => 'string',
        'area' => 'string',
        'caso' => 'string',

        'valor_cotizado_1' => 'string',
        'moneda_1' => 'string',
        'tipo_cambio_1' => 'float',
        'no_piezas_1' => 'string',

        'valor_cotizado_2' => 'string',
        'moneda_2' => 'string',
        'tipo_cambio_2' => 'float',
        'no_piezas_2' => 'string',
        'valor_equipo_usado' => 'string',

        'vrn' => 'float',
        'porcentaje_dep' => 'integer',
        'dep_acum' => 'float',
        'vnr' => 'float',

        'link_1' => 'string',
        'link_2' => 'string',
        'observaciones' => 'string',
        'cotizo' => 'string',

        'vur' => 'float',
        'da' => 'float',
        'vut' => 'float',
        'gastos_instalacion' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'avaluo_id' => 'required',
        'asset' => 'required',
        'gastos_instalacion' => 'required',
        'rubro_id' => 'required',
        'rubro' => 'required',
        // 'desglose' => 'required',
        // 'asset_descripcion' => 'required',
        // 'edificio' => 'required',
        // 'piso' => 'required',
        // 'rubro_vegsa' => 'required',
        // 'catalogo' => 'required',
        'catalogo' => 'required|exists:activo_fijo,nombre',
        'descripcion' => 'required',
        'serie' => 'required',
        // 'area' => 'required',
        // 'caso' => 'required',

        'valor_cotizado_1' => 'required',
        // 'moneda_1' => 'required',
        'moneda_1' => 'required|exists:tipo_cambio,nombre',
        'tipo_cambio_1' => 'required',
        'no_piezas_1' => 'required',

        'valor_cotizado_2' => 'required',
        // 'moneda_2' => 'required',
        'moneda_2' => 'required|exists:tipo_cambio,nombre',
        'tipo_cambio_2' => 'required',
        'no_piezas_2' => 'required',
        // 'valor_equipo_usado' => 'required',

        'vrn' => 'required',
        'porcentaje_dep' => 'required',
        'dep_acum' => 'required',
        'vnr' => 'required',

    ];


}
