<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ArchivoMaestroDetalle
 * @package App\Models
 * @version April 29, 2020, 11:18 am CDT
 *
 * @property integer archivo_maestro_id
 * @property string company_code
 * @property string asset_class
 * @property string asset
 * @property string subnumber
 * @property string name
 * @property string additional_description
 * @property string serial_number
 * @property string inventory_number
 * @property string capitalized
 * @property string deactivation_on
 * @property string cost_center
 * @property string evaluation_group_1
 * @property string evaluation_group_2
 * @property string text
 * @property string country_key
 * @property string country_name
 * @property number ad_value
 * @property number dprn_rate_01
 * @property number dprn_value_01
 * @property number depreciacion_acumulada_01
 * @property number book_value_for_area_01
 * @property number dprn_rate_02
 * @property number dprn_value_02
 * @property number depreciacion_acumulada_02
 * @property number book_value_for_area_02
 * @property number dprn_rate_03
 * @property number dprn_value_03
 * @property number depreciacion_acumulada_03
 * @property number dprn_rate_04
 * @property number dprn_value_04
 * @property number depreciacion_acumulada_04
 * @property number dprn_rate_05
 * @property number dprn_value_05
 * @property number depreciacion_acumulada_05
 * @property integer blame_id
 */
class ArchivoMaestroDetalle extends Model
{
    use SoftDeletes;

    public $table = 'archivo_maestro_detalle';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const MAX_ERROR_ROWS = 10;

    protected $dates = ['deleted_at'];

    public $fillable = [
        'archivo_maestro_id',
        'company_code',
        'asset_class',
        'asset',
        'subnumber',
        'name',
        'additional_description',
        'serial_number',
        'inventory_number',
        'capitalized',
        'deactivation_on',
        'cost_center',
        'evaluation_group_1',
        'evaluation_group_2',
        'text',
        'country_key',
        'country_name',
        'ad_value',
        'dprn_rate_01',
        'dprn_value_01',
        'depreciacion_acumulada_01',
        'book_value_for_area_01',
        'dprn_rate_02',
        'dprn_value_02',
        'depreciacion_acumulada_02',
        'book_value_for_area_02',
        'dprn_rate_03',
        'dprn_value_03',
        'depreciacion_acumulada_03',
        'dprn_rate_04',
        'dprn_value_04',
        'depreciacion_acumulada_04',
        'dprn_rate_05',
        'dprn_value_05',
        'depreciacion_acumulada_05',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'archivo_maestro_id' => 'integer',
        'company_code' => 'string',
        'asset_class' => 'string',
        'asset' => 'string',
        'subnumber' => 'string',
        'name' => 'string',
        'additional_description' => 'string',
        'serial_number' => 'string',
        'inventory_number' => 'string',
        'capitalized' => 'date',
        'deactivation_on' => 'date',
        'cost_center' => 'string',
        'evaluation_group_1' => 'string',
        'evaluation_group_2' => 'string',
        'text' => 'string',
        'country_key' => 'string',
        'country_name' => 'string',
        'ad_value' => 'float',
        'dprn_rate_01' => 'float',
        'dprn_value_01' => 'float',
        'depreciacion_acumulada_01' => 'float',
        'book_value_for_area_01' => 'float',
        'dprn_rate_02' => 'float',
        'dprn_value_02' => 'float',
        'depreciacion_acumulada_02' => 'float',
        'book_value_for_area_02' => 'float',
        'dprn_rate_03' => 'float',
        'dprn_value_03' => 'float',
        'depreciacion_acumulada_03' => 'float',
        'dprn_rate_04' => 'float',
        'dprn_value_04' => 'float',
        'depreciacion_acumulada_04' => 'float',
        'dprn_rate_05' => 'float',
        'dprn_value_05' => 'float',
        'depreciacion_acumulada_05' => 'float',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'archivo_maestro_id' => 'required',
        'company_code' => 'required'
    ];


}
