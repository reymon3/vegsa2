<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrdenTrabajoConteo
 * @package App\Models
 * @version February 10, 2020, 4:29 pm CST
 *
 * @property integer orden_trabajo_id
 * @property string folio
 * @property integer rubro_id
 * @property string catalogo
 * @property string descripcion
 * @property string marca
 * @property string modelo
 * @property string serie
 * @property string no_mantenimiento
 * @property string area
 * @property string ubicacion
 * @property string usuario_conteo
 * @property string piso
 * @property string observaciones
 * @property string foto_1
 * @property string foto_2
 * @property string foto_3
 * @property integer blame_id
 */
class OrdenTrabajoConteo extends Model
{
    use SoftDeletes;

    public $table = 'ordenes_trabajo_conteos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const MAX_ERROR_ROWS = 10;


    protected $dates = ['deleted_at'];



    public $fillable = [
        'orden_trabajo_id',
        'folio',
        'placa_actual',
        'placa_anterior',
        'placa_homologada',
        'placa_auxiliar_1',
        'placa_auxiliar_2',
        'rubro_id',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'no_mantenimiento',
        'area',
        'ubicacion',
        'usuario_conteo',
        'piso',
        'observaciones',
        'foto_1',
        'foto_2',
        'foto_3',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'folio' => 'string',
        'placa_actual' => 'string',
        'placa_anterior' => 'string',
        'placa_homologada' => 'string',
        'placa_auxiliar_1' => 'string',
        'placa_auxiliar_2' => 'string',
        'rubro_id' => 'integer',
        'catalogo' => 'string',
        'descripcion' => 'string',
        'marca' => 'string',
        'modelo' => 'string',
        'serie' => 'string',
        'no_mantenimiento' => 'string',
        'area' => 'string',
        'ubicacion' => 'string',
        'usuario_conteo' => 'string',
        'piso' => 'string',

        'planta' => 'string',
        'porcentaje_depreciacion_fisica' => 'integer',
        'anio_fabricacion' => 'integer',
        'observaciones' => 'string',

        'foto_1' => 'string',
        'foto_2' => 'string',
        'foto_3' => 'string',

        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'folio' => 'required',
        // 'rubro' => 'required',
        'catalogo' => 'required|exists:activo_fijo,nombre',
        // 'descripcion' => 'required',
        'usuario_conteo' => 'required',
    ];


}
