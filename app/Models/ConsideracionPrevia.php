<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class ConsideracionPrevia extends Model
{
    protected	$table	=	'consideraciones_previas';
    protected	$primaryKey	=	'id';
    public	$timestamps	=	false;

    protected $fillable = [
        'id_orden_trabajo',
        'id_metodo',
        'costos',
        'comparativo_mercado',
        'capitalizacion_rentas',
        'valor_comercial',
        'condiciones_limitantes',
        'creado',
    ];
}