<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Conciliacion
 * @package App\Models
 * @version August 6, 2020, 9:34 am CDT
 *
 * @property integer orden_trabajo_id
 * @property integer blame_id
 */
class Conciliacion extends Model
{
    use SoftDeletes;

    public $table = 'conciliacion';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const MAX_ERROR_ROWS = 10;

    protected $dates = ['deleted_at'];
    public $with = ['blame', 'ordenTrabajo'];

    public $fillable = [
        'orden_trabajo_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ordenTrabajo()
    {
        return $this->belongsTo(\App\Models\OrdenTrabajo::class, 'orden_trabajo_id')->withTrashed();
    }


}
