<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class OrdenTrabajoPropositoAvaluo
 * @package App\Models
 * @version December 5, 2019, 8:02 am CST
 *
 * @property integer orden_trabajo_id
 * @property integer proposito_avaluo_id
 * @property integer blame_id
 */
class OrdenTrabajoPropositoAvaluo extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo_propositos_avaluo';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'propositoAvaluo'];

    public $fillable = [
        'orden_trabajo_id',
        'proposito_avaluo_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'proposito_avaluo_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'proposito_avaluo_id' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function propositoAvaluo()
    {
        return $this->belongsTo(\App\Models\PropositoAvaluo::class, 'proposito_avaluo_id')->withTrashed();
    }

}
