<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrdenTrabajoBaseContable
 * @package App\Models
 * @version November 14, 2019, 4:18 pm CST
 *
 * @property integer orden_trabajo_id
 * @property string json_cabeceras
 * @property string json_excel
 * @property integer blame_id
 */
class OrdenTrabajoBaseContable extends Model
{
    use SoftDeletes;

    public $table = 'orden_trabajo_base_contable';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'ordenTrabajo'];
    // public $with = ['blame'];

    public $fillable = [
        'nombre',
        'orden_trabajo_id',
        // 'json_cabeceras',
        // 'json_excel',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'orden_trabajo_id' => 'integer',
        'json_cabeceras' => 'string',
        'json_excel' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        // 'json_cabeceras' => 'required',
        // 'json_excel' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ordenTrabajo()
    {
        return $this->belongsTo(\App\Models\OrdenTrabajo::class, 'orden_trabajo_id')->withTrashed();
    }

}
