<?php

namespace App\Models;

use Eloquent as Model;

class PlcDos extends Model
{

    public $table = 'relacion_placas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame'];

    public $fillable = [
        'placa',
        'placa_pivote',
        'tipo',
        'estatus',
        'planta',
        'blame_id',
        'update_at'
    ];

}
