<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class Empleados
 * @package App\Models
 * @version November 4, 2019, 5:27 am UTC
 *
 * @property integer area_id
 * @property integer puesto_id
 * @property string nombre
 * @property string correo
 * @property string rfc
 * @property integer blame_id
 */
class Empleados extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'empleados';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];
    public $with = ['blame', 'area', 'puesto'];

    public $fillable = [
        'area_id',
        'puesto_id',
        'nombre',
        'correo',
        'rfc',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'area_id' => 'integer',
        'puesto_id' => 'integer',
        'nombre' => 'string',
        'correo' => 'string',
        'rfc' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'area_id' => 'required',
        'puesto_id' => 'required',
        'nombre' => 'required',
        'correo' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function area()
    {
        return $this->belongsTo(\App\Models\Areas::class, 'area_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function puesto()
    {
        return $this->belongsTo(\App\Models\Puestos::class, 'puesto_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function usuario()
    {
        return $this->hasOne(\App\Models\User::class, 'empleado_id')->withTrashed();
    }

}
