<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrdenTrabajoDocumento.
 *
 * @version January 7, 2020, 9:34 am CST
 *
 * @property string nombre
 * @property string ruta
 * @property int orden_trabajo_id
 * @property int blame_id
 */
class OrdenTrabajoDocumento extends Model
{
    use SoftDeletes;

    public $table = 'ordenes_trabajo_documentos';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    // Debe coincidir con la variable `root` de filesystems->ordenes-trabajo
    const CARPETA_ORDEN_TRABAJO = 'uploads/ordenes-trabajo';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'nombre',
        'ruta',
        'extension',
        'orden_trabajo_id',
        'blame_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'ruta' => 'string',
        'extension' => 'string',
        'orden_trabajo_id' => 'integer',
        'blame_id' => 'integer',
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'ruta' => 'required',
        'orden_trabajo_id' => 'required',
    ];
}
