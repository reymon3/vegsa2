<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CivilDetalle
 * @package App\Models
 * @version April 23, 2020, 10:25 am CDT
 *
 * @property integer avaluo_id
 * @property string nombre
 * @property string descripcion
 * @property integer blame_id
 */
class CivilDetalle extends Model
{
    use SoftDeletes;

    public $table = 'civil_detalle';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'avaluo_id',
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'avaluo_id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'avaluo_id' => 'required',
        'nombre' => 'required'
    ];

    
}
