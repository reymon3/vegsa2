<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class ClienteRazonSocial
 * @package App\Models
 * @version November 5, 2019, 12:03 am UTC
 *
 * @property integer cliente_id
 * @property string nombre
 * @property string rfc
 * @property string razon_social
 * @property string domicilio
 * @property string nombre_contacto
 * @property string puesto_contacto
 * @property string celular_contacto
 * @property string telefono_contacto
 * @property string email_contacto
 * @property integer blame_id
 */
class ClienteRazonSocial extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'clientes_razon_social';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'cliente_id',
        'nombre',
        'rfc',
        'razon_social',
        'domicilio',
        'nombre_contacto',
        'puesto_contacto',
        'celular_contacto',
        'telefono_contacto',
        'email_contacto',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'nombre' => 'string',
        'rfc' => 'string',
        'razon_social' => 'string',
        'domicilio' => 'string',
        'nombre_contacto' => 'string',
        'puesto_contacto' => 'string',
        'celular_contacto' => 'string',
        'telefono_contacto' => 'string',
        'email_contacto' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente_id' => 'required',
        'nombre' => 'required',
        'rfc' => 'required',
        'razon_social' => 'required',
        'domicilio' => 'required',
        'nombre_contacto' => 'required',
        'email_contacto' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

}
