<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class Servicio
 * @package App\Models
 * @version November 5, 2019, 6:59 am CST
 *
 * @property string nombre
 * @property string descripcion
 * @property integer blame_id
 */
class Servicio extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'servicios';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame'];


    public $fillable = [
        'nombre',
        'descripcion',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        // 'descripcion' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cotizacionServicio()
    {
        return $this->hasMany(\App\Models\CotizacionServicio::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

}
