<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class CaracteristicaUrbana extends Model
{
    protected	$table	=	'caracteristicas_urbanas';
    protected	$primaryKey	=	'id';
    public	$timestamps	=	false;

    protected $fillable = [
        'id_orden_trabajo',
        'id_clasificacion_zona',
        'id_tipo_contruccion_dominante',
        'indice_saturacion_zona',
        'id_poblacion',
        'id_uso_predio',
        'id_contaminacion_ambiental',
        'uso_principales_vias_acceso',
        'id_servicios_municipales',
        'creado',
        'id_creador',
    ];
}