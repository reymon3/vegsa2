<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Carbon\Carbon;
/**
 * Class Cotizacion
 * @package App\Models
 * @version November 5, 2019, 7:26 pm CST
 *
 * @property integer cliente_id
 * @property integer vendedor_id
 * @property string num_cotizacion
 * @property string fecha_cotizacion
 * @property number ingreso_factura
 * @property string forma_pago
 */
class Cotizacion extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'cotizaciones';
    public $with = ['vendedor', 'cliente'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'cliente_id',
        'vendedor_id',
        'num_cotizacion',
        'fecha_cotizacion',
        'nombre_contacto',
        'puesto_contacto',
        'telefono_contacto',
        'celular_contacto',
        'correo_contacto',
        'ingreso_factura',
        'forma_pago',
        'condiciones_pago'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cliente_id' => 'integer',
        'vendedor_id' => 'integer',
        'num_cotizacion' => 'string',
        'fecha_cotizacion' => 'string',
        'nombre_contacto' => 'string',
        'puesto_contacto' => 'string',
        'telefono_contacto' => 'string',
        'celular_contacto' => 'string',
        'correo_contacto' => 'string',
        'ingreso_factura' => 'float',
        'forma_pago' => 'string',
        'condiciones_pago' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cliente_id' => 'required',
        'forma_pago' => 'required',
        // 'vendedor_id' => 'required',
        'num_cotizacion' => 'required|unique:cotizaciones,num_cotizacion',
        // 'nombre_contacto' => 'required',
        // 'puesto_contacto' => 'required',
        // 'telefono_contacto' => 'required',
        // 'celular_contacto' => 'required',
        // 'correo_contacto' => 'required',
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rulesUpdate = [
        'forma_pago' => 'required',
        // 'num_cotizacion' => 'required|unique:cotizaciones,num_cotizacion',
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'vendedor_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function vendedor()
    {
        return $this->belongsTo(\App\Models\User::class, 'vendedor_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id')->withTrashed();
    }

    public function setFechaCotizacionAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::parse($value)->format('Y-m-d');
        $this->attributes['fecha_cotizacion'] = $value;
    }

    public function getFechaCotizacionAttribute($value)
    {
        return !is_null($value) ? Carbon::parse($value)->format('d/m/Y') : '';
    }

}
