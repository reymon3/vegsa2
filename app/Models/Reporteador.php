<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Reporteador extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'archivo_maestro_detalle';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame'];

    public $fillable = [
        'archivo_maestro_id',
        'company_code',
        'asset_class',
        'asset',
        'subnumber',
        'name',
        'additional_description',
        'serial_number',
        'inventory_number',
        'capitalized',
        'deactivation_on',
        'cost_center',
        'evaluation_group_1',
        'evaluation_group_2',
        'text',
        'country_key',
        'country_name',
        'ad_value',
        'dprn_rate_01',
        'dprn_value_01',
        'depreciacion_acumulada_01',
        'book_value_for_area_01',
        'dprn_rate_02',
        'dprn_value_02',
        'depreciacion_acumulada_02',
        'book_value_for_area_02',
        'dprn_rate_03',
        'dprn_value_03',
        'depreciacion_acumulada_03',
        'dprn_rate_04',
        'dprn_value_04',
        'depreciacion_acumulada_04',
        'dprn_rate_05',
        'dprn_value_05',
        'depreciacion_acumulada_05',
        'blame_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'archivo_maestro_id' => 'integer',
        'company_code' => 'string',
        'blame_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'archivo_maestro_id' => 'required',
        'blame_id' => 'required'
    ];


    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

}
