<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrdenTrabajoBaseContableDetalle
 * @package App\Models
 * @version February 24, 2020, 5:46 pm CST
 *
 * @property integer orden_trabajo_base_contable_id
 * @property string json_fila
 * @property integer blame_id
 */
class OrdenTrabajoBaseContableDetalle extends Model
{
    use SoftDeletes;

    public $table = 'orden_trabajo_base_contable_detalle';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'orden_trabajo_base_contable_id',
        'json_fila',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_base_contable_id' => 'integer',
        'json_fila' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_base_contable_id' => 'required',
        'json_fila' => 'required'
    ];

    
}
