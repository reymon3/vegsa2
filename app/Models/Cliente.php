<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class Cliente
 * @package App\Models
 * @version November 4, 2019, 11:24 pm UTC
 *
 * @property string nombre
 * @property string descripcion
 * @property string rfc
 * @property string domicilio
 * @property string nombre_contacto
 * @property string puesto_contacto
 * @property string celular_contacto
 * @property string telefono_contacto
 * @property string correo_contacto
 * @property integer blame_id
 */
class Cliente extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'clientes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame'];

    public $fillable = [
        'nombre',
        'descripcion',
        'domicilio',
        'rfc',
        'nombre_contacto',
        'puesto_contacto',
        'celular_contacto',
        'telefono_contacto',
        'correo_contacto',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'rfc' => 'string',
        'domicilio' => 'string',
        'nombre_contacto' => 'string',
        'puesto_contacto' => 'string',
        'celular_contacto' => 'string',
        'telefono_contacto' => 'string',
        'correo_contacto' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'descripcion' => 'required',
        'domicilio' => 'required',
        'nombre_contacto' => 'required',
        'puesto_contacto' => 'required',
        'correo_contacto' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

}
