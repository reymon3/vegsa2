<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Carbon\Carbon;

/**
 * Class OrdenTrabajo
 * @package App\Models
 * @version November 8, 2019, 10:54 am CST
 *
 * @property integer cotizacion_id
 * @property string fecha_orden_trabajo
 * @property string fecha_arranque
 * @property string fecha_estimada_entrega
 * @property integer num_orden_trabajo
 * @property string localidades
 * @property string observaciones
 * @property integer blame_id
 */
class OrdenTrabajo extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'cotizacion', 'razonSocial', 'servicios', 'empleados'];

    public $fillable = [
        'cotizacion_id',
        'fecha_orden_trabajo',
        'fecha_arranque',
        'fecha_estimada_entrega',
        'num_orden_trabajo',
        'localidades',
        'observaciones',
        'blame_id',
        'num_factura',
        'reporte_fotografico',
        'factura_revisada',
        'valor_agregado'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'cotizacion_id' => 'integer',
        'fecha_orden_trabajo' => 'string',
        'fecha_arranque' => 'string',
        'fecha_estimada_entrega' => 'string',
        'num_orden_trabajo' => 'string',
        'num_factura' => 'string',
        'localidades' => 'string',
        'observaciones' => 'string',
        'reporte_fotografico' => 'string',
        'factura_revisada' => 'string',
        'valor_agregado' => 'string',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'cotizacion_id' => 'required',
        // 'fecha_orden_trabajo' => 'required',
        // 'fecha_arranque' => 'required',
        // 'fecha_estimada_entrega' => 'required',
        'num_orden_trabajo' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function cotizacion()
    {
        return $this->belongsTo(\App\Models\Cotizacion::class, 'cotizacion_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function razonSocial()
    {
        return $this->hasMany(\App\Models\OrdenTrabajoRazonSocial::class, 'orden_trabajo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function servicios()
    {
        return $this->hasMany(\App\Models\OrdenTrabajoServicio::class, 'orden_trabajo_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function empleados()
    {
        return $this->hasMany(\App\Models\OrdenTrabajoEmpleado::class, 'orden_trabajo_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ordenTrabajo()
    {
        return $this->hasMany(\App\Models\OrdenTrabajoBaseContable::class, 'orden_trabajo_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function conciliacion()
    {
        return $this->hasMany(\App\Models\Conciliacion::class, 'orden_trabajo_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function avaluo()
    {
        return $this->hasMany(\App\Models\Avaluo::class, 'orden_trabajo_id');
        // return $this->hasMany(\App\Models\Avaluo::class, 'orden_trabajo_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function archivoMaestro()
    {
        return $this->hasMany(\App\Models\ArchivoMaestro::class, 'orden_trabajo_id')->withTrashed();
    }

    public function setFechaOrdenTrabajoAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['fecha_orden_trabajo'] = $value;
    }

    public function getFechaOrdenTrabajoAttribute($value)
    {
        return !is_null($value) ? Carbon::parse($value)->format('d/m/Y') : '';
    }

    public function setFechaArranqueAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['fecha_arranque'] = $value;
    }

    public function getFechaArranqueAttribute($value)
    {
        return !is_null($value) ? Carbon::parse($value)->format('d/m/Y') : '';
    }

    public function setFechaEstimadaEntregaAttribute($value)
    {
        $value = is_null($value) ? $value : Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        $this->attributes['fecha_estimada_entrega'] = $value;
    }

    public function getFechaEstimadaEntregaAttribute($value)
    {
        return !is_null($value) ? Carbon::parse($value)->format('d/m/Y') : '';
    }

}
