<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrdenTrabajoEmpleado
 * @package App\Models
 * @version November 29, 2019, 10:30 am CST
 *
 * @property integer orden_trabajo_id
 * @property integer empleado_id
 * @property integer rol_id
 * @property integer blame_id
 */
class OrdenTrabajoEmpleado extends Model
{
    use SoftDeletes;

    public $table = 'ordenes_trabajo_empleados';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'orden_trabajo_id',
        'empleado_id',
        'rol_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'empleado_id' => 'integer',
        'rol_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'empleado_id' => 'required',
        'rol_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function empleados()
    {
        return $this->belongsTo(\App\Models\Empleados::class, 'empleado_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function roles()
    {
        return $this->belongsTo(\App\Models\Rol::class, 'rol_id')->withTrashed();
    }
}
