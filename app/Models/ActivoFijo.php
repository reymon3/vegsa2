<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class ActivoFijo
 * @package App\Models
 * @version July 26, 2020, 9:57 pm CDT
 *
 * @property integer rubro_id
 * @property string nombre
 * @property integer vut
 * @property integer blame_id
 */
class ActivoFijo extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'activo_fijo';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'rubro'];

    public $fillable = [
        'rubro_id',
        'nombre',
        'vut',
        'porcentaje_gasto_instalacion',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'rubro_id' => 'integer',
        'nombre' => 'string',
        'vut' => 'integer',
        'porcentaje_gasto_instalacion' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rubro_id' => 'required',
        'nombre' => 'required|unique:activo_fijo,nombre',
        'vut' => 'required',
        'porcentaje_gasto_instalacion' => 'required',
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function rubro()
    {
        return $this->hasOne(\App\Models\Rubros::class, 'id', 'rubro_id')->withTrashed();
    }


}
