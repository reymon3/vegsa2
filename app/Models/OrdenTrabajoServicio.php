<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class OrdenTrabajoServicio
 * @package App\Models
 * @version November 27, 2019, 10:43 am CST
 *
 * @property integer orden_trabajo_id
 * @property integer servicio_id
 * @property integer blame_id
 */
class OrdenTrabajoServicio extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo_servicios';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'servicio'];

    public $fillable = [
        'orden_trabajo_id',
        'servicio_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'servicio_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'servicio_id' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function servicio()
    {
        return $this->belongsTo(\App\Models\Servicio::class, 'servicio_id')->withTrashed();
    }

}
