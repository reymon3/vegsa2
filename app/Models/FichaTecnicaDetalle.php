<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FichaTecnicaDetalle
 * @package App\Models
 * @version August 20, 2020, 2:23 am CDT
 *
 * @property integer ficha_tecnica_id
 * @property string asset
 * @property string rubro
 * @property string catalogo
 * @property string descripcion
 * @property string marca
 * @property string modelo
 * @property string serie
 * @property string anio_fabricacion
 * @property string porcentaje_depreciacion_fisica
 * @property string fecha_adquisicion
 * @property string area
 * @property string caso
 * @property number valor_cotizado_1
 * @property string moneda_1
 * @property number tipo_cambio_1
 * @property integer no_piezas_1
 * @property number valor_cotizado_2
 * @property string moneda_2
 * @property number tipo_cambio_2
 * @property integer no_piezas_2
 * @property number valor_equipo_usado
 * @property number vrn
 * @property boolean porcentaje_dep
 * @property number dep_acum
 * @property number vnr
 * @property string link_1
 * @property string link_2
 * @property string observaciones
 * @property string cotizo
 * @property number vcf
 * @property number vur
 * @property number da
 * @property number vut
 * @property number gastos_instalacion
 * @property integer blame_id
 */
class FichaTecnicaDetalle extends Model
{
    use SoftDeletes;

    public $table = 'ficha_tecnica_detalle';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ficha_tecnica_id',
        'asset',
        'rubro',
        'catalogo',
        'descripcion',
        'marca',
        'modelo',
        'serie',
        'anio_fabricacion',
        'porcentaje_depreciacion_fisica',
        'fecha_adquisicion',
        'area',
        'caso',
        'valor_cotizado_1',
        'moneda_1',
        'tipo_cambio_1',
        'no_piezas_1',
        'valor_cotizado_2',
        'moneda_2',
        'tipo_cambio_2',
        'no_piezas_2',
        'valor_equipo_usado',
        'vrn',
        'porcentaje_dep',
        'dep_acum',
        'vnr',
        'link_1',
        'link_2',
        'observaciones',
        'cotizo',
        'vcf',
        'vur',
        'da',
        'vut',
        'gastos_instalacion',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ficha_tecnica_id' => 'integer',
        'asset' => 'string',
        'rubro' => 'string',
        'catalogo' => 'string',
        'descripcion' => 'string',
        'marca' => 'string',
        'modelo' => 'string',
        'serie' => 'string',
        'anio_fabricacion' => 'string',
        'porcentaje_depreciacion_fisica' => 'string',
        'fecha_adquisicion' => 'date',
        'area' => 'string',
        'caso' => 'string',
        'valor_cotizado_1' => 'float',
        'moneda_1' => 'string',
        'tipo_cambio_1' => 'float',
        'no_piezas_1' => 'integer',
        'valor_cotizado_2' => 'float',
        'moneda_2' => 'string',
        'tipo_cambio_2' => 'float',
        'no_piezas_2' => 'integer',
        'valor_equipo_usado' => 'float',
        'vrn' => 'float',
        'porcentaje_dep' => 'boolean',
        'dep_acum' => 'float',
        'vnr' => 'float',
        'link_1' => 'string',
        'link_2' => 'string',
        'observaciones' => 'string',
        'cotizo' => 'string',
        'vcf' => 'float',
        'vur' => 'float',
        'da' => 'float',
        'vut' => 'float',
        'gastos_instalacion' => 'float',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ficha_tecnica_id' => 'required',
        'asset' => 'required',
        'rubro' => 'required',
        'catalogo' => 'required',
        'descripcion' => 'required',
        'serie' => 'required',
        'porcentaje_depreciacion_fisica' => 'required',
        'caso' => 'required',
        'valor_cotizado_1' => 'required',
        'moneda_1' => 'required',
        'tipo_cambio_1' => 'required',
        'valor_cotizado_2' => 'required',
        'moneda_2' => 'required',
        'tipo_cambio_2' => 'required',
        'vrn' => 'required',
        'porcentaje_dep' => 'required',
        'dep_acum' => 'required',
        'vnr' => 'required',
        'vcf' => 'required'
    ];

    
}
