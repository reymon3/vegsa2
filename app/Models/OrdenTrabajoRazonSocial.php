<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class OrdenTrabajoRazonSocial
 * @package App\Models
 * @version November 14, 2019, 10:43 am CST
 *
 * @property integer orden_trabajo_id
 * @property integer cliente_razon_social_id
 * @property integer blame_id
 */
class OrdenTrabajoRazonSocial extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo_razon_social';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'clienteRazonSocial'];

    public $fillable = [
        'orden_trabajo_id',
        'cliente_razon_social_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'cliente_razon_social_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'cliente_razon_social_id' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function clienteRazonSocial()
    {
        return $this->belongsTo(\App\Models\ClienteRazonSocial::class, 'cliente_razon_social_id')->withTrashed();
    }
}
