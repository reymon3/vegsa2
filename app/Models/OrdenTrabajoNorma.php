<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class OrdenTrabajoNorma
 * @package App\Models
 * @version November 27, 2019, 2:03 pm CST
 *
 * @property integer orden_trabajo_id
 * @property integer norma_id
 * @property integer blame_id
 */
class OrdenTrabajoNorma extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'ordenes_trabajo_normas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'norma'];

    public $fillable = [
        'orden_trabajo_id',
        'norma_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'norma_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'norma_id' => 'required'
    ];

    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function norma()
    {
        return $this->belongsTo(\App\Models\Normas::class, 'norma_id')->withTrashed();
    }
}
