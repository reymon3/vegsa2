<?php

namespace App\Models;

use Eloquent as Model;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Direccion extends Model
{
    //
    protected	$table	=	'direccion';
    protected	$primaryKey	=	'id_direccion';
    public	$timestamps	=	false;

    protected $fillable = [
        'calle',
        'numero',
        'colonia',
        'cp',
        'estado',
        'alcaldia',
        'capital',
        'id_pais'
    ];
}