<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Indice extends Model
{
    use SoftDeletes, BlameableTrait;

    public $table = 'indice';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    // public $timestamps = false;

    protected $dates = ['deleted_at'];
    public $with = ['blame'];

    public $fillable = [
        'numeral',
    ];

 

}
