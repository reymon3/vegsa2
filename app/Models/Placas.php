<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class Rubros
 * @package App\Models
 * @version November 4, 2019, 5:31 am UTC
 *
 * @property string nombre
 * @property string descripcion
 * @property integer blame_id
 * @property string|\Carbon\Carbon update_at
 */
class Placas extends Model
{
    protected $guarded = [];
    use SoftDeletes, BlameableTrait;

    public $table = 'relacion_placas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame'];

    public $fillable = [
        'placa',
        'placa_pivote',
        'tipo',
        'estatus',
        'planta',
        'blame_id',
        'update_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'placa' => 'string',
        'placa_pivote' => 'string',
        'blame_id' => 'integer',
        'update_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'placa' => 'required',
        // 'descripcion' => 'required'
    ];


    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }

}
