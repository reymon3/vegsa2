<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

/**
 * Class Cartas
 * @package App\Models
 * @version November 4, 2019, 5:31 am UTC
 *
 * @property string orden_trabajo_id
 * @property string nombre
 * @property integer blame_id
 * @property string|\Carbon\Carbon update_at
 */
class Cartas extends Model
{
    // protected $guarded = [];
    use SoftDeletes, BlameableTrait;

    public $table = 'carta_entrega';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    public $with = ['blame', 'ordenTrabajo'];

    public $fillable = [
        'orden_trabajo_id',
        'nombre',
        'blame_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'string',
        'nombre' => 'string',
        'blame_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'nombre' => 'required'
    ];


    public function blameable()
    {
        return [
            'user' => \App\User::class,
            'createdBy' => 'blame_id',
            'updatedBy' => null,
            'deletedBy' => null
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blame()
    {
        return $this->belongsTo(\App\Models\User::class, 'blame_id')->withTrashed();
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ordenTrabajo()
    {
        return $this->belongsTo(\App\Models\OrdenTrabajo::class, 'orden_trabajo_id')->withTrashed();
    }


}
