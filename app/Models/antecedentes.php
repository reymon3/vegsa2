<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Antecedente extends Model
{
    //
    protected	$table	=	'antecedentes';
    protected	$primaryKey	=	'id';
    public	$timestamps	=	false;

    protected $fillable = [
        'id_ordentrabajo',
        'id_regimensolicitante',
        'nombre',
        'segundo_nombre',
        'apa',
        'ama',
        'id_razon_social',
        'id_domicilio_solicitante',
        'fecha_avaluo',
        'id_perito_valuador',
        'id_inmueble_valuado',
        'id_regimen_propiedad',
        'id_regimenpropietario',
        'nombre_propietario',
        'segundo_nombre_propietario',
        'apa_propietario',
        'ama_propietario',
        'id_razon_social_propietario',
        'id_destino_avaluo',
        'id_direccion_ubicacion_inmueble',
        'clave_catastral',
        'contrato_agua',
        'creado',
        'modificado',
        'id_usuario_alta',
    ];
}