<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Civil
 * @package App\Models
 * @version April 23, 2020, 10:24 am CDT
 *
 * @property integer orden_trabajo_id
 * @property integer seccion_id
 * @property integer blame_id
 */
class Civil extends Model
{
    use SoftDeletes;

    public $table = 'civil';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'orden_trabajo_id',
        'seccion_id',
        'blame_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'orden_trabajo_id' => 'integer',
        'seccion_id' => 'integer',
        'blame_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'orden_trabajo_id' => 'required',
        'seccion_id' => 'required'
    ];

    
}
