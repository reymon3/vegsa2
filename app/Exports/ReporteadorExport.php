<?php

namespace App\Exports;

use App\Models\Reporteador;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class ReporteadorExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $xtc = DB::select('select * from archivo_maestro_detalle limit 5 ');
        //$xtc = Reporteador::all();
        //dd($xtc);
        return $xtc;
    }
}
