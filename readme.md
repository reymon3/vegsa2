# Vegsa Avalúos S.A. de C.V.

## Instrucciones de instalación.
***

* Crear la base de datos e importar los registros desde el archivo SQL (vegsa_17-03-2020.sql) 
* Abrir la terminal de su preferencia
* Entrar a la ruta correspondiente del servidor web por ejemplo `var\www\html`
* Clonar el proyecto (git clone $URL)
* Entrar en la carpeta del proyecto (cd $folder_path)
* Ejecutar el comando de inatalación de composer `composer install`
* Ejecutar el comando `composer run post-root-package-install`
* Ejecutar el comando `composer run post-create-project-cmd`
* Configurar datos de conexión en el archivo .env

```
DB_DATABASE={$database}
DB_USERNAME={$username}
DB_PASSWORD={$password}
```

Si todo ha salido bien se puede entrar al sistema de manera local, abriendo el navegador de su preferencia y colocando la ruta http://{$folder_path}/public, por ejemplo `http://localhost/vegsa/public`

Para entrar al sistema se pueden utilizar las siguientes credenciales.


| Usuario              			| Contraseña |         Rol	        |
| ------------------------------|------------|----------------------|
| admin@vegsa.com.mx   			| 2a3s4d5f   | Adminstrador			|
| rafael.lopez@vegsa.com.mx     | 2a3s4d5f   | Gerente Asignador	|

Al capturar alguna de las credenciales anteriores, se debe mostrar el
la pantalla principal con el menú correspondiente al rol del usuario.

## License
***

**VEGSA** all right reserved.
