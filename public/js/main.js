    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    
    function currencyFormat(num) {
      return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    $(":submit").click(function() {
        var $btn = $(this);
        $btn.button('loading');
        // Then whatever you actually want to do i.e. submit form
        // After that has finished, reset the button state using
        setTimeout(function () {
            $btn.button('reset');
        }, 5000);
    });

    function CloseModal(frameElement, reloadTable) {
        return parent.$(frameElement).modal('hide');
    }

    function datatableReload(){

        location.reload();
        return true;

        if(
            typeof reloadPage !== 'undefined'
        ){
        }

        // get all visible DT instances
        var tables = $.fn.dataTable.tables(true);

        if(
            typeof reload !== 'undefined'
        ){
            // perform refresh
            $(tables).DataTable().ajax.reload();
        }

        if(
            typeof reset !== 'undefined'
        ){
            // perform reset
            $(tables).DataTable().search('').draw();
        }

        return true;
    }

    function llenaSelect(urlData, selectDestinoId){

        //  http://localhost/{file}/public/{GetModelos}/${event.target.value}

        $.get(urlData, function(data, status){
            $("#"+selectDestinoId).empty();
            $("#"+selectDestinoId).append(`<option value = 0>Seleccione...</option>`);
            data.forEach(element => {
                // console.log(element);
                $("#"+selectDestinoId).append(`<option value = ${element.id}> ${element.nombre} </option>`);
            });
        });

    }

    function duplicateEmail(element, url){

        var email = $(element).val();

        $.ajax({
            type: "GET",
            url: url,
            data: {'email':email},
            dataType: "json",
            success: function(res) {
                $(element).next().text('')
                if(res.exists){
                    $(element).next().text('Este correo ya esta registrado.')
                }
            },
            error: function (jqXHR, exception) {

            }
        });

    }

    function disableAutocomplete( jQuery ) {
        $('input:password').removeAttr('disabled');
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
