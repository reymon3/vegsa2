$("[id^=fancybox-iframe]").fancybox({
    toolbar: false,
    smallBtn: true,
    lang: 'es',
    i18n: {
        'es': {
            CLOSE: 'Cerrar',
            NEXT: 'Siguiente',
            PREV: 'Anterior',
            ERROR: 'Este contenido no se pudó cargar. <br/> Por favor intente mas tarde.',
            PLAY_START: 'Inicio slideshow',
            PLAY_STOP: 'Pausa slideshow',
            FULL_SCREEN: 'Full screen',
            THUMBS: 'Thumbnails',
            DOWNLOAD: 'Download',
            SHARE: 'Share',
            ZOOM: 'Zoom'
        },
    },
    iframe: {
        preload: false,
        css: {
            height: '500px',
            width: '850px'
        }
    },
    beforeLoad: function() {
        //console.log('open');
        this.src = this.src + '?pop=1'
        //console.log(this)
    },
    afterClose: function() {
        datatableReload();
        //console.log('afterClose');
    }
});

function parseOptionUrl(url) {

    var opcion  = url.split("/").slice(-1)[0].split('?')[0];

    if(opcion == 'edit'){
        var id = url.split("/").slice(-2)[0];
        return opcion + '-' + id
    }

    return opcion;
}

function fancyboxOpenIframe(url, alto, ancho, nombre) {

    if(typeof nombre === 'undefined'){
        nombre = 'fancybox-frame-' + parseOptionUrl(url);
    }

    if (nombre == 'rnd') {
        nombre = "fancybox-frame{rnd}";
    }

    $.fancybox.open({
        src  : url,
        type: 'iframe',
        opts : {
            lang: 'es',
            i18n: {
                'es': {
                    CLOSE: 'Cerrar',
                    NEXT: 'Siguiente',
                    PREV: 'Anterior',
                    ERROR: 'Este contenido no se pudó cargar. <br/> Por favor intente mas tarde.',
                    PLAY_START: 'Inicio slideshow',
                    PLAY_STOP: 'Pausa slideshow',
                    FULL_SCREEN: 'Full screen',
                    THUMBS: 'Thumbnails',
                    DOWNLOAD: 'Download',
                    SHARE: 'Share',
                    ZOOM: 'Zoom'
                },
            },
            toolbar: false,
            smallBtn: true,
            iframe: {
                preload: false,
                css: {
                    height: alto,
                    width: ancho
                },
                tpl: '<iframe id="fancybox-frame{rnd}" name="' + nombre + '" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
                attr: {
                    scrolling: "auto"
                }

            },
            maxWidth: 1024,
            maxHeight: 850,
            fitToView: false,
            autoSize: false,
            padding: 0,
            beforeLoad: function() {
                //console.log('beforeLoad');
            },
            afterShow : function( instance, current ) {
                //console.info('afterShow!');
            },
            afterClose: function() {
                datatableReload();
                //console.log('afterClose');
            }
        }
    });

}

function closeFancyboxIframe() {

    return parent.$.fancybox.close();
}

// $.fancybox.defaults.afterClose = function() {
//     datatableReload();
//     return;
// }
