<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoBaseContable;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoBaseContable::class, function (Faker $faker) {

    return [
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'json_cabeceras' => $faker->word,
        'json_excel' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
