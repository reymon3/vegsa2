<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rubros;
use Faker\Generator as Faker;

$factory->define(Rubros::class, function (Faker $faker) {

    return [
        'nombre' => $faker->word,
        'descripcion' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'update_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
