<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cotizacion;
use Faker\Generator as Faker;

$factory->define(Cotizacion::class, function (Faker $faker) {

    return [
        'cliente_id' => $faker->randomDigitNotNull,
        'vendedor_id' => $faker->randomDigitNotNull,
        'num_cotizacion' => $faker->word,
        'fecha_cotizacion' => $faker->word,
        'ingreso_factura' => $faker->randomDigitNotNull,
        'forma_pago' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
