<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoTipoPlaqueo;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoTipoPlaqueo::class, function (Faker $faker) {

    return [
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'tipo_plaqueo_id' => $faker->randomDigitNotNull,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
