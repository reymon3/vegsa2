<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoDocumento;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoDocumento::class, function (Faker $faker) {

    return [
        'nombre' => $faker->word,
        'ruta' => $faker->word,
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
