<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoBaseContableDetalle;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoBaseContableDetalle::class, function (Faker $faker) {

    return [
        'orden_trabajo_base_contable_id' => $faker->randomDigitNotNull,
        'json_fila' => $faker->text,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
