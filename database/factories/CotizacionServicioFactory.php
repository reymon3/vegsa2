<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CotizacionServicio;
use Faker\Generator as Faker;

$factory->define(CotizacionServicio::class, function (Faker $faker) {

    return [
        'cotizacion_id' => $faker->randomDigitNotNull,
        'servicio_id' => $faker->randomDigitNotNull,
        'importe' => $faker->randomDigitNotNull,
        'iva' => $faker->randomDigitNotNull,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->randomDigitNotNull,
        'updated_at' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->randomDigitNotNull
    ];
});
