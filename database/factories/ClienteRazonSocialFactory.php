<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ClienteRazonSocial;
use Faker\Generator as Faker;

$factory->define(ClienteRazonSocial::class, function (Faker $faker) {

    return [
        'cliente_id' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'rfc' => $faker->word,
        'razon_social' => $faker->word,
        'domicilio' => $faker->word,
        'nombre_contacto' => $faker->word,
        'puesto_contacto' => $faker->word,
        'celular_contacto' => $faker->word,
        'telefono_contacto' => $faker->word,
        'email_contacto' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
