<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Normas;
use Faker\Generator as Faker;

$factory->define(Normas::class, function (Faker $faker) {

    return [
        'id' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'descripcion' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
