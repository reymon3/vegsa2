<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Avaluo;
use Faker\Generator as Faker;

$factory->define(Avaluo::class, function (Faker $faker) {

    return [
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
