<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajo;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajo::class, function (Faker $faker) {

    return [
        'cotizacion_id' => $faker->randomDigitNotNull,
        'fecha_orden_trabajo' => $faker->word,
        'fecha_arranque' => $faker->word,
        'fecha_estimada_entrega' => $faker->word,
        'num_orden_trabajo' => $faker->randomDigitNotNull,
        'localidades' => $faker->word,
        'observaciones' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
