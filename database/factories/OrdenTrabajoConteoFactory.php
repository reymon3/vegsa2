<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoConteo;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoConteo::class, function (Faker $faker) {

    return [
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'folio' => $faker->word,
        'rubro_id' => $faker->randomDigitNotNull,
        'activo' => $faker->word,
        'marca' => $faker->word,
        'modelo' => $faker->word,
        'serie' => $faker->word,
        'area' => $faker->word,
        'ubicacion' => $faker->word,
        'piso' => $faker->word,
        'observaciones' => $faker->word,
        'vrn' => $faker->word,
        'porcentaje_dep' => $faker->word,
        'dep_acum' => $faker->word,
        'vnr' => $faker->word,
        'vur' => $faker->word,
        'da' => $faker->word,
        'vut' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
