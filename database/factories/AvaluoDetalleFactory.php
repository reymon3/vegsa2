<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AvaluoDetalle;
use Faker\Generator as Faker;

$factory->define(AvaluoDetalle::class, function (Faker $faker) {

    return [
        'avaluo_id' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'descripcion' => $faker->word,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
