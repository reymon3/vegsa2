<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrdenTrabajoTipoAvaluo;
use Faker\Generator as Faker;

$factory->define(OrdenTrabajoTipoAvaluo::class, function (Faker $faker) {

    return [
        'orden_trabajo_id' => $faker->randomDigitNotNull,
        'tipo_avaluo_id' => $faker->randomDigitNotNull,
        'blame_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
